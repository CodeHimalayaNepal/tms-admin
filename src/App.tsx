import React from 'react';
import { routesLists } from 'routes/route-lists';
import AppRoutes from 'routes/routes';

import './App.css';

function App() {
  return <AppRoutes routes={routesLists} />;
}

export default App;
