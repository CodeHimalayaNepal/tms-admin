import { useState, useEffect } from 'react';

export default function useWidth(defaultValue: number = 768): [boolean] {
  const [isMobilewidth, setisMobilewidth] = useState(false);
  const handleResize = () => {
    setisMobilewidth(window.innerWidth < defaultValue);
  };
  useEffect(() => {
    handleResize();
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  useEffect(() => {}, [isMobilewidth]);

  return [isMobilewidth];
}
