import React from 'react';

interface Options {
  callback?: (...args: any[]) => void;
  toggle?: boolean;
}

const useOutsideClick = (initialState = false, options: Options = {}) => {
  const [isOutsideClick, setIsOutsideClick] = React.useState<boolean>(initialState);
  const ref = React.useRef<HTMLElement>(null);

  const handler = (ev: MouseEvent) => {
    if (ref.current?.contains(ev.target as HTMLElement)) {
      setIsOutsideClick(true);
    } else {
      // clicked within
      if (options.toggle) setIsOutsideClick((prev) => !prev);
      else setIsOutsideClick(false);
    }
  };

  const forceClose = () => setIsOutsideClick(false);
  const forceOpen = () => setIsOutsideClick(true);

  React.useEffect(() => {
    document.addEventListener('click', handler);
    return () => {
      document.removeEventListener('click', handler);
    };
  }, []);

  return {
    targetRef: ref,
    isOutsideClick,
    forceOpen,
    forceClose
  };
};

export default useOutsideClick;
