/* eslint-disable react/display-name */
import React, { ReactNode } from 'react';
import { IRoutes } from '../route-lists';
import Dashboard from '../../pages/private/Dashboard';
import Administrators from 'pages/private/Administrators';
import UserManagement from 'pages/private/UserManagement';
import InternalUserManagement from 'pages/private/InternalUser';
import InternalUserDetails from 'pages/private/InternalUser/InternalUserDetails';

import SideBar from 'pages/private/SideBar';
import RolesAndPermissions from 'pages/private/RolesAndPermissions';
import Roles from 'pages/private/RolesAndPermissions/Roles';
import Permissions from 'pages/private/RolesAndPermissions/Permissions';
import Trainee from 'pages/private/Trainee';
import CoursesAndResources from 'pages/private/CourseAndResources/course';

import Training from 'pages/private/TrainingManagments/Training';
import TrainingForm from 'pages/private/TrainingManagments/Training/TrainingForm';
import TrainingManagement from 'pages/private/TrainingManagments';
import RoutineFeedback from 'pages/private/Feedback/Routine Feedback';

import RoutineManagement from 'pages/private/RoutineManagement';
import Attendance from 'pages/private/Attendance';
import RosterManagement from 'pages/private/RosterManagement';
import Calender from 'pages/private/Calender';
import Configurations from 'pages/private/Configurations';
import AssignmentManagement from 'pages/private/AssignmentManagement';

import AssignmentType from 'pages/private/AssignmentManagement/AssignmentType';
import Assignment from 'pages/private/AssignmentManagement/Assignement';
import TrainingType from 'pages/private/TrainingManagments/TrainingType';
import { Profile } from 'pages/private/Profile';
import Organizations from 'pages/private/OrganizationManagement/Organizations';
import OrganizationHalls from 'pages/private/HallManagement';
import OrganizationDepartments from 'pages/private/OrganizationManagement/OrganizationsDepartment';
import OrganizationCentres from 'pages/private/OrganizationManagement/OrganizationCenter';
import OrganizationManagement from 'pages/private/OrganizationManagement';
import FormsCreations from 'pages/private/FormsCreations';
import FormTypes from 'pages/private/FormsCreations/FormsTypes';
import FormFields from 'pages/private/FormsCreations/FormFields';
import Evaluation from 'pages/private/evaluation';
import AttendanceRequest from 'pages/private/AttendanceRequest';
import RoutineDetailPage from 'pages/private/RoutineManagement/RoutineDetailPage';
import TraineeProfile from 'pages/private/TraineeProfile';
import CourseForm from 'pages/private/CourseAndResources/course/CourseForm';
import { FormikConsumer, useFormik } from 'formik';
import SessionManagementForm from 'pages/private/CourseAndResources/Session/SessionManagementModal';
import RoutineSessionDetailPage from 'pages/private/RoutineManagement/RoutineSessionDetailPage';
import SessionTable from 'pages/private/CourseAndResources/Session/SessionTable';
import { ISessionInput } from 'redux/reducers/session-resources';
import ExternalUserProfile from 'pages/private/ExternalUserProfile';
import CourseCategory from 'pages/private/CourseCategory';
import EvaluationCriteria from 'pages/private/evaluationCriteria/EvaluationCriteria';
import EvaluationCriteriaDetails from 'pages/private/EvaluationDetails/EvaluationCriteriaDetails';

import Feedback from 'pages/private/Feedback/Feedbacks/Feedback';
import FeedbackManagement from 'pages/private/Feedback';
import FeedbackResult from 'pages/private/Feedback/Feedback Result';
import FeedbackModuleResult from 'pages/private/Feedback/Feedback Module Result';
import Session from 'pages/private/CourseAndResources/Session/index';
import FeedbackEvaluation from 'pages/private/FeedbackEvaluation/FeedbackEvaluation2';
import InternalUsersManagement from 'pages/private/InternalUser/InternalUserEdit';
import { MdDashboard, MdOutlineAssignmentTurnedIn, MdOutlineManageAccounts } from 'react-icons/md';
import { BiUserCheck, BiUserCircle, BiUserPlus } from 'react-icons/bi';
// import { GrCertificate } from 'react-icons/gr';
import { TbCertificate, TbDatabaseImport } from 'react-icons/tb';
import {
  RiCheckboxMultipleBlankLine,
  RiFileEditFill,
  RiFileSettingsLine,
  RiUserSettingsLine
} from 'react-icons/ri';
import { FiDatabase } from 'react-icons/fi';
import { FaChalkboardTeacher, FaGraduationCap } from 'react-icons/fa';
import { TbFileSettings } from 'react-icons/tb';
import { VscFeedback, VscOrganization, VscOutput } from 'react-icons/vsc';
import { BsFillDoorOpenFill } from 'react-icons/bs';
import ViewFeedBack from 'pages/private/Feedback/ViewFeedBack';
import TraineeEvaluation from 'pages/private/TraineeEvaluation/TraineeEvaluation';
import ViewFeedBackSubmit from 'pages/private/Feedback/FeedBackSubmitSession';
import ViewFeedBackSubmitModule from 'pages/private/Feedback/FeedbackSubmitModule';
import InternalUser from 'pages/private/InternalUser/InternalUserDetails';
import InternalGeneralForm from 'pages/private/InternalUser/InternalUserEdit/InternalUserGeneralDetails';
import AssignRoles from 'pages/private/RolesAndPermissions/Roles/AssignRoles';
import TraineeCertification from 'pages/private/TraineeCertification';
import GenerateCertificate from 'pages/private/TraineeCertification/GenerateCertificate';
import ViewTemplate from 'pages/private/TraineeCertification/ViewTemplate';
import ExternalProfileDetail from 'pages/private/ExternalUserProfile/ProfileDetail';
import Landscape from 'pages/private/TraineeCertification/Landscape';
import Resources from 'pages/private/CourseAndResources/Session/Resources';
import TrainingNew from 'pages/private/Dashboard/TrainingDetails/training';
import AssignmentDetail from 'pages/private/AssignmentManagement/Assignement/AssignmentDetail';

import ReportForm from 'pages/private/RoutineManagement/Report/ReportForm';

import AdminPasswordChange from 'pages/private/InternalUser/InternalUserEdit/IndexAdmin';
import TraineesRanksReport from 'pages/private/TraineeEvaluation/TraineesRanksReport';
import TrainingFeedbackResult from 'pages/private/Feedback/Feedback Training Result';
import ViewFeedbackSubmitTraining from 'pages/private/Feedback/FeedbackSubmitTraining';
import LunchFeedbackResult from 'pages/private/Feedback/Feedback Lunch Result';
import ViewFeedBackSubmitLunch from 'pages/private/Feedback/FeedbackSubmitLunch';
import MasterDetailsPage from 'pages/private/MasterData/ServiceGroup/MasterDetailsPage';
import ServiceGroup from 'pages/private/MasterData/ServiceGroup';
import MasterData from 'pages/private/MasterData';
import Ethnicity from 'pages/private/MasterData/Ethinicity';
import EthnicityDetailsPage from 'pages/private/MasterData/Ethinicity/EthnicityDetailsPage';
import ServiceSubGroup from 'pages/private/MasterData/ServiceSubGroup';
import ServiceSubDetailsPage from 'pages/private/MasterData/ServiceSubGroup/ServiceSubDetailsPage';
const style = { color: 'white', className: 'nav-icon', size: 30 };
export const privateRoutes = [
  {
    path: '/',
    title: 'Sidebar',
    element: <SideBar />,
    children: [
      {
        title: 'Dashboard',
        path: '/',
        index: true,
        icon: <MdDashboard style={style} />,
        element: <Dashboard />,
        isForTrainee: true,
        isDivider: true
      },
      {
        title: 'User Management',
        path: '/internal-user-management',
        icon: <BiUserCircle style={style} />,
        element: <InternalUserManagement />,
        isDivider: true
      },
      {
        title: 'User Management',
        path: '/internal-user-management/internal/:id',
        icon: 'ri-group-line',
        element: <InternalUser />,
        hideInSideBar: true
      },
      {
        title: 'User Management',
        path: '/external-user-management/external/:id',
        icon: 'ri-group-line',
        element: <ExternalUserProfile />,
        hideInSideBar: true
      },
      {
        title: 'User Management',
        path: '/external-user-management/external-profile/:id',
        icon: 'ri-group-line',
        element: <ExternalProfileDetail />,
        hideInSideBar: true
      },
      {
        title: 'User Management',
        path: '/internal-user-management/internal/edit/:id',
        icon: <BiUserCheck style={style} />,
        element: <InternalUsersManagement />,
        hideInSideBar: true
      },

      {
        title: 'Trainee Approval',
        path: '/user-management',
        icon: <BiUserCheck style={style} />,
        element: <UserManagement />
      },
      {
        title: 'Trainee Management',
        path: '/trainee-management',
        icon: <BiUserPlus style={style} />,
        element: <Trainee />
      },
      {
        title: 'Attendance Request',
        path: '/Attendance-request',
        icon: <RiFileEditFill style={style} />,
        element: <AttendanceRequest />,
        isForTrainee: true
      },
      {
        title: 'Attendance Management',
        path: '/attendance-management',
        icon: <RiUserSettingsLine style={style} />,
        element: <Attendance />,
        isForTrainee: false,
        isDivider: true
      },
      {
        title: 'Assignment Management',
        path: '/assignment-mgmt',
        icon: <RiUserSettingsLine style={style} />,

        element: <AssignmentManagement />,
        hideInSideBar: true,
        children: [
          {
            title: 'Assignment Type',
            path: '/assignment-mgmt/assignment-type',
            icon: 'ri-dashboard-2-line',

            element: <AssignmentType />
          },

          {
            title: 'Assignment',
            path: '/assignment-mgmt/assignment',
            icon: 'ri-dashboard-2-line',

            element: <Assignment />
          }
        ]
      },
      {
        title: 'Evaluation',
        path: '/',
        icon: <MdOutlineAssignmentTurnedIn style={style} />,
        children: [
          // {
          //   title: 'Evaluation',
          //   path: '/Evaluation',
          //   icon: 'ri-group-line',
          //   element: <Evaluation />
          // },
          {
            title: 'Evaluation Criteria',
            path: '/Evaluation/EvaluationCriteria',
            icon: 'ri-group-line',
            element: <EvaluationCriteria />
          },

          {
            title: 'Evalutation Details',
            path: '/EvaluationDetails/EvaluationCriteriaDetails',
            icon: 'ri-user-2-line',
            element: <EvaluationCriteriaDetails />,
            hideInSideBar: true
          }
        ],
        isDivider: true
      },
      //   {
      //     title: 'Roster Management',
      //     path: '/roster-management',

      //     icon: 'ri-todo-line',
      //     element: <RosterManagement />
      //   },
      {
        title: 'Module Category',
        path: '/course-category',
        icon: <RiCheckboxMultipleBlankLine style={style} />,
        element: <CourseCategory />
      },
      {
        title: 'Modules And Sessions',
        path: '/modules',
        icon: <FaChalkboardTeacher style={style} />,
        element: <CoursesAndResources />
      },
      {
        title: 'Training Management',
        path: '/training-mgmt/training/edit/:trainingId',
        icon: <FaGraduationCap style={style} />,
        element: <TrainingForm />,
        hideInSideBar: true
      },
      {
        title: 'Training Management',
        path: '/training-mgmt/training/new',
        icon: 'ri-user-2-line',
        element: <TrainingForm />,
        hideInSideBar: true
      },
      {
        title: 'External User Management',
        path: '/external-user-profile/external/:id',
        icon: <FaGraduationCap style={style} />,
        element: <ExternalUserProfile />,
        hideInSideBar: true
      },
      {
        title: 'Internal User Management',
        path: '/internal-user-profile/internal/:id',
        icon: <FaGraduationCap style={style} />,
        element: <InternalUsersManagement />,
        hideInSideBar: true
      },
      {
        title: 'Admin Password Change',
        path: '/passwordChange',
        icon: <FaGraduationCap style={style} />,
        element: <AdminPasswordChange />,
        hideInSideBar: true
      },
      {
        title: 'Training Management',
        path: '/training-mgmt/training',
        icon: <FaGraduationCap style={style} />,
        element: <Training />
      },
      {
        title: 'Certificate Template',
        path: '/create-certificate-template',
        icon: <TbCertificate style={style} />,
        element: <TraineeCertification />
      },
      {
        title: 'Trainee Certification View',
        path: '/create-certificate-template/view/:templateId',
        icon: <TbCertificate style={style} />,
        element: <ViewTemplate />,
        hideInSideBar: true
      },
      {
        title: 'Routine Management',
        path: '/routine-management/view/:routineId',
        icon: 'ri-user-2-line',
        element: <RoutineSessionDetailPage />,
        hideInSideBar: true
      },
      {
        title: 'View Feedback',
        path: '/routine-management/:routineId/feedback/',
        icon: <VscFeedback style={style} />,
        isDivider: true,
        element: <ViewFeedBack />,
        hideInSideBar: true,
        isForTrainee: true
      },
      {
        title: 'Feedback Result',
        path: '/routine-management/:routineId/feedback/session/:sessionId/view',
        icon: 'ri-draft-line',
        element: <FeedbackResult />,
        hideInSideBar: true
      },
      {
        title: 'Feedback Result',
        path: '/training/routine/:routineId/training/:trainingId/feedback',
        icon: 'ri-draft-line',
        element: <ViewFeedbackSubmitTraining />,
        hideInSideBar: true
      },
      {
        title: 'Feedback Result',
        path: '/training/routine/:routineId/lunch/:lunchId/feedback/:date',
        icon: 'ri-draft-line',
        element: <ViewFeedBackSubmitLunch />,
        hideInSideBar: true
      },

      {
        title: 'Feedback Result',
        path: '/routine-management/:routineId/feedback/training/:trainingId/view',
        icon: 'ri-draft-line',
        element: <TrainingFeedbackResult />,
        hideInSideBar: true
      },
      {
        title: 'Feedback Result',
        path: '/routine-management/:routineId/feedback/lunch/:lunchId/view/:date',
        icon: 'ri-draft-line',
        element: <LunchFeedbackResult />,
        hideInSideBar: true
      },
      {
        title: 'Feedback Module Result',
        path: '/routine-management/:routineId/feedback/module/:moduleId/view',
        icon: 'ri-draft-line',
        element: <FeedbackModuleResult />,
        hideInSideBar: true
      },
      {
        title: 'Routine Management',
        path: '/routine-management/edit/:routineId',
        icon: 'ri-user-2-line',
        element: <RoutineDetailPage />,
        hideInSideBar: true
      },
      {
        title: 'Routine Management',
        path: '/routine-management/create',
        icon: 'ri-user-2-line',
        element: <RoutineDetailPage />,
        hideInSideBar: true
      },

      {
        title: 'Routine Management',
        path: '/routine-management/trainee-evaluation/:id',
        icon: 'ri-user-2-line',
        element: <TraineeEvaluation />,
        hideInSideBar: true
      },
      {
        title: 'Routine Report',
        path: '/routine-management/:id/View_Report',
        icon: 'ri-user-2-line',
        element: <TraineesRanksReport />,
        hideInSideBar: true
      },
      {
        title: 'Routine Management',
        path: '/routine-management/generate-report/:id',
        icon: 'ri-user-2-line',
        element: <ReportForm />,
        hideInSideBar: true
      },
      {
        title: 'Generate Certificate',
        path: '/routine-management/trainee-evaluation/:id/generate-certificate/:traineeId',
        icon: 'ri-user-2-line',
        element: <GenerateCertificate />,
        hideInSideBar: true
      },
      {
        title: 'Feedback Evaluation',
        path: '/feedback/evaluation/:id',
        icon: <VscFeedback style={style} />,
        isDivider: true,
        element: <FeedbackEvaluation />,
        hideInSideBar: true
      },
      // {
      //   title: 'Certificate',
      //   path: '/routine-management/trainee-evaluation/:id/generate-certificate/:traineeId/certificate',
      //   icon: 'ri-user-2-line',
      //   element: <Landscape />,
      //   hideInSideBar: true
      // },
      {
        title: 'Trainee',
        path: '/trainee/profile',
        icon: 'ri-user-2-line',
        element: <TraineeProfile />,
        hideInSideBar: true,
        isForTrainee: true
      },
      {
        title: 'Master Data',
        path: '/master-data',
        icon: <FiDatabase style={style} />,
        element: <MasterData />,
        children: [
          {
            title: 'Service Group',
            path: '/master-data/service-group',
            icon: <TbDatabaseImport style={style} />,
            element: <ServiceGroup />
          },
          {
            title: 'Service Group Management',
            path: '/master-data/service-group/create',
            icon: 'ri-user-2-line',
            element: <MasterDetailsPage />,
            hideInSideBar: true
          },
          {
            title: 'Service Group Management',
            path: '/master-data/service-group/edit/:id',
            icon: 'ri-user-2-line',
            element: <MasterDetailsPage />,
            hideInSideBar: true
          },
          {
            title: 'Service Sub Group',
            path: '/master-data/service-sub-group',
            icon: <TbDatabaseImport style={style} />,
            element: <ServiceSubGroup />
          },
          {
            title: 'Service Sub Group Management',
            path: '/master-data/service-sub-group/create',
            icon: 'ri-user-2-line',
            element: <ServiceSubDetailsPage />,
            hideInSideBar: true
          },
          {
            title: 'Service Sub Group Management',
            path: '/master-data/service-sub-group/edit/:id',
            icon: 'ri-user-2-line',
            element: <ServiceSubDetailsPage />,
            hideInSideBar: true
          },
          {
            title: 'Ethnicity',
            path: '/master-data/ethnicity',
            icon: <TbDatabaseImport style={style} />,
            element: <Ethnicity />
          },
          {
            title: 'Ethnicity Group Management',
            path: '/master-data/ethnicity/create',
            icon: 'ri-user-2-line',
            element: <EthnicityDetailsPage />,
            hideInSideBar: true
          },
          {
            title: 'Ethnicity Group Management',
            path: '/master-data/ethnicity/edit/:id',
            icon: 'ri-user-2-line',
            element: <EthnicityDetailsPage />,
            hideInSideBar: true
          }
        ]
      },

      {
        title: 'Routine Management',
        path: '/routine-management',
        icon: <TbFileSettings style={style} />,
        element: <RoutineManagement />,
        isDivider: true
      },

      {
        title: 'Profile',
        path: '/profile',
        icon: 'ri-dashboard-2-line',
        hideInSideBar: true,
        element: <Profile />,
        isForTrainee: true
      },
      {
        title: 'View Feedback session',
        path: '/training/routine/:routineId/session/:sessionId/feedback',
        element: <ViewFeedBackSubmit />,
        hideInSideBar: true,
        isDivider: true
      },
      {
        title: 'View Feedback module',
        path: '/training/routine/:routineId/module/:module_id/feedback',
        element: <ViewFeedBackSubmitModule />,
        hideInSideBar: true,
        isDivider: true
      },
      {
        title: 'Organization Management',
        path: '/organizations-mgmt',
        icon: <VscOrganization style={style} />,
        element: <OrganizationManagement />,
        children: [
          {
            title: 'Organizations',
            path: '/organizations-mgmt/organizations',
            icon: 'ri-group-line',
            element: <Organizations />
          },

          {
            title: 'Organizations Department',
            path: '/organizations-mgmt/organizations-department',
            icon: 'ri-group-line',
            element: <OrganizationDepartments />
          },
          {
            title: 'Organizations Center',
            path: '/organizations-mgmt/organizations-center',
            icon: 'ri-group-line',
            element: <OrganizationCentres />
          }
        ]
      },

      {
        title: 'Hall Management',
        path: '/hall-mgmt',
        icon: <BsFillDoorOpenFill style={style} />,
        children: [
          {
            title: 'Halls',
            path: '/hall-mgmt/halls',
            icon: 'ri-group-line',
            element: <OrganizationHalls />
          }
        ]
      },
      {
        title: 'Roles & Permissions',
        path: '/roles-permissions',
        icon: <MdOutlineManageAccounts style={style} />,
        isDivider: true,
        element: <RolesAndPermissions />,
        children: [
          // {
          //   title: 'Administrators',
          //   path: '/roles-permissions/administrators',
          //   icon: 'ri-dashboard-2-line',
          //   index: true,
          //   element: <Administrators />
          // },
          {
            title: 'Roles',
            path: '/roles-permissions/roles',
            icon: 'ri-dashboard-2-line',

            index: true,
            element: <Roles />
          }
        ]
      },
      {
        title: 'Assign roles',
        path: '/role/assignroles/roleId=:id',
        element: <AssignRoles />,
        hideInSideBar: true,
        isDivider: true
      },
      {
        title: 'Feedback Management',
        path: '/feedback-mgmt',
        icon: <VscFeedback style={style} />,
        isDivider: true,
        element: <FeedbackManagement />,
        children: [
          {
            title: 'Feedback',
            path: '/feedback-mgmt/feedback',
            icon: 'ri-draft-line',
            element: <Feedback />
          },
          {
            title: 'Routine Feedback',
            path: '/feedback-mgmt/routinefeedback/:id/tab=:tab',
            icon: 'ri-draft-line',
            element: <RoutineFeedback />,
            hideInSideBar: true
          },
          {
            title: 'Feedback Result',
            path: '/feedback-mgmt/feedbackresult',
            icon: 'ri-draft-line',
            element: <FeedbackResult />,
            hideInSideBar: true
          }
        ]
      },
      {
        title: 'Modules And Sessions',
        path: '/modules/session/:sessionId',
        icon: <VscOutput style={style} />,
        element: <SessionTable />,
        hideInSideBar: true
      },
      {
        title: 'Resourcses',
        path: '/modules/session/resources/:id',
        icon: <VscOutput style={style} />,
        element: <Resources />,
        hideInSideBar: true
      },
      {
        title: 'Assignments',
        path: '/modules/session/assignments/:id',
        icon: <VscOutput style={style} />,
        element: <Assignment />,
        hideInSideBar: true
      },
      {
        title: 'Assignments',
        path: '/modules/session/assignments/assignment/:id',
        icon: <VscOutput style={style} />,
        element: <AssignmentDetail />,
        hideInSideBar: true
      },

      {
        title: 'Trainee',
        path: '/trainee/profile',
        icon: 'ri-user-2-line',
        element: <TraineeProfile />,
        hideInSideBar: true,
        isForTrainee: true
      },
      {
        title: 'Routine Management',
        path: '/routine-management/:routineId',
        icon: 'ri-user-2-line',
        element: <RoutineDetailPage />,
        hideInSideBar: true
      },
      {
        title: 'Modules And Sessions',
        path: '/modules/modal',
        icon: <VscOutput style={style} />,
        element: <CourseForm />,
        hideInSideBar: true
      },
      {
        title: 'Modules And Sessions',
        path: '/modules/modal/edit/:courseId',
        icon: <VscOutput style={style} />,
        element: <CourseForm />,
        hideInSideBar: true
      },

      {
        title: 'Courses & Resource',
        path: '/modules/sessionForm/:courseId',
        icon: <VscOutput style={style} />,
        element: <SessionManagementForm />,
        hideInSideBar: true
      },
      {
        title: 'Courses & Resource',
        path: '/modules/sessionForm/edit/:sessionId/course_id/:courseId',
        icon: 'ri-user-2-line',
        element: <SessionManagementForm />,
        hideInSideBar: true
      },
      {
        title: 'Routines & Trainings',
        path: '/trainings/:id',
        icon: 'ri-user-2-line',
        element: <TrainingNew />,
        hideInSideBar: true
      }

      // {
      //   title: 'Dynamic Form Creations',
      //   path: '/form-creations',
      //   icon: 'ri-user-2-line',
      //   element: <FormsCreations />,
      //   children: [
      //     {
      //       title: 'Form Type',
      //       path: '/form-creations/types',
      //       icon: 'ri-user-2-line',
      //       element: <FormTypes />
      //     },

      //     {
      //       title: 'Form Fields',
      //       path: '/form-creations/form-fields',
      //       icon: 'ri-user-2-line',
      //       element: <FormFields />
      //     }
      //   ]
      // }

      // {
      //   title: 'Calendar',
      //   path: '/calendar',
      //   icon: 'ri-calendar-check-line',
      //   element: <Calender />
      // },
      // {
      //   title: 'Configuration',
      //   path: '/configurations',
      //   icon: 'ri-home-gear-line',
      //   element: <Configurations />
      // }
    ]
  }
];
