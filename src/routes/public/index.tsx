/* eslint-disable react/display-name */
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import ForgotPassword from 'pages/public-route/ForgotPassword';
import ResetPassword from 'pages/public-route/ResetPassword';
import ResendVerificationEmail from 'pages/public-route/ResendVerificationEmail';
import SignUp from 'pages/public-route/SignUp';
import VerifyEmail from 'pages/public-route/VerifyEmail';
import React, { ReactNode } from 'react';
import { Navigate } from 'react-router-dom';
import { IRoutes } from '../route-lists';

const SignIn = React.lazy(() => import('../../pages/public-route/Login'));

export const publicRoutes: IRoutes[] = [
  {
    path: SCREENS_ROUTES_ENUMS.SIGN_IN,
    title: 'Sign',
    element: <SignIn />,
    isPublic: true,
    children: [{ path: '*', element: <Navigate to="/404" /> }]
  },
  {
    path: SCREENS_ROUTES_ENUMS.REGISTER,
    title: 'Register',
    element: <SignUp />,
    isPublic: true,
    children: [{ path: '*', element: <Navigate to="/404" /> }]
  },
  {
    path: SCREENS_ROUTES_ENUMS.FORGOT_PASSWORD,
    title: 'ForgotPassword',
    element: <ResendVerificationEmail />,
    isPublic: true,
    children: [{ path: '*', element: <Navigate to="/404" /> }]
  },
  {
    path: SCREENS_ROUTES_ENUMS.VERIFY_OTP,
    title: 'Verify OTP',
    element: <VerifyEmail />,
    isPublic: true,
    children: [{ path: '*', element: <Navigate to="/404" /> }]
  },
  {
    path: SCREENS_ROUTES_ENUMS.RESEND_VERIFY_OTP,
    title: 'Resend Verify OTP',
    element: <ResendVerificationEmail />,
    isPublic: true,
    children: [{ path: '*', element: <Navigate to="/404" /> }]
  },
  {
    path: SCREENS_ROUTES_ENUMS.RESET_PASSWORD,
    title: 'Reset Password',
    element: <ResetPassword />,
    isPublic: true,
    children: [{ path: '*', element: <Navigate to="/404" /> }]
  }
];
