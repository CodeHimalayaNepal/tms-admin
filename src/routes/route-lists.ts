/* eslint-disable react/display-name */
import React, { ReactNode } from 'react';
import { privateRoutes } from './private';
import { publicRoutes } from 'routes/public';

export interface IRoutes {
  path: string;
  title?: string;
  element?: React.ReactNode;
  hideInSideBar?: boolean;
  children?: IRoutes[];
  isPublic?: boolean;
  icon?: any;
  isForTrainee?: boolean;
}

export const routesLists: IRoutes[] = [...privateRoutes, ...publicRoutes];
