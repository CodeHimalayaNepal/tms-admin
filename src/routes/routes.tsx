import React from 'react';
import { Navigate, useRoutes } from 'react-router-dom';

const AppRoutes: React.FC<{ routes: any }> = ({ routes }): JSX.Element => {
  const routing = useRoutes(routes);

  return <>{routing}</>;
};

export default AppRoutes;
