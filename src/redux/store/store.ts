import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import reducer, { middleware } from 'redux/reducers';
import { setupListeners } from '@reduxjs/toolkit/query';
import { useDispatch } from 'react-redux';

export const store = configureStore({
  reducer,
  middleware
});

export const useAppDispatch: () => AppDispatch = useDispatch;
setupListeners(store.dispatch);
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
