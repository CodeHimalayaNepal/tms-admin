import { combineReducers } from 'redux';
import { assignmentApi } from './assignment';
import { authenticationApi } from './auth';
import { coursesApi } from './courses';
import { organizationDepartmentApi } from './organization-department';
import { organizationHallApi } from './organization-halls';
import { organizationApi } from './organizations';
import { organizationCentreApi } from './organizations-centre';
import { rolesApi, moduleApi } from './roles';
import { trainingApi } from './training';
import { trainingTypeApi } from './training-type';
import { attendanceApi } from './attendance';
import { routineApi, TraineeRankReport } from './routine';
import { routineEnrollmentApi } from './routine-enrollment';
import { administratorApi } from './administrator';
import { formTypeApi } from './form-types';
import { formFieldsApi } from './form-fields';
import { roasterApi } from './roster';
import { evaluationApi } from './evaluation';
import { attendanceRequestApi } from './attendance/attendanceRequest';
import { courseResourceApi } from './course-resource';
import { userApi } from './user';
import { geographyApi } from './geography';
import { internalUserApi } from './internal-user';
import { ResourcesApi, sessionsApi } from './session-resources';
import { courseAsPerTrainingApi, courseCategoryApi } from './courseCategory';
import { evaluationCriteriaApi } from './evaluationCriteria';
import { evaluationCriteriaDetailsApi } from './evaluationCriteriaDetails';
import { newfeedbackApi } from './Feedback2';
import { feedbackevaluationCriteriaApi, inviteEvaluatorApi } from './FeedbackEvaluation';
import { routineFeedbackApi } from './routineFeedback';
import { dashboardApi } from './dashboard';
import { routinewiseSessionFeedback } from './Feedback/sessionAndModuleFeedback';
import {
  assignTraineeMarksApi,
  traineeListApi,
  traineeEvaluationCriteriaApi
} from './TraineeEvaluation';
import { submitFeedback } from './Feedback/SubmitAndUnsubmitFeedbackSession';
import { submitFeedbackModule } from './Feedback/SubmitAndUnsubmitFeedbackModule';
import { certificationApi } from './certification';
import { masterApi } from './master';

const reducers = combineReducers({
  [authenticationApi.reducerPath]: authenticationApi.reducer,
  [coursesApi.reducerPath]: coursesApi.reducer,
  [rolesApi.reducerPath]: rolesApi.reducer,
  [moduleApi.reducerPath]: moduleApi.reducer,
  [trainingApi.reducerPath]: trainingApi.reducer,
  [trainingTypeApi.reducerPath]: trainingTypeApi.reducer,
  [organizationApi.reducerPath]: organizationApi.reducer,
  [organizationHallApi.reducerPath]: organizationHallApi.reducer,
  [organizationDepartmentApi.reducerPath]: organizationDepartmentApi.reducer,
  [organizationCentreApi.reducerPath]: organizationCentreApi.reducer,
  [assignmentApi.reducerPath]: assignmentApi.reducer,
  [attendanceApi.reducerPath]: attendanceApi.reducer,
  [attendanceRequestApi.reducerPath]: attendanceRequestApi.reducer,
  [routineApi.reducerPath]: routineApi.reducer,
  [TraineeRankReport.reducerPath]: TraineeRankReport.reducer,
  [routineEnrollmentApi.reducerPath]: routineEnrollmentApi.reducer,
  [administratorApi.reducerPath]: administratorApi.reducer,
  [formTypeApi.reducerPath]: formTypeApi.reducer,
  [formFieldsApi.reducerPath]: formFieldsApi.reducer,
  [roasterApi.reducerPath]: roasterApi.reducer,
  [evaluationApi.reducerPath]: evaluationApi.reducer,
  [courseResourceApi.reducerPath]: courseResourceApi.reducer,
  [userApi.reducerPath]: userApi.reducer,
  [geographyApi.reducerPath]: geographyApi.reducer,
  [internalUserApi.reducerPath]: internalUserApi.reducer,
  [sessionsApi.reducerPath]: sessionsApi.reducer,
  [courseCategoryApi.reducerPath]: courseCategoryApi.reducer,
  [evaluationCriteriaApi.reducerPath]: evaluationCriteriaApi.reducer,
  [evaluationCriteriaDetailsApi.reducerPath]: evaluationCriteriaDetailsApi.reducer,
  [newfeedbackApi.reducerPath]: newfeedbackApi.reducer,
  [feedbackevaluationCriteriaApi.reducerPath]: feedbackevaluationCriteriaApi.reducer,
  [inviteEvaluatorApi.reducerPath]: inviteEvaluatorApi.reducer,
  [routineFeedbackApi.reducerPath]: routineFeedbackApi.reducer,
  [traineeListApi.reducerPath]: traineeListApi.reducer,
  [traineeEvaluationCriteriaApi.reducerPath]: traineeEvaluationCriteriaApi.reducer,
  [dashboardApi.reducerPath]: dashboardApi.reducer,
  [routinewiseSessionFeedback.reducerPath]: routinewiseSessionFeedback.reducer,
  [submitFeedback.reducerPath]: submitFeedback.reducer,
  [submitFeedbackModule.reducerPath]: submitFeedbackModule.reducer,
  [assignTraineeMarksApi.reducerPath]: assignTraineeMarksApi.reducer,
  [certificationApi.reducerPath]: certificationApi.reducer,
  [ResourcesApi.reducerPath]: ResourcesApi.reducer,
  [courseAsPerTrainingApi.reducerPath]: courseAsPerTrainingApi.reducer,
  [masterApi.reducerPath]: masterApi.reducer
});

export const middleware = (getDefaultMiddleware: any) =>
  getDefaultMiddleware()
    .concat(authenticationApi.middleware)
    .concat(coursesApi.middleware)
    .concat(rolesApi.middleware)
    .concat(moduleApi.middleware)
    .concat(trainingApi.middleware)
    .concat(trainingTypeApi.middleware)
    .concat(organizationApi.middleware)
    .concat(organizationHallApi.middleware)
    .concat(organizationDepartmentApi.middleware)
    .concat(organizationCentreApi.middleware)
    .concat(internalUserApi.middleware)
    .concat(assignmentApi.middleware)
    .concat(attendanceApi.middleware)
    .concat(routineApi.middleware)
    .concat(TraineeRankReport.middleware)
    .concat(routineEnrollmentApi.middleware)
    .concat(administratorApi.middleware)
    .concat(formTypeApi.middleware)
    .concat(formFieldsApi.middleware)
    .concat(evaluationApi.middleware)
    .concat(roasterApi.middleware)
    .concat(courseResourceApi.middleware)
    .concat(userApi.middleware)
    .concat(geographyApi.middleware)
    .concat(attendanceRequestApi.middleware)
    .concat(sessionsApi.middleware)
    .concat(courseCategoryApi.middleware)
    .concat(evaluationCriteriaApi.middleware)
    .concat(evaluationCriteriaDetailsApi.middleware)
    .concat(newfeedbackApi.middleware)
    .concat(feedbackevaluationCriteriaApi.middleware)
    .concat(inviteEvaluatorApi.middleware)
    .concat(routineFeedbackApi.middleware)
    .concat(traineeListApi.middleware)
    .concat(traineeEvaluationCriteriaApi.middleware)
    .concat(dashboardApi.middleware)
    .concat(submitFeedback.middleware)
    .concat(assignTraineeMarksApi.middleware)
    .concat(submitFeedbackModule.middleware)
    .concat(routinewiseSessionFeedback.middleware)
    .concat(ResourcesApi.middleware)
    .concat(courseAsPerTrainingApi.middleware)
    .concat(certificationApi.middleware)
    .concat(masterApi.middleware);

export default reducers;
