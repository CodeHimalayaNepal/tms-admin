import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { AnyARecord } from 'dns';
import { baseQuery } from 'redux/reducers/settings';

export interface IEvaluationCriteriaResponse extends IEvaluationCriteriaFormInput {
  evaluation_criteria_id: string;
  evaluation_criteria_details: {
    evaluation_criteria_detail_id: number;
    name: string;
  };
  evaluation_criteria_Detail: IEvaluationCriteriaDetails;
}
export interface IEvaluationCriteriaDetails {
  id?: number;
  evaluation_criteria_detail?: any;
  criteria?: string;
  parentCriteria?: string;
  max_marks?: number;
  routine?: number;

  routine_evaluation?: any;
}

export interface IEvaluationCriteriaFormInput {
  id?: number;
  // evaluation_criteria_detail?: number;
  evaluation_criteria_details?: {
    evaluation_criteria_detail_id: number;
    name: string;
  };
  evaluation_criteria_detail: any;
  parentCriteria: string;
  criteria: string;
  max_marks: number;
  routine: number;
  routine_evaluation: any;
}

export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
  isHover?: number | void;
  id?: number | void;
}

export const feedbackevaluationCriteriaApi: { [key: string]: any } = createApi({
  reducerPath: 'feedbackEvaluationCriteriaApi',
  baseQuery: baseQuery,
  tagTypes: ['FeedbackEvaluationCriteria'],
  endpoints: (build) => ({
    listEvaluationCriteria: build.query<
      Response<PaginatedDataResponse<IEvaluationCriteriaResponse[]>>,
      IPageQueryParams | null
    >({
      // query: (ids) => `/evaluation/evaluation-criteria/routine/${ids?.id}?trainee=${ids?.isHover}`,
      query: (ids) => `/evaluation/evaluation-criteria/routine/${ids?.id}/?page=${ids?.page}`,

      // `/evaluation/evaluation-criteria/routine/${id}/?page=${paging?.page}`,
      providesTags: (result: any, error, page) =>
        result?.results
          ? [
              result.results.data.map(({ id }: { id: any }) => ({
                type: 'FeedbackEvaluationCriteria' as const,
                id: id
              })),
              { type: 'FeedbackEvaluationCriteria', id: 'PARTIAL-LIST' }
            ]
          : [
              result.data.map(({ id }: { id: any }) => ({
                type: 'FeedbackEvaluationCriteria' as const,
                id: id
              })),
              { type: 'FeedbackEvaluationCriteria', id: 'PARTIAL-LIST' }
            ]
    }),
    createEvaluationCriteria: build.mutation<
      IEvaluationCriteriaResponse,
      IEvaluationCriteriaFormInput
    >({
      query(body) {
        return {
          url: `/evaluation/routine-evaluation-criteria/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'FeedbackEvaluationCriteria', id: 'PARTIAL-LIST' }]
    }),

    updateEvaluationCriteria: build.mutation<
      IEvaluationCriteriaResponse,
      IEvaluationCriteriaFormInput
    >({
      query(body) {
        const { id } = body;
        return {
          url: `/evaluation/routine-evaluation-criteria/${id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'FeedbackEvaluationCriteria', id: 'PARTIAL-LIST' }]
    }),
    deleteEvaluationCriteria: build.mutation<IEvaluationCriteriaResponse, string>({
      query(id) {
        return {
          url: `evaluation/routine-evaluation-criteria/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'FeedbackEvaluationCriteria', id: 'PARTIAL-LIST' }]
    }),
    listEvalCriteriDetails: build.query<
      Response<PaginatedDataResponse<IEvaluationCriteriaResponse[]>>,
      IPageQueryParams | null
    >({
      query: (paging) =>
        `/evaluation/criteria-detail/?page=${paging?.page}&page_size=${paging?.pageSize}`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ id }) => ({
                type: 'FeedbackEvaluationCriteria' as const,
                id: id
              })),
              { type: 'FeedbackEvaluationCriteria', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'FeedbackEvaluationCriteria', id: 'PARTIAL-LIST' }]
    })
  })
});
export const {
  useListEvaluationCriteriaQuery,
  useCreateEvaluationCriteriaMutation,
  useUpdateEvaluationCriteriaMutation,
  useDeleteEvaluationCriteriaMutation,
  useListEvalCriteriDetailsQuery
} = feedbackevaluationCriteriaApi;

///////Evaluator details///////

export interface IInviteEvaluatorResponse extends IInviteEvaluatorFormInput {
  invite_evaluator_id: string;
  invite_evaluator_detail: IInviteEvaluatorDetails;
}

export interface IInviteEvaluatorDetails {
  tms_invite_evaluator_id?: number;
  module?: string;
  evaluator?: string;
}

export interface IInviteEvaluatorFormInput {
  tms_invite_evaluator_id?: number;
  module: string;
  evaluator: string;
}

export const inviteEvaluatorApi: { [key: string]: any } = createApi({
  reducerPath: 'inviteEvaluatorApi',
  baseQuery: baseQuery,
  tagTypes: ['InviteEvaluator'],
  endpoints: (build) => ({
    listInviteEvaluator: build.query<
      Response<PaginatedDataResponse<IInviteEvaluatorResponse[]>>,
      IPageQueryParams | null
    >({
      query: (paging) => `evaluation/routine-evaluator/?page=${paging?.page}`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ tms_invite_evaluator_id }) => ({
                type: 'InviteEvaluator' as const,
                id: tms_invite_evaluator_id
              })),
              { type: 'InviteEvaluator', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'InviteEvaluator', id: 'PARTIAL-LIST' }]
    }),
    createInviteEvaluator: build.mutation<
      IEvaluationCriteriaResponse,
      IEvaluationCriteriaFormInput
    >({
      query(body) {
        return {
          url: `training/feedback_question/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'InviteEvaluator', id: 'PARTIAL-LIST' }]
    })
  })
});

export const { useListInviteEvaluatorQuery, useCreateInviteEvaluatorMutation } = inviteEvaluatorApi;
