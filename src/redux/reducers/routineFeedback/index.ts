import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';

export interface IFeedBackFormInput {
  training_routine: number;
  feedback_question: string;
  question_for: string;
}

export interface IRoutineFeedbackQuestResponse extends IFeedbackFormInput {
  routine_feedback_que_id: string;
  feedback_question_details: IFeedbackDetails;
  question_for: string;
}

export interface IFeedbackFormInput {
  tms_feedback_question_id?: number;
  question: string;
  question_np: string;
  question_type: string;
}

export interface IFeedbackDetails {
  tms_feedback_question_id: number;
  question: string;
  question_np: string;
  question_type: string;
}

export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
}

export const routineFeedbackApi: { [key: string]: any } = createApi({
  reducerPath: 'routineFeedbackApi',
  baseQuery: baseQuery,
  tagTypes: ['RoutineFeedback'],
  endpoints: (build) => ({
    listRoutineSessionQuestion: build.query<
      Response<PaginatedDataResponse<IRoutineFeedbackQuestResponse[]>>,
      IPageQueryParams | null
    >({
      query: (id) => `training/routine/${id}/feedback/questions/?question_for=daily_session`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ tms_feedback_question_id }) => ({
                type: 'RoutineFeedback' as const,
                id: tms_feedback_question_id
              })),
              { type: 'RoutineFeedback', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'RoutineFeedback', id: 'PARTIAL-LIST' }]
    }),
    listRoutineLunchQuestion: build.query<
      Response<PaginatedDataResponse<IRoutineFeedbackQuestResponse[]>>,
      IPageQueryParams | null
    >({
      query: (id) => `training/routine/${id}/feedback/questions/?question_for=lunch`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ tms_feedback_question_id }) => ({
                type: 'RoutineFeedback' as const,
                id: tms_feedback_question_id
              })),
              { type: 'RoutineFeedback', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'RoutineFeedback', id: 'PARTIAL-LIST' }]
    }),
    listRoutineTrainingQuestion: build.query<
      Response<PaginatedDataResponse<IRoutineFeedbackQuestResponse[]>>,
      IPageQueryParams | null
    >({
      query: (id) => `training/routine/${id}/feedback/questions/?question_for=training`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ tms_feedback_question_id }) => ({
                type: 'RoutineFeedback' as const,
                id: tms_feedback_question_id
              })),
              { type: 'RoutineFeedback', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'RoutineFeedback', id: 'PARTIAL-LIST' }]
    }),

    listRoutineModuleQuestion: build.query<
      Response<PaginatedDataResponse<IRoutineFeedbackQuestResponse[]>>,
      IPageQueryParams | null
    >({
      query: (id) => `training/routine/${id}/feedback/questions/?question_for=module`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ tms_feedback_question_id }) => ({
                type: 'RoutineFeedback' as const,
                id: tms_feedback_question_id
              })),
              { type: 'RoutineFeedback', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'RoutineFeedback', id: 'PARTIAL-LIST' }]
    }),
    createFeedbackMS: build.mutation<IRoutineFeedbackQuestResponse, IFeedBackFormInput>({
      query(body) {
        return {
          url: `training/routine_feedback_question/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'RoutineFeedback', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListRoutineSessionQuestionQuery,
  useListRoutineLunchQuestionQuery,
  useListRoutineModuleQuestionQuery,
  useListRoutineTrainingQuestionQuery,
  useCreateFeedbackMSMutation
} = routineFeedbackApi;

// export const postModSessFeedbackApi: { [key: string]: any } = createApi({
//   reducerPath: 'post_Mod_Sess_FeedbackApi',
//   baseQuery: baseQuery,
//   tagTypes: ['Post_Mod_Sess_Feedback'],
//   endpoints: (build) => ({
//     createFeedbackMS: build.mutation<IRoutineFeedbackQuestResponse, IFeedBackFormInput>({
//       query(body) {
//         return {
//           url: `training/routine_feedback_question/create/`,
//           method: 'POST',
//           body
//         };
//       },
//       invalidatesTags: [{ type: 'Post_Mod_Sess_Feedback', id: 'PARTIAL-LIST' }]
//     })
//   })
// });
// export const { useCreateFeedbackMSMutation } = postModSessFeedbackApi;
