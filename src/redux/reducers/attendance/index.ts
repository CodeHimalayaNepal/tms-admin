// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';
import { ITrainingTypeResponse } from '../training-type';
import { IOrganizationHallResponse } from '../organization-halls';
import { IRoutineDetail } from '../routine';
import { number } from 'yup';

export interface IAttendanceResponse extends IAttendanceInput {
  routine_details: IRoutineDetails; // routine remanint
  trainee_details: ITrainingTypeResponse;
  hall_details: IOrganizationHallResponse;
  approved_by_details: IApprovedByDetails; // approved_by_details remaingin
}

export interface IAttendanceInput {
  attendance_id?: number;
  UserPin?: number;
  VerifyType: boolean;
  Temperature: string;
  DeviceSn: string;
  BranchCode: string;
  VerifyTime: string;
  status: boolean;
  check_in: string;
  check_out: string;
  reason: string;
  requested_date: string;
  approved_by: number;
  approved_date: string;
}
export interface IRoutineDetails {
  id: number;
  name: string;
}
export interface IApprovedByDetails {
  id: number;
  email: string;
}

export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
  start_date?: string;
  end_date?: string;
}

export interface IAttendance {
  approved_by: number | null;
  approved_date: string;
  branch_code: string;
  check_in: string;
  check_out: string;
  id: number;
  reason: null;
  requested_date: string;
  source: string;
  status: boolean;
  temperature: number;
  user_id: number;
}

export const attendanceApi: { [key: string]: any } = createApi({
  reducerPath: 'attendanceApi',
  baseQuery: baseQuery,
  tagTypes: ['Attendance'],
  endpoints: (build) => ({
    listAttendance: build.query<
      Response<PaginatedDataResponse<IAttendanceResponse[]>>,
      IPageQueryParams | null
    >({
      query: (paging) => {
        if (paging?.start_date && paging.end_date) {
          return `attendance/all-detail/list/?page=${paging?.page}&pageSize=${paging?.pageSize}&start_date=${paging?.start_date}&end_date=${paging?.end_date}`;
        } else {
          return `attendance/all-detail/list/?page=${paging?.page}&pageSize=${paging?.pageSize}`;
        }
      },
      keepUnusedDataFor: 0,
      // providesTags: [{ type: 'Attendance', id: 'PARTIAL-LIST' }]
      providesTags: (result, error, page) =>
        result && result.results
          ? [
              ...result.results.data.map(({ attendance_id }: { attendance_id: number }) => ({
                type: 'Attendance' as const,
                id: attendance_id
              })),
              { type: 'Attendance', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Attendance', id: 'PARTIAL-LIST' }]
    }),

    createAttendance: build.mutation<IAttendanceResponse, IAttendanceInput>({
      query(body) {
        return {
          url: `attendance/attendance_detail/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'Attendance', id: 'PARTIAL-LIST' }]
    }),

    updateAttendance: build.mutation<IAttendanceResponse, IAttendanceInput>({
      query(body) {
        const { attendance_id } = body;
        return {
          url: `organization/update/${attendance_id}`,
          method: 'PUT',
          body
        };
      },
      invalidatesTags: [{ type: 'Attendance', id: 'PARTIAL-LIST' }]
    }),
    deleteAttendance: build.mutation<IAttendanceResponse, string>({
      query(id) {
        return {
          url: `attendance/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'Attendance', id: 'PARTIAL-LIST' }] // TODO:
    })
  })
});

export const {
  useListAttendanceQuery,
  useCreateAttendanceMutation,
  useUpdateAttendanceMutation,
  useDeleteAttendanceMutation
} = attendanceApi;
