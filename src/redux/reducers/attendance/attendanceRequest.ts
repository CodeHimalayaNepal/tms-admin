// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';
import { ITrainingTypeResponse } from '../training-type';
import { IOrganizationHallResponse } from '../organization-halls';
import { IRoutineDetail } from '../routine';
import moment from 'moment';

export interface IAttendanceRequestResponse extends IAttendanceRequestInput {
  attendance_id?: number;
  routine_details: IRoutineDetails; // routine remanint
  trainee_details: ITrainingTypeResponse;
  hall_details: IOrganizationHallResponse;
  approved_by_details: IApprovedByDetails; // approved_by_details remaingin
}

export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
}

export interface IAttendanceRequestInput {
  id?: number;
  user?: string;
  check_in: string;
  check_out: string;
  reason: string;
  source: string;
  approved_by: number;
  status?: boolean;
}
export interface IRoutineDetails {
  id: number;
  name: string;
}
export interface IApprovedByDetails {
  id: number;
  email: string;
}

export const attendanceRequestApi: { [key: string]: any } = createApi({
  reducerPath: 'attendanceRequestApi',
  baseQuery: baseQuery,
  tagTypes: ['AttendanceRequest'],
  endpoints: (build) => ({
    listAttendance: build.query<Response<IAttendanceRequestResponse[]>, IPageQueryParams | null>({
      query: (paging) => {
        return `attendance/request-attendance/list/`;
      },
      providesTags: ['AttendanceRequest']
    }),

    createAttendance: build.mutation<IAttendanceRequestResponse, IAttendanceRequestInput>({
      query(body) {
        return {
          url: `attendance/request-attendance/create/`,
          method: 'POST',
          body: {
            ...body,
            check_in: moment(body.check_in, [moment.ISO_8601, 'HH:mm']).toISOString(),
            check_out: moment(body.check_out, [moment.ISO_8601, 'HH:mm']).toISOString()
          }
        };
      },
      invalidatesTags: ['AttendanceRequest']
    }),

    updateAttendance: build.mutation<IAttendanceRequestResponse, IAttendanceRequestInput>({
      query(body) {
        const { id } = body;
        return {
          url: `organization/update/${id}`,
          method: 'PUT',
          body
        };
      },
      invalidatesTags: ['AttendanceRequest']
    }),
    deleteAttendance: build.mutation<IAttendanceRequestResponse, string>({
      query(id) {
        return {
          url: `attendance/request-attendance/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: ['AttendanceRequest'] // TODO:
    }),
    approveAttendance: build.mutation<IAttendanceRequestResponse, string>({
      query(id) {
        return {
          url: `attendance/verify-attendance/${id}/`,
          method: 'PATCH'
        };
      },
      invalidatesTags: ['AttendanceRequest'] // TODO:
    })
  })
});

export const {
  useListAttendanceQuery,
  useCreateAttendanceMutation,
  useUpdateAttendanceMutation,
  useDeleteAttendanceMutation,
  useApproveAttendanceMutation
} = attendanceRequestApi;
