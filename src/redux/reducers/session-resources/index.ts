import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse, PaginatedDataResponse, Response } from 'common/types';
import { STATUS } from 'common/enum';

export interface SessionsResponse extends ISessionInput {
  created_by: number;
  code: string;
  id: number;
  course_topic_id: number;
}

export interface ISessionInput {
  // session_id?: number;
  title: string;
  description: string;
  learning_objective: string;
  duration_hr: number;
  order: number;
  course_topic_files: any;
  course: string;
  methodology: string;
  course_topic_id: number;
}
export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
  id?: number | void;
  course_id?: number | void;
}

export const sessionsApi: { [key: string]: any } = createApi({
  reducerPath: 'SessionAPI',
  baseQuery: baseQuery,
  tagTypes: ['Sessions'],
  endpoints: (build) => ({
    listSession: build.query<
      Response<PaginatedDataResponse<SessionsResponse[]>>,
      IPageQueryParams | null
    >({
      query: (id) =>
        `training/course-session-list/${id?.id}/?page=${id?.page}&page_size=${id?.pageSize}`,
      providesTags: (result, error, id) =>
        result
          ? [
              ...result.data.results.map(({ course_topic_id }) => ({
                type: 'Sessions' as const,
                id: course_topic_id
              })),
              { type: 'Sessions', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Sessions', id: 'PARTIAL-LIST' }]
    }),

    createSession: build.mutation<SessionsResponse, ISessionInput>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof ISessionInput];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData.append(`${data}[${index}]`, v);
              });
            }
          } else {
            formData.append(
              data,
              value instanceof File
                ? value
                : value instanceof Object
                ? JSON.stringify(value)
                : value ?? ''
            );
          }
        }
        return {
          url: `training/course-session-create/`,
          method: 'POST',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'Sessions', id: 'PARTIAL-LIST' }]
    }),
    listSessionById: build.query<PaginatedDataResponse<ISessionInput[]>, void>({
      query: (course_topic_id) => `/training/course-session-detail/${course_topic_id}/`
    }),
    updateSession: build.mutation<SessionsResponse, ISessionInput>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof ISessionInput];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData?.append(`${data}[${index}]`, v);
              });
            }
          } else {
            formData.append(
              data,
              value instanceof File
                ? value
                : value instanceof Object
                ? JSON.stringify(value)
                : value ?? ''
            );
          }
        }
        return {
          url: `training/course-session-update/${body.course_topic_id}/`,
          method: 'PATCH',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'Sessions', id: 'PARTIAL-LIST' }]
    }),

    deleteSession: build.mutation<SessionsResponse, string>({
      query(course_topic_id) {
        return {
          url: `training/course-session-delete/${course_topic_id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'Sessions', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListSessionQuery,
  useCreateSessionMutation,
  useListSessionByIdQuery,
  useDeleteSessionMutation,
  useUpdateSessionMutation
} = sessionsApi;

export const ResourcesApi: { [key: string]: any } = createApi({
  reducerPath: 'ResourcesApi',
  baseQuery: baseQuery,
  tagTypes: ['resources'],
  endpoints: (build) => ({
    listResources: build.query<Response<PaginatedDataResponse<any[]>>, IPageQueryParams | null>({
      query: (id) =>
        `training/course-session-resource-list/?course_topic=${id?.id}&page=${id?.page}&page_size=${id?.pageSize}`,
      providesTags: (result, error, id) =>
        result
          ? [
              ...result.data.results.map(({ course_topic_resource_id }) => ({
                type: 'resources' as const,
                id: course_topic_resource_id
              })),
              { type: 'resources', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'resources', id: 'PARTIAL-LIST' }]
    }),

    createResources: build.mutation<any, any>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof any];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData.append(`${data}`, v);
              });
            }
          } else {
            formData.append(
              data,
              value instanceof File
                ? value
                : value instanceof Object
                ? JSON.stringify(value)
                : value ?? ''
            );
          }
        }

        return {
          url: `training/course-session-resource-create/`,
          method: 'POST',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'resources', id: 'PARTIAL-LIST' }]
    }),
    updateResource: build.mutation<any, any>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof any];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData.append(`${data}`, v);
              });
            }
          } else {
            formData.append(
              data,
              value instanceof File
                ? value
                : value instanceof Object
                ? JSON.stringify(value)
                : value ?? ''
            );
          }
        }
        return {
          url: `training/course-session-resource-update/${body.course_topic_resource_id}/`,
          method: 'PATCH',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'resources', id: 'PARTIAL-LIST' }]
    }),
    deleteResources: build.mutation<any, any>({
      query(tms_feedback_question_id) {
        return {
          url: `training/course-session-resource-delete/${tms_feedback_question_id}`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'resources', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useCreateResourcesMutation,
  useUpdateResourceMutation,
  useDeleteResourcesMutation,
  useListResourcesQuery
} = ResourcesApi;
