// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { baseQuery } from 'redux/reducers/settings';

// get assignment
export interface IPageQueryParams {
  id?: string | void;
  page?: number | void;
  pageSize?: number | void;
}
export interface Assignment {
  count?: number;
  next: string;
  previous: string;
  results: Result[];
}

interface Result {
  tms_assignment_id: string;
  assignment_file: string;
  start_date: string;
  finish_date: string;
  assignment_description: string;
  course_details: CourseDetails;
}

interface CourseDetails {
  tms_course_id: string;
  title: string;
  code: string;
  description: string;
  duration: number;
  status: boolean;
}

// post create assignment
export interface CreateAssignment {
  assignment_id: string;
  assignment_file: string;
  start_date: string;
  finish_date: string;
  assignment_description: string;
  course_id: number;
}

// update assignment
export interface UpdateAssignent extends CreateAssignment {}

export const assignmentApi = createApi({
  reducerPath: 'assignmentApi',
  baseQuery: baseQuery,
  tagTypes: ['assignment'],
  endpoints: (build) => ({
    assignment: build.query<Response<PaginatedDataResponse<Assignment[]>>, IPageQueryParams | null>(
      {
        query: (id) => `assignment/?topic_id=${id?.id}&page=${id?.page}&page_size=${id?.pageSize}`,
        providesTags: (result) =>
          result
            ? [
                ...result.data.results.map(({ count }) => ({
                  type: 'assignment' as const,
                  id: count
                })),
                { type: 'assignment', id: 'ASSIGNMENT-LIST' }
              ]
            : [{ type: 'assignment', id: 'ASSIGNMENT-LIST' }]
      }
    ),
    assignmentDetail: build.query<Response<any>, IPageQueryParams | null>({
      query: (id) => `assignment/${id?.id}/`,
      providesTags: (result) => [{ type: 'assignment', id: 'ASSIGNMENT-LIST' }]
    }),
    createAssignment: build.mutation<any, any>({
      query(body) {
        const Id = body.topic_id;
        let formData = new FormData();
        for (let property in body) {
          const value = body[property as keyof any];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData.append(`${property}`, v);
              });
            } else {
              formData.append(property, JSON.stringify(value));
            }
          } else {
            formData.append(property, value);
          }
        }
        return {
          url: `assignment/topic/${Id}/create/`,
          method: 'POST',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'assignment', id: 'ASSIGNMENT-LIST' }]
    }),
    updateAssignment: build.mutation<any, any>({
      query(body) {
        const topic_id = body.topic_id;
        let formData = new FormData();
        for (let property in body) {
          const value = body[property as keyof any];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData.append(`${property}`, v);
              });
            } else {
              formData.append(property, JSON.stringify(value));
            }
          } else {
            formData.append(property, value);
          }
        }
        return {
          url: `assignment/update/${topic_id}/`,
          method: 'PATCH',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'assignment', id: 'ASSIGNMENT-LIST' }]
    }),
    deleteAssignment: build.mutation<any, any>({
      query(id) {
        return {
          url: `assignment/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'assignment', id: 'ASSIGNMENT-LIST' }]
    }),
    createMCQAssignment: build.mutation<any, any>({
      query(body) {
        const Id = body.assignment_id;

        return {
          url: `assignment/assignment/${Id}/mcq/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'assignment', id: 'ASSIGNMENT-LIST' }]
    }),
    updateMCQAssignment: build.mutation<any, any>({
      query(body) {
        const mcq_id = body.mcq_id;
        return {
          url: `assignment/mcq/update/${mcq_id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'assignment', id: 'ASSIGNMENT-LIST' }]
    }),
    deleteMCQAssignment: build.mutation<any, any>({
      query(id) {
        return {
          url: `assignment/mcq/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'assignment', id: 'ASSIGNMENT-LIST' }]
    }),
    deleteMCQAnswerAssignment: build.mutation<any, any>({
      query(id) {
        return {
          url: `assignment/mcq_answer/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'assignment', id: 'ASSIGNMENT-LIST' }]
    })
  })
});

export const {
  useAssignmentQuery,

  useAssignmentDetailQuery,
  useCreateAssignmentMutation,
  useUpdateAssignmentMutation,
  useDeleteAssignmentMutation,

  useCreateMCQAssignmentMutation,
  useUpdateMCQAssignmentMutation,
  useDeleteMCQAssignmentMutation,
  useDeleteMCQAnswerAssignmentMutation
} = assignmentApi;
