// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { baseQuery } from 'redux/reducers/settings';

export interface IOrganizationCentreResponse extends IOrganizationCentreInput {}

export interface IOrganizationCentreInput {
  tms_center_id?: number;
  department: number;
  name: string;
  resource_person: string;
  resource_email: string;
  resource_phone: string;
  status: boolean;
}
export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
}

export const organizationCentreApi: { [key: string]: any } = createApi({
  reducerPath: 'organizationCentreApi',
  baseQuery: baseQuery,
  tagTypes: ['OrganizationCentre'],
  endpoints: (build) => ({
    listOrganizationCentre: build.query<
      Response<IOrganizationCentreResponse[]>,
      IPageQueryParams | null
    >({
      query: (paging) => `organization/center/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ tms_center_id }) => ({
                type: 'OrganizationCentre' as const,
                id: tms_center_id
              })),
              { type: 'OrganizationCentre', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'OrganizationCentre', id: 'PARTIAL-LIST' }]
    }),
    createOrganizationCentre: build.mutation<IOrganizationCentreResponse, IOrganizationCentreInput>(
      {
        query(body) {
          return {
            url: `organization/center/create/`,
            method: 'POST',
            body
          };
        },
        invalidatesTags: [{ type: 'OrganizationCentre', id: 'PARTIAL-LIST' }]
      }
    ),

    updateOrganizationCentre: build.mutation<IOrganizationCentreResponse, IOrganizationCentreInput>(
      {
        query(body) {
          const { tms_center_id } = body;
          return {
            url: `organization/center/update/${tms_center_id}/`,
            method: 'PATCH',
            body
          };
        },
        invalidatesTags: [{ type: 'OrganizationCentre', id: 'PARTIAL-LIST' }]
      }
    ),
    deleteOrganizationCentre: build.mutation<IOrganizationCentreResponse, string>({
      query(id) {
        return {
          url: `organization/center/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'OrganizationCentre', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListOrganizationCentreQuery,
  useCreateOrganizationCentreMutation,
  useUpdateOrganizationCentreMutation,
  useDeleteOrganizationCentreMutation
} = organizationCentreApi;
