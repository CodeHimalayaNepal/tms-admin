// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse } from 'common/types';
import { ITrainingTypeResponse } from '../training-type';
import { IOrganizationHallResponse } from '../organization-halls';
import { IRoutineDetail } from '../routine';

export interface IEvaluationDetailsResponse extends IEvaluationDetailsInput {
  routine_details: IRoutineDetail; // routine remanint
}

export interface IEvaluationDetailsInput {
  evaluation_detail_id?: number;
  routine: number;
  total_weightage: string;
  coordinator_max_marks: string;
  coordinator_enforce: string;
  attendance_enforce: number;
  attendance_points: number;
  evaluator_enforce: number;
  evaluator_max_marks: number;
}

export const evaluationDetailsApi: { [key: string]: any } = createApi({
  reducerPath: 'evaluationDetailsApi',
  baseQuery: baseQuery,
  tagTypes: ['EvaluationDetails'],
  endpoints: (build) => ({
    listEvaluationDetails: build.query<PaginatedDataResponse<IEvaluationDetailsResponse[]>, void>({
      query: () => `evaluation/ev-detail/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.results.map(({ evaluation_detail_id }) => ({
                type: 'EvaluationDetails' as const,
                id: evaluation_detail_id
              })),
              { type: 'EvaluationDetails', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'EvaluationDetails', id: 'PARTIAL-LIST' }]
    }),

    createEvaluationDetails: build.mutation<IEvaluationDetailsResponse, IEvaluationDetailsInput>({
      query(body) {
        return {
          url: `evaluation/ev-detail/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'EvaluationDetails', id: 'PARTIAL-LIST' }]
    }),

    updateEvaluationDetails: build.mutation<IEvaluationDetailsResponse, IEvaluationDetailsInput>({
      query(body) {
        const { evaluation_detail_id } = body;
        return {
          url: `/evaluation/ev-detail/update/${evaluation_detail_id}`,
          method: 'PUT',
          body
        };
      },
      invalidatesTags: [{ type: 'EvaluationDetails', id: 'PARTIAL-LIST' }]
    }),
    updateEvaluationDetailSingle: build.mutation<
      IEvaluationDetailsResponse,
      IEvaluationDetailsInput
    >({
      query(body) {
        const { evaluation_detail_id } = body;
        return {
          url: `/evaluation/ev-detail/update/${evaluation_detail_id}`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'EvaluationDetails', id: 'PARTIAL-LIST' }]
    }),
    deleteEvaluationDetails: build.mutation<IEvaluationDetailsResponse, string>({
      query(evaluation_detail_id) {
        return {
          url: `evaluation/ev-detail/delete/${evaluation_detail_id}`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'EvaluationDetails', id: 'PARTIAL-LIST' }] // TODO:
    })
    //     getEvaluationDetailSingle: build.mutation<IEvaluationDetailsResponse, string>({
    //         query(evaluation_detail_id) {
    //           return {
    //             url: `evaluation/ev-detail/update/${evaluation_detail_id}`,
    //             method: 'GET'
    //           };
    //         },
    //         invalidatesTags: [{ type: 'EvaluationDetails', id: 'PARTIAL-LIST' }] // TODO:
    //       }),
  })
});

export const {
  useListEvaluationDetailsQuery,
  useCreateEvaluationDetailsMutation,
  useUpdateEvaluationDetailsMutation,
  useDeleteEvaluationDetailsMutation,
  useUpdateEvaluationDetailSingleMutation
} = evaluationDetailsApi;
