// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { baseQuery } from 'redux/reducers/settings';
import { IOrganizationResponse } from '../organizations';

export interface IOrganizationDepartmentResponse extends IOrganizationDepartmentInput {
  organization_details: IOrganizationResponse;
}

export interface IOrganizationDepartmentInput {
  tms_department_id?: number;
  name: string;
  // phone_no: string;
  email: string;
  organization: string;
  status: boolean;
}
export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
}

export const organizationDepartmentApi: { [key: string]: any } = createApi({
  reducerPath: 'organizationDepartmentApi',
  baseQuery: baseQuery,
  tagTypes: ['OrganizationDepartment'],
  endpoints: (build) => ({
    listOrganizationDepartment: build.query<
      Response<IOrganizationDepartmentResponse[]>,
      IPageQueryParams | null
    >({
      query: (paging) => `organization/department/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ tms_department_id }) => ({
                type: 'OrganizationDepartment' as const,
                id: tms_department_id
              })),
              { type: 'OrganizationDepartment', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'OrganizationDepartment', id: 'PARTIAL-LIST' }]
    }),
    createOrganizationDepartment: build.mutation<
      IOrganizationDepartmentResponse,
      IOrganizationDepartmentInput
    >({
      query(body) {
        return {
          url: `organization/department/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'OrganizationDepartment', id: 'PARTIAL-LIST' }]
    }),

    updateOrganizationDepartment: build.mutation<
      IOrganizationDepartmentResponse,
      IOrganizationDepartmentInput
    >({
      query(body) {
        const { tms_department_id } = body;
        return {
          url: `organization/department/update/${tms_department_id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'OrganizationDepartment', id: 'PARTIAL-LIST' }]
    }),
    deleteOrganizationDepartment: build.mutation<IOrganizationDepartmentResponse, string>({
      query(id) {
        return {
          url: `organization/department/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'OrganizationDepartment', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListOrganizationDepartmentQuery,
  useCreateOrganizationDepartmentMutation,
  useUpdateOrganizationDepartmentMutation,
  useDeleteOrganizationDepartmentMutation
} = organizationDepartmentApi;
