import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { useParams } from 'react-router-dom';
import { baseQuery } from 'redux/reducers/settings';

export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
  isHover?: number | void;
  id?: number | void;
}
export interface ITraineeListResponse {
  id: number;
  first_name: string;
  last_name: string;
  mobile_number?: number;
}

export interface IFeedbackResponse {
  id: number;
  routine_evaluator_details: {
    id: 0;
    evaluator_details: {
      id: 0;
      first_name: string;
      middle_name: string;
      last_name: string;
    };
  };
  trainee_details: {
    id: number;
    first_name: string;
    middle_name?: string;
    last_name: string;
    email: string;
  };
  obtain_marks: number;
}
export interface ITraineeMarksresponse {
  id: number;
  evaluation_criteria_details: {
    evaluation_criteria_detail_id: number;
    name: string;
    evaluation_criteria_detail: {
      evaluation_criteria_id: number;
      name: string;
      code: string;
      parent_detail: {
        parent_id: number;
        name: string;
        code: string;
      };
    };
    assign_marks: {
      obtain_marks: number;
    };
  };
}

export interface IFeedbackFormInput {
  newId?: number;
  routine_evaluator: number;
  trainee: number;
  obtain_marks: number;
}
export const traineeListApi: { [key: string]: any } = createApi({
  reducerPath: 'traineeListApi',
  baseQuery: baseQuery,
  tagTypes: ['TraineeListApi'],
  endpoints: (build) => ({
    listTrainee: build.query<
      Response<PaginatedDataResponse<ITraineeListResponse[]>>,
      IPageQueryParams | null
    >({
      query: (id) => `/training/schedule/list/?routine=${id}`,

      providesTags: (result: any, error, page) =>
        result
          ? [
              ...result?.results?.data.map(({ id }: { id: any }) => ({
                type: 'TraineeListApi' as const,
                id: id
              })),
              { type: 'TraineeListApi', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'TraineeListApi', id: 'PARTIAL-LIST' }]
    }),
    listTraineeMarks: build.query<
      Response<PaginatedDataResponse<ITraineeListResponse[]>>,
      IPageQueryParams | null
    >({
      query: (ids) => `/training/schedule/list/?routine=${ids?.id}&trainee=${ids?.isHover}`,
      // query: (ids) => `/training/schedule/list/?routine=${ids?.id}/?trainee=${ids?.traineeID}`,
      // `/evaluation/evaluation-criteria/routine/${id}/?page=${paging?.page}`,
      providesTags: (result: any, error, page) =>
        result
          ? [
              ...result?.results?.data.map(({ id }: { id: any }) => ({
                type: 'TraineeListApi' as const,
                id: id
              })),
              { type: 'TraineeListApi', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'TraineeListApi', id: 'PARTIAL-LIST' }]
    })
  })
});

export const { useListTraineeQuery, useListTraineeMarksQuery } = traineeListApi;

export interface ITraineeListResponse {
  id: number;
  first_name: string;
  last_name: string;
  mobile_number?: number;
}
export const assignTraineeMarksApi: { [key: string]: any } = createApi({
  reducerPath: 'assignTraineeMarksApi',
  baseQuery: baseQuery,
  tagTypes: ['AssignTraineeMarks', 'listTraineeEvaluationCriteria', 'listTraineeMarkCriteria'],
  endpoints: (build) => ({
    listTraineeMark: build.query<
      Response<PaginatedDataResponse<ITraineeMarksresponse[]>>,
      IPageQueryParams | null
    >({
      query: (id) => `training/feedback_question/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ id }) => ({
                type: 'AssignTraineeMarks' as const,
                id: id
              })),
              { type: 'AssignTraineeMarks', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'AssignTraineeMarks', id: 'PARTIAL-LIST' }]
    }),
    createTraineeMarks: build.mutation<IFeedbackResponse, IFeedbackFormInput>({
      query(body) {
        return {
          url: `evaluation/assign-marks/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [
        { type: 'listTraineeEvaluationCriteria', id: 'PARTIAL-LIST' },
        { type: 'listTraineeMarkCriteria', id: 'PARTIAL-LIST' },
        { type: 'AssignTraineeMarks', id: 'PARTIAL-LIST' }
      ]
    }),
    updateTraineeMarks: build.mutation<IFeedbackResponse, IFeedbackFormInput>({
      query(body) {
        const { newId, obtain_marks } = body;
        console.log(body);
        return {
          url: `evaluation/assign-marks/${newId}/`,
          method: 'PATCH',
          body: { obtain_marks }
        };
      },
      invalidatesTags: [
        { type: 'listTraineeEvaluationCriteria', id: 'PARTIAL-LIST' },
        { type: 'listTraineeMarkCriteria', id: 'PARTIAL-LIST' },
        { type: 'AssignTraineeMarks', id: 'PARTIAL-LIST' }
      ]
    })
  })
});

export const {
  useListTraineeMarkQuery,
  useCreateTraineeMarksMutation,
  useUpdateTraineeMarksMutation
} = assignTraineeMarksApi;
// new evaluation criteria api is made because it it used by trainee evaluation criterai detail and feedback/evaluation -conifguration

export interface IEvaluationCriteriaResponse extends IEvaluationCriteriaFormInput {
  evaluation_criteria_id: string;
  evaluation_criteria_details: {
    evaluation_criteria_detail_id: number;
    name: string;
  };
  evaluation_criteria_Detail: IEvaluationCriteriaDetails;
}
export interface IEvaluationCriteriaDetails {
  id?: number;
  evaluation_criteria_detail?: any;
  criteria?: string;
  parentCriteria?: string;
  max_marks?: number;
  routine?: number;

  routine_evaluation?: any;
}
export interface IEvaluationCriteriaFormInput {
  id?: number;
  // evaluation_criteria_detail?: number;
  evaluation_criteria_details?: {
    evaluation_criteria_detail_id: number;
    name: string;
  };
  evaluation_criteria_detail: any;
  parentCriteria: string;
  criteria: string;
  max_marks: number;
  routine: number;
  routine_evaluation: any;
}
export const traineeEvaluationCriteriaApi: { [key: string]: any } = createApi({
  reducerPath: 'traineeEvaluationCriteriaApi',
  baseQuery: baseQuery,
  tagTypes: [
    'TraineeEvaluationCriteriaApi',
    'listTraineeEvaluationCriteria',
    'listTraineeMarkCriteria'
  ],
  endpoints: (build) => ({
    listTraineeEvaluationCriteria: build.query<
      Response<PaginatedDataResponse<IEvaluationCriteriaResponse[]>>,
      IPageQueryParams | null
    >({
      query: (ids) =>
        `/evaluation/evaluation-criteria/routine/${ids?.id}/?trainee=${
          ids?.isHover !== undefined ? `${ids?.isHover}` : ''
        }`,
      providesTags: (result: any, error, page) => [
        { type: 'listTraineeEvaluationCriteria', id: 'PARTIAL-LIST' }
      ]
    }),
    //  { type: 'listTraineeEvaluationCriteria', id: 'PARTIAL-LIST' },
    // { type: 'listTraineeMarkCriteria', id: 'PARTIAL-LIST' },
    listTraineeMarkCriteria: build.query<
      Response<PaginatedDataResponse<any>>,
      IPageQueryParams | null
    >({
      query: (ids) =>
        `/evaluation/criteria/totalmarks/routine/${ids?.id}/?${
          ids?.isHover !== undefined ? `trainee=${ids?.isHover}` : ''
        }`,
      providesTags: (result: any, error, page) => [
        { type: 'listTraineeMarkCriteria', id: 'PARTIAL-LIST' }
      ]
    }),

    listTraineeEvaluationCriteria2: build.mutation<IEvaluationCriteriaResponse[], IPageQueryParams>(
      {
        query(ids) {
          return {
            url: `/evaluation/evaluation-criteria/routine/${ids?.id}?trainee=${ids?.isHover}`,
            method: 'POST'
          };
        },
        invalidatesTags: [{ type: 'TraineeEvaluationCriteriaApi', id: 'PARTIAL-LIST' }]
      }
    )
  })
});
export const {
  useListTraineeEvaluationCriteriaQuery,
  useListTraineeMarkCriteriaQuery,
  useListTraineeEvaluationCriteria2Mutation
} = traineeEvaluationCriteriaApi;
