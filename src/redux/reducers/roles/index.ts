// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';
interface IRolesResponse extends IRolesInput {
  created_by: number;
}

export interface IRolesInput {
  id?: number;
  name: string;
  desc: string;
  routine_role: string;
  menus?: [];
}

export interface IRolePermissionUpdateInput {
  role_id: number;
  modules: any;
  can_view: number;
  can_add: number;
  can_update: number;
  can_delete: number;
}

export interface IRoleDetailInput {
  role_id: number;
}

export enum ROLE_ASSIGNEE_ACTION {
  REMOVE = 'REMOVE',
  ASSIGN = 'ADD'
}
export interface IAssignRoleToUserInput {
  role_id: number;
  user_ids: Array<number>;
  action: ROLE_ASSIGNEE_ACTION;
}

export const rolesApi: { [key: string]: any } = createApi({
  reducerPath: 'rolesApi',
  baseQuery: baseQuery,
  tagTypes: ['roles', 'Resources'],
  endpoints: (build) => ({
    listRoles: build.query<Response<IRolesResponse[]>, void>({
      query: () => `role-permission/role/list/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ id }) => ({
                type: 'roles' as const,
                id
              })),
              { type: 'roles', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'roles', id: 'PARTIAL-LIST' }]
    }),
    getRoleDetails: build.mutation<IRolesResponse, IRoleDetailInput>({
      query(body) {
        return {
          url: `role-permission/role/list/`,
          method: 'GET',
          body
        };
      },
      invalidatesTags: [{ type: 'roles', id: 'PARTIAL-LIST' }]
    }),
    getRolePermission: build.query<any, any>({
      query: (id) => `role-permission/role/detail/${id}/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.menus.map(({ id }: { id: any }) => ({
                type: 'roles' as const,
                id
              })),
              { type: 'roles', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'roles', id: 'PARTIAL-LIST' }]
    }),
    createRoles: build.mutation<IRolesResponse, IRolesInput>({
      query(body) {
        return {
          url: `role-permission/role/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'roles', id: 'PARTIAL-LIST' }]
    }),

    updateRole: build.mutation<IRolesResponse, IRolesInput>({
      query(body) {
        return {
          url: `role-permission/role/update/${body.id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'roles', id: 'PARTIAL-LIST' }]
    }),
    assignRoleToUser: build.mutation<IRolesResponse, IAssignRoleToUserInput>({
      query(body) {
        return {
          url: `role-permission/role/assign-or-remove-user/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'roles', id: 'PARTIAL-LIST' }]
    }),

    updateRolePermission: build.mutation<IRolesResponse, IRolePermissionUpdateInput>({
      query(body) {
        return {
          url: `role-permission/permission/update/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'roles', id: 'PARTIAL-LIST' }]
    }),
    deleteRole: build.mutation<IRolesResponse, string>({
      query(id) {
        return {
          url: `role-permission/role/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'roles', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListRolesQuery,
  useCreateRolesMutation,
  useUpdateRoleMutation,
  useDeleteRoleMutation,
  useUpdateRolePermissionMutation,
  useGetRoleDetailsMutation,
  useGetRolePermissionQuery,
  useAssignRoleToUserMutation
} = rolesApi;

//list modules

export interface IModule {
  id: number;
  code: string;
  order_id: number;
  permissions: [];
}
export interface IPageQueryParams {
  roleID?: number | void;
}
export const moduleApi: { [key: string]: any } = createApi({
  reducerPath: 'moduleApi',
  baseQuery: baseQuery,
  tagTypes: ['ModuleApi'],
  endpoints: (build) => ({
    listModule: build.query<Response<IModule[]>, IPageQueryParams | null>({
      query: (roleID) => `role-permission/module/list/${roleID}`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ id }) => ({
                type: 'ModuleApi' as const,
                id
              })),
              { type: 'ModuleApi', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'ModuleApi', id: 'PARTIAL-LIST' }]
    })
  })
});

export const { useListModuleQuery } = moduleApi;
