// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';

export interface IOrganizationResponse extends IOrganizationInput {}

export interface IOrganizationInput {
  tms_organization_id?: number;
  name: string;
  location: string;
  phone_no: string;
  email: string;
  status: boolean;
}
export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
}

export const organizationApi: { [key: string]: any } = createApi({
  reducerPath: 'organizationApi',
  baseQuery: baseQuery,
  tagTypes: ['Organization'],
  endpoints: (build) => ({
    listOrganization: build.query<Response<IOrganizationResponse[]>, void>({
      query: () => `organization/`,

      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ tms_organization_id }) => ({
                type: 'Organization' as const,
                id: tms_organization_id
              })),
              { type: 'Organization', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Organization', id: 'PARTIAL-LIST' }]
    }),
    createOrganization: build.mutation<IOrganizationResponse, IOrganizationInput>({
      query(body) {
        return {
          url: `organization/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'Organization', id: 'PARTIAL-LIST' }]
    }),

    updateOrganization: build.mutation<IOrganizationResponse, IOrganizationInput>({
      query(body) {
        const { tms_organization_id } = body;
        return {
          url: `organization/update/${tms_organization_id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'Organization', id: 'PARTIAL-LIST' }]
    }),
    deleteOrganization: build.mutation<IOrganizationResponse, string>({
      query(id) {
        return {
          url: `organization/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'Organization', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListOrganizationQuery,
  useCreateOrganizationMutation,
  useUpdateOrganizationMutation,
  useDeleteOrganizationMutation
} = organizationApi;
