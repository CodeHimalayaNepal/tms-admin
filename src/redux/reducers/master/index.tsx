import { createApi } from '@reduxjs/toolkit/dist/query/react';
import { IServiceGroup } from 'pages/private/MasterData/ServiceGroup/Schema';
import { IServiceSubGroup } from 'pages/private/MasterData/ServiceSubGroup/Schema';
import { baseApiUrlv2, baseQuery } from '../settings';

interface IServiceGroupResponse {
  id: string;
  name: string;
  status: boolean;
}
interface SuccessResponse<T> {
  status: string;
  statusCode: number;
  data: T;
  message: string;
}

interface ResultResponse<T> {
  count: number;
  next: string;
  results: T;
  previous: string | null;
}
interface IPageQueryParams {
  page: number;
  pageSize: number;
}
interface IServiceSubGroupResponse {
  id: string;
  name: string;
  service_group_master: IServiceGroupMaster;
}
interface IServiceGroupMaster {
  id: string;
  name: string;
}
interface ServiceSubSuccessResponse<T> {
  message: string;
  data: T;
  status: string;
}

export const masterApi = createApi({
  reducerPath: 'masterApi',
  baseQuery: baseQuery,
  tagTypes: ['Master', 'Service', 'ServiceSub'],
  endpoints: (build) => ({
    listService: build.query<IServiceGroupResponse[], void>({
      query: () => `users/service-group/`,
      providesTags: [{ type: 'Service', id: 'SERVICEID' }]
    }),
    createService: build.mutation<
      SuccessResponse<IServiceGroupResponse>,
      Omit<IServiceGroupResponse, 'id'>
    >({
      query(body) {
        return {
          url: `users/service-group/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'Service', id: 'SERVICEID' }]
    }),
    deleteService: build.mutation<SuccessResponse<never>, number>({
      query(id) {
        return {
          url: `users/service-group/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'Service', id: 'SERVICEID' }]
    }),
    updateService: build.mutation<SuccessResponse<IServiceGroupResponse>, IServiceGroup>({
      query(body) {
        const { id } = body;
        return {
          url: `users/service-group/update/${id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'Service', id: 'SERVICEID' }]
    }),
    listServiceSubGroup: build.query<ServiceSubSuccessResponse<IServiceSubGroupResponse[]>, void>({
      query: () => baseApiUrlv2 + `users/service-sub-group/`,
      providesTags: [{ type: 'ServiceSub', id: 'SERVICEIDSUB' }]
    }),
    getServiceGroupById: build.query<SuccessResponse<any>, string>({
      query: (id) => `users/service-group/${id}/`,
      providesTags: [{ type: 'Master', id: 'LIST' }]
    }),
    createServiceSubGroup: build.mutation<
      ServiceSubSuccessResponse<IServiceSubGroupResponse>,
      Omit<IServiceSubGroup, 'id'>
    >({
      query(body) {
        return {
          url: baseApiUrlv2 + `users/service-sub-group/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'ServiceSub', id: 'SERVICEIDSUB' }]
    }),
    updateServiceSubGroup: build.mutation<
      ServiceSubSuccessResponse<IServiceSubGroupResponse>,
      IServiceSubGroup
    >({
      query(body) {
        const { id } = body;
        return {
          url: baseApiUrlv2 + `users/service-sub-group/update/${id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'ServiceSub', id: 'SERVICEIDSUB' }]
    }),
    deleteServiceSubGroup: build.mutation<ServiceSubSuccessResponse<never>, number>({
      query(id) {
        return {
          url: baseApiUrlv2 + `users/service-sub-group/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'ServiceSub', id: 'SERVICEIDSUB' }]
    }),
    listEthnicity: build.query<ResultResponse<IServiceGroupResponse[]>, IPageQueryParams>({
      query: (paging) => `users/ethnicity/?page=${paging?.page}&?pageSize=${paging?.pageSize}`,
      providesTags: [{ type: 'Master', id: 'LIST' }]
    }),
    getEthnicityById: build.query<SuccessResponse<any>, string>({
      query: (id) => `users/ethnicity/${id}/`,
      providesTags: [{ type: 'Master', id: 'LIST' }]
    }),
    createEthnicity: build.mutation<
      ResultResponse<IServiceGroupResponse>,
      Omit<IServiceGroupResponse, 'id'>
    >({
      query(body) {
        return {
          url: `users/ethnicity/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'Master', id: 'LIST' }]
    }),
    updateEthnicity: build.mutation<ResultResponse<IServiceGroupResponse>, IServiceGroup>({
      query(body) {
        const { id } = body;
        return {
          url: `users/ethnicity/update/${id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'Master', id: 'LIST' }]
    }),
    deleteEthnicity: build.mutation<ResultResponse<never>, number>({
      query(id) {
        return {
          url: `users/ethnicity/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'Master', id: 'LIST' }]
    })
  })
});

export const {
  useListServiceQuery,
  useCreateServiceMutation,
  useDeleteServiceMutation,
  useUpdateServiceMutation,
  useListServiceSubGroupQuery,
  useCreateServiceSubGroupMutation,
  useUpdateServiceSubGroupMutation,
  useDeleteServiceSubGroupMutation,
  useListEthnicityQuery,
  useCreateEthnicityMutation,
  useUpdateEthnicityMutation,
  useDeleteEthnicityMutation,
  useGetEthnicityByIdQuery,
  useGetServiceGroupByIdQuery
} = masterApi;
