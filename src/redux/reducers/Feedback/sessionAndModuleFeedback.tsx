import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';

export interface RoutineSession {
  id: number;
  routine: number;
  session_name: string;
  start_time: string;
  end_time: string;
}
export interface IRoutineSessionFeedback {
  id: number;
  routine_session: RoutineSession;
  routine_date: string;
  altered_start_time: string;
  altered_end_time: string;
  course_topic: number;
  feedback_enabled: boolean;
}

export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
  routineId: number;
}

export interface IQueryParams {
  routineId: number;
  sessionId: number;
  moduleId: number;
}
export interface IFeedbackData {
  feedback_point: number;
  count: number;
}
export interface IFeedbackResponse {
  feedback_question_id: number;
  question: string;
  question_np: string;
  question_type: string;
  feedback_data: {
    feedback_data: IFeedbackData[];
  };
}

export const routinewiseSessionFeedback: { [key: string]: any } = createApi({
  reducerPath: 'routinewiseSessionFeedback',
  baseQuery: baseQuery,
  tagTypes: ['TrainingFeedback', 'FeedbackSession', 'FeedbackModule', 'FeedbackSessionCount'],
  endpoints: (build) => ({
    routineWiseSessionFeedback: build.query<
      Response<PaginatedDataResponse<IRoutineSessionFeedback[]>>,
      IPageQueryParams | null
    >({
      query: (paging) =>
        `training/routine/${paging?.routineId}/feedback/dailysession/?page=${paging?.page}&page_size=${paging?.pageSize}`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ id }) => ({
                type: 'FeedbackSession' as const,
                id: id
              })),
              { type: 'FeedbackSession', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'FeedbackSession', id: 'PARTIAL-LIST' }]
    }),
    trainingFeedbackTableData: build.query({
      query: (routineId) => `training/admin/training_feedback/?routine_id=${routineId}`
    }),
    lunchFeedbackTableData: build.query({
      query: (routineId) => `training/admin/lunch_feedback/routine/${routineId}/`
    }),
    routineWiseSessionFeedbackCount: build.query<
      Response<IFeedbackResponse[]>,
      IQueryParams | null
    >({
      query: (queryParam) =>
        `training/routine/${queryParam?.routineId}/sessions/${queryParam?.sessionId}/feedback/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ feedback_question_id }) => ({
                type: 'FeedbackSessionCount' as const,
                id: feedback_question_id
              })),
              { type: 'FeedbackSessionCount', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'FeedbackSessionCount', id: 'PARTIAL-LIST' }]
    }),
    trainingFeedbackResult: build.query({
      query: (queryParam) =>
        `training/routine/${queryParam?.routineId}/training_id/${queryParam?.trainingId}/`
    }),
    lunchFeedbackResult: build.query({
      query: (queryParam) =>
        `training/lunch_feedback/routine/${queryParam?.routineId}/?feedback_date=${queryParam?.date}`
    }),
    routineWiseModuleFeedback: build.query<
      Response<PaginatedDataResponse<IRoutineSessionFeedback[]>>,
      IPageQueryParams | null
    >({
      query: (paging) =>
        `training/routine/${paging?.routineId}/feedback/module/?page=${paging?.page}&page_size=${paging?.pageSize}`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ id }) => ({
                type: 'FeedbackModule' as const,
                id: id
              })),
              { type: 'FeedbackModule', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'FeedbackModule', id: 'PARTIAL-LIST' }]
    }),
    routineWiseModuleFeedbackCount: build.query<Response<IFeedbackResponse[]>, IQueryParams | null>(
      {
        query: (queryParam) =>
          `training/routine/${queryParam?.routineId}/module/${queryParam?.moduleId}/feedback/`,
        providesTags: (result, error, page) =>
          result
            ? [
                ...result.data.map(({ feedback_question_id }) => ({
                  type: 'FeedbackSessionCount' as const,
                  id: feedback_question_id
                })),
                { type: 'FeedbackSessionCount', id: 'PARTIAL-LIST' }
              ]
            : [{ type: 'FeedbackSessionCount', id: 'PARTIAL-LIST' }]
      }
    )
  })
});

export const {
  useRoutineWiseModuleFeedbackCountQuery,
  useRoutineWiseSessionFeedbackQuery,
  useRoutineWiseModuleFeedbackQuery,
  useRoutineWiseSessionFeedbackCountQuery,
  useTrainingFeedbackTableDataQuery,
  useTrainingFeedbackResultQuery,
  useLunchFeedbackTableDataQuery,
  useLunchFeedbackResultQuery
} = routinewiseSessionFeedback;
