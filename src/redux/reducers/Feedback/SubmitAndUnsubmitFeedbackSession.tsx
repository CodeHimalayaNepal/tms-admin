import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';

export interface RoutineSession {
  id: number;
  routine: number;
  session_name: string;
  start_time: string;
  end_time: string;
}
export interface IRoutineSessionFeedback {
  id: number;
  routine_session: RoutineSession;
  routine_date: string;
  altered_start_time: string;
  altered_end_time: string;
  course_topic: number;
  feedback_enabled: boolean;
}

export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
  routineId: number;
  sessionId: number;
}

export interface IQueryParams {
  routineId: number;
  sessionId: number;
}

export const submitFeedback: { [key: string]: any } = createApi({
  reducerPath: 'submittedFeedback',
  baseQuery: baseQuery,
  tagTypes: ['FeedbackSession', 'FeedbackModule', 'FeedbackSessionCount'],
  endpoints: (build) => ({
    submittedFeedback: build.query<
      Response<PaginatedDataResponse<IRoutineSessionFeedback[]>>,
      IPageQueryParams | null
    >({
      query: (paging) =>
        `training/routine/${paging?.routineId}/sessions/${paging?.sessionId}/feedback/users/?submitted=true&page=${paging?.page}&page_size=${paging?.pageSize}`,
      providesTags: (result, error, page) => {
        return result
          ? [
              ...result.data.results.map(({ id }) => ({
                type: 'FeedbackSession' as const,
                id: id
              })),
              { type: 'FeedbackSession', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'FeedbackSession', id: 'PARTIAL-LIST' }];
      }
    }),
    trainingSubmittedFeedback: build.query({
      query: (paging) =>
        `training/routine/${paging.routineId}/training/${paging.trainingId}/feedback/users/?submitted=true`
    }),
    trainingUnSubmittedFeedback: build.query({
      query: (paging) =>
        `training/routine/${paging.routineId}/training/${paging.trainingId}/feedback/users/?submitted=false`
    }),
    lunchSubmittedFeedback: build.query({
      query: (paging) =>
        `training/routine/${paging.routineId}/lunch_feedback/users/?submitted=true&feedback_date=${paging.date}`
    }),
    lunchUnSubmittedFeedback: build.query({
      query: (paging) =>
        `training/routine/${paging.routineId}/lunch_feedback/users/?submitted=false&feedback_date=${paging.date}`
    }),
    unsubmittedFeedback: build.query<
      Response<PaginatedDataResponse<IRoutineSessionFeedback[]>>,
      IPageQueryParams | null
    >({
      query: (paging) =>
        `training/routine/${paging?.routineId}/sessions/${paging?.sessionId}/feedback/users/?submitted=false&page=${paging?.page}&page_size=${paging?.pageSize}`,
      providesTags: (result, error, page) => {
        return result
          ? [
              ...result.data.results.map(({ id }) => ({
                type: 'FeedbackModule' as const,
                id: id
              })),
              { type: 'FeedbackModule', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'FeedbackModule', id: 'PARTIAL-LIST' }];
      }
    })
  })
});

export const {
  useSubmittedFeedbackQuery,
  useUnsubmittedFeedbackQuery,
  useTrainingSubmittedFeedbackQuery,
  useTrainingUnSubmittedFeedbackQuery,
  useLunchSubmittedFeedbackQuery,
  useLunchUnSubmittedFeedbackQuery
} = submitFeedback;
