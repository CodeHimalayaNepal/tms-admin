import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';

export interface RoutineSession {
  id: number;
  routine: number;
  session_name: string;
  start_time: string;
  end_time: string;
}
export interface IRoutineSessionFeedback {
  id: number;
  routine_session: RoutineSession;
  routine_date: string;
  altered_start_time: string;
  altered_end_time: string;
  course_topic: number;
  feedback_enabled: boolean;
}

export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
  routineId: number;
  module_id: number;
}

export interface IQueryParams {
  routineId: number;
  sessionId: number;
}

export const submitFeedbackModule: { [key: string]: any } = createApi({
  reducerPath: 'submittedFeedbackModule',
  baseQuery: baseQuery,
  tagTypes: ['FeedbackSession', 'FeedbackModule', 'FeedbackSessionCount'],
  endpoints: (build) => ({
    submittedFeedbackModule: build.query<
      Response<PaginatedDataResponse<IRoutineSessionFeedback[]>>,
      IPageQueryParams | null
    >({
      query: (paging) =>
        `training/routine/${paging?.routineId}/module/${paging?.module_id}/feedback/users/?submitted=true`,
      providesTags: (result, error, page) => {
        return result
          ? [
              ...result.data.results.map(({ id }) => ({
                type: 'FeedbackSession' as const,
                id: id
              })),
              { type: 'FeedbackSession', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'FeedbackSession', id: 'PARTIAL-LIST' }];
      }
    }),
    unsubmittedFeedbackModule: build.query<
      Response<PaginatedDataResponse<IRoutineSessionFeedback[]>>,
      IPageQueryParams | null
    >({
      query: (paging) =>
        `training/routine/${paging?.routineId}/module/${paging?.module_id}/feedback/users/?submitted=false`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ id }) => ({
                type: 'FeedbackModule' as const,
                id: id
              })),
              { type: 'FeedbackModule', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'FeedbackModule', id: 'PARTIAL-LIST' }]
    })
  })
});

export const { useSubmittedFeedbackModuleQuery, useUnsubmittedFeedbackModuleQuery } =
  submitFeedbackModule;
