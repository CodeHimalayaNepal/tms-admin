import { createApi, CreateApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';

export interface IFeedbackResponse extends IFeedbackFormInput {
  feedback_detail_id: string;
  feedback_detail: IFeedbackDetails;
}

export interface IFeedbackFormInput {
  tms_feedback_question_id?: number;
  question: string;
  question_np: string;
  question_type: string;
}

export interface IFeedbackDetails {
  tms_feedback_question_id?: number;
  question: string;
  question_np?: string;
}

export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
}
export const feedbackApi: { [key: string]: any } = createApi({
  reducerPath: 'feedbackApi',
  baseQuery: baseQuery,
  tagTypes: ['Feedback'],
  endpoints: (build) => ({
    listFeedback: build.query<
      Response<PaginatedDataResponse<IFeedbackResponse[]>>,
      IPageQueryParams | null
    >({
      query: (paging) => `training/feedback_question/?page=${paging?.page}`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ tms_feedback_question_id }) => ({
                type: 'Feedback' as const,
                id: tms_feedback_question_id
              })),
              { type: 'Feedback', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Feedback', id: 'PARTIAL-LIST' }]
    }),
    createFeedback: build.mutation<IFeedbackResponse, IFeedbackFormInput>({
      query(body) {
        return {
          url: `training/feedback_question/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'Feedback', id: 'PARTIAL-LIST' }]
    }),

    updateFeedback: build.mutation<IFeedbackResponse, IFeedbackFormInput>({
      query(body) {
        const { tms_feedback_question_id } = body;
        return {
          url: `training/feedback_question/update/${tms_feedback_question_id}`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'Feedback', id: 'PARTIAL-LIST' }]
    }),
    deleteFeedback: build.mutation<IFeedbackResponse, string>({
      query(tms_feedback_question_id) {
        return {
          url: `training/feedback_question/delete/${tms_feedback_question_id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'Feedback', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListFeedbackQuery,
  useCreateFeedbackMutation,
  useUpdateFeedbackMutation,
  useDeleteFeedbackMutation
} = feedbackApi;
