// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';

import { CoursesResponse, IPageQueryParams } from '../courses';
import { ITrainingTypeResponse } from '../training-type';

interface ITrainingResponse extends ITrainingInput {
  created_at: string;
  updated_at: string;
  course_details: CoursesResponse;
  training_type_details: ITrainingTypeResponse;
  training_code: string;
}

export interface ITrainingInput {
  training_name: string;
  training_description: string;
  training_code: string;
  center: number;
  courses: string[];
  training_id?: number;
  image?: File[];
  id: number;
}

export const trainingApi: { [key: string]: any } = createApi({
  reducerPath: 'trainingApi',
  baseQuery: baseQuery,
  tagTypes: ['Training', 'TrainingCourseTopics'],
  endpoints: (build) => ({
    listTraining: build.query<
      Response<PaginatedDataResponse<ITrainingInput[]>>,
      IPageQueryParams | null
    >({
      query: () => `/training/`,
      transformResponse: (res) => res.data,
      providesTags: (result, error, page) => [{ type: 'Training', id: 'PARTIAL-LIST' }]
    }),
    listTrainingById: build.query<PaginatedDataResponse<ITrainingInput[]>, string>({
      query: (id) => `training/detail/${id}/`
    }),
    createTraining: build.mutation<ITrainingResponse, ITrainingInput>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof ITrainingInput];
          if (value instanceof File) {
            formData.append(data, value);
          } else if (value instanceof Array) {
            value.forEach((v) => {
              formData.append(data, v?.toString());
            });
          } else {
            formData?.append(data, value?.toString() ?? '');
          }
        }

        return {
          url: `training/create/`,
          method: 'POST',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'Training', id: 'PARTIAL-LIST' }]
    }),

    updateTraining: build.mutation<ITrainingResponse, ITrainingInput>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof ITrainingInput];
          if (value instanceof File) {
            formData.append(data, value);
          } else if (value instanceof Array) {
            value.forEach((v) => {
              formData.append(data, v?.toString());
            });
          } else {
            formData?.append(data, value?.toString() ?? '');
          }
        }
        return {
          url: `training/update/${body.id}/`,
          method: 'PATCH',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'Training', id: 'PARTIAL-LIST' }]
    }),
    deleteTraining: build.mutation<ITrainingResponse, string>({
      query(id) {
        return {
          url: `training/delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'Training', id: 'PARTIAL-LIST' }]
    }),

    listTrainingCourseTopics: build.query<Response<any[]>, string>({
      query: (trainingId) => `training/course-topics/${trainingId}/`,
      transformResponse: (res) => res.data,
      providesTags: () => [{ type: 'TrainingCourseTopics', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListTrainingQuery,
  useListTrainingByIdQuery,
  useCreateTrainingMutation,
  useUpdateTrainingMutation,
  useDeleteTrainingMutation,
  useListTrainingCourseTopicsQuery
} = trainingApi;
