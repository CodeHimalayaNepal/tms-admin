// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';

export interface IEvaluationResponse extends IEvaluationFormInuput {
  evaluation_criteria_detail_id: string;
  name: string;
  evaluation_criteria_detail: IEvaluationCriteriaDetails;
}

export interface IEvaluationCriteriaDetails {
  evaluation_criteria_id?: number;
  name?: string;
  code?: string;
  parent_detail: IParentDetail;
}
export interface IParentDetail {
  parent_id?: number;
  name?: string;
  code?: string;
}

export interface IEvaluationFormInuput {
  evaluation_criteria_id?: number;
  name: string;
  code: string;
  parent?: number;
}
export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
}
export const evaluationCriteriaApi: { [key: string]: any } = createApi({
  reducerPath: 'evaluationCriteriaApi',
  baseQuery: baseQuery,
  tagTypes: ['EvaluationCriteria'],
  endpoints: (build) => ({
    listEvaluationCriteria: build.query<
      Response<PaginatedDataResponse<IEvaluationResponse[]>>,
      IPageQueryParams | null
    >({
      query: (searchQuery) => `evaluation/criteria/?page=1&page_size=100&search=${searchQuery}`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ evaluation_criteria_detail_id }) => ({
                type: 'EvaluationCriteria' as const,
                id: evaluation_criteria_detail_id
              })),
              { type: 'EvaluationCriteria', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'EvaluationCriteria', id: 'PARTIAL-LIST' }]
    }),

    createEvaluationCriteria: build.mutation<IEvaluationResponse, IEvaluationFormInuput>({
      query(body) {
        return {
          url: `evaluation/criteria/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'EvaluationCriteria', id: 'PARTIAL-LIST' }]
    }),

    updateEvaluationCriteria: build.mutation<IEvaluationResponse, IEvaluationFormInuput>({
      query(body) {
        const { evaluation_criteria_id } = body;
        return {
          url: `evaluation/criteria/${evaluation_criteria_id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'EvaluationCriteria', id: 'PARTIAL-LIST' }]
    }),
    deleteEvaluationCriteria: build.mutation<IEvaluationResponse, string>({
      query(evaluation_criteria_id) {
        return {
          url: `evaluation/criteria/${evaluation_criteria_id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'EvaluationCriteria', id: 'PARTIAL-LIST' }]
    }),
    listParentDetails: build.query<Response<IEvaluationResponse[]>, void>({
      query: () => `evaluation/parent/list/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ evaluation_criteria_id }) => ({
                type: 'EvaluationCriteria' as const,
                id: evaluation_criteria_id
              })),
              { type: 'EvaluationCriteria', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'EvaluationCriteria', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListEvaluationCriteriaQuery,
  useListParentDetailsQuery,
  useCreateEvaluationCriteriaMutation,
  useUpdateEvaluationCriteriaMutation,
  useDeleteEvaluationCriteriaMutation
} = evaluationCriteriaApi;

//////////evaluation criteria after routine management-congiguration table process

export interface IEvaluationCriteriaFormInput {
  evaluation_criteria_Id?: number;
  parentCriteria?: number;
  criteria: number;
  details: number;
  evaluator: number;
  marks: number;
}
