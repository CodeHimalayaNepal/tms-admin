// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { baseQuery } from 'redux/reducers/settings';

export interface IFormTypeResponse extends IFormTypeInput {}

export interface IFormTypeInput {
  id?: number;
  title: string;
  is_required: boolean;
}

export const formTypeApi: { [key: string]: any } = createApi({
  reducerPath: 'formTypeApi',
  baseQuery: baseQuery,
  tagTypes: ['FormType'],
  endpoints: (build) => ({
    listFormType: build.query<Response<IFormTypeResponse[]>, void>({
      query: () => `users/fields-type-list-create`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ id }) => ({
                type: 'FormType' as const,
                id
              })),
              { type: 'FormType', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'FormType', id: 'PARTIAL-LIST' }]
    }),
    createFormType: build.mutation<IFormTypeResponse, IFormTypeInput>({
      query(body) {
        return {
          url: `users/fields-type-list-create`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'FormType', id: 'PARTIAL-LIST' }]
    }),

    updateFormType: build.mutation<IFormTypeResponse, IFormTypeInput>({
      query(body) {
        const { id } = body;
        return {
          url: `users/fields-type-retrive-update-delete/${id}`,
          method: 'PUT',
          body
        };
      },
      invalidatesTags: [{ type: 'FormType', id: 'PARTIAL-LIST' }]
    }),
    deleteFormType: build.mutation<IFormTypeResponse, string>({
      query(id) {
        return {
          url: `users/fields-type-retrive-update-delete/${id}`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'FormType', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListFormTypeQuery,
  useCreateFormTypeMutation,
  useUpdateFormTypeMutation,
  useDeleteFormTypeMutation
} = formTypeApi;
