// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { Response } from 'common/types';
import {
  IEducation,
  IExperience,
  IGeneral
} from 'pages/private/InternalUser/InternalUserEdit/Schema';
import { IInternalUserInput } from 'pages/private/InternalUser/schema';
import { baseQuery } from 'redux/reducers/settings';

export interface IInternalUserResponse extends IInternalUserInput {}
export interface IGeneralResponse extends IGeneral {}
export interface IEducationResponse extends IEducation {}
export interface IExperienceRespose extends IExperience {}

interface ISuccesResponse<T> {
  status: string;
  statusCode: number;
  data: T;
}
interface IUnblockUserResponse {
  is_locked: boolean;
  login_attempts: number;
}

export const internalUserApi: { [key: string]: any } = createApi({
  reducerPath: 'internalUserApi',
  baseQuery: baseQuery,
  tagTypes: ['InternalUser'],
  endpoints: (build) => ({
    listInternalUsers: build.query<Response<IInternalUserResponse[]>, void>({
      query: (search) => `users/admin/list/?search=${search}`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ id }) => ({
                type: 'InternalUser' as const,
                id: id
              })),
              { type: 'InternalUser', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'InternalUser', id: 'PARTIAL-LIST' }]
    }),
    listInternalUsersByType: build.query<Response<IInternalUserResponse[]>, string>({
      query: (type: 'module-coordinator' | 'faculties') =>
        `users/admin/list/${type !== undefined ? `?role_name=${type}` : ''}`,
      transformResponse: (res) => res.data,
      providesTags: (result, error, page) => [{ type: 'InternalUser', id: 'PARTIAL-LIST' }]
    }),
    listUser: build.query<Response<IInternalUserResponse[]>, null>({
      query: () => `users/admin/list/`,
      transformResponse: (res) => res.data,
      providesTags: (result, error, page) => [{ type: 'InternalUser', id: 'PARTIAL-LIST' }]
    }),
    listInternalUserDetails: build.query<Response<IGeneralResponse[]>, null>({
      query: (id) => `users/admin/internal-user/detail/${id}/`,
      transformResponse: (res) => res,
      providesTags: (result, error, page) => [{ type: 'InternalUser', id: 'PARTIAL-LIST' }]
    }),
    unBlockInternalUser: build.mutation<ISuccesResponse<IUnblockUserResponse>, { id: number }>({
      query({ id }) {
        return {
          url: `users/unblock-user/${id}/`,
          method: 'PATCH'
        };
      },
      invalidatesTags: [{ type: 'InternalUser', id: 'PARTIAL-LIST' }]
    }),
    createInternalUser: build.mutation<IInternalUserResponse, IInternalUserInput>({
      query(body) {
        return {
          url: `users/admin/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'InternalUser', id: 'PARTIAL-LIST' }]
    }),

    updateInternalUser: build.mutation<IInternalUserResponse, IInternalUserInput>({
      query(body) {
        const { id } = body;
        return {
          url: `users/admin/partial-update/${id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'InternalUser', id: 'PARTIAL-LIST' }]
    }),
    updateInternalUserEducation: build.mutation<IEducationResponse, IEducation>({
      query(body) {
        const { id, education } = body;
        return {
          url: `users/admin/internal-user/education/${id}/`,
          method: 'PATCH',
          body: education
        };
      },
      invalidatesTags: [{ type: 'InternalUser', id: 'PARTIAL-LIST' }]
    }),

    updateInternalUserExperience: build.mutation<IExperienceRespose, IExperience>({
      query(body) {
        const { id, inhouse_experience } = body;
        console.log('bodies', body);
        return {
          url: `users/admin/internal-user/inhouse-experience/${id}/`,
          method: 'PATCH',
          body: [...inhouse_experience]
        };
      },
      invalidatesTags: [{ type: 'InternalUser', id: 'PARTIAL-LIST' }]
    }),
    updateInternalUserGeneral: build.mutation<IGeneralResponse, IGeneral>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof IGeneral];
          if (value instanceof File) {
            formData.append(data, value);
          } else if (value instanceof Array) {
            value.forEach((v) => {
              formData.append(data, v.toString());
            });
          } else {
            formData?.append(data, value?.toString() ?? '');
          }
        }

        return {
          url: `users/update-internal-user/${body.id}/`,
          method: 'PATCH',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'InternalUser', id: 'PARTIAL-LIST' }]
    }),
    getAvailableUserList: build.query<any, any>({
      query: (id) =>
        `users/admin/available/list/?routine_session=${id.sessionId}&routine_date=${id.routineDate}`,
      transformResponse: (res) => res,
      providesTags: (result, error, page) => [{ type: 'InternalUser', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useGetAvailableUserListQuery,
  useListInternalUsersQuery,
  useListInternalUsersByTypeQuery,
  useCreateInternalUserMutation,
  useUpdateInternalUserMutation,
  useListUserQuery,
  useListInternalUserDetailsQuery,
  useUpdateInternalUserGeneralMutation,
  useUpdateInternalUserEducationMutation,
  useUpdateInternalUserExperienceMutation,
  useUnBlockInternalUserMutation
} = internalUserApi;

interface ISuccesResponse<T> {
  status: string;
  statusCode: number;
  data: T;
}
interface IUnblockUserResponse {
  is_locked: boolean;
  login_attempts: number;
}
