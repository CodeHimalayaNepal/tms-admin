// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse, PaginatedDataResponse, Response } from 'common/types';
import { STATUS } from 'common/enum';
import { buildSelectors } from '@reduxjs/toolkit/dist/query/core/buildSelectors';

export interface CoursesResponse extends ICoursesInput {
  created_by: number;
  code: string;
  course_id: number;
}

export interface ICoursesInput {
  tms_course_id?: string;
  title: string;
  description: string;
  duration_hr: number;
  code: string;
  publish?: boolean;
  category: number | null;
  course_files?: any;
  id: number;
  image?: any;
}
export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
}

export const coursesApi: { [key: string]: any } = createApi({
  reducerPath: 'coursesApi',
  baseQuery: baseQuery,
  tagTypes: ['Courses', 'Resources'],
  endpoints: (build) => ({
    listCourses: build.query<
      Response<PaginatedDataResponse<CoursesResponse[]>>,
      IPageQueryParams | null
    >({
      query: (paging) =>
        `training/course-list/${paging ? `?page=${paging.page}` : ``}&${
          paging ? `page_size=${paging.pageSize ? paging.pageSize : ''}` : ``
        }`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ course_id }) => ({
                type: 'Courses' as const,
                id: course_id
              })),
              { type: 'Courses', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Courses', id: 'PARTIAL-LIST' }]
    }),
    listCoursesByTraining: build.query<PaginatedDataResponse<ICoursesInput[]>, string>({
      query: (routineId) => `/training/routine-training-course/list/${routineId}/`
    }),
    listCoursesCategory: build.query<PaginatedDataResponse<ICoursesInput[]>, void>({
      query: () => `/training/course-category-list/`
    }),
    listCoursesEdit: build.query<PaginatedDataResponse<ICoursesInput[]>, void>({
      query: (course_id) => `/training/course-detail/${course_id}/`,
      providesTags: (result) =>
        result
          ? [{ type: 'Courses', id: 'PARTIAL-LIST' }]
          : [{ type: 'Courses', id: 'PARTIAL-LIST' }]
    }),
    createCourse: build.mutation<CoursesResponse, ICoursesInput>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof ICoursesInput];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData?.append(`${data}`, v);
              });
            }
          } else {
            formData?.append(
              data,
              value instanceof File
                ? value
                : value instanceof Object
                ? JSON.stringify(value)
                : value ?? ''
            );
          }
        }

        return {
          url: `training/course-create/`,
          method: 'POST',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'Courses', id: 'PARTIAL-LIST' }]
    }),

    updateCourse: build.mutation<CoursesResponse, ICoursesInput>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof ICoursesInput];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData?.append(`${data}`, v);
              });
            }
          } else {
            formData?.append(
              data,
              value instanceof File
                ? value
                : value instanceof Object
                ? JSON.stringify(value)
                : value ?? ''
            );
          }
        }
        return {
          url: `training/course-update/${body.id}/`,
          method: 'PATCH',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'Courses', id: 'PARTIAL-LIST' }]
    }),
    deleteCourse: build.mutation<CoursesResponse, any>({
      query(id) {
        return {
          url: `training/course-delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'Courses', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListCoursesQuery,
  useListCoursesByTrainingQuery,
  useCreateCourseMutation,
  useUpdateCourseMutation,
  useDeleteCourseMutation,
  useListCoursesCategoryQuery,
  UseListCoursesRoutineQuery,
  useListCoursesEditQuery
} = coursesApi;
