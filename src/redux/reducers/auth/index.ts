// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { BaseResponse, PaginatedDataResponse, Response } from 'common/types';
import { EndpointBuilder } from '@reduxjs/toolkit/dist/query/endpointDefinitions';

export interface LoginInterface {
  username: string;
  password: string;
}

export interface SignUpInterface {
  name: string;
  username: string;
  password: string;
  email: string;
  userId: number;
  // email: string;
  // password: string;
  // confirm_password: string;
  // first_name: string;
  // middle_name?: string;
  // last_name: string;
  // mobile_number: string;
  // citizenship_no: string;
  // dob: string;
  // gender: string;
}
export interface ResetInterface {
  password: string;
}

export interface AuthResponse {
  accessToken: string;
  refreshToken: string;
}

export interface SignUpResponse {
  id: number;
  name: string;
  username: string;
  email: string;
  isAccountDisabled: true;
  createdAt: string;
  updatedAt: string;
}
export enum ITraineeStatus {
  'Accepted' = 1,
  'Pending' = 2,
  'Rejected' = 3
}
export interface ITraineeDetail {
  first_name: string;
  middle_name: string;
  last_name: string;
  extra_fields: {};
  mobile_number: string;
  user_details: {
    id: number;
    email: string;
    status: ITraineeStatus;
  };
  image: null;
}
export interface IPasswordResetQueryParams {
  paramToken?: number | void;
  uid?: number | void;
}
export const authenticationApi: { [key: string]: any } = createApi({
  reducerPath: 'authenticationApi',
  baseQuery: baseQuery,
  tagTypes: ['Posts', 'traineeList'],
  endpoints: (build) => ({
    listTrainee: build.query<Response<SignUpResponse[]>, void>({
      query: (search) => `/users/trainee/view/${search === undefined ? '' : `?search=${search}`}`,
      providesTags: (result) =>
        result
          ? [{ type: 'traineeList', id: 'PARTIAL-LIST' }]
          : [{ type: 'traineeList', id: 'PARTIAL-LIST' }]
    }),
    unblockTrainee: build.mutation<ITraineeDetail, { id?: string }>({
      query({ id }) {
        return {
          url: `users/unblock-user/${id}/`,
          method: 'PATCH'
        };
      },
      invalidatesTags: [{ type: 'traineeList', id: 'PARTIAL-LIST' }]
    }),

    login: build.mutation<BaseResponse<AuthResponse>, LoginInterface>({
      query(body) {
        return {
          url: `users/userlogin/`,
          method: 'POST',
          body
        };
      }
    }),
    changePassword: build.mutation<
      BaseResponse<AuthResponse>,
      { old_password: string; new_password: string; confirm_password: string }
    >({
      query(body) {
        return {
          url: `/users/change-password/`,
          method: 'PATCH',
          body
        };
      }
    }),
    forgotPassword: build.mutation<BaseResponse<SignUpResponse>, SignUpInterface>({
      query(body) {
        return {
          url: `users/resend/verification-email/`,

          method: 'POST',
          body
        };
      }
    }),

    verifyOTP: build.mutation<BaseResponse<SignUpResponse>, SignUpInterface>({
      query(body) {
        return {
          url: `users/verify-otp/`,
          method: 'POST',
          body
        };
      }
    }),
    sendNewPasswordReset: build.mutation<BaseResponse<SignUpResponse>, SignUpInterface>({
      query(body) {
        return {
          url: `/users/request-reset-email/userID/1/`,
          method: 'POST',
          body
        };
      }
    }),
    sendResetTokenToEmail: build.mutation<BaseResponse<SignUpResponse>, ResetInterface>({
      query(body) {
        return {
          url: `users/password-reset-complete/`,
          method: 'PATCH',
          body
        };
      }
    }),

    signUp: build.mutation<BaseResponse<SignUpResponse>, SignUpInterface>({
      query(body) {
        return {
          url: `users/registration/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'Posts', id: 'PARTIAL-LIST' }]
    }),

    getPassResetToken: build.mutation<BaseResponse<any>, IPasswordResetQueryParams | null>({
      query: (paging) => ({
        url: `/users/password-reset/${paging?.uid}/${paging?.paramToken}/`,
        method: 'GET'
      })
    })
  })
});

export const {
  useLoginMutation,
  useSignUpMutation,
  useForgotPasswordMutation,
  useVerifyOTPMutation,
  useResendVerificationEmailMutation,
  useSendNewPasswordResetMutation,
  useListTraineeQuery,
  useUnblockTraineeMutation,
  useChangePasswordMutation,
  useSendResetTokenToEmailMutation,
  useGetPassResetTokenMutation
} = authenticationApi;
