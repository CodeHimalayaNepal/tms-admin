// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { baseQuery } from 'redux/reducers/settings';

export interface IRoutineEnrollmentResponse extends IRoutineEnrollment {
  routine_details: IRoutineDetail;
  trainee_details: ITrainingDetail;
}

export interface IRoutineDetail {
  id?: number;
  name: string;
}

export interface ITrainingDetail {
  id?: number;
  first_name: string;
  last_name: string;
}
export interface IRoutineEnrollment {
  id?: number;
  routine: number;
  trainee: number[];
  status: boolean;
}
export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
}

export const routineEnrollmentApi: { [key: string]: any } = createApi({
  reducerPath: 'routineEnrollmentApi',
  baseQuery: baseQuery,
  tagTypes: ['RoutineEnrollment', 'RoutineEnrollmentUser'],
  endpoints: (build) => ({
    listRoutineEnrollment: build.query<
      Response<PaginatedDataResponse<IRoutineEnrollmentResponse[]>>,
      { routineId: string }
    >({
      query: ({ routineId }) => `training/schedule/list/?routine=${routineId}`,
      providesTags: [{ type: 'RoutineEnrollmentUser', id: 'PARTIAL-LIST' }]
    }),

    enrollTraineeInRoutine: build.mutation({
      query(body) {
        return {
          url: `training/enrollment/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'RoutineEnrollmentUser', id: 'PARTIAL-LIST' }]
    }),
    createRoutineEnrollment: build.mutation<IRoutineEnrollmentResponse, IRoutineDetail>({
      query(body) {
        return {
          url: `routine/enrollment/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'RoutineEnrollment', id: 'PARTIAL-LIST' }]
    }),

    updateRoutineEnrollment: build.mutation<IRoutineEnrollmentResponse, IRoutineDetail>({
      query(body) {
        const { id } = body;
        return {
          url: `routine/enrollment/${id}`,
          method: 'PUT',
          body
        };
      },
      invalidatesTags: [{ type: 'RoutineEnrollment', id: 'PARTIAL-LIST' }]
    }),

    patchUpdateRoutineEnrollment: build.mutation<IRoutineEnrollmentResponse, IRoutineDetail>({
      query(body) {
        const { id } = body;
        return {
          url: `routine/enrollment/${id}`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'RoutineEnrollment', id: 'PARTIAL-LIST' }]
    }),
    deleteRoutineEnrollment: build.mutation<IRoutineEnrollmentResponse, string>({
      query(id) {
        return {
          url: `routine/enrollment/${id}`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'RoutineEnrollment', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListRoutineEnrollmentQuery,
  useCreateRoutineEnrollmentMutation,
  useUpdateRoutineEnrollmentMutation,
  usePatchUpdateRoutineEnrollmentMutation,
  useDeleteRoutineEnrollmentMutation,

  useEnrollTraineeInRoutineMutation
} = routineEnrollmentApi;
