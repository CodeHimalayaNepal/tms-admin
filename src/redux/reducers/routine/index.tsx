// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { baseApiUrl, baseApiUrlv2, baseQuery } from 'redux/reducers/settings';
import { ICoursesInput } from '../courses';
import { ICoordinatorDetail, IElevatorDetail, IHallDetail, ItrainingDetail } from './types';

export interface IRoutineResponse extends IRoutineDetail {
  training_details: ItrainingDetail;
  course_details: ICoursesInput[];
  hall_details: IHallDetail;
  coordinator_details: ICoordinatorDetail;
  evaluator_details: IElevatorDetail[];
  code: string;
}
export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
  routineID?: number | void;
  routineStatusValue?: string;
  training?: string;
}

//confusion with Typescript types for GET and POST requests

export interface IRoutineDetail {
  routine_master_id?: number;
  training: number;
  hall: number;
  batch_name: string;
  description: string;
  start_date: string;
  end_date: string;
  is_active: boolean;
  tr_routine_coordinator?: { coordinator: number; is_module_coordinator: boolean }[];
  tr_routine_coordinators?: { coordinator: number; is_module_coordinator: boolean }[];
  attendance_type: 'daily_session' | 'routine';
  attendance_checkin_time: string;
  attendance_checkout_time: string;
  buffer_time_before: string;
  buffer_time_after: string;
}

export interface IRoutineDetailRequest extends Omit<IRoutineDetail, 'tr_routine_coordinators'> {
  tr_routine_coordinators?: {
    [key: string]: { coordinator: number; is_module_coordinator: boolean };
  };
}

export interface IRoutineInfo {
  routine_master_id: number;
  training_details: TrainingDetails;
  hall_details: HallDetails;
  batch_name: string;
  start_date: string;
  end_date: string;
  description: string;
  is_active: boolean;
  tr_routine_coordinator: TrRoutineCoordinator[];
  attendance_checkin_time: string;
  attendance_checkout_time: string;
  attendance_type: 'routine' | 'daily_session';
  buffer_time_after: string;
  buffer_time_before: string;
}

export interface TrainingDetails {
  id: number;
  training_code: string;
  training_name: string;
  training_description: string;
  courses: any[];
}

export interface HallDetails {
  hall_id: number;
  organization_details: OrganizationDetails;
  hall_name: string;
  hall_name_np: string;
  hall_unique_key: string;
  status: boolean;
}

export interface OrganizationDetails {
  tms_organization_id: string;
  name: string;
  location: string;
  phone_no: string;
  email: string;
  status: boolean;
}

export interface TrRoutineCoordinator {
  routine_coordinator_id: number;
  coordinator_details: CoordinatorDetails;
  is_module_coordinator: boolean;
  course_details: {
    course_id: number;
    course_name: string;
  };
}

export interface CoordinatorDetails {
  coordinator_id: number;
  first_name: string;
  middle_name: any;
  last_name: string;
}

export interface IRoutineSession {
  routine: number;
  session_name: string;
  start_time: string;
  end_time: string;
}

export interface IRoutineSessionDetail {
  end_time: string;
  id: number;
  routine: number;
  session_name: string;
  start_time: string;
}

export interface IRoutineDailySession {
  daily_sessions: {
    id(id: any, arg1: string): unknown;
    altered_end_time: string;
    altered_start_time: string;
    course_topic: {
      id: string;
      title: string;
    };
    routine_date: string;
    lecturers: {
      first_name: string;
      id: number;
      last_name: string;
    }[];
  }[];
  end_time: string;
  id: number;
  session_name: string;
  start_time: string;
}

export const routineApi: { [key: string]: any } = createApi({
  reducerPath: 'routineApi',
  baseQuery: baseQuery,
  tagTypes: ['Routine', 'Session', 'DailySession', 'RoutineEnrollmentUser'],
  endpoints: (build) => ({
    listRoutine: build.query<
      Response<PaginatedDataResponse<IRoutineResponse[]>>,
      IPageQueryParams | null
    >({
      query: (paging) =>
        `training/routine-master/${
          paging
            ? `?page=${paging.page}&pageSize=${paging.pageSize}&routine_status=${paging.routineStatusValue}&training_id=${paging.training}`
            : ``
        }`,
      providesTags: (result, error, page) => {
        console.log(result);
        return result
          ? [
              ...result.results.data.map(
                ({ routine_master_id }: { routine_master_id: number }) => ({
                  type: 'Routine' as const,
                  id: routine_master_id
                })
              ),
              { type: 'Routine', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Routine', id: 'PARTIAL-LIST' }];
      }
    }),
    listRoutineById: build.query<PaginatedDataResponse<IRoutineResponse[]>, string>({
      query: (id) => baseApiUrlv2 + `training/routine-master/${id}/`,
      providesTags: (result, error, page) => {
        return [{ type: 'Routine', id: 'PARTIAL-LIST' }];
      }
    }),

    listCoordinatorRoutine: build.query<PaginatedDataResponse<IRoutineResponse[]>, void>({
      query: () => `routine/coordinatorInvolvedRoutineStatus`
    }),
    listTraineeRoutine: build.query<PaginatedDataResponse<IRoutineResponse[]>, void>({
      query: () => `routine/traineeInvolvedRoutineStatus`
    }),
    createRoutine: build.mutation<IRoutineResponse, IRoutineDetail>({
      query(body) {
        return {
          url: baseApiUrlv2 + `training/routine-master/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'Routine', id: 'PARTIAL-LIST' }]
    }),

    updateRoutine: build.mutation<IRoutineResponse, IRoutineDetail>({
      query(body) {
        const { routine_master_id } = body;
        return {
          url: `routine/update/${routine_master_id}/`,
          method: 'PUT',
          body
        };
      },
      invalidatesTags: [{ type: 'Routine', id: 'PARTIAL-LIST' }]
    }),

    patchUpdateRoutine: build.mutation<IRoutineResponse, IRoutineDetail>({
      query(body) {
        const { routine_master_id } = body;
        return {
          url: baseApiUrlv2 + `training/routine-master/${routine_master_id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'Routine', id: 'PARTIAL-LIST' }]
    }),
    deleteRoutine: build.mutation<IRoutineResponse, string>({
      query(routine_master_id) {
        return {
          url: `training/routine-master/${routine_master_id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'Routine', id: 'PARTIAL-LIST' }]
    }),

    createRoutineSession: build.mutation<any, IRoutineSession>({
      query(body) {
        return {
          url: `training/routine-session/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'Session', id: 'PARTIAL-LIST' }]
    }),
    listRoutineSession: build.query<Response<IRoutineSessionDetail[]>, string>({
      query: (routineId) => `training/routine-session/list/${routineId}/`,
      transformResponse: (res) => res.data,
      providesTags: [{ type: 'Session', id: 'PARTIAL-LIST' }]
    }),

    createRoutineDailySession: build.mutation<any, IRoutineSession>({
      query(body) {
        return {
          url: `training/daily-session/?routine=${body.routine}`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'DailySession', id: 'PARTIAL-LIST' }]
    }),
    updateRoutineDailySession: build.mutation<any, any>({
      query(body) {
        const { daily_session_id, routine } = body;
        return {
          url: `training/daily-session/${daily_session_id}/?routine=${routine}`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'DailySession', id: 'PARTIAL-LIST' }]
    }),
    listRoutineDailySession: build.query<Response<IRoutineDailySession>, string>({
      query: (routineId) => `training/routine-session/${routineId}/`,
      transformResponse: (res) => res.data,
      providesTags: [{ type: 'DailySession', id: 'PARTIAL-LIST' }]
    }),
    listRoutineDailySessionById: build.query<Response<IRoutineDailySession>, any>({
      query: (id) => `training/daily-session/${id.dailySessionId}/?routine=${id.routineId}/`,
      transformResponse: (res) => res,
      providesTags: [{ type: 'DailySession', id: 'PARTIAL-LIST' }]
    }),
    deleteRoutineDailySessionById: build.mutation<IRoutineDailySession, any>({
      query(id) {
        return {
          url: `training/daily-session/${id.dailySessionId}/?routine=${id.routineId}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'DailySession', id: 'PARTIAL-LIST' }]
    }),
    completeModuleForRoutine: build.mutation({
      query({ routineId, moduleId, body }) {
        return {
          url: `training/routine/${routineId}/module/${moduleId}/mark_complete/`,
          method: 'PATCH',
          body
        };
      }
    }),
    downloadReport: build.query({
      query: (id) => `reports/download_pdf/routine/${id}/`
    }),
    saveRoutine: build.query({
      query(id) {
        return {
          url: `training/approve_routine_draft/routine/${id}/`,
          method: 'GET'
        };
      }
    }),
    deleteReport: build.mutation({
      query(id) {
        return {
          url: `/reports/create_pdf/routine/${id}/`,
          method: 'DELETE'
        };
      }
    })
  })
});

//api/v1/training/routine/{routine_id}/module/{module_id}/mark_complete/
export const {
  useListRoutineQuery,
  useCreateRoutineMutation,
  useUpdateRoutineMutation,
  usePatchUpdateRoutineMutation,
  useDeleteRoutineMutation,
  useListCoordinatorRoutineQuery,
  useListTraineeRoutineQuery,
  useListRoutineByIdQuery,
  useListRoutineDailySessionByIdQuery,
  useCreateRoutineSessionMutation,
  useListRoutineSessionQuery,
  useDeleteRoutineDailySessionByIdMutation,
  useCreateRoutineDailySessionMutation,
  useListRoutineDailySessionQuery,
  useUpdateRoutineDailySessionMutation,
  useCompleteModuleForRoutineMutation,
  useDownloadReportQuery,
  useDeleteReportMutation,
  useSaveRoutineQuery
} = routineApi;

export const TraineeRankReport: { [key: string]: any } = createApi({
  reducerPath: 'traineeRankReport',
  baseQuery: baseQuery,
  tagTypes: ['TraineeRankReport'],
  endpoints: (build) => ({
    listTraineeRank: build.query<
      Response<PaginatedDataResponse<IRoutineResponse[]>>,
      IPageQueryParams | null
    >({
      query: (id) => `evaluation/trainee/totalmarks/${id?.routineID}/`
    })
  })
});
export const { useListTraineeRankQuery } = TraineeRankReport;
