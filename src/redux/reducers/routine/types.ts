import { ICoursesInput } from '../courses';
import { IOrganizationDepartmentInput } from '../organization-department';
import { IOrganizationHallInput } from '../organization-halls';
import { ITrainingTypeInput } from '../training-type';

export interface IElevatorDetail {
  id?: number;
  first_name: string;
  middle_name: string;
  last_name: string;
  mobile_number: string;
  image: string;
  signature: string;
  role: number;
  user: number;
}

export interface ICoordinatorDetail {
  id?: number;
  first_name: string;
  middle_name: string;
  last_name: string;
  mobile_number: string;
  image: string;
  signature: string;
  role: number;
  user: number;
}

export interface IResourceDetail {
  tms_resources_id?: string;
  course: number;
  url: string;
  name: string;
  type: string;
}

export interface ItrainingDetail {
  id?: number;
  training_name: string;
  training_duration: string;
  training_id: string;
  training_detail: string;
  course_details: ICoursesInput;
  resource_details: IResourceDetail;
  training_type_details: ITrainingTypeInput;
}

export interface IHallDetail {
  hall_id?: string;
  organization_details: IOrganizationDepartmentInput;
  hall_name: string;
  hall_name_np: string;
  hall_unique_key: string;
  status: boolean;
}
export interface IRoutineDetails {
  routine_id?: number;
  name: string;
  duration: string;
  start_date: string;
  end_date: string;
  // completion_date: string;
  // status: boolean;
}
