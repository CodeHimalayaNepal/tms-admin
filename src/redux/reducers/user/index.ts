import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { Response } from 'common/types';
import { PlaceType, TraineeProfile } from 'pages/private/TraineeProfile';
import { IAttendanceResponse } from '../attendance';

export enum ITraineeStatus {
  'Accepted' = 1,
  'Pending' = 2,
  'Rejected' = 3
}

export interface ITraineeDetail {
  first_name: string;
  middle_name: string;
  last_name: string;
  extra_fields: {};
  mobile_number: string;
  user_details: {
    id: number;
    email: string;
    status: ITraineeStatus;
  };
  image: null;
}

export interface TraineeProfileDetail {
  id: number;
  user: number;
  first_name: string;
  middle_name: string;
  last_name: string;
  email: string;
  signature: string;
  image: string;
  mobile_number: string;
  first_name_np: string;
  middle_name_np: string;
  last_name_np: string;
  dob: string;
  address: Address;
  current_training: any;
  previous_training: PreviousTraining[];
  experience: Experience[];
}

export interface Address {
  id: number;
  province: Province;
  district: number;
  municipality: string;
  ward_no: number;
  house_num: string;
  tole_street: string;
  block_no: string;
}

export interface Province {
  name_en: string;
  name_np: string;
}

export interface PreviousTraining {
  id: number;
  name: string;
  from_date: string;
  to_date: string;
  place_type: PlaceType;
  place: string;
}

export interface Experience {
  id: number;
  from_date: string;
  to_date: string;
  organization: string;
  designation: any;
  department: string;
  level: string;
  is_first_service: boolean;
  is_current_service: boolean;
}

export interface IAdminUSer {
  id: number;
  first_name: string;
  middle_name: string | null;
  last_name: string;
  mobile_number: string;
  email: string;
  user_type: string | number;
  attendance_pin: string | number | null;
  main_user_id: number;
  role_name: string;
  admin_role: number;
}

export interface IAdmin {
  first_name: string;
  middle_name?: string | null;
  last_name?: string;
  mobile_number?: string;
  email?: string;
}

export const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: baseQuery,
  tagTypes: ['User', 'User_List', 'Attendance', 'Admin_User_List', 'traineeList'],
  endpoints: (build) => ({
    listTraineeAttendance: build.query<IAttendanceResponse[], void>({
      query: () => `attendance/trainee/attendance/`,
      transformResponse: (res: Response<IAttendanceResponse[]>) => res.data,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.map(({ attendance_id }) => ({
                // ...result.map(({ id }) => ({
                type: 'Attendance' as const,
                id: attendance_id
                // id: id
              })),
              { type: 'Attendance', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Attendance', id: 'PARTIAL-LIST' }]
    }),
    traineeDetail: build.query<TraineeProfileDetail, { traineeId: string }>({
      query: ({ traineeId }) => `users/detail/${traineeId}/`,
      providesTags: (result, query, { traineeId }) =>
        result
          ? [
              {
                type: 'User' as const,
                id: traineeId.toString()
              },
              { type: 'User', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'User', id: 'PARTIAL-LIST' }]
    }),
    updateTraineeDetail: build.mutation<
      TraineeProfile,
      { traineeId: string; body: TraineeProfile }
    >({
      query({ traineeId, body }) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof TraineeProfile];
          formData.append(
            data,
            value instanceof File
              ? value
              : value instanceof Object
              ? JSON.stringify(value)
              : value ?? ''
          );
        }

        return {
          url: `users/update-trainee/${traineeId}/`,
          method: 'PUT',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'User', id: 'PARTIAL-LIST' }]
    }),
    updateSomeTraineeDetail: build.mutation<
      Partial<TraineeProfile & { attendance_pin: string; email: string }>,
      {
        traineeId: string;
        body: Partial<TraineeProfile & { attendance_pin: string; email: string }>;
      }
    >({
      query({ traineeId, body }) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof TraineeProfile];
          formData.append(
            data,
            value instanceof File
              ? value
              : value instanceof Object
              ? JSON.stringify(value)
              : value ?? ''
          );
        }

        return {
          url: `users/update-trainee/${traineeId}/`,
          method: 'PATCH',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'User', id: 'PARTIAL-LIST' }]
    }),

    listPendingTrainee: build.query({
      query: () => `users/unverified-user/`,
      transformResponse: (res: Response<ITraineeDetail[]>) => res.data,
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ user_details: { id } }) => ({
                type: 'User_List' as const,
                id: id.toString()
              })),
              { type: 'User_List', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'User_List', id: 'PARTIAL-LIST' }]
    }),
    listRejectedTrainee: build.query({
      query: () => `users/rejected-user/`,
      transformResponse: (res: Response<ITraineeDetail[]>) => res.data,
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ user_details: { id } }) => ({
                type: 'User_List' as const,
                id: id.toString()
              })),
              { type: 'User_List', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'User_List', id: 'PARTIAL-LIST' }]
    }),
    traineeReg: build.mutation({
      query(body) {
        return {
          url: `users/trainee-registration/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'User_List', id: 'PARTIAL-LIST' }]
    }),
    approveTrainee: build.mutation<ITraineeDetail, { id: string }>({
      query({ id }) {
        return {
          url: `users/verify-user/${id}/`,
          method: 'PATCH',
          body: { status: '1' }
        };
      },
      invalidatesTags: [{ type: 'User_List', id: 'PARTIAL-LIST' }]
    }),
    bulkApproveTrainee: build.mutation<ITraineeDetail, { allID: Array<string> }>({
      query({ allID }) {
        return {
          url: `users/verify-all-user/`,
          method: 'PATCH',
          body: { user_list: allID }
        };
      },
      invalidatesTags: [{ type: 'User_List', id: 'PARTIAL-LIST' }]
    }),
    rejectTrainee: build.mutation<ITraineeDetail, { id: string }>({
      query({ id }) {
        return {
          url: `users/verify-user/${id}/`,
          method: 'PATCH',
          body: { status: '3' }
        };
      },
      invalidatesTags: [{ type: 'User_List', id: 'PARTIAL-LIST' }]
    }),

    listAdminUsers: build.query<IAdminUSer[], void>({
      query: () => `users/admin/list/`,
      transformResponse: (res: Response<IAdminUSer[]>) => res.data,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.map(({ id }) => ({
                type: 'Admin_User_List' as const,
                id: id
              })),
              { type: 'Admin_User_List', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Admin_User_List', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListTraineeAttendanceQuery,
  useListRejectedTraineeQuery,
  useListPendingTraineeQuery,
  useUpdateTraineeDetailMutation,
  useUpdateSomeTraineeDetailMutation,
  useTraineeDetailQuery,
  useApproveTraineeMutation,
  useRejectTraineeMutation,
  useBulkApproveTraineeMutation,
  useListAdminUsersQuery,
  useTraineeRegMutation
} = userApi;
