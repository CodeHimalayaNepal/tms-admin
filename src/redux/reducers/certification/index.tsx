import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from '../settings';

export const certificationApi: { [key: string]: any } = createApi({
  reducerPath: 'CertificationApi',
  baseQuery: baseQuery,
  tagTypes: ['CERTIFICATE'],
  endpoints: (build) => ({
    listCertificateTemplate: build.query<any, any>({
      query: () => `certificate/certificate-template/`,
      transformResponse: (res) => res.data,
      // keepUnusedDataFor: 0,
      //  providesTags: [{ type: 'CERTIFICATE', id: 'LIST' }]
      providesTags: (result, error, page) =>
        result && result.results
          ? [
              ...result.results.data.map(({ id }: { id: number }) => ({
                type: 'CERTIFICATE' as const,
                id: id
              })),
              { type: 'CERTIFICATE', id: 'LIST' }
            ]
          : [{ type: 'CERTIFICATE', id: 'LIST' }]
    }),
    createTemplate: build.mutation<any, any>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof any];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData?.append(`${data}[${index}]`, v);
              });
            }
          } else {
            formData?.append(
              data,
              value instanceof File
                ? value
                : value instanceof Object
                ? JSON.stringify(value)
                : value ?? ''
            );
          }
        }
        return {
          url: `certificate/certificate-template/`,
          method: 'POST',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'CERTIFICATE', id: 'LIST' }]
    }),
    viewTemplateById: build.query<any, any>({
      query: (templateId) => `/certificate/certificate-template/${templateId}/`
    }),

    deleteTemplate: build.mutation<any, number>({
      query(id) {
        return {
          url: `/certificate/delete-template/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'CERTIFICATE', id: 'LIST' }]
    }),

    getCertificateDetails: build.query<any, any>({
      query: ({ id, traineeId }) =>
        `/certificate/certificate-by-trainee/routine/${id}/trainee/${traineeId}/`
    }),

    assignToRoutine: build.mutation<any, any>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof any];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData?.append(`${data}[${index}]`, v);
              });
            }
          } else {
            formData?.append(
              data,
              value instanceof File
                ? value
                : value instanceof Object
                ? JSON.stringify(value)
                : value ?? ''
            );
          }
        }
        return {
          url: `certificate/routine-certificate/`,
          method: 'POST',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'CERTIFICATE', id: 'LIST' }]
    })
  })
});

export const {
  useCreateTemplateMutation,
  useListCertificateTemplateQuery,
  useAssignToRoutineMutation,
  useViewTemplateByIdQuery,
  useGetCertificateDetailsQuery,
  useDeleteTemplateMutation
} = certificationApi;
