// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';

import { IPageQueryParams } from '../courses';

export interface ITrainingTypeResponse extends ITrainingTypeInput {
  created_at: string;
  updated_at: string;
}

export interface ITrainingTypeInput {
  training_type_id?: number;
  title: string;
  description: string;
}

export const trainingTypeApi: { [key: string]: any } = createApi({
  reducerPath: 'trainingTypeApi',
  baseQuery: baseQuery,
  tagTypes: ['TrainingTypes'],
  endpoints: (build) => ({
    listTrainingType: build.query<Response<ITrainingTypeResponse[]>, IPageQueryParams>({
      query: () => `training/type/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ training_type_id }) => ({
                type: 'TrainingTypes' as const,
                id: training_type_id
              })),
              { type: 'TrainingTypes', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'TrainingTypes', id: 'PARTIAL-LIST' }]
    }),

    createTrainingType: build.mutation<ITrainingTypeResponse, ITrainingTypeInput>({
      query(body) {
        return {
          url: `training/type`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'TrainingTypes', id: 'PARTIAL-LIST' }]
    }),

    updateTrainingType: build.mutation<ITrainingTypeResponse, ITrainingTypeInput>({
      query(body) {
        const { training_type_id } = body;
        return {
          url: `training/type/${training_type_id}`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'TrainingTypes', id: 'PARTIAL-LIST' }]
    }),
    deleteTrainingType: build.mutation<ITrainingTypeResponse, string>({
      query(id) {
        return {
          url: `training/type/${id}`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'TrainingTypes', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListTrainingTypeQuery,
  useCreateTrainingTypeMutation,
  useUpdateTrainingTypeMutation,
  useDeleteTrainingTypeMutation
} = trainingTypeApi;
