// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { baseQuery } from 'redux/reducers/settings';
import { IOrganizationResponse } from '../organizations';

interface ICoordinatorDetail {
  coordinator_id: number;
  coordinator: string;
  routine: IRoutineDetail[];
}

interface IRoutineDetail {
  routine_id: number;
  routine_name: string;
  start_date: string;
  end_date: string;
  completion_date: string;
}
interface IEvaluator {
  evaluator_id: number;
  evaluator_name: string;
  routine: IRoutineDetail[];
}

// export interface IOrganizationHallResponse {
//   organization_details: IOrganizationResponse;
// }

export const roasterApi: { [key: string]: any } = createApi({
  reducerPath: 'roasterApi',
  baseQuery: baseQuery,
  tagTypes: ['Roster'],
  endpoints: (build) => ({
    listCoordinatorRoutineForRoster: build.query<
      Response<PaginatedDataResponse<ICoordinatorDetail[]>>,
      void
    >({
      query: () => `roster/coordinator/`
    }),
    listEvaluatorRoutineForRoster: build.query<Response<PaginatedDataResponse<IEvaluator[]>>, void>(
      {
        query: () => `roster/evaluator/`
      }
    ),
    listCoordinatorRoutineForRosterById: build.query<
      Response<PaginatedDataResponse<ICoordinatorDetail[]>>,
      void
    >({
      query: () => `roster/coordinator/detail/{id}/`
    }),
    listEvaluatorForRosterById: build.query<Response<PaginatedDataResponse<IEvaluator[]>>, void>({
      query: () => `roster/evaluator/detail/{id}/`
    }),
    listCoordinatorRoutineForRosterAvailable: build.query<
      Response<PaginatedDataResponse<ICoordinatorDetail[]>>,
      void
    >({
      query: () => `roster/coordinator/available/`
    })
  })
});

export const {
  useListCoordinatorRoutineForRosterQuery,
  useListEvaluatorRoutineForRosterQuery,
  uselistCoordinatorRoutineForRosterByIdQuery,
  uselistEvaluatorRoutineForRosterByIdQuery,
  useListCoordinatorRoutineForRosterAvailableQuery
} = roasterApi;
