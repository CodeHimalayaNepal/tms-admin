export interface IRoutineDetail {
  routine_id?: number;
  name: string;
  duration: string;
  start_date: string;
  end_date: string;
  // completion_date: string;
  // status: boolean;
}
