// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';
import { IEvaluationResponse } from '../evaluationCriteria';
import { IEvaluationFormInuput } from '../evaluationCriteria';

export interface IEvaluationCriteriaResponse extends IEvaluationDetailsFormInuput {
  evaluation_criteria_detail_id: number;
  name: string;

  evaluation_criteria_detail: EvaluationCriteriaDetail;
}

export interface EvaluationCriteriaDetail {
  evaluation_criteria_id: number;
  name: string;
  code: string;
  parent_detail: ParentDetail;
}

export interface ParentDetail {
  parent_id: number;
  name: string;
  code: string;
}

export interface IEvaluationDetailsFormInuput {
  evaluation_criteria_detail_id?: number;
  evaluation_criteria_id?: number;
  name: string;
}

export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
}

export const evaluationCriteriaDetailsApi: { [key: string]: any } = createApi({
  reducerPath: 'evaluationCriteriaDetailsApi',
  baseQuery: baseQuery,
  tagTypes: ['EvaluationCriteriaDetails'],
  endpoints: (build) => ({
    listEvaluationCriteriaDetails: build.query<
      Response<PaginatedDataResponse<IEvaluationCriteriaResponse[]>>,
      IPageQueryParams | null
    >({
      query: (evaluation_criteria_detail_id) =>
        `evaluation/criteria/${evaluation_criteria_detail_id}/details/`,
      providesTags: (result, error, page) => {
        return result
          ? result.data.results?.length
            ? [
                ...result.data.results.map(({ evaluation_criteria_detail_id }) => ({
                  type: 'EvaluationCriteriaDetails' as const,
                  id: evaluation_criteria_detail_id
                })),
                { type: 'EvaluationCriteriaDetails', id: 'PARTIAL-LIST' }
              ]
            : [{ type: 'EvaluationCriteriaDetails', id: 'PARTIAL-LIST' }]
          : [{ type: 'EvaluationCriteriaDetails', id: 'PARTIAL-LIST' }];
      }
    }),

    createEvaluationCriteriaDetails: build.mutation<IEvaluationResponse, IEvaluationFormInuput>({
      query(body) {
        return {
          url: `evaluation/criteria-detail/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'EvaluationCriteriaDetails', id: 'PARTIAL-LIST' }]
    }),

    updateEvaluationCriteriaDetails: build.mutation<
      IEvaluationResponse,
      IEvaluationDetailsFormInuput
    >({
      query(body) {
        const { evaluation_criteria_detail_id } = body;
        return {
          url: `evaluation/criteria-detail/${evaluation_criteria_detail_id}`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'EvaluationCriteriaDetails', id: 'PARTIAL-LIST' }]
    }),
    deleteEvaluationCriteriaDetails: build.mutation<IEvaluationResponse, string>({
      query(evaluation_criteria_detail_id) {
        return {
          url: `evaluation/criteria-detail/${evaluation_criteria_detail_id}`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'EvaluationCriteriaDetails', id: 'PARTIAL-LIST' }]
    }),
    listParentDetails: build.query<Response<IEvaluationResponse[]>, void>({
      query: () => `evaluation/parent/list/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ evaluation_criteria_id }) => ({
                type: 'EvaluationCriteriaDetails' as const,
                id: evaluation_criteria_id
              })),
              { type: 'EvaluationCriteriaDetails', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'EvaluationCriteriaDetails', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListEvaluationCriteriaDetailsQuery,
  useListParentDetailsQuery,
  useCreateEvaluationCriteriaDetailsMutation,
  useUpdateEvaluationCriteriaDetailsMutation,
  useDeleteEvaluationCriteriaDetailsMutation
} = evaluationCriteriaDetailsApi;
