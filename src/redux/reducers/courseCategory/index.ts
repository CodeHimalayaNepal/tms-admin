// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { baseQuery } from 'redux/reducers/settings';

export interface ICourseCategoryResponse extends ICourseCategoryInput {}

export interface ICourseCategoryInput {
  course_category_id?: string;
  name: string;
  description: string;
}
export interface IPageQueryParams {
  page?: number | void;
  pageSize?: number | void;
}

export const courseCategoryApi: { [key: string]: any } = createApi({
  reducerPath: 'courseCategoryApi',
  baseQuery: baseQuery,
  tagTypes: ['CourseCategory'],
  endpoints: (build) => ({
    listCourseCategory: build.query<Response<ICourseCategoryResponse[]>, IPageQueryParams | null>({
      query: (paging) => `training/course-category-list/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ course_category_id }) => ({
                type: 'CourseCategory' as const,
                id: course_category_id
              })),
              { type: 'CourseCategory', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'CourseCategory', id: 'PARTIAL-LIST' }]
    }),
    createCourseCategory: build.mutation<ICourseCategoryResponse, ICourseCategoryInput>({
      query(body) {
        return {
          url: `training/course-category-create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'CourseCategory', id: 'PARTIAL-LIST' }]
    }),

    updateCourseCategory: build.mutation<ICourseCategoryResponse, ICourseCategoryInput>({
      query(body) {
        const { course_category_id } = body;
        return {
          url: `training/course-category-update/${course_category_id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'CourseCategory', id: 'PARTIAL-LIST' }]
    }),
    deleteCourseCategory: build.mutation<ICourseCategoryResponse, string>({
      query(id) {
        return {
          url: `training/course-category-delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'CourseCategory', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListCourseCategoryQuery,
  useCreateCourseCategoryMutation,
  useUpdateCourseCategoryMutation,
  useDeleteCourseCategoryMutation
} = courseCategoryApi;

export interface ICourseAsPerTrainingResponse extends ICourseAsPerTrainingInput {}

export interface ICourseAsPerTrainingInput {
  course_id: number;
  title: string;
  code: string;
}
export const courseAsPerTrainingApi: { [key: string]: any } = createApi({
  reducerPath: 'courseAsPerTrainingApi',
  baseQuery: baseQuery,
  tagTypes: ['CourseAsPerTraining'],
  endpoints: (build) => ({
    listcourseAsPerTraining: build.query<
      Response<ICourseAsPerTrainingResponse[]>,
      IPageQueryParams | null
    >({
      query: (id) => `training/course-as-per-training/${id}/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ course_id }) => ({
                type: 'CourseAsPerTraining' as const,
                id: course_id
              })),
              { type: 'CourseAsPerTraining', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'CourseAsPerTraining', id: 'PARTIAL-LIST' }]
    })
  })
});

export const { useListcourseAsPerTrainingQuery } = courseAsPerTrainingApi;
