import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';

export const dashboardApi: { [key: string]: any } = createApi({
  reducerPath: 'feedbackApi',
  baseQuery: baseQuery,
  tagTypes: ['Dashboard'],
  endpoints: (build) => ({
    dashboardDetails: build.query({
      query: () => `training/admin/dashboard/count/`
    }),
    listRoutineMaster: build.query({
      query: () => `training/routine-master/`
    }),
    getDataByRoutineMasterId: build.query({
      query: (id) => `training/routine-master/${id}/`
    }),
    getCourseTopicsByModuleId: build.query({
      query: (id) => `/training/course-session-list/${id}/`
    }),
    getActiveCourseTopicsByModuleId: build.query({
      query: (id) => `training/routine-course-session-list/${id.isHover}/?routine=${id.id}`
    }),
    getOngoingTrainings: build.query({
      query: () => `/users/admin/routines/?filter=active`
    }),
    getCommentsBySessionId: build.query<
      any,
      { type: any; id: any; routine_session: any; routine: number }
    >({
      query: (arg) => {
        const { type, id, routine } = arg;
        return {
          url: `/routine_discussion/comments/?type=${type}&id=${id}&routine=${routine}`
        };
      },
      providesTags: [{ type: 'Dashboard', id: 'PARTIAL-LIST' }]
    }),
    createCommentOnSession: build.mutation<any, any>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof any];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData?.append(`${data}`, v);
              });
            }
          } else {
            formData?.append(
              data,
              value instanceof File
                ? value
                : value instanceof Object
                ? JSON.stringify(value)
                : value ?? ''
            );
          }
        }
        const { id, type = 'CourseTopic' } = body;
        return {
          url: `/routine_discussion/comments/create/?type=${type}&id=${+id}`,
          method: 'POST',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'Dashboard', id: 'PARTIAL-LIST' }]
    }),
    deleteComment: build.mutation({
      query(id) {
        return {
          url: `/routine_discussion/comments/${id}/delete/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'Dashboard', id: 'PARTIAL-LIST' }]
    }),
    getAttachmentsByCourse: build.query({
      query: (id) =>
        `/routine_discussion/comments/attachment/?type=coursetopic&id=${id.isActiveSessionId}&routine=${id.id}`
    }),
    createReplyOnComment: build.mutation<any, any>({
      query(body) {
        let formData = new FormData();
        for (let data in body) {
          const value = body[data as keyof any];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData?.append(`${data}`, v);
              });
            }
          } else {
            formData?.append(
              data,
              value instanceof File
                ? value
                : value instanceof Object
                ? JSON.stringify(value)
                : value ?? ''
            );
          }
        }
        const { type = 'CourseTopic', id, parent_id, content } = body;
        return {
          url: `/routine_discussion/comments/create/?type=${type}&id=${id}&parent_id=${parent_id}`,
          method: 'POST',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'Dashboard', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useCreateReplyOnCommentMutation,
  useCreateCommentOnSessionMutation,
  useDashboardDetailsQuery,
  useListRoutineMasterQuery,
  useGetDataByRoutineMasterIdQuery,
  useGetCourseTopicsByModuleIdQuery,
  useGetCommentsBySessionIdQuery,
  useGetActiveCourseTopicsByModuleIdQuery,
  useGetOngoingTrainingsQuery,
  useGetAttachmentsByCourseQuery,
  useDeleteCommentMutation
} = dashboardApi;
