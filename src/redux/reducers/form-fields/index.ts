// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { baseQuery } from 'redux/reducers/settings';
import { IFormTypeResponse } from '../form-types';

export interface IFormFieldsResponse extends IFormFieldsInput {
  field_type_details: IFormTypeResponse;
  option_details: [];
}

export interface IFormFieldsInput {
  dynamic_field_id?: number;
  fieldname: string;
  status: boolean;
  assign_field: 'TRAINEE' | 'ASSIGNEE';
  option?: [] | null;
  field_type: number;
  is_required: boolean;
  order_by: number;
}

export const formFieldsApi: { [key: string]: any } = createApi({
  reducerPath: 'formFieldsApi',
  baseQuery: baseQuery,
  tagTypes: ['FormFields'],
  endpoints: (build) => ({
    listFormFields: build.query<IFormFieldsResponse[], void>({
      query: () => `/users/dynamic-fields-list/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.map(({ dynamic_field_id }) => ({
                type: 'FormFields' as const,
                id: dynamic_field_id
              })),
              { type: 'FormFields', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'FormFields', id: 'PARTIAL-LIST' }]
    }),

    listDynamicFieldsOfTrainee: build.query<IFormFieldsResponse[], void>({
      query: () => `/users/dynamic-fields-trainee/`
    }),
    createFormFields: build.mutation<IFormFieldsResponse, IFormFieldsInput>({
      query(body) {
        return {
          url: `/users/dynamic-fields-create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'FormFields', id: 'PARTIAL-LIST' }]
    }),
    uploadCsvFile: build.mutation<any, { data: { csvFile: File } }>({
      query: ({ data }) => ({
        url: `/users/upload-csv-trainee/`,
        method: 'POST',
        body: data
      }),

      invalidatesTags: [{ type: 'FormFields', id: 'PARTIAL-LIST' }]
    }),
    uploadCsvFileAdmin: build.mutation<any, { data: { csvFile: File } }>({
      query: ({ data }) => ({
        url: `users/upload-csv/`,
        method: 'POST',
        body: data
      }),

      invalidatesTags: [{ type: 'FormFields', id: 'PARTIAL-LIST' }]
    }),

    updateFormFields: build.mutation<IFormFieldsResponse, IFormFieldsInput>({
      query(body) {
        const { dynamic_field_id } = body;
        return {
          url: `users/update-dynamic-fields/${dynamic_field_id}/`,
          method: 'PUT',
          body
        };
      },
      invalidatesTags: [{ type: 'FormFields', id: 'PARTIAL-LIST' }]
    }),

    deleteFormFields: build.mutation<IFormFieldsResponse, string>({
      query(id) {
        return {
          url: `users/dynamic-fields-delete/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'FormFields', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListFormFieldsQuery,
  useCreateFormFieldsMutation,
  useUpdateFormFieldsMutation,
  useDeleteFormFieldsMutation,
  useListDynamicFieldsOfTraineeQuery,
  useUploadCsvFileMutation,
  useUploadCsvFileAdminMutation
} = formFieldsApi;
