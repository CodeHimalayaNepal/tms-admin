import { IInviteUserInput } from './../../../pages/private/Administrators/schemas';
// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse } from 'common/types';
import { baseQuery } from 'redux/reducers/settings';
import { ICoordinatorDetail } from '../routine/types';

export interface IAdministratorResponse extends IAdministratorInput {
  roleadmin: [];
  desc?: string;
  staff?: ICoordinatorDetail;
}
export interface IAdministratorInput {
  id?: number;
  name: string;
}

export interface IProfileDetail {
  id: number;
  user_id: number;
  first_name: string;
  middle_name: string;
  last_name: string;
  email: string;
  bio: string;
  signature: any;
  image: string;
  mobile_number: string;
  first_name_np: string;
  middle_name_np: string;
  last_name_np: string;
  pan_number: string;
  current_position: string;
  associated_organization: string;
  area_of_expertise: string;
  experience: string;
  user_role: string;
  user_document: UserDocument[];
  bank_details: UserBankDetail[];
}

export interface UserBankDetail {
  bank_name: string;
  bank_branch: string;
  account_number: string;
  account_name: string;
}

export interface UserDocument {
  document_name: string;
  document: string;
}

export const administratorApi: { [key: string]: any } = createApi({
  reducerPath: 'administratorApi',
  baseQuery: baseQuery,
  tagTypes: ['Administrator', 'UserDetail'],
  endpoints: (build) => ({
    listAdministrator: build.query<PaginatedDataResponse<IAdministratorResponse[]>, void>({
      query: () => `users/adminrole/view/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.results.map(({ id }) => ({
                type: 'Administrator' as const,
                id: id
              })),
              { type: 'Administrator', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Administrator', id: 'PARTIAL-LIST' }]
    }),
    listsUsersByRole: build.query<IAdministratorResponse[], void>({
      query: () => `users/userrole/view/`
    }),
    ListUsersOfThisrole: build.query<any, any>({
      query: (RoleId) =>
        `role-permission/role/assign-user/${RoleId !== undefined ? `${RoleId}` : ''}/`
    }),
    currentUserDetails: build.query<IAdministratorResponse[], void>({
      query: () => `users/get-logged-in-user-profile/`
    }),
    fetchUserRole: build.query<IAdministratorResponse[], void>({
      query: () => `users/role-with-admin/view/`
    }),
    fetchUserProfile: build.query<IProfileDetail, string>({
      query: (id) => `users/admin/detail/${id}/`,
      providesTags: (result) => [{ type: 'UserDetail', id: 'PARTIAL-LIST' }]
    }),
    updateUserProfile: build.mutation<any, IProfileDetail>({
      query(body) {
        const Id = body.id;
        let formData = new FormData();
        for (let property in body) {
          const value = body[property as keyof IProfileDetail];
          if (value instanceof Array) {
            if (value.every((v) => v instanceof File)) {
              value.forEach((v, index) => {
                formData.append(`${property}`, v);
              });
            } else {
              formData.append(property, JSON.stringify(value));
            }
          } else {
            formData.append(property, value);
          }
        }
        return {
          url: `users/update-external-user/${Id}/`,
          header: 'multipart/form-data',
          method: 'PATCH',
          contentType: 'application/json',
          body: formData
        };
      },
      invalidatesTags: [{ type: 'UserDetail', id: 'PARTIAL-LIST' }]
    }),
    inviteUser: build.mutation<IAdministratorResponse, IInviteUserInput>({
      query(body) {
        return {
          url: `users/invite-user/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'Administrator', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListAdministratorQuery,
  useInviteUserMutation,
  useListsUsersByRoleQuery,
  useListUsersOfThisroleQuery,
  useCurrentUserDetailsQuery,
  useFetchUserRoleQuery,
  useFetchUserProfileQuery,
  useUpdateUserProfileMutation
} = administratorApi;
