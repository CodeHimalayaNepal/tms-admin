// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { PaginatedDataResponse, Response } from 'common/types';
import { baseQuery } from 'redux/reducers/settings';
import { IOrganizationResponse } from '../organizations';

export interface IOrganizationHallResponse extends IOrganizationHallInput {
  organization_details: IOrganizationResponse;
}

export interface IOrganizationHallInput {
  hall_id?: number;
  organization_id: number;
  hall_name: string;
  hall_name_np: string;
  hall_unique_key: string;
  status: boolean;
  organization: number;
}
export interface IPageQueryParams {
  startDate?: number | void;
  endDate?: number | void;
}
export const organizationHallApi: { [key: string]: any } = createApi({
  reducerPath: 'organizationHallApi',
  baseQuery: baseQuery,
  tagTypes: ['OrganizationHall'],
  endpoints: (build) => ({
    listOrganizationHall: build.query<Response<IOrganizationHallResponse[]>, void>({
      query: (date) => `organization/hall/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ hall_id }) => ({
                type: 'OrganizationHall' as const,
                id: hall_id
              })),
              { type: 'OrganizationHall', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'OrganizationHall', id: 'PARTIAL-LIST' }]
    }),
    listAvailableOrganizationHall: build.query<
      Response<IOrganizationHallResponse[]>,
      IPageQueryParams | null
    >({
      query: (date) =>
        `organization/hall/available/?start_date=${date?.startDate}&end_date=${date?.endDate}`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.map(({ hall_id }) => ({
                type: 'OrganizationHall' as const,
                id: hall_id
              })),
              { type: 'OrganizationHall', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'OrganizationHall', id: 'PARTIAL-LIST' }]
    }),
    createOrganizationHall: build.mutation<IOrganizationHallResponse, IOrganizationHallInput>({
      query(body) {
        return {
          url: `organization/hall/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'OrganizationHall', id: 'PARTIAL-LIST' }]
    }),

    updateOrganizationHall: build.mutation<IOrganizationHallResponse, IOrganizationHallInput>({
      query(body) {
        const { hall_id } = body;
        return {
          url: `organization/hall/${hall_id}/`,
          method: 'PATCH',
          body
        };
      },
      invalidatesTags: [{ type: 'OrganizationHall', id: 'PARTIAL-LIST' }]
    }),
    deleteOrganizationHall: build.mutation<IOrganizationHallResponse, string>({
      query(id) {
        return {
          url: `organization/hall/${id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'OrganizationHall', id: 'PARTIAL-LIST' }]
    })
  })
});

export const {
  useListOrganizationHallQuery,
  useListAvailableOrganizationHallQuery,
  useCreateOrganizationHallMutation,
  useUpdateOrganizationHallMutation,
  useDeleteOrganizationHallMutation
} = organizationHallApi;
