import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { Response } from 'common/types';

import { IPageQueryParams } from '../courses';
import { TraineeProfile } from 'pages/private/TraineeProfile';

export enum ITraineeStatus {
  'Accepted' = 1,
  'Pending' = 2,
  'Rejected' = 3
}

export interface IProvince {
  created_at: string;
  id: number;
  name_en: string;
  name_np: string;
  province_code: string;
  province_number: number;
  updated_at: string;
}

export const geographyApi = createApi({
  reducerPath: 'geographyApi',
  baseQuery: baseQuery,
  tagTypes: ['Province', 'District'],
  endpoints: (build) => ({
    listProvince: build.query({
      query: () => `core/provinces/`,
      transformResponse: (res: { results: IProvince[] }) => res.results,
      providesTags: (result) =>
        result
          ? [
              ...result.map((r) => ({ type: 'Province' as const, id: r.id })),
              { type: 'Province', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Province', id: 'PARTIAL-LIST' }]
    }),
    listDistrictByProvince: build.query<
      { results: IProvince[]; next: string | null },
      { provinceId: string; page: number }
    >({
      query: ({ provinceId, page }) => `core/districts/?province=${provinceId}&page=${page}`,
      // FIXME: Disabled Caching for infinite query
      keepUnusedDataFor: 0,
      providesTags: (result) =>
        result
          ? [
              ...result.results.map((r) => ({ type: 'District' as const, id: r.id })),
              { type: 'District', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'District', id: 'PARTIAL-LIST' }]
    })
  })
});

export const { useListProvinceQuery, useListDistrictByProvinceQuery } = geographyApi;
