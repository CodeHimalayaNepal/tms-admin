import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery, convertJsonToFormData } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';

export interface ICourseResponse {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}

export interface CreateCourseResource {
  tms_resources_id?: string;
  course: number;
  file: string;
  name: string;
  type: string;
}

export const courseResourceApi: { [key: string]: any } = createApi({
  reducerPath: 'courseResourceApi',
  baseQuery: baseQuery,
  tagTypes: ['Resource'],
  endpoints: (build) => ({
    listCourseResource: build.query<Response<PaginatedDataResponse<ICourseResponse[]>>, void>({
      query: () => `training/course-resource-list/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ id }) => ({
                type: 'Resource' as const,
                id: id
              })),
              { type: 'Resource', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Resource', id: 'PARTIAL-LIST' }]
    }),
    createCourseResource: build.mutation<ICourseResponse, CreateCourseResource>({
      query(body) {
        return {
          url: `course/resource/create/`,
          method: 'POST',
          body: convertJsonToFormData(body)
        };
      },
      invalidatesTags: [{ type: 'Resource', id: 'PARTIAL-LIST' }]
    }),
    updateCourseResource: build.mutation<ICourseResponse, CreateCourseResource>({
      query(body) {
        const { tms_resources_id } = body;
        return {
          url: `course/resource/update/${tms_resources_id}`,
          method: 'PUT',
          body
        };
      }
    }),
    deleteCourseResource: build.mutation<ICourseResponse, string>({
      query(id) {
        return {
          url: `course/resource/delete/${id}`,
          method: 'DELETE'
        };
      }
    })
  })
});

export const {
  useListCourseResourceQuery,
  useCreateCourseResourceMutation,
  useUpdateCourseResourceMutation,
  useDeleteCourseResourceMutation
} = courseResourceApi;
