import { fetchBaseQuery } from '@reduxjs/toolkit/query';
import { refreshTokenRef, tokenRef } from 'common/constant';
import { Mutex } from 'async-mutex';

export const baseApiUrl =
  process.env.REACT_APP_API_URL || 'http://116.202.10.98:8000/backend/api/v1/';
export const baseApiUrlv2 = baseApiUrl.replace('v1', 'v2');

const baseQueryForUrl = fetchBaseQuery({
  baseUrl: baseApiUrl,
  prepareHeaders: (headers, { getState }) => {
    const token = window.localStorage[tokenRef];
    if (token) {
      headers.set('Authorization', `Bearer ${JSON.parse(token)}`);
    }
    return headers;
  }
});

const mutex = new Mutex();

export const baseQuery = async (args: any, api: any, extraOptions: any) => {
  await mutex.waitForUnlock();
  let result: any = await baseQueryForUrl(args, api, extraOptions);
  if (result.error && result.error.status === 401) {
    if (!mutex.isLocked()) {
      const release = await mutex.acquire();
      try {
        const token = window.localStorage[tokenRef];
        if (!token) {
          throw new Error('No token found');
        }

        const refreshResult = await baseQueryForUrl('/users/api/token/refresh/', api, extraOptions);
        if (refreshResult.data) {
          localStorage.setItem(tokenRef, '');
          localStorage.setItem(refreshTokenRef, '');
          result = await baseQueryForUrl(args, api, extraOptions);
        } else {
          window.location.href = '/';
          localStorage.removeItem(tokenRef);
          localStorage.removeItem(refreshTokenRef);
        }
      } finally {
        release();
      }
    } else {
      await mutex.waitForUnlock();
      result = await baseQuery(args, api, extraOptions);
    }
  }
  return result;
};

export function convertJsonToFormData(requestData: { [key: string]: any }) {
  let formData = new FormData();
  for (let data in requestData) {
    if (requestData[data] instanceof Array) {
      requestData[data].forEach((dataEl: any, index: number) => {
        if (dataEl instanceof Object && !(dataEl instanceof File)) {
          Object.keys(dataEl).forEach((elKey) =>
            formData.append(`${data}[${index}].${elKey}`, dataEl[elKey])
          );
        } else if (dataEl instanceof File) {
          formData.append(`${data}[${index}]`, dataEl);
        } else if (typeof dataEl === 'number' || typeof dataEl === 'string') {
          formData.append(`${data}[${index}]`, dataEl.toString());
        }
      });
    } else if (requestData[data] instanceof Object && !(requestData[data] instanceof File)) {
      Object.entries(requestData[data]).forEach(([key, value]: [string, any]) =>
        formData.append(`${data}.${key}`, value)
      );
    } else {
      formData.append(data, requestData[data]);
    }
  }

  return formData;
}
