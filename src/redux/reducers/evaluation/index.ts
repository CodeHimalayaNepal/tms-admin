// Or from '@reduxjs/toolkit/query' if not using the auto-generated hooks
import { createApi } from '@reduxjs/toolkit/query/react';
import { baseQuery } from 'redux/reducers/settings';
import { PaginatedDataResponse, Response } from 'common/types';
import { CoursesResponse } from '../courses';
import { IRoutineEnrollmentResponse } from '../routine-enrollment';
import { IEvaluationDetailsResponse } from '../evaluation-ev-details';
export interface IEvaluationResponse extends IEvaluationInuput {
  assignment_details: IAssignmentResponse;
  enrollment_details: IRoutineEnrollmentResponse;
  evaluation_details: IEvaluationDetailsResponse; // remaingin
}

export interface IEvaluationInuput {
  tms_evaluation_id?: number;
  enrollment: string;
  evaluation_detail: number;
  assignment: number;
  evaluator_marks: string;
  evaluator_remarks: string;
  coordinator_marks: string;
  coordinator_remarks: string;
  status: boolean;
}
export interface IAssignmentResponse {
  tms_assignment_id: string;
  assignment_file: string;
  start_date: string;
  finish_date: string;
  assignment_description: string;
  course_details: CoursesResponse;
}

export const evaluationApi: { [key: string]: any } = createApi({
  reducerPath: 'evaluationApi',
  baseQuery: baseQuery,
  tagTypes: ['Evaluation'],
  endpoints: (build) => ({
    listEvaluation: build.query<Response<PaginatedDataResponse<IEvaluationResponse[]>>, void>({
      query: () => `evaluation/`,
      providesTags: (result, error, page) =>
        result
          ? [
              ...result.data.results.map(({ tms_evaluation_id }) => ({
                type: 'Evaluation' as const,
                id: tms_evaluation_id
              })),
              { type: 'Evaluation', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Evaluation', id: 'Evaluation-LIST' }]
    }),
    createEvaluation: build.mutation<IEvaluationResponse, IEvaluationInuput>({
      query(body) {
        return {
          url: `evaluation/create/`,
          method: 'POST',
          body
        };
      },
      invalidatesTags: [{ type: 'Evaluation', id: 'PARTIAL-LIST' }]
    }),

    updateEvaluation: build.mutation<IEvaluationResponse, IEvaluationInuput>({
      query(body) {
        const { tms_evaluation_id } = body;
        return {
          url: `evaluation/update/${tms_evaluation_id}`,
          method: 'PUT',
          body
        };
      },
      invalidatesTags: [{ type: 'Evaluation', id: 'PARTIAL-LIST' }]
    }),
    deleteEvaluation: build.mutation<IEvaluationResponse, string>({
      query(tms_evaluation_id) {
        return {
          url: `evaluation/delete/${tms_evaluation_id}/`,
          method: 'DELETE'
        };
      },
      invalidatesTags: [{ type: 'Evaluation', id: 'PARTIAL-LIST' }]
    }),
    updateEvaluationsingle: build.mutation<IEvaluationResponse, string>({
      query(tms_evaluation_id) {
        return {
          url: `evaluation/update/${tms_evaluation_id}`,
          method: 'PATCH'
        };
      },
      invalidatesTags: [{ type: 'Evaluation', id: 'PARTIAL-LIST' }]
    }),
    //confused
    listSingle: build.query<IEvaluationResponse, IEvaluationInuput>({
      query: (tms_evaluation_id) => `evaluation/update/${tms_evaluation_id}/`
    })
    // listEvaluationSingle: build.query<Response<PaginatedDataResponse<IEvaluationResponse[]>>, void>({
    //   query: (tms_evaluation_id) => `evaluation/update/${tms_evaluation_id}/`,
    //   providesTags: (result, error, page) =>
    //     result
    //       ? [
    //           ...result.data.results.map(({ tms_evaluation_id }) => ({
    //             type: 'Evaluation' as const,
    //             id: tms_evaluation_id
    //           })),
    //           { type: 'Evaluation', id: 'PARTIAL-LIST' }
    //         ]
    //       : [{ type: 'Evaluation', id: 'Evaluation-LIST' }]
    // }),
  })
});

export const {
  useListEvaluationQuery,
  // useListSingleQuery,
  useCreateEvaluationMutation,
  useUpdateEvaluationMutation,
  useDeleteEvaluationMutation,
  useUpdateEvaluationsingleMutation
} = evaluationApi;
