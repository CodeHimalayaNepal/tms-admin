const validations = {
  password: {
    regex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{6,})/,
    message:
      'Must Contain 6 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character'
  },
  username: {
    regex: /^[A-Za-z][A-Za-z0-9_]{7,29}$/,
    message: 'Must start with an alphabet and can contain alphabet, number and underscore'
  },
  phone: {
    regex: /^[9]\d{9}$/,
    message: 'Must be a valid mobile number'
  }
};

export default validations;
