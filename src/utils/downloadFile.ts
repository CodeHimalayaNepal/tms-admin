import axios from 'axios';
import fileDownload from 'js-file-download';
import { tokenRef } from 'common/constant';
import { baseApiUrl } from 'redux/reducers/settings';

export default function download(url: string, filename: string) {
  const token = window.localStorage[tokenRef];
  axios
    .get(baseApiUrl + url, {
      responseType: 'blob',
      headers: {
        Authorization: `Bearer ${JSON.parse(token)}`
      }
    })
    .then((res: { data: string }) => {
      fileDownload(res.data, filename);
    });
}
