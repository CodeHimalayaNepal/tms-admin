export const downloadPdf = (data: string, filename: string, type: string) => {
  const element = document.createElement('a');
  const blob = new Blob([data], { type });
  const url = window.URL.createObjectURL(blob);
  element.setAttribute('href', url);
  element.setAttribute('download', filename);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  element.remove();
  window.URL.revokeObjectURL(url);
};
