/**
 * Convert 24 to 12 hr format
 * tConvert('18:00:00')
 * @param time
 * @returns
 */
export function tConvert(time: string) {
  // Check correct time format and split into components
  let timeValue = [
    ...(time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) ?? [])
  ] || [time];

  if (timeValue.length > 1) {
    // If time format correct
    timeValue = timeValue.slice(1); // Remove full string match value
    timeValue[5] = +timeValue[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
    timeValue[0] = (+timeValue[0] % 12).toString() || '12'; // Adjust hours
  }
  return timeValue.join(''); // return adjusted time or original string
}
