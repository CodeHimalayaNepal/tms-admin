import { DateTime } from 'luxon';

/**
 * Formats date to locale string
 * @param datatime Date
 * @returns
 */
function getLocaleDate(datatime: string | Date) {
  return DateTime.fromJSDate(new Date(datatime)).toLocaleString(DateTime.DATETIME_MED_WITH_SECONDS);
}

/**
 * Formats date to locale date
 * @param datatime Date
 * @returns
 */
function getLocaleDateOnly(datatime: Date) {
  return DateTime.fromJSDate(datatime).toLocaleString(DateTime.DATE_MED_WITH_WEEKDAY);
}

function formatTime(time: string) {
  if (!time) return time;
  const [hour, min] = time.split(':');

  return `${(+hour % 12).toString().padStart(2, '0')}:${min.toString().padStart(2, '0')} ${
    +hour >= 12 ? 'PM' : 'AM'
  }`;
}

export { getLocaleDate, formatTime, getLocaleDateOnly };
