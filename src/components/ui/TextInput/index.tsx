import React, { FC, useState } from 'react';
import FormikValidationError from '../FormikErrors';

interface ITextInput
  extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  containerClassName?: string;
  label: string;
  formik?: any;
  required?: boolean;
  disableRequiredValidation?: boolean;
}
const TextInput: FC<ITextInput> = (props) => {
  return (
    <div className={props.containerClassName ?? 'mt-3 col-lg-4 col-md-6 mb-2'}>
      <label htmlFor="fname-field" className="form-label" style={{ textTransform: 'capitalize' }}>
        {props.label}
        {props.required && <span>*</span>}
      </label>
      <input
        {...props}
        className="form-control"
        required={props.disableRequiredValidation ? false : true}
      />
      <FormikValidationError
        name={props.name ?? ''}
        errors={props.formik.errors}
        touched={props.formik.touched}
      />
    </div>
  );
};

export default TextInput;
