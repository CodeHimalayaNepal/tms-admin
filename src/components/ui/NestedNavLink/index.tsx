import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import { IRoutes } from 'routes/route-lists';

interface INestedNavLink {
  routes: IRoutes;
  id: string;
  isDivider?: boolean;
  toggle: () => void;
}
const NestedNavLink: FC<INestedNavLink> = ({ routes, id, isDivider, toggle }) => {
  return (
    <li className="nav-item">
      <a
        className="nav-link menu-link"
        href={`#sidebarDashboard${id}`}
        data-bs-toggle="collapse"
        role="button"
        aria-expanded="false"
        aria-controls={'sidebarDashboard' + id}
      >
        {routes.icon} <span data-key="t-dashboards">{routes.title}</span>
      </a>

      <div className="collapse menu-dropdown" id={'sidebarDashboard' + id}>
        <ul className="nav nav-sm flex-column">
          {routes.children
            ?.filter((item) => !item.hideInSideBar)
            ?.map((item) => {
              return (
                <li className="nav-item " onClick={toggle}>
                  <Link to={item.path} className="nav-link" data-key="t-analytics">
                    {item.title}
                  </Link>
                </li>
              );
            })}
        </ul>
      </div>
      {isDivider ? (
        <>
          <hr className="bg-danger border border-1 mx-3 my-1" />{' '}
        </>
      ) : (
        ''
      )}
    </li>
  );
};

export default NestedNavLink;
