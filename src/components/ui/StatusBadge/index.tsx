import React from 'react';
interface IStatusBadge {
  status: string;
}
const StatusBadge = (props: IStatusBadge) => {
  const { status } = props;
  return (
    <span
      style={{ width: '70px', height: '20px' }}
      className={`badge rounded-pill bg-${
        status == 'Completed' ? 'light text-dark' : status == 'Upcoming' ? 'info' : 'primary'
      } p-2 d-flex align-items-center justify-content-center`}
    >
      {status == 'Completed' ? 'Completed' : status === 'Upcoming' ? 'Upcoming' : 'Running'}
    </span>
  );
};
export default StatusBadge;
