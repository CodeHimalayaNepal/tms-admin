import React, { FC } from 'react';

interface IModal {
  children?: React.ReactNode;
  renderFooter?: () => React.ReactNode;
  title: string;
  isModalOpen: boolean;
  closeModal: () => void;
  modalSize?: 'modal-xl' | 'modal-md' | 'modal-sm';
  height?: string;
  overflowY?: 'auto' | 'visible';
}
const Modal: FC<IModal> = ({
  children,
  renderFooter,
  title,
  isModalOpen,
  closeModal,
  height,
  overflowY = 'visible',
  modalSize = 'modal-xl'
}) => {
  return (
    <div>
      {isModalOpen && (
        <div
          className="modal fade show d-block"
          tabIndex={-1}
          aria-labelledby="exampleModalLabel"
          aria-hidden="false"
          style={{ backgroundColor: 'rgba(0, 0, 0, 0.1)' }}
        >
          <div className={'modal-dialog modal-dialog-centered ' + modalSize}>
            {/* <div className="modal-content" style={{ height: height }}> */}
            <div className="modal-content" style={{ height: height, overflowY: overflowY }}>
              <div className="modal-header bg-light p-3">
                <h5 className="modal-title">{title}</h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                  id="close-modal"
                  onClick={closeModal}
                />
              </div>

              <div className="modal-body">{children}</div>
              <div className="modal-footer">{renderFooter && renderFooter()}</div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Modal;
