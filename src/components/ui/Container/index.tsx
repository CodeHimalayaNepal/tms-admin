import React, { FC } from 'react';
import SecondaryButton from '../SecondaryButton';

const Container: FC<{
  children?: React.ReactNode;
  title: string;
  OnBackClicked?: () => void;
  subTitle?: string;
}> = ({ children, title, subTitle = null, OnBackClicked }) => {
  return (
    <div className="main-content overflow-hidden">
      <div className="page-content">
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-12">
              <div className="card" style={{ borderRadius: 12 }}>
                <div className="card-header " style={{ borderRadius: 12 }}>
                  <div className="d-flex justify-content-between">
                    <h4 className="card-title mb-1 ">{title}</h4>
                    {OnBackClicked && (
                      <div className="">
                        <SecondaryButton title="Go Back" onClick={OnBackClicked} />
                      </div>
                    )}
                  </div>
                  <div>
                    {subTitle != null && (
                      <h5 className="card-title mb-0" style={{ fontSize: '12px' }}>
                        {subTitle}
                      </h5>
                    )}
                  </div>
                </div>

                <div className="card-body" style={{ borderRadius: 12 }}>
                  <div id="customerList">{children}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Container;
