import React, { Component, useCallback } from 'react';
import { EditorState, convertToRaw, ContentState, ContentBlock } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import '../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import htmlToDraft from 'html-to-draftjs';
import draftToHtml from 'draftjs-to-html';

interface IEditorComponent {
  onChangeValue: (value: string) => void;
  editorValue: string;
  containerClassName?: string;
  label: string;
  withoutMinHeight?: boolean;
  placeholder?: string;
}

interface HTMLBlock {
  contentBlocks: ContentBlock[];
  entityMap: any;
}

const EditorComponent = (props: IEditorComponent) => {
  const { onChangeValue, editorValue } = props;

  const [editorState, setEditorState] = React.useState(() => EditorState.createEmpty());
  const [rawHtmlContent, setrawHtmlContent] = React.useState('');

  const initEditorData = useCallback((htmlText: string) => {
    if (htmlText.trim().length) {
      const blocksFromHtml = htmlToDraft(htmlText) as HTMLBlock;
      const { contentBlocks, entityMap } = blocksFromHtml;
      const content = ContentState.createFromBlockArray(contentBlocks, entityMap);
      const draftContent = EditorState.createWithContent(content);
      setEditorState(draftContent);
    } else {
      const editorState = EditorState.createEmpty();
      setEditorState(editorState);
      setrawHtmlContent('');
    }
  }, []);

  const onEditorStateChange = (editorState: EditorState) => {
    const rawContentState = convertToRaw(editorState.getCurrentContent());
    let htmlContent = draftToHtml(rawContentState);

    if (!editorState.getCurrentContent().hasText()) {
      htmlContent = '';
    }

    setEditorState(editorState);
    onChangeValue(htmlContent);
    setrawHtmlContent(htmlContent);
  };

  React.useEffect(() => {
    if (editorValue !== rawHtmlContent) {
      initEditorData(editorValue);
    }
  }, [editorValue, rawHtmlContent, initEditorData]);

  return (
    <div className={props.containerClassName ?? 'mt-3 col-lg-4 col-md-6'}>
      <label htmlFor="fname-field" className="profile-details-heading">
        {props.label}
      </label>
      <div>
        <Editor
          // defaultContentState={
          //   convertToRaw(ContentState.createFromText(editorValue)) || "some ="
          // }
          editorState={editorState}
          editorStyle={{ minHeight: props.withoutMinHeight ? 160 : 300 }}
          wrapperClassName="demo-wrapper"
          editorClassName={props.withoutMinHeight ? 'demo-editor-small' : 'demo-editor'}
          onEditorStateChange={onEditorStateChange as any}
          placeholder={props.placeholder}
        />
      </div>
    </div>
  );
};

export default EditorComponent;
