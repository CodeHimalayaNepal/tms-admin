import React from 'react';
import Select from 'react-select';

const CustomSelect2 = (props: any) => {
  return (
    <div className={props.containerClassName ?? 'mt-3 col-lg-4 col-md-6'}>
      <label htmlFor="fname-field" className="form-label">
        {props.label}
      </label>
      <Select
        {...props}
        value={
          props.isMulti
            ? props?.options?.filter((item: any) => props?.value?.includes(item?.value?.toString()))
            : props?.options?.find(
                (item: any) => item.value?.toString() === props.value?.toString()
              ) ?? null
        }
        onChange={(value: any) => {
          if (props.isMulti) {
            props.onChange(value.map((item: any) => item.value));
          } else {
            props.onChange(value);
          }
        }}
        styles={{
          menu: (base) => ({
            ...base,
            zIndex: 100
          })
        }}
      />
    </div>
  );
};

export default CustomSelect2;
