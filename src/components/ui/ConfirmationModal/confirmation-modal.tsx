import React, { FC } from 'react';
import Modal from '../Modal/modal';
import PrimaryButton from '../PrimaryButton';
import SecondaryButton from '../SecondaryButton';

interface IConfirmationModal {
  children: React.ReactNode;
  isOpen: boolean;
  title: string;
  onClose: () => void;
  onConfirm: () => void;
  isLoading?: boolean;
  btnText?: string;
}
const ConfirmationModal: FC<IConfirmationModal> = ({
  children,
  isOpen,
  title,
  onClose,
  onConfirm,
  isLoading = false,
  btnText
}) => {
  return (
    <div>
      <Modal
        title={title}
        isModalOpen={isOpen}
        closeModal={onClose}
        renderFooter={() => (
          <>
            <div className="d-flex gap-2 justify-content-center mt-4 mb-2">
              <SecondaryButton onClick={onClose} title="Close" isLoading={isLoading} />
              <PrimaryButton
                onClick={onConfirm}
                title={btnText || ' Yes, Delete It!'}
                type="button"
                isDanger={!btnText}
                isLoading={isLoading}
              />
            </div>
          </>
        )}
      >
        {children}
      </Modal>
    </div>
  );
};

export default ConfirmationModal;
