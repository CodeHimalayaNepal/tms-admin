import Timeline from 'react-calendar-timeline';
// make sure you include the timeline stylesheet or the timeline will not be styled
import 'react-calendar-timeline/lib/Timeline.css';
import moment from 'moment';
import { FC } from 'react';

interface ITimelineGantt {
  groups: { id: number; title: string }[];
  items: {
    id: number;
    group: number;
    title: string;
    start_time: any;
    end_time: any;
  }[];
}

var minTime = moment().add(-6, 'months').valueOf();
var maxTime = moment().add(6, 'months').valueOf();

const TimelineGant: FC<ITimelineGantt> = ({ groups, items }) => {
  const moveResizeValidator = (action: any, item: any, time: any, resizeEdge: any) => {
    if (time < new Date().getTime()) {
      var newTime = Math.ceil(new Date().getTime() / (15 * 60 * 1000)) * (15 * 60 * 1000);
      return newTime;
    }

    return time;
  };

  const handleTimeChange = (
    visibleTimeStart: any,
    visibleTimeEnd: any,
    updateScrollCanvas: any
  ) => {
    if (visibleTimeStart < minTime && visibleTimeEnd > maxTime) {
      updateScrollCanvas(minTime, maxTime);
    } else if (visibleTimeStart < minTime) {
      updateScrollCanvas(minTime, minTime + (visibleTimeEnd - visibleTimeStart));
    } else if (visibleTimeEnd > maxTime) {
      updateScrollCanvas(maxTime - (visibleTimeEnd - visibleTimeStart), maxTime);
    } else {
      updateScrollCanvas(visibleTimeStart, visibleTimeEnd);
    }
  };
  return (
    <div>
      <Timeline
        groups={groups}
        items={items}
        sidebarWidth={150}
        stackItems={false}
        defaultTimeStart={moment().add(-12, 'hour')}
        defaultTimeEnd={moment().add(12, 'hour')}
        sidebarContent={<div>Above The Left</div>}
        moveResizeValidator={moveResizeValidator}
        itemHeightRatio={0.75}
        // onTimeChange={handleTimeChange}
      />
    </div>
  );
};

export default TimelineGant;
