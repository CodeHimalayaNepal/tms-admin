import React from 'react';

interface IActiveInactiveBadge {
  isActive?: boolean;
}
const ActiveInactiveBadge = (props: IActiveInactiveBadge) => {
  const { isActive } = props;
  return (
    <div>
      <span className={`badge badge-soft-${isActive ? 'success' : 'danger'} p-2`}>
        {isActive ? 'Verified' : 'Inactive'}
      </span>
    </div>
  );
};

export default ActiveInactiveBadge;
