import React, { ReactElement } from 'react';

interface IFormikValidationError {
  name: string;
  touched: { [key: string]: any | undefined };
  errors: { [key: string]: any | undefined };
}

interface IFormikFieldArrayValidationError extends IFormikValidationError {
  index: number;
  keyName: string;
}

export function FormikFieldArrayValidationError(
  props: IFormikFieldArrayValidationError
): ReactElement {
  const { name, touched, errors, index, keyName } = props;

  if (`${keyName}` in errors && `${keyName}` in touched && errors[keyName!][index!]) {
    return (
      <span className="text-danger">
        {' '}
        {errors[keyName!][index!][name] && <span></span>} {errors[keyName!][index!][name] as string}
      </span>
    );
  }

  return <span></span>;
}

function FormikValidationError(props: IFormikValidationError): ReactElement {
  const { name, touched, errors } = props;
  let fieldName = name.split('.');
  let fieldError = fieldName.length > 1 ? errors[fieldName[0]]?.[fieldName[1]] : errors[name];
  let fieldTouched = fieldName.length > 1 ? touched[fieldName[0]]?.[fieldName[1]] : touched[name];

  return fieldTouched && !!fieldError ? (
    <span className="text-danger"> {fieldError ? (fieldError as string) : ''}</span>
  ) : (
    <></>
  );
}
export default FormikValidationError;
