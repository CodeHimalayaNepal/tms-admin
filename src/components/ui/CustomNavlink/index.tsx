import React, { FC } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { IRoutes } from 'routes/route-lists';

interface INavLink {
  routes: IRoutes;
  toggle: () => void;
  isDivider?: boolean;
}
const CustomNavlink: FC<INavLink> = ({ routes, isDivider, toggle }) => {
  return (
    <>
      <li className="nav-item" onClick={toggle}>
        <NavLink className="nav-link menu-link " to={routes.path}>
          {routes.icon} <span data-key="t-widgets">{routes.title}</span>
        </NavLink>
        {isDivider ? (
          <>
            <hr className="bg-danger border border-1 mx-3 my-1" />{' '}
          </>
        ) : (
          ''
        )}
      </li>
    </>
  );
};

export default CustomNavlink;
