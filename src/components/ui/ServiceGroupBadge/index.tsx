import React from 'react';
interface IServiceGroupStatus {
  status: boolean;
}
const ServiceGroupBadge = (props: IServiceGroupStatus) => {
  const { status } = props;
  return (
    <span
      style={{ width: '70px', height: '20px' }}
      className={`badge rounded-pill bg-${
        status == true ? 'primary' : 'light text-dark'
      } p-2 d-flex align-items-center justify-content-center`}
    >
      {status == true ? 'Active' : 'InActive'}
    </span>
  );
};
export default ServiceGroupBadge;
