export interface BaseResponse<T> {
  meta: any;
  data: T | null;
  error: errorType | null;
}

export type errorType = {
  statusCode: number;
  message: string;
  localizedMessage: string;
  errorName: string;
  details: any;
  path: string;
  requestId: string;
  timestamp: string;
};

export interface AbstractInterface {
  id: number;

  createdAt: Date;

  updatedAt: Date;
}

export interface PaginatedDataResponse<T> {
  count: number;
  next: number | null;
  previous: number | null;
  results: T;
}

export interface Response<T> {
  [x: string]: any;
  message: string;
  status: number;
  data: T;
}
