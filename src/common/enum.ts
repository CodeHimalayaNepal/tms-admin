export enum SCREENS_ROUTES_ENUMS {
  DASHBOARD = '/',
  SIGN_IN = '/sign-in',
  REGISTER = '/register',
  FORGOT_PASSWORD = '/forgot-password',
  VERIFY_OTP = '/verify-otp',
  RESEND_VERIFY_OTP = '/resend-verify-opt',
  RESET_PASSWORD = '/reset-password/:uid/:paramToken/'
}

export enum STATUS {
  active = 'active',
  inactive = 'inactive'
}

export enum USERTYPE {
  'admin' = 1,
  'trainee' = 2
}

export enum USERSTATUS {
  'Accepted' = 1,
  'Pending' = 2,
  'Rejected' = 3
}
