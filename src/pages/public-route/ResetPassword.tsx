import * as Yup from 'yup';
import { useFormik } from 'formik';
import { tokenRef } from 'common/constant';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import { useLocalStorage } from 'hooks/useStorage';
import PrimaryButton from 'components/ui/PrimaryButton';
import FormikValidationError from 'components/ui/FormikErrors';
import {
  useGetPassResetTokenMutation,
  useLoginMutation,
  useSendNewPasswordResetMutation,
  useSendResetTokenToEmailMutation
} from 'redux/reducers/auth';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import { toast } from 'react-toastify';
import { useEffect, useState } from 'react';
import Spinner from 'components/ui/Spinner/spinner';

const resetPasswordSchema = Yup.object().shape({
  password: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Password is required'),
  passwordConfirmation: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match')
});

const ResetPassword = () => {
  let navigate = useNavigate();
  const { paramToken, uid } = useParams();
  const { value: token, setValue: setToken } = useLocalStorage(tokenRef);
  const [checkResetToken, { isLoading: resetTokenLoading, isSuccess }] =
    useGetPassResetTokenMutation();

  const [passwordToogle, setPasswordToogle] = useState('password');

  const togglePassword = () => {
    if (passwordToogle === 'password') {
      setPasswordToogle('text');
      return;
    }
    setPasswordToogle('password');
  };

  const [login, { isLoading }] = useSendResetTokenToEmailMutation();
  const formik = useFormik({
    validationSchema: resetPasswordSchema,
    initialValues: {
      passwordConfirmation: '',
      password: ''
    },
    onSubmit: (values) => {
      login({ ...values, token: paramToken, uidb64: uid })
        .unwrap()
        .then((data: any) => {
          toast.success(data.message);
          navigate(SCREENS_ROUTES_ENUMS.SIGN_IN);
        })
        .catch((err: any) => {
          if (err?.data?.email) {
            toast.error('Email is Required');
          }
          if (err?.data?.errors) {
            toast.error(Object.values(err.data.errors).join(', '));
            return;
          }
        });
    }
  });

  useEffect(() => {
    if (!!paramToken && !!uid) {
      checkResetToken({ paramToken, uid });
    }
  }, [paramToken, uid]);

  if (token) {
    return <Navigate to="/" replace={true} />;
  }

  return (
    <>
      <div className="auth-page-wrapper auth-bg-cover py-5 d-flex justify-content-center align-items-center min-vh-100">
        <div className="bg-overlay" />
        {/* auth-page content */}
        <div className="auth-page-content overflow-hidden pt-lg-5">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="card overflow-hidden">
                  <div className="row g-0">
                    <div className="col-lg-6">
                      <div className="p-lg-5 p-4 auth-one-bg h-100">
                        <div className="bg-overlay" />
                        <div className="position-relative h-100 d-flex flex-column">
                          <div className="mt-auto">
                            <div className="mb-3">
                              <i className="ri-double-quotes-l display-4 text-success" />
                            </div>
                            <div
                              id="qoutescarouselIndicators"
                              className="carousel slide"
                              data-bs-ride="carousel"
                            >
                              <div className="carousel-indicators">
                                <button
                                  type="button"
                                  data-bs-target="#qoutescarouselIndicators"
                                  data-bs-slide-to={0}
                                  className="active"
                                  aria-current="true"
                                  aria-label="Slide 1"
                                />
                                <button
                                  type="button"
                                  data-bs-target="#qoutescarouselIndicators"
                                  data-bs-slide-to={1}
                                  aria-label="Slide 2"
                                />
                                <button
                                  type="button"
                                  data-bs-target="#qoutescarouselIndicators"
                                  data-bs-slide-to={2}
                                  aria-label="Slide 3"
                                />
                              </div>
                              <div className="carousel-inner text-center text-white-50 pb-5">
                                <div className="carousel-item active">
                                  <p className="fs-15 fst-italic">
                                    " Create Assignment, Courses and Training "
                                  </p>
                                </div>
                                <div className="carousel-item">
                                  <p className="fs-15 fst-italic">
                                    " Assign Coordinator and Evaluator to the routine"
                                  </p>
                                </div>
                                <div className="carousel-item">
                                  <p className="fs-15 fst-italic">
                                    " Add trainee, coordinator and evaluator in the system "
                                  </p>
                                </div>
                              </div>
                            </div>
                            {/* end carousel */}
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* end col */}

                    <div className="col-lg-6">
                      <div className="p-lg-5 p-4">
                        <div>
                          <h5 className="text-primary">Forgot Password!</h5>
                          <p className="text-muted">
                            Reset password and continue to Training Management System.
                          </p>
                        </div>
                        <div className="mt-4">
                          {resetTokenLoading ? (
                            <Spinner />
                          ) : isSuccess ? (
                            <form className="" onSubmit={formik.handleSubmit}>
                              <div>
                                <label className="form-label">Password</label>
                                <div className="position-relative auth-pass-inputgroup">
                                  <input
                                    name="password"
                                    type={passwordToogle}
                                    className="form-control pe-5"
                                    placeholder="Enter password"
                                    id="password-input"
                                    onChange={formik.handleChange}
                                    value={formik.values.password}
                                    onBlur={formik.handleBlur}
                                  />
                                  <button
                                    className="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted"
                                    type="button"
                                    id="password-addon"
                                  >
                                    <i
                                      className="ri-eye-fill align-middle"
                                      onClick={togglePassword}
                                    ></i>
                                  </button>
                                </div>
                                <FormikValidationError
                                  name="password"
                                  errors={formik.errors}
                                  touched={formik.touched}
                                />
                                <br />

                                <label className="form-label">Re-Password</label>
                                <div className="position-relative auth-pass-inputgroup">
                                  <input
                                    name="passwordConfirmation"
                                    type={passwordToogle}
                                    className="form-control pe-5"
                                    placeholder="Enter password agian"
                                    id="password-input"
                                    onChange={formik.handleChange}
                                    value={formik.values.passwordConfirmation}
                                    onBlur={formik.handleBlur}
                                  />
                                  <button
                                    className="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted"
                                    type="button"
                                    id="password-addon"
                                  >
                                    <i
                                      className="ri-eye-fill align-middle"
                                      onClick={togglePassword}
                                    ></i>
                                  </button>
                                </div>
                              </div>

                              <FormikValidationError
                                name="passwordConfirmation"
                                errors={formik.errors}
                                touched={formik.touched}
                              />

                              <div className="hstack gap-2 justify-center mt-4">
                                <PrimaryButton
                                  title="Reset"
                                  type="submit"
                                  className="w-100"
                                  isLoading={isLoading}
                                />
                              </div>
                            </form>
                          ) : (
                            <div>Token verification failed</div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <footer className="footer">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="text-center">
                  <p className="mb-0">© All Right Reserved</p>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </>
  );
};

export default ResetPassword;
