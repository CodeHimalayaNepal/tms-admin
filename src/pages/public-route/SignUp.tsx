import { SCREENS_ROUTES_ENUMS } from 'common/enum';

import FormikValidationError from 'components/ui/FormikErrors';
import PrimaryButton from 'components/ui/PrimaryButton';
import CustomSelect from 'components/ui/Editor/CustomSelect3';
import TextInput from 'components/ui/TextInput';
import { useFormik } from 'formik';
import { useState } from 'react';

import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { POSITION } from 'react-toastify/dist/utils';
import { useSignUpMutation } from 'redux/reducers/auth';
import { useListDynamicFieldsOfTraineeQuery } from 'redux/reducers/form-fields';
import validations from 'utils/validation';
import * as Yup from 'yup';

const SignUpValidationSchema = {
  first_name: Yup.string().required('First Name is required'),
  last_name: Yup.string().required('Last Name is required'),
  citizenship_no: Yup.string().required('Citizenship no. is required'),
  middle_name: Yup.string().optional(),
  gender: Yup.string().required('Gender is required'),
  password: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Password is required'),
  confirm_password: Yup.string()
    .oneOf([Yup.ref('password'), null], "Password doesn't Match")
    .required('Required'),
  email: Yup.string().email('Invalid email').required('Email is required'),
  dob: Yup.string().required('DOB is required'),
  mobile_number: Yup.string()
    .matches(validations.phone.regex, validations.phone.message)
    .required('Mobile Number is required')
};

const option1 = [
  { value: '1', label: 'Male' },
  { value: '2', label: 'Female' }
];

const SignUp = () => {
  let navigate = useNavigate();
  const [passwordToogle, setPasswordToogle] = useState('password');
  const togglePassword = () => {
    if (passwordToogle === 'password') {
      setPasswordToogle('text');
      return;
    }
    setPasswordToogle('password');
  };

  const [cPasswordToogle, setCPasswordToogle] = useState('password');
  const cTogglePassword = () => {
    if (cPasswordToogle === 'password') {
      setCPasswordToogle('text');
      return;
    }
    setCPasswordToogle('password');
  };

  const { data: traineeDynamicFields } = useListDynamicFieldsOfTraineeQuery();
  const [signUp, { isLoading }] = useSignUpMutation();
  const dynamicFieldsValidations: any = {};
  traineeDynamicFields?.map((item: any) => {
    dynamicFieldsValidations[item.fieldname] =
      item.field_type_details.title === 'number'
        ? item.is_required
          ? Yup.number().required(`${item.fieldname} is required`)
          : Yup.number()
        : item.is_required
        ? Yup.string().required(`${item.fieldname} is required`)
        : Yup.string();
  });

  interface IInitialValue {
    email: string;
    password: string;
    confirm_password: string;
    first_name: string;
    middle_name?: string;
    last_name: string;
    mobile_number: string;
    citizenship_no: string;
    dob: string;
    gender: string;
  }

  const initialValue: IInitialValue = {
    email: '',
    password: '',
    confirm_password: '',
    first_name: '',
    middle_name: '',
    last_name: '',
    mobile_number: '',
    citizenship_no: '',
    gender: '',
    dob: ''
  };

  const formik = useFormik({
    validationSchema: Yup.object().shape({
      ...SignUpValidationSchema
      //   ...dynamicFieldsValidations
    }),
    initialValues: initialValue,
    onSubmit: (values) => {
      signUp({ ...values, middle_name: '' })
        .unwrap()
        .then(() => {
          toast.success('Registration Successful');
          navigate(SCREENS_ROUTES_ENUMS.SIGN_IN);
        })
        .catch((err: any) => {
          if (err?.data?.dob) {
            toast.error(err?.data?.dob[0]);
          }
          if (err?.data?.email) {
            toast.error(err?.data?.email[0]);
          }
        });
      //   setToken(values.email)
    }
  });
  return (
    <div className="auth-page-wrapper auth-bg-cover py-5 d-flex justify-content-center align-items-center min-vh-100">
      <div className="bg-overlay" />
      {/* auth-page content */}
      <div className="auth-page-content overflow-hidden pt-lg-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="card overflow-hidden m-0">
                <div className="row justify-content-center g-0">
                  <div className="col-lg-6">
                    <div className="p-lg-5 p-4 auth-one-bg h-100">
                      <div className="bg-overlay" />
                      <div className="position-relative h-100 d-flex flex-column">
                        <div className="mt-auto">
                          <div className="mb-3">
                            <i className="ri-double-quotes-l display-4 text-success" />
                          </div>
                          <div
                            id="qoutescarouselIndicators"
                            className="carousel slide"
                            data-bs-ride="carousel"
                          >
                            <div className="carousel-indicators">
                              <button
                                type="button"
                                data-bs-target="#qoutescarouselIndicators"
                                data-bs-slide-to={0}
                                className="active"
                                aria-current="true"
                                aria-label="Slide 1"
                              />
                              <button
                                type="button"
                                data-bs-target="#qoutescarouselIndicators"
                                data-bs-slide-to={1}
                                aria-label="Slide 2"
                              />
                              <button
                                type="button"
                                data-bs-target="#qoutescarouselIndicators"
                                data-bs-slide-to={2}
                                aria-label="Slide 3"
                              />
                            </div>
                            <div className="carousel-inner text-center text-white-50 pb-5">
                              <div className="carousel-item active">
                                <p className="fs-15 fst-italic">
                                  " Create Assignment, Courses and Training "
                                </p>
                              </div>
                              <div className="carousel-item">
                                <p className="fs-15 fst-italic">
                                  " Assign Coordinator and Evaluator to the routine"
                                </p>
                              </div>
                              <div className="carousel-item">
                                <p className="fs-15 fst-italic">
                                  " Add trainee, coordinator and evaluator in the system "
                                </p>
                              </div>
                            </div>
                          </div>
                          {/* end carousel */}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="p-lg-5 p-4">
                      <div>
                        <h5 className="text-primary">Register Account</h5>
                        <p className="text-muted">
                          Fill up the field and register your account now
                        </p>
                      </div>
                      <div className="mt-4">
                        <form onSubmit={formik.handleSubmit}>
                          <TextInput
                            label="First Name"
                            name="first_name"
                            containerClassName="mb-3"
                            type="text"
                            onChange={formik.handleChange}
                            value={formik.values.first_name}
                            onBlur={formik.handleBlur}
                            formik={formik}
                          />
                          {/* <TextInput
                            label="Middle Name"
                            name="middle_name"
                            containerClassName="mb-3"
                            type="text"
                            onChange={formik.handleChange}
                            value={formik.values.middle_name}
                            onBlur={formik.handleBlur}
                            formik={formik}
                          /> */}

                          <TextInput
                            label="Last Name"
                            name="last_name"
                            containerClassName="mb-3"
                            type="text"
                            onChange={formik.handleChange}
                            value={formik.values.last_name}
                            onBlur={formik.handleBlur}
                            formik={formik}
                          />

                          <div style={{ marginBottom: '10px' }}>
                            <CustomSelect
                              options={option1}
                              placeholder={'Select gender'}
                              value={formik.values.gender}
                              onChange={(value: any) => {
                                formik.setFieldValue('gender', value);
                              }}
                              label={'Gender'}
                            />
                          </div>
                          <TextInput
                            label="Citizenship No"
                            name="citizenship_no"
                            type="text"
                            containerClassName="mb-3"
                            onChange={formik.handleChange}
                            value={formik.values.citizenship_no}
                            onBlur={formik.handleBlur}
                            formik={formik}
                          />
                          <TextInput
                            label="DOB"
                            name="dob"
                            placeholder="YYYY-MM-DD"
                            containerClassName="mb-3"
                            type="text"
                            onChange={formik.handleChange}
                            value={formik.values.dob}
                            onBlur={formik.handleBlur}
                            formik={formik}
                          />
                          <TextInput
                            label="Email"
                            name="email"
                            type="email"
                            containerClassName="mb-3"
                            onChange={formik.handleChange}
                            value={formik.values.email}
                            onBlur={formik.handleBlur}
                            formik={formik}
                          />

                          <TextInput
                            label="Mobile No"
                            name="mobile_number"
                            type="text"
                            containerClassName="mb-3"
                            onChange={formik.handleChange}
                            value={formik.values.mobile_number}
                            onBlur={formik.handleBlur}
                            formik={formik}
                          />
                          <div className="position-relative auth-pass-inputgroup mb-3">
                            <TextInput
                              label="Password"
                              name="password"
                              type={passwordToogle}
                              containerClassName="mb-3"
                              onChange={formik.handleChange}
                              value={formik.values.password}
                              onBlur={formik.handleBlur}
                              formik={formik}
                            />
                            <button
                              className="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted"
                              type="button"
                              id="password-addon"
                            >
                              <i
                                className="ri-eye-fill align-middle"
                                onClick={togglePassword}
                                style={{ position: 'absolute', top: 35, right: 10 }}
                              ></i>
                            </button>
                          </div>

                          <div className="position-relative auth-pass-inputgroup mb-3">
                            <TextInput
                              label="Confirm Password"
                              name="confirm_password"
                              type={cPasswordToogle}
                              containerClassName="mb-3"
                              onChange={formik.handleChange}
                              value={formik.values.confirm_password}
                              onBlur={formik.handleBlur}
                              formik={formik}
                            />
                            <button
                              className="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted"
                              type="button"
                              id="password-addon"
                            >
                              <i
                                className="ri-eye-fill align-middle"
                                onClick={cTogglePassword}
                                style={{ position: 'absolute', top: 35, right: 10 }}
                              ></i>
                            </button>
                          </div>

                          {/* {traineeDynamicFields?.map((item: any) => {
                            console.log(item, 'item');
                            return (
                              <>
                                <TextInput
                                  label={item.fieldname}
                                  name={item.fieldname}
                                  type={item.field_type_details.title}
                                  containerClassName="mb-3"
                                  onChange={formik.handleChange}
                                  //@ts-ignore
                                  value={formik.values[item.fieldname]}
                                  onBlur={formik.handleBlur}
                                  formik={formik}
                                />
                              </>
                            );
                          })} */}
                          <div className="hstack gap-2 justify-center mt-4">
                            <PrimaryButton
                              isLoading={isLoading}
                              title="Register"
                              type="submit"
                              className="w-100"
                            />
                          </div>
                        </form>
                      </div>
                      <div className="mt-5 text-center">
                        <p className="mb-0">
                          Already have an account ?{' '}
                          <a
                            onClick={() => navigate(SCREENS_ROUTES_ENUMS.SIGN_IN)}
                            className="fw-semibold text-primary text-decoration-underline"
                          >
                            {' '}
                            <span style={{ cursor: 'pointer' }}> Signin </span>
                          </a>{' '}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* end card */}
            </div>
            {/* end col */}
          </div>
          {/* end row */}
        </div>
        {/* end container */}
      </div>
      {/* end auth page content */}
      {/* footer */}
      <footer className="footer">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="text-center">
                <p className="mb-0">© All Right Reserved</p>
              </div>
            </div>
          </div>
        </div>
      </footer>
      {/* end Footer */}
    </div>
  );
};

export default SignUp;
