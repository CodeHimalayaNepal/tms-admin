import * as Yup from 'yup';
import { useFormik } from 'formik';

import { useNavigate } from 'react-router-dom';

import TextInput from 'components/ui/TextInput';
import PrimaryButton from 'components/ui/PrimaryButton';

import FormikValidationError from 'components/ui/FormikErrors';
import {
  useResendVerificationEmailMutation,
  useSendNewPasswordResetMutation
} from 'redux/reducers/auth';
import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import { toast } from 'react-toastify';

const ResendVerificationEmailValidationSchema = Yup.object().shape({
  email: Yup.string().email('Invalid email').required('Email is required')
});

const ResendVerificationEmail = () => {
  let navigate = useNavigate();

  const [resendVerificationEmail, { isLoading }] = useSendNewPasswordResetMutation();
  const formik = useFormik({
    validationSchema: ResendVerificationEmailValidationSchema,
    initialValues: {
      email: ''
    },
    onSubmit: (values) => {
      resendVerificationEmail(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Verification Code sent');
          navigate(SCREENS_ROUTES_ENUMS.VERIFY_OTP);
        })
        .catch((err: any) => {
          if (err.data.email) {
            toast.error(err.data.email[0]);
            return;
          }
          toast.error('Transaction failed');
        });
    }
  });

  return (
    <>
      <div className="auth-page-wrapper auth-bg-cover py-5 d-flex justify-content-center align-items-center min-vh-100">
        <div className="bg-overlay" />
        {/* auth-page content */}
        <div className="auth-page-content overflow-hidden pt-lg-5">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="card overflow-hidden">
                  <div className="row g-0">
                    <div className="col-lg-6">
                      <div className="p-lg-5 p-4 auth-one-bg h-100">
                        <div className="bg-overlay" />
                        <div className="position-relative h-100 d-flex flex-column">
                          <div className="mt-auto">
                            <div className="mb-3">
                              <i className="ri-double-quotes-l display-4 text-success" />
                            </div>
                            <div
                              id="qoutescarouselIndicators"
                              className="carousel slide"
                              data-bs-ride="carousel"
                            >
                              <div className="carousel-indicators">
                                <button
                                  type="button"
                                  data-bs-target="#qoutescarouselIndicators"
                                  data-bs-slide-to={0}
                                  className="active"
                                  aria-current="true"
                                  aria-label="Slide 1"
                                />
                                <button
                                  type="button"
                                  data-bs-target="#qoutescarouselIndicators"
                                  data-bs-slide-to={1}
                                  aria-label="Slide 2"
                                />
                                <button
                                  type="button"
                                  data-bs-target="#qoutescarouselIndicators"
                                  data-bs-slide-to={2}
                                  aria-label="Slide 3"
                                />
                              </div>
                              <div className="carousel-inner text-center text-white-50 pb-5">
                                <div className="carousel-item active">
                                  <p className="fs-15 fst-italic">
                                    " Create Assignment, Courses and Training "
                                  </p>
                                </div>
                                <div className="carousel-item">
                                  <p className="fs-15 fst-italic">
                                    " Assign Coordinator and Evaluator to the routine"
                                  </p>
                                </div>
                                <div className="carousel-item">
                                  <p className="fs-15 fst-italic">
                                    " Add trainee, coordinator and evaluator in the system "
                                  </p>
                                </div>
                              </div>
                            </div>
                            {/* end carousel */}
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* end col */}
                    <div className="col-lg-6">
                      <div className="p-lg-5 p-4">
                        <div>
                          <h5 className="text-primary">Welcome Back !</h5>
                          <p className="text-muted">
                            Sign in and continue to Training Management System.
                          </p>
                        </div>
                        <div className="mt-4">
                          <form className="" onSubmit={formik.handleSubmit}>
                            <TextInput
                              label="Email"
                              containerClassName="mb-3"
                              name="email"
                              type="email"
                              onChange={formik.handleChange}
                              value={formik.values.email}
                              onBlur={formik.handleBlur}
                              formik={formik}
                            />

                            <div className="hstack gap-2 justify-center mt-4">
                              <PrimaryButton
                                title="Submit"
                                type="submit"
                                className="w-100"
                                isLoading={isLoading}
                              />
                            </div>
                          </form>
                        </div>
                        {/* <div className="mt-5 text-center">
                          <p className="mb-0">
                            Don't have an account ?{' '}
                            <a
                              onClick={() => navigate(SCREENS_ROUTES_ENUMS.REGISTER)}
                              className="fw-semibold text-primary text-decoration-underline pointer-event"
                            >
                              {' '}
                              Signup
                            </a>{' '}
                          </p>
                        </div> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <footer className="footer">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="text-center">
                  <p className="mb-0">© All Right Reserved</p>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </>
  );
};

export default ResendVerificationEmail;
