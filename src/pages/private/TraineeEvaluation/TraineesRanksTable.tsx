import ActiveInactiveBadge from 'components/ui/ActiveInactiveBage/ActiveInactiveBadge';
import React, { FC, useState } from 'react';
import { useListTraineeRankQuery } from 'redux/reducers/routine';
import 'css/style.css';
import DataTable2 from 'components/ui/DataTable/DataTable2';
import DataTable from 'components/ui/DataTable/data-table';
import { useParams } from 'react-router-dom';

interface ITraineeRankReport {
  tableHeaders: () => React.ReactNode;
}
const TraineesRanksTable: FC<ITraineeRankReport> = ({ tableHeaders }) => {
  const [page, setPage] = useState(1);
  const { id } = useParams();
  const routineID = id;

  const [pageSize, setPageSize] = useState(10);
  const { data: routineData, isLoading } = useListTraineeRankQuery({ page, pageSize, routineID });

  const canNext = routineData?.next;
  const canPrevious = routineData?.previous;

  const columns = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: 'Trainee Name',
      accessor: 'participation_name'
    },
    {
      Header: 'Total Marks',
      accessor: 'total_marks'
    },
    {
      Header: 'Obtain Marks',
      accessor: 'obtain_marks'
    },
    {
      Header: 'Division',
      accessor: 'division'
    },
    {
      Header: 'Percentage',
      accessor: 'percentage'
    },

    {
      Header: 'Rank',
      accessor: 'rank'
    }
  ];
  return (
    <div className="list form-check-all">
      <DataTable
        columns={columns}
        data={routineData?.data || []}
        isLoading={isLoading}
        tableHeaders={tableHeaders}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (canPrevious) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (canNext) {
              setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default TraineesRanksTable;
