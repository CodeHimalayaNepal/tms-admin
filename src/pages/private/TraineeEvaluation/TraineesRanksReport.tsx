import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import { useParams } from 'react-router-dom';
import download from 'utils/downloadFile';
import TraineesRanksTable from './TraineesRanksTable';

const TraineesRanksReport = () => {
  const { id } = useParams();

  return (
    <Container title="Trainee Rank Report">
      <TraineesRanksTable
        tableHeaders={() => (
          <div style={{ marginBottom: '15px' }}>
            <PrimaryButton
              title={'Download Rank Report'}
              iconName="align-bottom me-1"
              onClick={() => {
                download(`evaluation/download/report/${id}/`, 'Report.csv');
              }}
              type="button"
            />
          </div>
        )}
      />
    </Container>
  );
};

export default TraineesRanksReport;
