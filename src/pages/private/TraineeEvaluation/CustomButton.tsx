import { FC } from 'react';

interface IPrimaryButton {
  title: any;
  type: 'button' | 'submit';
  isOutline?: boolean;
  iconName?: string;
  onClick?: () => void;
  className?: string;
  isLoading?: boolean;
  isDanger?: boolean;
  disabled?: boolean;
}

const CustomButton: FC<IPrimaryButton> = ({
  title,
  type,
  isOutline,
  iconName,
  onClick,
  className,
  isLoading = false,
  isDanger = false,
  disabled
}) => {
  return (
    <button
      type={type}
      className={
        `btn btn-${isOutline ? 'outline-' : ''}${isDanger ? 'danger ' : 'secondary '} add-btn ` +
        className
      }
      id="create-btn"
      style={{ height: '38px' }}
      onClick={onClick}
      disabled={isLoading || disabled}
    >
      <div className="d-flex align-items-center justify-content-center">
        {iconName && <i className={`${iconName} align-bottom me-1`} />}
        {title}
        {isLoading && (
          <div className="spinner-border spinner-border-sm text-light ms-2" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        )}
      </div>
    </button>
  );
};

export default CustomButton;
