import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useListEvaluationCriteriaQuery } from 'redux/reducers/FeedbackEvaluation';
import {
  useCreateTraineeMarksMutation,
  useUpdateTraineeMarksMutation,
  useListTraineeQuery,
  useListTraineeMarksQuery,
  useListTraineeEvaluationCriteriaQuery,
  useListTraineeEvaluationCriteria2Mutation,
  useListTraineeMarkCriteriaQuery
} from 'redux/reducers/TraineeEvaluation';
import { useNavigate } from 'react-router-dom';
import CustomButton from './CustomButton';
import { resourceLimits } from 'worker_threads';
import { boolean } from 'yup';

function isNumber(str: any) {
  if (str === '') {
    return false;
  }

  return !isNaN(str);
}

const ItemField = ({ item, item3, isHover, handleAllMarks }: any) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [inputMarks, setInputMarks] = useState<number | null>(null);
  const [loadingID, setLoadingID] = useState(null);
  const [changeButton, setChangebutton] = useState<any[]>([]);
  // const [editState, setEditState] = useState<boolean | null>(false);
  // const [fetchMarksAgain, setFetchMarksAgain] = useState<boolean | null>(false);
  const [updateTraineeMarks, { isLoading: isUpdating }] = useUpdateTraineeMarksMutation();
  const [createTraineeMarks, { isLoading: isCreating }] = useCreateTraineeMarksMutation();

  return (
    <div className="col-md-4 col-sm-12 " key={item.id} style={{ marginTop: '10px' }}>
      Evaluator:
      {item.evaluator_details.first_name + ' ' + item.evaluator_details.last_name}
      <div className={'mt-2 col-lg-11 '}>
        <>
          <label
            htmlFor="fname-field"
            className="form-label"
            style={{
              textTransform: 'capitalize',
              marginBottom: '0px'
            }}
          >
            <h6>{item3.evaluation_criteria_details?.name}</h6>
          </label>
          <div style={{ display: 'flex' }}>
            <input
              required
              className="form-control"
              type="number"
              ref={inputRef}
              placeholder={
                item3.assign_marks?.length === 0 || item3.assign_marks === null
                  ? `0/${item3.max_marks}`
                  : `${item3.assign_marks[0]?.obtain_marks}/${item3.max_marks}`
              }
              style={{ marginBottom: '30px' }}
              onChange={(event) => {
                if (isNumber(event.target.value)) {
                  setInputMarks(event.target.value as any);
                }
              }}
            />
            <>
              {item3.assign_marks?.length ? (
                // {item3.assign_marks?.length || changeButton?.includes(item.id) ? (
                <CustomButton
                  isDanger={true}
                  title={
                    loadingID === item.id ? (
                      <div className="spinner-border spinner-border-sm" role="status">
                        <span className="sr-only">Loading...</span>
                      </div>
                    ) : (
                      'Edit'
                    )
                  }
                  onClick={() => {
                    // setFetchMarksAgain((prev) => !prev);
                    // setEditState((prev) => !prev);

                    // handleFetchMarks();

                    setLoadingID(item.id);
                    updateTraineeMarks({
                      newId: item3.assign_marks[0].id,
                      obtain_marks: inputMarks
                    })
                      .unwrap()

                      .then((data: any) => {
                        toast.success('Marks edited successfully');
                        handleAllMarks(inputMarks);

                        // setFetchMarksAgain((prev) => !prev);
                        // setEditState((prev) => !prev);

                        (inputRef.current as HTMLInputElement).value = '';
                      })
                      .catch((err: any) => {
                        setLoadingID(null);
                        if (err.data?.trainee) {
                          toast.error('Please select trainee');
                        }
                        if (err.data?.obtain_marks) {
                          toast.error('Please input marks');
                        }
                        if (err.data?.non_field_errors) {
                          toast.error('Obtain marks greater than full marks');
                        }
                      })
                      .finally(() => {
                        setLoadingID(null);
                      });
                  }}
                  type="button"
                ></CustomButton>
              ) : (
                <CustomButton
                  title={
                    loadingID === item.id ? (
                      <div className="spinner-border spinner-border-sm" role="status">
                        <span className="sr-only">Loading...</span>
                      </div>
                    ) : (
                      'Submit'
                    )
                  }
                  onClick={() => {
                    // handleFetchMarks();
                    setLoadingID(item.id);

                    createTraineeMarks({
                      routine_evaluator: item?.id,
                      trainee: isHover,
                      obtain_marks: inputMarks
                    })
                      .unwrap()

                      .then((data: any) => {
                        setChangebutton((prev) => [...prev, item?.id]);
                        // setFetchMarksAgain(!fetchMarksAgain);
                        // setEditState(!editState);
                        handleAllMarks(inputMarks);

                        (inputRef.current as HTMLInputElement).value = '';

                        toast.success('Marks assigned successfully');
                      })
                      .catch((err: any) => {
                        setLoadingID(null);
                        if (err.data?.non_field_errors) {
                          toast.error('Obtain marks should be below than full marks');
                        }
                        if (err.data?.trainee) {
                          toast.error('Please select trainee');
                        }
                        if (err.data?.obtain_marks) {
                          toast.error('Please input marks');
                        }
                      })
                      .finally(() => {
                        setLoadingID(null);
                      });
                  }}
                  type="button"
                />
              )}
            </>
          </div>
        </>
      </div>
    </div>
  );
};

const TraineeEvaluation = () => {
  const [editStatee, setEditStatee] = useState<any>(null);
  const [fetchMarksAgainn, setFetchMarksAgainn] = useState<any>(null);

  const { id } = useParams();
  const { data: TraineeList, isLoading: TraineeLoading } = useListTraineeQuery(id);
  const [alldata, setAllData] = useState([]);
  const [filtereddata, setFilteredData] = useState(alldata);
  const valueRef = useRef();
  const [isHover, setIsHover] = useState();
  const navigate = useNavigate();
  const { data: EvaluationQueryData, isLoading: EvaluationListLoading } =
    useListTraineeEvaluationCriteriaQuery({ id, isHover, fetchMarksAgainn });
  // useListTraineeEvaluationCriteriaQuery({ id, isHover });

  const { data: TraineeMarkData, isLoading: TraineeMarkLoading } = useListTraineeMarkCriteriaQuery({
    id,
    isHover,
    editStatee
  });
  // const [loadingID, setLoadingID] = useState(null);
  const filteredArr = EvaluationQueryData?.results?.data.reduce((acc: any, current: any) => {
    const x = acc.find(
      (item: any) =>
        item.evaluation_criteria_details.evaluation_criteria_detail.name ===
        current.evaluation_criteria_details.evaluation_criteria_detail.name
    );
    if (!x) {
      return acc.concat([current]);
    } else {
      return acc;
    }
  }, []);

  useEffect(() => {
    if (!filtereddata || !filtereddata?.length) return;
    const id = (filtereddata?.[0] as { id: any })?.id;
    valueRef.current = id;
  }, [filtereddata]);

  const handleSearch = (event: any) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    result = alldata.filter((data: any) => {
      return data.first_name.toLowerCase().search(value) != -1;
    });

    setFilteredData(result);
  };

  const Result = {
    routine_evaluator: '',
    trainee: '',
    obtain_marks: ''
  };
  // const handleFetchMarks = () => {
  //   // console.log('boolean', fetchMarks);
  //   setFetchMarksAgainn((prev) => !prev);
  // };

  const handleAllMarks = (newMark: any) => {
    setEditStatee(newMark);
    setFetchMarksAgainn(newMark);
  };
  useEffect(() => {
    setAllData(TraineeList?.results.data);
    setFilteredData(TraineeList?.results.data);
    setIsHover(TraineeList?.results?.data[0]?.id);
  }, [TraineeList]);

  return (
    <Container title="Trainee Evaluation">
      <div>
        <div className="row">
          <div className="col-lg-2 border-end mb-2" style={{ height: '80vh', overflowY: 'auto' }}>
            <div className="col-sm">
              <div className="d-flex justify-content-sm-end">
                <div className="search-box ms-2 mb-2">
                  <input
                    type="text"
                    className="form-control search"
                    placeholder="Search trainee"
                    name="key"
                    style={{ backgroundColor: '#FAFAFA' }}
                    onChange={(event) => handleSearch(event)}
                  />
                  <i className="ri-search-line search-icon" />
                </div>
              </div>
            </div>

            {TraineeLoading ? (
              <div className="d-flex align-items-center justify-content-center">
                <div className="spinner-border" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            ) : (
              filtereddata?.map((item: any) => (
                <div
                  key={item.id}
                  className={`${item.id == isHover ? 'active' : 'inactive'}`}
                  style={{
                    padding: '10px',
                    backgroundColor: item.id == isHover ? 'rgb(225 219 219)' : 'white'
                  }}
                  onClick={(e) => {
                    setIsHover(item.id);
                  }}
                >
                  <h5 style={{ textAlign: 'center', cursor: 'pointer' }}>
                    {item.first_name + ' ' + item.last_name}
                  </h5>
                </div>
              ))
            )}
          </div>

          <div className="col ">
            <div className="row">
              <div className="col-12 d-flex justify-content-end">
                <div>
                  <PrimaryButton
                    type="button"
                    title="View Report"
                    onClick={() => {
                      navigate(`/routine-management/${id}/View_Report`);
                    }}
                  />
                </div>
                <div style={{ marginLeft: '15px' }}>
                  <PrimaryButton
                    type="button"
                    title="Generate Certificate"
                    onClick={() => {
                      navigate(
                        `/routine-management/trainee-evaluation/${id}/generate-certificate/${isHover}`
                      );
                    }}
                  />
                </div>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col-md-3">
                <b>Total Marks:</b>
                <b> {TraineeMarkData?.total_marks}</b>
              </div>
              <div className="col-md-3">
                <b>Obtained Marks:</b>
                <b> {TraineeMarkData?.obtain_marks}</b>{' '}
              </div>
              <div className="col-md-3">
                <b>Division:</b>

                <b> {TraineeMarkData?.division}</b>
              </div>

              <div className="col-md-3">
                <b>Percentage:</b> <b> {TraineeMarkData?.percentage}%</b>
              </div>
            </div>
            <div className="card-body">
              {/* <h5 className="mb-4">
                <b>Total :</b>
                {initialMarks}
              </h5> */}

              <div>
                {EvaluationListLoading ? (
                  <div className="d-flex align-items-center justify-content-center">
                    <div className="spinner-border" role="status">
                      <span className="sr-only">Loading...</span>
                    </div>
                  </div>
                ) : (
                  filteredArr?.map((item: any) => (
                    <div className="row" key={item.id} style={{ display: 'flex' }}>
                      <h5>
                        <h4>
                          <b>
                            {item.evaluation_criteria_details?.evaluation_criteria_detail?.name}
                          </b>
                        </h4>

                        <div className="row-sm-12">
                          {EvaluationQueryData?.results.data.map((item3: any) =>
                            item.evaluation_criteria_details?.evaluation_criteria_detail?.name ===
                            item3.evaluation_criteria_details?.evaluation_criteria_detail?.name
                              ? item3.routine_evaluation.map((item2: any) => (
                                  <ItemField
                                    item={item2}
                                    item3={item3}
                                    isHover={isHover}
                                    // handleFetchMarks={handleFetchMarks}
                                    handleAllMarks={handleAllMarks}
                                  />
                                ))
                              : null
                          )}
                        </div>
                      </h5>
                    </div>
                  ))
                )}
              </div>
            </div>
          </div>
        </div>

        <hr />
      </div>
    </Container>
  );
};

export default TraineeEvaluation;
