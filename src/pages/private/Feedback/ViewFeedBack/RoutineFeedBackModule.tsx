import DataTable from 'components/ui/DataTable/data-table';
import React, { FC, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useRoutineWiseModuleFeedbackQuery } from 'redux/reducers/Feedback/sessionAndModuleFeedback';
import { BiLastPage } from 'react-icons/bi';
import ViewIcon from '../../../../assets/images/icons/view.png';

interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
  status: number;
}

const RoutineFeedBackSession: FC<IRoutineManagementTable> = ({ tableHeaders, status }) => {
  const navigate = useNavigate();
  const { routineId } = useParams<{ routineId: string }>();

  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data: sessionFeedBack, isLoading: isLoadingFeedbackSession } =
    useRoutineWiseModuleFeedbackQuery({ page, pageSize, routineId });
  const canNext = sessionFeedBack?.data?.next;
  const canPrevious = sessionFeedBack?.data?.previous;

  const columns = [
    {
      Header: 'Module Name',
      Cell: (data: any) => {
        return <>{`Module ${data.row.index + 1} : ${data.row.original.title}`}</>;
      }
    },
    {
      Header: 'Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={ViewIcon}
              onClick={() => {
                navigate(
                  `/routine-management/${routineId}/feedback/module/${data.row.original.course_id}/view`,
                  {
                    state: {
                      topic: `Module ${data.row.index + 1}: ${data.row.original.title}`
                    }
                  }
                );
              }}
            />
            <BiLastPage
              style={{ marginLeft: 10 }}
              onClick={() =>
                navigate(
                  `/training/routine/${routineId}/module/${data.row.original.course_id}/feedback`
                )
              }
            />
          </>
        );
      }
    }
  ];
  return (
    <>
      <div>
        <DataTable
          columns={columns}
          isLoading={isLoadingFeedbackSession}
          data={sessionFeedBack?.data?.results || []}
          tableHeaders={tableHeaders}
          pagination={{
            canNext,
            canPrevious,
            prevPage: () => {
              if (canPrevious) {
                page > 1 && setPage(page - 1);
              }
            },
            nextPage: () => {
              if (canNext) {
                setPage(page + 1);
              }
            }
          }}
        />
      </div>
    </>
  );
};

export default RoutineFeedBackSession;
