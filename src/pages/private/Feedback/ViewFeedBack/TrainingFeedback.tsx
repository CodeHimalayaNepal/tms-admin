import DataTable from 'components/ui/DataTable/data-table';
import React, { FC, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useTrainingFeedbackTableDataQuery } from 'redux/reducers/Feedback/sessionAndModuleFeedback';
import ViewIcon from '../../../../assets/images/icons/view.png';
import { BiLastPage } from 'react-icons/bi';
interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
}
const TrainingFeedback: FC<IRoutineManagementTable> = ({ tableHeaders }) => {
  const { routineId } = useParams<{ routineId: string }>();
  const navigate = useNavigate();

  const { data: trainingFeedbackTableData, isLoading: isLoadingTrainingFeedback } =
    useTrainingFeedbackTableDataQuery(routineId);

  const columns = [
    {
      Header: 'Training Name',
      accessor: 'training_name'
    },
    {
      Header: 'Action',
      accessor: (data: any) => {
        return (
          <>
            <img
              src={ViewIcon}
              onClick={() => {
                navigate(`/routine-management/${routineId}/feedback/training/${data.id}/view`);
              }}
            />
            <BiLastPage
              style={{ fontSize: '20px', marginLeft: 10 }}
              onClick={() =>
                navigate(`/training/routine/${routineId}/training/${data.id}/feedback`)
              }
            />
          </>
        );
      }
    }
  ];
  return (
    <>
      <div>
        <DataTable
          columns={columns}
          isLoading={isLoadingTrainingFeedback}
          data={
            trainingFeedbackTableData?.data[0]?.training_details
              ? [trainingFeedbackTableData?.data[0]?.training_details]
              : []
          }
          tableHeaders={tableHeaders}
        />
      </div>
    </>
  );
};

export default TrainingFeedback;
