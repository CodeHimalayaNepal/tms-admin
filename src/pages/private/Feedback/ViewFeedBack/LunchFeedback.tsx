import DataTable from 'components/ui/DataTable/data-table';
import React, { FC } from 'react';
import { BiLastPage } from 'react-icons/bi';
import { useNavigate, useParams } from 'react-router-dom';
import { useLunchFeedbackTableDataQuery } from 'redux/reducers/Feedback/sessionAndModuleFeedback';
import ViewIcon from '../../../../assets/images/icons/view.png';
interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
}
const LunchFeedback: FC<IRoutineManagementTable> = ({ tableHeaders }) => {
  const { routineId } = useParams();
  const navigate = useNavigate();

  const { data: lunchFeedbackTableData, isLoading: isLoadingLunchFeedback } =
    useLunchFeedbackTableDataQuery(routineId);

  const columns = [
    {
      Header: 'Date',
      accessor: (data: any) => {
        return data?.routine_date;
      }
    },
    {
      Header: 'Action',
      accessor: (data: any) => {
        return (
          <>
            <img
              src={ViewIcon}
              onClick={() => {
                const { routine_date } = data;
                navigate(
                  `/routine-management/${routineId}/feedback/lunch/${data.routine}/view/${routine_date}`
                );
              }}
            />
            <BiLastPage
              style={{ fontSize: '20px', marginLeft: 10 }}
              onClick={() =>
                navigate(
                  `/training/routine/${routineId}/lunch/${data.routine}/feedback/${data.routine_date}`
                )
              }
            />
          </>
        );
      }
    }
  ];
  return (
    <>
      <div>
        <DataTable
          columns={columns}
          isLoading={isLoadingLunchFeedback}
          data={lunchFeedbackTableData?.data || []}
          tableHeaders={tableHeaders}
        />
      </div>
    </>
  );
};

export default LunchFeedback;
