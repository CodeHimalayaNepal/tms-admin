import DataTable from 'components/ui/DataTable/data-table';
import React, { FC, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useRoutineWiseSessionFeedbackQuery } from 'redux/reducers/Feedback/sessionAndModuleFeedback';
import { BiLastPage } from 'react-icons/bi';
import ViewIcon from '../../../../assets/images/icons/view.png';
interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
  status: number;
}

const RoutineFeedBackSession: FC<IRoutineManagementTable> = ({
  tableHeaders,

  status
}) => {
  const navigate = useNavigate();
  const { routineId } = useParams<{ routineId: string }>();
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data: sessionFeedBack, isLoading: isLoadingFeedbackSession } =
    useRoutineWiseSessionFeedbackQuery({ page, pageSize, routineId });
  const canNext = sessionFeedBack?.data?.next;
  const canPrevious = sessionFeedBack?.data?.previous;

  const columns = [
    {
      Header: 'Session Name',
      accessor: (data: any, index: number) => {
        return `Session ${page === 1 ? index + 1 : index + (page - 1) * 10 + 1} : ${
          data?.course_topic?.title
        }`;
      }
    },
    {
      Header: 'Lecturer',
      accessor: (data: any) => {
        return (
          <>
            {data?.lecturer.map((lect: any) => (
              <div style={{ marginRight: '5px' }}>{lect}</div>
            ))}
          </>
        );
      }
    },
    {
      Header: 'Date',
      accessor: 'routine_date'
    },
    {
      Header: 'Action',
      accessor: (data: any, index: number) => {
        return (
          <>
            <img
              src={ViewIcon}
              onClick={() =>
                navigate(`/routine-management/${routineId}/feedback/session/${data.id}/view`, {
                  state: {
                    topic: `Session ${index + 1}: ${data.routine_session.session_name}`
                  }
                })
              }
            />

            <BiLastPage
              style={{ fontSize: '20px', marginLeft: 10 }}
              onClick={() => navigate(`/training/routine/${routineId}/session/${data.id}/feedback`)}
            />
          </>
        );
      }
    }
  ];
  return (
    <>
      <div>
        <DataTable
          columns={columns}
          isLoading={isLoadingFeedbackSession}
          data={sessionFeedBack?.data?.results || []}
          tableHeaders={tableHeaders}
          pagination={{
            canNext,
            canPrevious,
            prevPage: () => {
              if (canPrevious) {
                page > 1 && setPage(page - 1);
              }
            },
            nextPage: () => {
              if (canNext) {
                setPage(page + 1);
              }
            }
          }}
        />
      </div>
    </>
  );
};

export default RoutineFeedBackSession;
