import Container from 'components/ui/Container';
import { useState } from 'react';
import LunchFeedback from './LunchFeedback';
import RoutineFeedBackModule from './RoutineFeedBackModule';
import RoutineFeedBackSession from './RoutineFeedBackSession';
import TrainingFeedback from './TrainingFeedback';

const ViewFeedBack = () => {
  const [tabValue, setTabValue] = useState<number>(1);
  return (
    <Container title={'FeedBack'}>
      <div className="d-flex flex-column">
        <ul className="nav nav-pills gap-2 gap-lg-3 flex-grow-1 mb-4" role="tablist">
          <li className="nav-item" onClick={() => setTabValue(1)}>
            <a
              className={`nav-link fs-14 ${tabValue == 1 ? 'active' : ''}`}
              data-bs-toggle="tab"
              href="#overview-tab"
              role="tab"
            >
              <i className="ri-airplay-fill d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block">Session Feedback</span>
            </a>
          </li>
          <li
            onClick={() => {
              setTabValue(2);
            }}
            className="nav-item"
          >
            <a
              className={`nav-link fs-14 ${tabValue == 2 ? 'active' : ''}`}
              data-bs-toggle="tab"
              href="#completedTraining"
              role="tab"
            >
              <i className="ri-price-tag-line d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block">Module Feedback</span>
            </a>
          </li>
          <li
            onClick={() => {
              setTabValue(3);
            }}
            className="nav-item"
          >
            <a
              className={`nav-link fs-14 ${tabValue == 3 ? 'active' : ''}`}
              data-bs-toggle="tab"
              href="#completedTraining"
              role="tab"
            >
              <i className="ri-price-tag-line d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block">Lunch Feedback</span>
            </a>
          </li>
          <li
            onClick={() => {
              setTabValue(4);
            }}
            className="nav-item"
          >
            <a
              className={`nav-link fs-14 ${tabValue == 4 ? 'active' : ''}`}
              data-bs-toggle="tab"
              href="#completedTraining"
              role="tab"
            >
              <i className="ri-price-tag-line d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block">Training Feedback</span>
            </a>
          </li>
        </ul>
        {tabValue === 1 ? (
          <RoutineFeedBackSession status={tabValue} />
        ) : tabValue === 2 ? (
          <RoutineFeedBackModule status={tabValue} />
        ) : tabValue === 3 ? (
          <LunchFeedback />
        ) : (
          <TrainingFeedback />
        )}
      </div>
    </Container>
  );
};

export default ViewFeedBack;
