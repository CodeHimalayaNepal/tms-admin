import Container from 'components/ui/Container';
import useToggle from 'hooks/useToggle';
import FeedbackSubmit from './FeedbackSubmit';
import FeedbackUnsubmit from './FeedbackUnsubmit';

const ViewFeedbackSubmitTraining = () => {
  const [tabValue, setTabValue] = useToggle(false);
  return (
    <Container title={'FeedBack'}>
      <div className="d-flex flex-column">
        <ul className="nav nav-pills gap-2 gap-lg-3 flex-grow-1 mb-4" role="tablist">
          <li className="nav-item">
            <a
              className={`nav-link fs-14 ${!tabValue ? 'active' : ''}`}
              data-bs-toggle="tab"
              href="#overview-tab"
              role="tab"
            >
              <i className="ri-airplay-fill d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block" onClick={() => setTabValue()}>
                Submitted
              </span>
            </a>
          </li>
          <li
            onClick={() => {
              setTabValue(true);
            }}
            className="nav-item"
          >
            <a
              className={`nav-link fs-14 ${tabValue ? 'active' : ''}`}
              data-bs-toggle="tab"
              href="#completedTraining"
              role="tab"
            >
              <i className="ri-price-tag-line d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block" onClick={() => setTabValue()}>
                Unsubmitted
              </span>
            </a>
          </li>
        </ul>
        {!tabValue ? <FeedbackSubmit status={tabValue} /> : <FeedbackUnsubmit status={tabValue} />}
      </div>
    </Container>
  );
};

export default ViewFeedbackSubmitTraining;
