import { useState } from 'react';
import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import useToggle from 'hooks/useToggle';
import Modal from 'components/ui/Modal/modal';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import FeedbackForm from './FeedbackForm';
import { FeedbackInitialValues, FeedbackSchemaValidations } from './schema';
import {
  IFeedbackFormInput,
  useCreateFeedbackMutation,
  useDeleteFeedbackMutation,
  useUpdateFeedbackMutation
} from 'redux/reducers/Feedback2/index';
import { useNavigate } from 'react-router-dom';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import { toast } from 'react-toastify';
import FeedbackTable from './FeedbackTable';

const Feedback = () => {
  const navigate = useNavigate();

  const [feedbackModal, setFeedbackModal] = useToggle(false);
  const [feedbackInitialFormState, setFeedbackInitialForm] =
    useState<IFeedbackFormInput>(FeedbackInitialValues);

  const [deletingEvaluationState, setDeletingEvaluation] = useState<IFeedbackFormInput | null>(
    null
  );
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [deleteFeedback, { isLoading: isDeleting }] = useDeleteFeedbackMutation();
  const [createFeedback, { isLoading: isCreating }] = useCreateFeedbackMutation();
  const [updateFeedback, { isLoading: isUpdating }] = useUpdateFeedbackMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: FeedbackSchemaValidations,
    initialValues: feedbackInitialFormState,

    onSubmit: (values, { resetForm }) => {
      let executeFunc = createFeedback;
      if (values.tms_feedback_question_id) {
        executeFunc = updateFeedback;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success(' Created Successfully');
          resetForm();
          setFeedbackInitialForm(FeedbackInitialValues);
          setFeedbackModal();
        })
        .catch((err: any) => {
          if (values.tms_feedback_question_id) {
            toast.error(err.data.errors.question);
            toast.error(err.data.errors.question_np);
          } else {
            setFeedbackInitialForm(FeedbackInitialValues);
            resetForm();
            toast.error(err.data.errors.question);
            toast.error(err.data.errors.question_np);
          }
        });
    }
  });
  const onEditClicked = (data: IFeedbackFormInput) => {
    setFeedbackInitialForm(data);
    setFeedbackModal();
  };

  const onDeleteConfirm = () => {
    deletingEvaluationState &&
      deleteFeedback(deletingEvaluationState?.tms_feedback_question_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setFeedbackInitialForm(FeedbackInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IFeedbackFormInput) => {
    setDeletingEvaluation(data);
    toggleDeleteModal();
  };

  const onView = (data: any) => {
    navigate('/EvaluationDetails/EvaluationCriteriaDetails', {
      state: {
        name: data.name,
        parent_detail: data.parent_detail,
        evaluation_criteria_id: data.evaluation_criteria_id
      }
    });
  };

  return (
    <Container title="Feedback Management" subTitle="List of all feedbacks">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <div>
        <FeedbackTable
          tableHeaders={() => (
            <>
              <PrimaryButton
                title="Create Feedback"
                iconName="ri-add-line align-bottom me-1"
                onClick={() => {
                  setFeedbackModal();
                  setFeedbackInitialForm(FeedbackInitialValues);
                }}
                type="button"
              />
            </>
          )}
          onEditClicked={onEditClicked}
          onDeleteClicked={onDeleteClicked}
          onViewClicked={onView}
        />
      </div>

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={!formik.values.tms_feedback_question_id ? 'Create Feedback' : 'Edit Feedback'}
          isModalOpen={feedbackModal}
          closeModal={setFeedbackModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <PrimaryButton title="Save" type="submit" isLoading={isUpdating || isCreating} />

              <SecondaryButton
                onClick={setFeedbackModal}
                title="Discard"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <FeedbackForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default Feedback;
