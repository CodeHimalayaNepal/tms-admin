import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import EditIcon from '../../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../../assets/images/icons/delete.png';
import { useListFeedbackQuery, IFeedbackFormInput } from 'redux/reducers/Feedback2/index';
import 'css/style.css';
interface ITraineeDataTable {
  tableHeaders?: () => React.ReactNode;
  onEditClicked: (data: IFeedbackFormInput) => void;
  onDeleteClicked: (data: IFeedbackFormInput) => void;
  onViewClicked: (data: IFeedbackFormInput) => void;
}

const shortener = (string: any) => {
  if (string?.length > 50) {
    string = string.substring(0, 50) + '...';
  }
  return string;
};

const FeedbackTable: FC<ITraineeDataTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked,
  onViewClicked
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data: feedbackData, isLoading } = useListFeedbackQuery({ page, pageSize });
  const canNext = feedbackData?.data?.next;
  const canPrevious = feedbackData?.data?.previous;

  const columns: any = [
    {
      Header: ' Feedbacks in English',
      Cell: (data: any) => {
        return shortener(data.row.original.question);
      }
    },
    {
      Header: 'Feedbacks in Nepali',
      Cell: (data: any) => {
        return shortener(data.row.original.question_np);
      }
    },

    {
      Header: 'Question type',

      Cell: (data: any) => {
        return <>{data.row.original.question_type == 'm' ? 'MCQ' : 'Q&A'}</>;
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={EditIcon}
              className="icons"
              onClick={() =>
                onEditClicked({
                  ...data.row.original
                })
              }
            />
            <img
              src={DeleteIcon}
              className="m-2 icons"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div className="list form-check-all">
      <DataTable
        columns={columns}
        data={feedbackData?.data?.results || []}
        tableHeaders={tableHeaders}
        isLoading={isLoading}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (canPrevious) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (canNext) {
              setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default FeedbackTable;
