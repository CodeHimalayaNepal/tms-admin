//import { IEvaluationInuput } from 'redux/reducers/evaluation';
import { IFeedbackFormInput } from 'redux/reducers/Feedback2';
import * as Yup from 'yup';

export const FeedbackInitialValues: IFeedbackFormInput = {
  question: '',
  question_np: '',
  question_type: 'm'
};

export const FeedbackSchemaValidations = Yup.object().shape({
  question: Yup.string().required('Feedback is required'),
  question_np: Yup.string().required('Feedback in Nepali is required')
  // question_type: Yup.string().required('feedback type is required')
});
