import CustomSelect from 'components/ui/Editor/CustomSelect';
import TextInput from 'components/ui/TextInput';
import { Field, FormikProps } from 'formik';
import { FC } from 'react';
import { IFeedbackFormInput, useListFeedbackQuery } from 'redux/reducers/Feedback/index';
import { convertToNepali } from 'utils/nepali-convert';

interface IFeedbackForm {
  formik: FormikProps<IFeedbackFormInput>;
}

const FeedbackForm: FC<IFeedbackForm> = ({ formik }) => {
  const { data: question_type } = useListFeedbackQuery();
  const selectedEvaluationCriteria = question_type?.data?.map((item: any) => {
    return {
      // value: item?.id?.toString(),
      label: item?.name
    };
  });
  return (
    <form>
      <div className="row">
        {/*end col*/}
        <TextInput
          containerClassName="mt-3 "
          label="Feedback"
          name="question"
          type="text"
          height={20}
          onChange={formik.handleChange}
          value={formik.values.question}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          containerClassName="mt-3 "
          label="Feedback in nepali"
          name="question_np"
          type="text"
          height={20}
          onChange={(e) => {
            e.target.value = convertToNepali(e.target.value);
            formik.handleChange(e);
          }}
          value={formik.values.question_np}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <div className="mt-3">Feedback type</div>
        <div className="mt-3" role="group" aria-labelledby="my-radio-group">
          <label>
            <input
              className="form-check-input ms-2"
              type="radio"
              name="question_type"
              value="m"
              onChange={(event: any) => formik.setFieldValue('question_type', event.target.value)}
              onBlur={formik.handleBlur}
              checked={formik.values.question_type == 'm'}
              // defaultChecked
            />{' '}
            Multiple Choice Question
          </label>

          <label className="ms-3">
            <input
              className="form-check-input"
              type="radio"
              name="question_type"
              value="s"
              onChange={(event: any) => formik.setFieldValue('question_type', event.target.value)}
              onBlur={formik.handleBlur}
              checked={formik.values.question_type == 's'}
            />{' '}
            Question and Answer
          </label>
        </div>
      </div>
    </form>
  );
};

export default FeedbackForm;
