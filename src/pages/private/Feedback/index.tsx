import React from 'react';
import { Outlet } from 'react-router-dom';

const FeedbackManagement = () => {
  return (
    <div>
      <Outlet />
    </div>
  );
};

export default FeedbackManagement;
