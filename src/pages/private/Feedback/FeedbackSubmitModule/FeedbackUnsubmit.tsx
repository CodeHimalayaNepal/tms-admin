import DataTable from 'components/ui/DataTable/data-table';
import React, { FC, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useUnsubmittedFeedbackModuleQuery } from 'redux/reducers/Feedback/SubmitAndUnsubmitFeedbackModule';
interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
  status: boolean;
}

const RoutineFeedBackSession: FC<IRoutineManagementTable> = ({ tableHeaders, status }) => {
  const { routineId } = useParams<{ routineId: string }>();
  const { module_id } = useParams<{ module_id: string }>();

  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data: sessionFeedBack, isLoading: isLoadingFeedbackSession } =
    useUnsubmittedFeedbackModuleQuery({
      page,
      pageSize,
      routineId,
      module_id
    });
  const canNext = sessionFeedBack?.data?.next;
  const canPrevious = sessionFeedBack?.data?.previous;

  const columns = [
    {
      Header: 'First Name',
      Cell: (data: any) => {
        return <>{`${data.row.original?.first_name}`}</>;
      }
    },
    {
      Header: 'Last Name',
      Cell: (data: any) => {
        return <>{`${data.row.original?.last_name}`}</>;
      }
    },
    {
      Header: 'Mobile Number',
      Cell: (data: any) => {
        return <>{`${data.row.original?.mobile_number}`}</>;
      }
    }
  ];
  return (
    <>
      <div>
        <DataTable
          columns={columns}
          isLoading={isLoadingFeedbackSession}
          data={sessionFeedBack?.data?.results || []}
          tableHeaders={tableHeaders}
          pagination={{
            canNext,
            canPrevious,
            prevPage: () => {
              if (canPrevious) {
                page > 1 && setPage(page - 1);
              }
            },
            nextPage: () => {
              if (canNext) {
                setPage(page + 1);
              }
            }
          }}
        />
      </div>
    </>
  );
};

export default RoutineFeedBackSession;
