import React, { FC, useState } from 'react';
import DataTable from 'components/ui/DataTable/data-table';

import { useParams } from 'react-router-dom';
import { useLunchSubmittedFeedbackQuery } from 'redux/reducers/Feedback/SubmitAndUnsubmitFeedbackSession';

interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
  status: boolean;
}

const LunchFeedbackSubmitted: FC<IRoutineManagementTable> = ({ tableHeaders, status }) => {
  const { routineId, date } = useParams();
  const { data: lunchFeedbackSubmitData, isLoading } = useLunchSubmittedFeedbackQuery({
    routineId,
    date
  });
  const columns = [
    {
      Header: 'First Name',
      accessor: (data: any) => {
        return data.first_name;
      }
    },
    {
      Header: 'Last Name',
      accessor: (data: any) => {
        return data.last_name;
      }
    },
    {
      Header: 'Mobile Number',
      accessor: (data: any) => {
        return data.mobile_number;
      }
    }
  ];
  return (
    <>
      <div>
        <DataTable
          columns={columns}
          isLoading={isLoading}
          data={lunchFeedbackSubmitData?.data?.results || []}
          tableHeaders={tableHeaders}
        />
      </div>
    </>
  );
};
export default LunchFeedbackSubmitted;
