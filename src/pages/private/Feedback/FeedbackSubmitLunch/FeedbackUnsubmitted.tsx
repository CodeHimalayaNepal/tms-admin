import React, { FC, useState } from 'react';
import DataTable from 'components/ui/DataTable/data-table';

import { useParams } from 'react-router-dom';
import { useLunchUnSubmittedFeedbackQuery } from 'redux/reducers/Feedback/SubmitAndUnsubmitFeedbackSession';

interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
  status: boolean;
}
const LunchFeedbackUnsubmitted: FC<IRoutineManagementTable> = ({ tableHeaders, status }) => {
  const { routineId, date } = useParams();
  const { data: lunchFeedbackUnsubmittedData, isLoading } = useLunchUnSubmittedFeedbackQuery({
    routineId,
    date
  });
  const columns = [
    {
      Header: 'First Name',
      accessor: (data: any) => {
        return data.first_name;
      }
    },
    {
      Header: 'Last Name',
      accessor: (data: any) => {
        return data.last_name;
      }
    },
    {
      Header: 'Mobile Number',
      accessor: (data: any) => {
        return data.mobile_number;
      }
    }
  ];
  return (
    <>
      <div>
        <DataTable
          columns={columns}
          isLoading={isLoading}
          data={lunchFeedbackUnsubmittedData?.data?.results || []}
          tableHeaders={tableHeaders}
        />
      </div>
    </>
  );
};
export default LunchFeedbackUnsubmitted;
