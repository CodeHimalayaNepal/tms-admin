import { FC } from 'react';
import Container from 'components/ui/Container';

import { useParams } from 'react-router-dom';
import { useLunchFeedbackResultQuery } from 'redux/reducers/Feedback/sessionAndModuleFeedback';

interface IFeedbackResult {
  status: boolean;
}

const QuestionAndAnswers: FC<IFeedbackResult> = ({ status }) => {
  const { routineId } = useParams();
  const { date } = useParams();
  const { data: lunchData } = useLunchFeedbackResultQuery({ routineId, date });
  const shortAnswerQuestion = lunchData?.data?.filter((data: any, index: number) => {
    return data.question_type === 's';
  });
  return (
    <>
      <Container title="">
        <h4>{shortAnswerQuestion?.length ? null : 'No record'} </h4>
        {shortAnswerQuestion?.map((data: any) => {
          return (
            <>
              <p>
                <strong>{data?.question}</strong>
              </p>

              {data?.feedback_answer_data?.map((answer: { feedback_answer: string }) => {
                return <>{answer?.feedback_answer != null && <p>- {answer?.feedback_answer}</p>}</>;
              })}
            </>
          );
        })}
      </Container>
    </>
  );
};
export default QuestionAndAnswers;
