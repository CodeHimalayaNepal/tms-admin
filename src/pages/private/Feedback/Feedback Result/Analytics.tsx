import Container from 'components/ui/Container';
import React, { FC } from 'react';
import { useParams, useLocation } from 'react-router-dom';
import { useRoutineWiseSessionFeedbackCountQuery } from 'redux/reducers/Feedback/sessionAndModuleFeedback';
import get from 'lodash/get';

interface IFeedbackResult {
  status: boolean;
}
interface CustomizedState {
  topic: string;
}
const Analytics: FC<IFeedbackResult> = ({ status }) => {
  const { routineId, sessionId } = useParams<{ routineId: string; sessionId: string }>();
  let location = useLocation();
  const state = location.state as CustomizedState; // Type Casting, then you can get the params passed via router
  const { topic } = state;
  const { data: feedbackData } = useRoutineWiseSessionFeedbackCountQuery({ routineId, sessionId });
  const multipleChoiceQuestion = feedbackData?.data?.filter((data: any, index: number) => {
    return data.question_type === 'm';
  });
  const total = 40;
  return (
    <>
      {multipleChoiceQuestion?.map((item: any, index: number) => {
        return (
          // <Container title={topic}>

          <div
            style={{
              margin: '10px',
              padding: '40px',
              boxShadow: 'rgba(0, 0, 0, 0.12) 0px 1px 3px, rgba(0, 0, 0, 0.24) 0px 1px 2px'
            }}
          >
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <h3 style={{ marginBottom: '10px' }}>{item.question}</h3>
              <div>
                <h5>Average :&nbsp;{item?.question_avg}</h5>
              </div>
            </div>

            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                gap: '1rem',
                marginBottom: '8px',
                marginTop: '10px'
              }}
            >
              <div
                style={{ height: '2rem', width: '2rem', border: '1px solid black' }}
                className="rounded-circle d-flex align-items-center justify-content-center"
              >
                1
              </div>
              <div style={{ width: '800px', display: 'flex' }}>
                <h4
                  style={{
                    width: `calc((${
                      item?.feedback_data?.feedback_data.find((data: any, index: number) => {
                        return data.feedback_point === 1;
                      })?.count ?? 0
                    }/${total})*100%)`,
                    height: '25px',
                    background: 'red'
                  }}
                ></h4>
                <span style={{ marginLeft: '5px' }}>
                  {
                    item?.feedback_data?.feedback_data.find((data: any, index: number) => {
                      return data.feedback_point === 1;
                    })?.count
                  }
                </span>
              </div>
            </div>
            <div
              style={{ display: 'flex', alignItems: 'center', gap: '1rem', marginBottom: '5px' }}
            >
              <div
                style={{ height: '2rem', width: '2rem', border: '1px solid black' }}
                className="rounded-circle d-flex align-items-center justify-content-center"
              >
                2
              </div>
              <div style={{ width: '800px', display: 'flex' }}>
                <h4
                  style={{
                    width: `calc((${
                      item?.feedback_data?.feedback_data.find((data: any, index: number) => {
                        return data.feedback_point === 2;
                      })?.count ?? 0
                    }/${total})*100%)`,
                    height: '25px',
                    background: '#c16e28'
                  }}
                ></h4>
                <span style={{ marginLeft: '5px' }}>
                  {
                    item?.feedback_data?.feedback_data.find((data: any, index: number) => {
                      return data.feedback_point === 2;
                    })?.count
                  }
                </span>
              </div>
            </div>
            <div
              style={{ display: 'flex', alignItems: 'center', gap: '1rem', marginBottom: '5px' }}
            >
              <div
                style={{ height: '2rem', width: '2rem', border: '1px solid black' }}
                className="rounded-circle d-flex align-items-center justify-content-center"
              >
                3
              </div>

              <div style={{ width: '800px', display: 'flex' }}>
                <h4
                  style={{
                    width: `calc((${
                      item?.feedback_data?.feedback_data.find((data: any, index: number) => {
                        return data.feedback_point === 3;
                      })?.count ?? 0
                    }/${total})*100%)`,
                    height: '25px',
                    background: '#3180e3'
                  }}
                ></h4>

                <span style={{ marginLeft: '5px' }}>
                  {
                    item?.feedback_data?.feedback_data.find((data: any, index: number) => {
                      return data.feedback_point === 3;
                    })?.count
                  }
                </span>
              </div>
            </div>
            <div
              style={{ display: 'flex', alignItems: 'center', gap: '1rem', marginBottom: '5px' }}
            >
              <div
                style={{ height: '2rem', width: '2rem', border: '1px solid black' }}
                className="rounded-circle d-flex align-items-center justify-content-center"
              >
                4
              </div>
              <div style={{ width: '800px', display: 'flex' }}>
                <h4
                  style={{
                    width: `calc((${
                      item?.feedback_data?.feedback_data.find((data: any, index: number) => {
                        return data.feedback_point === 4;
                      })?.count ?? 0
                    }/${total})*100%)`,
                    height: '25px',
                    background: '#289f41'
                  }}
                ></h4>
                <span style={{ marginLeft: '5px' }}>
                  {
                    item?.feedback_data?.feedback_data.find((data: any, index: number) => {
                      return data.feedback_point === 4;
                    })?.count
                  }
                </span>
              </div>
            </div>
          </div>

          // </Container>
        );
      })}
    </>
  );
};

export default Analytics;
