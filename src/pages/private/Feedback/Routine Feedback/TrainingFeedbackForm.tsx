import CustomSelect from 'components/ui/Editor/CustomSelect';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { FC, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useListParentDetailsQuery } from 'redux/reducers/evaluationCriteria';
import { useListFeedbackQuery } from 'redux/reducers/Feedback2';
import { useListInviteEvaluatorQuery } from 'redux/reducers/FeedbackEvaluation';
import { IFeedBackFormInput } from 'redux/reducers/routineFeedback';

interface IEvaluationCriteriaForm {
  formik: FormikProps<IFeedBackFormInput>;
}

const TrainingFeedbackForm: FC<IEvaluationCriteriaForm> = ({ formik }) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(1000);
  const { id } = useParams();

  const { data: feedbackData } = useListFeedbackQuery({ page, pageSize });

  const selectedEvaluationCriteria = feedbackData?.data?.results.map((item: any) => {
    return {
      value: item?.tms_feedback_question_id?.toString(),
      label: item?.question
    };
  });

  useEffect(() => {
    if (id) {
      formik.setFieldValue('training_routine', id);
      formik.setFieldValue('question_for', 'training');
    }
  }, []);

  return (
    <form>
      <div className="row">
        <CustomSelect
          formik={formik}
          height={20}
          options={selectedEvaluationCriteria}
          placeholder={'Search by criteria'}
          value={formik.values.feedback_question}
          onChange={(value: any) => formik.setFieldValue('feedback_question', value)}
          label={'Select Feedback'}
          isMulti
        />
      </div>
    </form>
  );
};

export default TrainingFeedbackForm;
