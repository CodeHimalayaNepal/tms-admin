import DataTable from 'components/ui/DataTable/data-table';
import CustomDataTable from 'pages/private/FeedbackEvaluation/tables/CustomDataTable';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import React, { FC, useState } from 'react';
import { Cell } from 'react-table';
import { toast } from 'react-toastify';
import {
  ITraineeDetail,
  ITraineeStatus,
  useApproveTraineeMutation,
  useListPendingTraineeQuery,
  useListRejectedTraineeQuery,
  useRejectTraineeMutation
} from 'redux/reducers/user';
import { boolean } from 'yup';
import { useListRoutineModuleQuestionQuery } from 'redux/reducers/routineFeedback';
import { useParams } from 'react-router-dom';
interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
  // status: boolean;
}

const RoutineFeedbackTable: FC<IRoutineManagementTable> = ({ tableHeaders }) => {
  // const [userDetail, setUserDetail] = useState<ITraineeDetail | null>(null);
  // const [isReject, setIsReject] = useState<boolean>(false);

  // const { data: userList = [] } = useListPendingTraineeQuery({});
  // const { data: rejectedUserList = [] } = useListRejectedTraineeQuery({});
  // const [approveTrainee, { isLoading }] = useApproveTraineeMutation();
  // const [rejectTrainee, { isLoading: rejectLoading }] = useRejectTraineeMutation();

  // const onApproveUser = async () => {
  //   if (!userDetail) return;

  //   try {
  //     await approveTrainee({ id: userDetail.user_details.id.toString() }).unwrap();
  //     setUserDetail(null);
  //     toast.success('Trainee approved successfully!');
  //   } catch (error) {
  //     toast.error('Failed to approve trainee!');
  //   }
  // };
  // const onRejectUser = async () => {
  //   if (!userDetail) return;

  //   try {
  //     await rejectTrainee({ id: userDetail.user_details.id.toString() }).unwrap();
  //     setIsReject(false);
  //     setUserDetail(null);
  //     toast.success('Trainee rejected successfully!');
  //   } catch (error) {
  //     toast.error('Failed to reject trainee!');
  //   }
  // };
  const data = [
    {
      name: 'rishav',
      address: 'kathmandu'
    }
  ];
  const { id } = useParams();
  const { data: routinefeedbackModuleData, isLoading } = useListRoutineModuleQuestionQuery(id);

  const mappedData = routinefeedbackModuleData?.data?.results.filter((item: any, index: number) => {
    if (item.feedback_question_details.length !== 0) {
      return item.feedback_question_details;
    }
  });

  const newMappedData = mappedData?.map(function (item: any) {
    return item.feedback_question_details;
  });

  const columns = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (cell: Cell) => {
        return cell.row.index + 1;
      }
    },
    {
      Header: 'Feedback in English',
      accessor: 'question'
    },
    {
      Header: 'Feedback in Nepali',
      accessor: 'question_np'
    },

    {
      Header: 'Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={require('./View.png')}
              alt="view"
              className="m-2"
              // onClick={() => onViewClicked(data.row.original.data)}
            />
          </>
        );
      }
    }

    // {
    //   Header: 'Status',
    //   accessor: (row: ITraineeDetail) => ITraineeStatus[Number(row.user_details.status)]
    // },
    //   {
    //     Header: '  Action',
    //     Cell: (data: Cell<ITraineeDetail>) => {
    //       return +data.row.original?.user_details.status === ITraineeStatus.Pending ? (
    //         <>
    //           <PrimaryButton
    //             title="Approve"
    //             type="button"
    //             onClick={() => setUserDetail(data.row.original)}
    //           />
    //           <PrimaryButton
    //             className="ms-2"
    //             isDanger
    //             title="Reject"
    //             type="button"
    //             onClick={() => {
    //               setIsReject(true);
    //               setUserDetail(data.row.original);
    //             }}
    //           />
    //         </>
    //       ) : +data.row.original?.user_details.status === ITraineeStatus.Rejected ? (
    //         <PrimaryButton
    //           title="Approve"
    //           type="button"
    //           onClick={() => setUserDetail(data.row.original)}
    //         />
    //       ) : (
    //         ''
    //       );
    //     }
    //   }
  ];
  return (
    <>
      <div>
        <CustomDataTable
          columns={columns}
          isLoading={isLoading}
          // data={Array.isArray(newMappedData?.[0]) ? newMappedData[0] : []}
          data={
            (!!routinefeedbackModuleData?.data?.results.length &&
              routinefeedbackModuleData?.data?.results?.[0]?.feedback_question_details) ||
            []
          }
          tableHeaders={tableHeaders}
        />
      </div>
    </>
  );
};

export default RoutineFeedbackTable;
