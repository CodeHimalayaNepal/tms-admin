import DataTable from 'components/ui/DataTable/data-table';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import React, { FC, useState } from 'react';
import { Cell } from 'react-table';
import { toast } from 'react-toastify';
import {
  ITraineeDetail,
  ITraineeStatus,
  useApproveTraineeMutation,
  useListPendingTraineeQuery,
  useListRejectedTraineeQuery,
  useRejectTraineeMutation
} from 'redux/reducers/user';
import { boolean } from 'yup';
interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
  status: boolean;
}

const RoutineFeedbackTable: FC<IRoutineManagementTable> = ({ tableHeaders, status }) => {
  // const [userDetail, setUserDetail] = useState<ITraineeDetail | null>(null);
  // const [isReject, setIsReject] = useState<boolean>(false);

  // const { data: userList = [] } = useListPendingTraineeQuery({});
  // const { data: rejectedUserList = [] } = useListRejectedTraineeQuery({});
  // const [approveTrainee, { isLoading }] = useApproveTraineeMutation();
  // const [rejectTrainee, { isLoading: rejectLoading }] = useRejectTraineeMutation();

  // const onApproveUser = async () => {
  //   if (!userDetail) return;

  //   try {
  //     await approveTrainee({ id: userDetail.user_details.id.toString() }).unwrap();
  //     setUserDetail(null);
  //     toast.success('Trainee approved successfully!');
  //   } catch (error) {
  //     toast.error('Failed to approve trainee!');
  //   }
  // };
  // const onRejectUser = async () => {
  //   if (!userDetail) return;

  //   try {
  //     await rejectTrainee({ id: userDetail.user_details.id.toString() }).unwrap();
  //     setIsReject(false);
  //     setUserDetail(null);
  //     toast.success('Trainee rejected successfully!');
  //   } catch (error) {
  //     toast.error('Failed to reject trainee!');
  //   }
  // };
  const data = [
    {
      name: 'rishav',
      address: 'kathmandu'
    }
  ];

  const columns = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (cell: Cell) => {
        return cell.row.index + 1;
      }
    },
    {
      Header: ' Module Name',
      accessor: 'module_name'
    },
    {
      Header: ' Date',
      accessor: 'date'
    },

    {
      Header: 'Action',
      Cell: (data: any) => {
        return (
          <>
            <i className="bi bi-eye"></i>
          </>
        );
      }
    }

    // {
    //   Header: 'Status',
    //   accessor: (row: ITraineeDetail) => ITraineeStatus[Number(row.user_details.status)]
    // },
    //   {
    //     Header: '  Action',
    //     Cell: (data: Cell<ITraineeDetail>) => {
    //       return +data.row.original?.user_details.status === ITraineeStatus.Pending ? (
    //         <>
    //           <PrimaryButton
    //             title="Approve"
    //             type="button"
    //             onClick={() => setUserDetail(data.row.original)}
    //           />
    //           <PrimaryButton
    //             className="ms-2"
    //             isDanger
    //             title="Reject"
    //             type="button"
    //             onClick={() => {
    //               setIsReject(true);
    //               setUserDetail(data.row.original);
    //             }}
    //           />
    //         </>
    //       ) : +data.row.original?.user_details.status === ITraineeStatus.Rejected ? (
    //         <PrimaryButton
    //           title="Approve"
    //           type="button"
    //           onClick={() => setUserDetail(data.row.original)}
    //         />
    //       ) : (
    //         ''
    //       );
    //     }
    //   }
  ];

  return (
    <>
      <div>
        <DataTable columns={columns} data={data} tableHeaders={tableHeaders} />
      </div>
    </>
  );
};

export default RoutineFeedbackTable;
