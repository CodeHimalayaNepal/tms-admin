import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import React, { useEffect, useState } from 'react';
import { IFeedBackFormInput, useCreateFeedbackMSMutation } from 'redux/reducers/routineFeedback';
import RoutineFeedbackForm from './RoutineFeedbackForm2';
import RoutineFeedbackSessionTable from './RoutineFeedbackSessionTable';
import RoutineFeedbackTable from './RoutineFeedbackModulTable';
import { FeedBackInitialValues, FeedBackSchemaValidations } from './schema';
import { useParams, useSearchParams } from 'react-router-dom';
import RoutineModuleFeedbackForm from './RoutinrModuleFeedbackForm';
import { toast } from 'react-toastify';
import LunchFeedbackTable from './LunchFeedbackTable';
import TrainingFeedbackTable from './TrainingFeedbackTable';
import LunchFeedbackForm from './LunchFeedbackForm';
import TrainingFeedbackForm from './TrainingFeedbackForm';

const RoutineFeedback = () => {
  const params = useParams();
  var tab = params?.tab;

  const [tabValue, setTabValue] = useState<string | number>(tab || 0);
  const [feedbackModal, setFeedbackModal] = useToggle(false);
  const [feedbackModuleModal, setFeedbackModuleModal] = useToggle(false);
  const [lunchFeedbackModal, setLunchFeedbackModal] = useToggle(false);
  const [trainingFeedbackModal, setTrainingFeedbackModal] = useToggle(false);

  const [feedbackInitialFormState, setFeedbackInitialForm] =
    useState<IFeedBackFormInput>(FeedBackInitialValues);

  const [createFeedback, { isLoading: isCreating }] = useCreateFeedbackMSMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: FeedBackSchemaValidations,
    initialValues: feedbackInitialFormState,

    onSubmit: (values, { resetForm }) => {
      let executeFunc = createFeedback;
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          resetForm();
          setFeedbackInitialForm(FeedBackInitialValues);
          setFeedbackModal();
        })
        .catch((err: any) => {
          resetForm();
          toast.error(err.data.errors.question);
          toast.error(err.data.errors.question_np);
        });
    }
  });

  const secondformik = useFormik({
    enableReinitialize: true,
    validationSchema: FeedBackSchemaValidations,
    initialValues: feedbackInitialFormState,

    onSubmit: (values, { resetForm }) => {
      let executeFunc = createFeedback;
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          resetForm();
          setFeedbackInitialForm(FeedBackInitialValues);
          setFeedbackModuleModal();
        })
        .catch((err: any) => {
          resetForm();
          toast.error(err.data.errors.question);
          toast.error(err.data.errors.question_np);
        });
    }
  });

  const thirdformik = useFormik({
    enableReinitialize: true,
    validationSchema: FeedBackSchemaValidations,
    initialValues: feedbackInitialFormState,

    onSubmit: (values, { resetForm }) => {
      let executeFunc = createFeedback;
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          resetForm();
          setFeedbackInitialForm(FeedBackInitialValues);
          setLunchFeedbackModal();
        })
        .catch((err: any) => {
          resetForm();
          toast.error(err.data.errors.question);
          toast.error(err.data.errors.question_np);
        });
    }
  });

  const fourthformik = useFormik({
    enableReinitialize: true,
    validationSchema: FeedBackSchemaValidations,
    initialValues: feedbackInitialFormState,

    onSubmit: (values, { resetForm }) => {
      let executeFunc = createFeedback;
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          resetForm();
          setFeedbackInitialForm(FeedBackInitialValues);
          setTrainingFeedbackModal();
        })
        .catch((err: any) => {
          resetForm();
          toast.error(err.data.errors.question);
          toast.error(err.data.errors.question_np);
        });
    }
  });

  return (
    <Container title="Routine Feedback">
      <div className="d-flex flex-column">
        <ul className="nav nav-pills gap-2 gap-lg-3 flex-grow-1 mb-4" role="tablist">
          <li className="nav-item">
            <a
              className={`nav-link fs-14 ${tabValue == 0 ? 'active' : ''}`}
              data-bs-toggle="tab"
              href="#overview-tab"
              role="tab"
            >
              <i className="ri-airplay-fill d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block" onClick={() => setTabValue(0)}>
                Module Feedback
              </span>
            </a>
          </li>

          {/* <li className="nav-item">
            <a className="nav-link fs-14" data-bs-toggle="tab" href="#attendance" role="tab">
              <i className="ri-list-unordered d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block">Approved</span>
            </a>
          </li> */}
          <li
            onClick={() => {
              setTabValue(1);
            }}
            className="nav-item"
          >
            <a
              className={`nav-link fs-14 ${tabValue == 1 ? 'active' : ''}`}
              data-bs-toggle="tab"
              href="#completedTraining"
              role="tab"
            >
              <i className="ri-price-tag-line d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block">Session Feedback</span>
            </a>
          </li>
          <li
            onClick={() => {
              setTabValue(2);
            }}
            className="nav-item"
          >
            <a
              className={`nav-link fs-14 ${tabValue == 2 ? 'active' : ''}`}
              data-bs-toggle="tab"
              href="#completedTraining"
              role="tab"
            >
              <i className="ri-price-tag-line d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block">Lunch Feedback</span>
            </a>
          </li>
          <li
            onClick={() => {
              setTabValue(3);
            }}
            className="nav-item"
          >
            <a
              className={`nav-link fs-14 ${tabValue == 3 ? 'active' : ''}`}
              data-bs-toggle="tab"
              href="#completedTraining"
              role="tab"
            >
              <i className="ri-price-tag-line d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block">Training Feedback</span>
            </a>
          </li>
        </ul>
        {tabValue == 0 ? (
          <RoutineFeedbackTable
            tableHeaders={() => (
              <>
                <PrimaryButton
                  title="Add Feedback"
                  iconName="ri-add-line align-bottom me-1"
                  onClick={() => {
                    setFeedbackModuleModal();
                    // setFeedbackInitialForm(FeedbackInitialValues);
                  }}
                  type="button"
                />
              </>
            )}
          />
        ) : tabValue == 1 ? (
          <RoutineFeedbackSessionTable
            tableHeaders={() => (
              <>
                <PrimaryButton
                  title="Add Feedback"
                  iconName="ri-add-line align-bottom me-1"
                  onClick={() => {
                    setFeedbackModal();
                    // setFeedbackInitialForm(FeedbackInitialValues);
                  }}
                  type="button"
                />
              </>
            )}
          />
        ) : tabValue == 2 ? (
          <LunchFeedbackTable
            tableHeaders={() => (
              <>
                <PrimaryButton
                  title="Add Feedback"
                  iconName="ri-add-line align-bottom me-1"
                  onClick={() => {
                    setLunchFeedbackModal();
                  }}
                  type="button"
                />
              </>
            )}
          />
        ) : (
          <TrainingFeedbackTable
            tableHeaders={() => (
              <>
                <PrimaryButton
                  title="Add Feedback"
                  iconName="ri-add-line align-bottom me-1"
                  onClick={() => {
                    setTrainingFeedbackModal();
                  }}
                  type="button"
                />
              </>
            )}
          />
        )}
      </div>
      <form onSubmit={formik.handleSubmit}>
        <Modal
          title="Add Feedback in Session"
          isModalOpen={feedbackModal}
          closeModal={setFeedbackModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <PrimaryButton title="Save" type="submit" />

              <SecondaryButton onClick={setFeedbackModal} title="Discard" />
            </div>
          )}
        >
          <RoutineFeedbackForm formik={formik} />
        </Modal>
      </form>
      <form onSubmit={secondformik.handleSubmit}>
        <Modal
          title="Add Feedback in Module "
          isModalOpen={feedbackModuleModal}
          closeModal={setFeedbackModuleModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <PrimaryButton title="Save" type="submit" />

              <SecondaryButton onClick={setFeedbackModuleModal} title="Discard" />
            </div>
          )}
        >
          <RoutineModuleFeedbackForm formik={secondformik} />
        </Modal>
      </form>
      <form onSubmit={thirdformik.handleSubmit}>
        <Modal
          title="Add Lunch Feedback"
          isModalOpen={lunchFeedbackModal}
          closeModal={setLunchFeedbackModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <PrimaryButton title="Save" type="submit" />

              <SecondaryButton onClick={setLunchFeedbackModal} title="Discard" />
            </div>
          )}
        >
          <LunchFeedbackForm formik={thirdformik} />
        </Modal>
      </form>
      <form onSubmit={fourthformik.handleSubmit}>
        <Modal
          title="Add Training Feedback"
          isModalOpen={trainingFeedbackModal}
          closeModal={setTrainingFeedbackModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <PrimaryButton title="Save" type="submit" />

              <SecondaryButton onClick={setTrainingFeedbackModal} title="Discard" />
            </div>
          )}
        >
          <TrainingFeedbackForm formik={fourthformik} />
        </Modal>
      </form>
    </Container>
  );
};

export default RoutineFeedback;
