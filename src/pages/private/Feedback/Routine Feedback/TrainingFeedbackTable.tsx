import { FC } from 'react';
import { Cell } from 'react-table';
import CustomDataTable from 'pages/private/FeedbackEvaluation/tables/CustomDataTable2';
import { useParams } from 'react-router-dom';
import { useListRoutineTrainingQuestionQuery } from 'redux/reducers/routineFeedback';

interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
}
const TrainingFeedbackTable: FC<IRoutineManagementTable> = ({ tableHeaders }) => {
  const { id } = useParams();

  const { data: trainingQuestionData, isLoading } = useListRoutineTrainingQuestionQuery(id);

  const columns = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (cell: Cell) => {
        return cell.row.index + 1;
      }
    },
    {
      Header: 'Feedback in English',
      accessor: 'question'
    },
    {
      Header: 'Feedback in Nepali',
      accessor: 'question_np'
    },

    {
      Header: 'Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={require('./View.png')}
              alt="view"
              className="m-2"
              // onClick={() => onViewClicked(data.row.original.data)}
            />
          </>
        );
      }
    }
  ];
  return (
    <>
      <CustomDataTable
        tableHeaders={tableHeaders}
        columns={columns}
        data={
          (!!trainingQuestionData?.data?.results.length &&
            trainingQuestionData?.data?.results?.[0]?.feedback_question_details) ||
          []
        }
      />
    </>
  );
};

export default TrainingFeedbackTable;
