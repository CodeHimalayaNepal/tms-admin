import DataTable from 'components/ui/DataTable/data-table';
import CustomDataTable from 'pages/private/FeedbackEvaluation/tables/CustomDataTable2';
import React, { FC, useState } from 'react';
import { Cell } from 'react-table';
import { useParams } from 'react-router-dom';
import { useListRoutineSessionQuestionQuery } from 'redux/reducers/routineFeedback';
interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
  // status: boolean;
}

const RoutineFeedbackSessionTable: FC<IRoutineManagementTable> = ({ tableHeaders }) => {
  const data = [
    {
      name: 'rishav',
      address: 'kathmandu'
    }
  ];
  const { id } = useParams();
  const { data: routinefeedbackData, isLoading } = useListRoutineSessionQuestionQuery(id);
  const mappedData = routinefeedbackData?.data?.results.filter((item: any, index: number) => {
    if (item.feedback_question_details.length !== 0) {
      return item.feedback_question_details;
    }
  });

  const newMappedData = mappedData?.map(function (item: any) {
    return item.feedback_question_details;
  });
  const columns = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (cell: Cell) => {
        return cell.row.index + 1;
      }
    },
    {
      Header: 'Feedback in English',
      accessor: 'question'
    },
    {
      Header: 'Feedback in Nepali',
      accessor: 'question_np'
    },

    {
      Header: 'Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={require('./View.png')}
              alt="view"
              className="m-2"
              // onClick={() => onViewClicked(data.row.original.data)}
            />
          </>
        );
      }
    }
  ];

  return (
    <>
      <div>
        <CustomDataTable
          columns={columns}
          isLoading={isLoading}
          data={
            (!!routinefeedbackData?.data?.results.length &&
              routinefeedbackData?.data?.results?.[0]?.feedback_question_details) ||
            []
          }
          tableHeaders={tableHeaders}
        />
      </div>
    </>
  );
};

export default RoutineFeedbackSessionTable;
