import { IFeedBackFormInput } from 'redux/reducers/routineFeedback';
import * as Yup from 'yup';

export const FeedBackInitialValues: IFeedBackFormInput = {
  feedback_question: '',
  question_for: '',
  training_routine: 0
};

export const FeedBackSchemaValidations = Yup.object().shape({
  feedback_question: Yup.array().min(1, 'Feedback is required')
});
