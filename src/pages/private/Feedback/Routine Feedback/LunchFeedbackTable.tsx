import { FC } from 'react';
import CustomDataTable from 'pages/private/FeedbackEvaluation/tables/CustomDataTable2';
import { Cell } from 'react-table';
import { useListRoutineLunchQuestionQuery } from 'redux/reducers/routineFeedback';
import { useParams } from 'react-router-dom';

interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
}

const LunchFeedbackTable: FC<IRoutineManagementTable> = ({ tableHeaders }) => {
  const { id } = useParams();

  const { data: lunchQuestionData, isLoading } = useListRoutineLunchQuestionQuery(id);

  const columns = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (cell: Cell) => {
        return cell.row.index + 1;
      }
    },
    {
      Header: 'Feedback in English',
      accessor: 'question'
    },
    {
      Header: 'Feedback in Nepali',
      accessor: 'question_np'
    },

    {
      Header: 'Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={require('./View.png')}
              alt="view"
              className="m-2"
              // onClick={() => onViewClicked(data.row.original.data)}
            />
          </>
        );
      }
    }
  ];

  return (
    <>
      <CustomDataTable
        tableHeaders={tableHeaders}
        columns={columns}
        data={
          (!!lunchQuestionData?.data?.results.length &&
            lunchQuestionData?.data?.results?.[0]?.feedback_question_details) ||
          []
        }
      />
    </>
  );
};

export default LunchFeedbackTable;
