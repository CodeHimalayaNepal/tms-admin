import Container from 'components/ui/Container';
import React, { FC } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { useTrainingFeedbackResultQuery } from 'redux/reducers/Feedback/sessionAndModuleFeedback';
interface IFeedbackResult {
  status: boolean;
}
const QuestionsAndAnswers: FC<IFeedbackResult> = ({ status }) => {
  const { routineId, trainingId } = useParams<{ routineId: string; trainingId: string }>();
  let location = useLocation();
  const { data: feedbackData } = useTrainingFeedbackResultQuery({ routineId, trainingId });
  const shortAnswerQuestion = feedbackData?.data?.filter((data: any, index: number) => {
    return data.question_type === 's';
  });

  return (
    <>
      <Container title="">
        <h4>{shortAnswerQuestion?.length ? null : 'No record'} </h4>
        {shortAnswerQuestion?.map((data: any) => {
          return (
            <>
              <p>
                <strong>{data?.question}</strong>
              </p>

              {data?.feedback_answer_data?.map((answer: { feedback_answer: string }) => {
                return (
                  <>
                    <p>- {answer?.feedback_answer}</p>
                  </>
                );
              })}
            </>
          );
        })}
      </Container>
    </>
  );
};

export default QuestionsAndAnswers;
