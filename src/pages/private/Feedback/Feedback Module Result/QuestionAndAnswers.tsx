import Container from 'components/ui/Container';
import React, { FC } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { useRoutineWiseModuleFeedbackCountQuery } from 'redux/reducers/Feedback/sessionAndModuleFeedback';
interface IFeedbackResult {
  status: boolean;
}
interface CustomizedState {
  topic: string;
}
const QuestionsAndAnswers: FC<IFeedbackResult> = ({ status }) => {
  const { routineId, moduleId } = useParams<{ routineId: string; moduleId: string }>();
  let location = useLocation();
  const state = location.state as CustomizedState; // Type Casting, then you can get the params passed via router
  const { topic } = state;
  const { data: feedbackData } = useRoutineWiseModuleFeedbackCountQuery({ routineId, moduleId });
  const shortAnswerQuestion = feedbackData?.data?.filter((data: any, index: number) => {
    return data.question_type === 's';
  });

  console.log(shortAnswerQuestion, 'shortAnswerssss');
  return (
    <>
      {/* <Container title="Questions And Answers"> */}
      <div>
        {shortAnswerQuestion?.map((data: any) => {
          return (
            <>
              <div
                style={{
                  margin: '10px',
                  height: '300px',
                  overflow: 'scroll',
                  boxShadow: 'rgba(0, 0, 0, 0.12) 0px 1px 3px, rgba(0, 0, 0, 0.24) 0px 1px 2px'
                }}
              >
                <h3 style={{ position: 'sticky', top: '0', background: '#fff', padding: '20px' }}>
                  {data?.question}
                </h3>
                {data?.feedback_answer_data?.feedback_answer_data?.map(
                  (answer: { feedback_answer: string }) => {
                    return (
                      <>
                        <p>- {answer?.feedback_answer}</p>
                      </>
                    );
                  }
                )}
              </div>
            </>
          );
        })}
      </div>
      {/* </Container> */}
    </>
  );
};

export default QuestionsAndAnswers;
