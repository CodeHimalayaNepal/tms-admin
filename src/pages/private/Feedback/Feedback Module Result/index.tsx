import Container from 'components/ui/Container';
import React, { useState } from 'react';
import Analytics from './Analytics';
import QuestionsAndAnswers from './QuestionAndAnswers';

const FeedbackModuleResult = () => {
  const [tabValue, setTabValue] = useState<boolean>(true);
  return (
    <Container title="Feedback result">
      <div className="d-flex flex-column">
        <ul className="nav nav-pills gap-2 gap-lg-3 flex-grow-1 mb-4" role="tablist">
          <li className="nav-item">
            <a
              className="nav-link fs-14 active"
              data-bs-toggle="tab"
              href="#overview-tab"
              role="tab"
            >
              <i className="ri-airplay-fill d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block" onClick={() => setTabValue(true)}>
                Analytics
              </span>
            </a>
          </li>

          {/* <li className="nav-item">
            <a className="nav-link fs-14" data-bs-toggle="tab" href="#attendance" role="tab">
              <i className="ri-list-unordered d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block">Approved</span>
            </a>
          </li> */}

          <li
            onClick={() => {
              setTabValue(false);
            }}
            className="nav-item"
          >
            <a className="nav-link fs-14" data-bs-toggle="tab" href="#completedTraining" role="tab">
              <i className="ri-price-tag-line d-inline-block d-md-none"></i>{' '}
              <span className="d-none d-md-inline-block">Questions And Answers</span>
            </a>
          </li>
        </ul>
        {tabValue ? <Analytics status={tabValue} /> : <QuestionsAndAnswers status={false} />}
      </div>
    </Container>
  );
};

export default FeedbackModuleResult;
