import DataTable from 'components/ui/DataTable/data-table';
import React, { FC, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useUnsubmittedFeedbackQuery } from 'redux/reducers/Feedback/SubmitAndUnsubmitFeedbackSession';

interface IRoutineManagementTable {
  tableHeaders?: () => React.ReactNode;
  status: boolean;
}

const RoutineFeedBackSession: FC<IRoutineManagementTable> = ({ tableHeaders }) => {
  const { routineId } = useParams<{ routineId: string }>();
  const { sessionId } = useParams<{ sessionId: string }>();

  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data: sessionFeedBack, isLoading: isLoadingFeedbackSession } =
    useUnsubmittedFeedbackQuery({
      page,
      pageSize,
      routineId,
      sessionId
    });
  const canNext = sessionFeedBack?.data?.next;
  const canPrevious = sessionFeedBack?.data?.previous;

  const columns = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: 'First Name',
      Cell: (data: any) => {
        return <>{`${data.row.original?.first_name}`}</>;
      }
    },
    {
      Header: 'Last Name',
      Cell: (data: any) => {
        return <>{`${data.row.original?.last_name}`}</>;
      }
    },
    {
      Header: 'Mobile Number',
      Cell: (data: any) => {
        return <>{`${data.row.original?.mobile_number}`}</>;
      }
    }
  ];
  return (
    <>
      <div>
        <DataTable
          columns={columns}
          isLoading={isLoadingFeedbackSession}
          data={sessionFeedBack?.data?.results || []}
          tableHeaders={tableHeaders}
          pagination={{
            canNext,
            canPrevious,
            prevPage: () => {
              if (canPrevious) {
                page > 1 && setPage(page - 1);
              }
            },
            nextPage: () => {
              if (canNext) {
                setPage(page + 1);
              }
            }
          }}
        />
      </div>
    </>
  );
};

export default RoutineFeedBackSession;
