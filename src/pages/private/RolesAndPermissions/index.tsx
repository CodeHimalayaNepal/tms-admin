import React from 'react';
import { Outlet } from 'react-router-dom';
import SideBar from '../SideBar';

const RolesAndPermissions = () => {
  return (
    <div>
      <Outlet />
    </div>
  );
};

export default RolesAndPermissions;
