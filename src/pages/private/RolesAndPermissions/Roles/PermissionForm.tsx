import DataTable2 from 'components/ui/DataTable/DataTable2';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { Form, Formik, FormikProps } from 'formik';
import React, { FC, useState } from 'react';
import { toast } from 'react-toastify';
import { feedbackevaluationCriteriaApi } from 'redux/reducers/FeedbackEvaluation';
import {
  useUpdateRolePermissionMutation,
  useGetRoleDetailsMutation,
  useListModuleQuery,
  useGetRolePermissionQuery
} from 'redux/reducers/roles';

interface IPermissionForm {
  formModal: boolean;
  setFormModal: () => void;
  role: any;
}
const random = [
  {
    module_name: 'Routine Assign Certificate',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Routine Evaluation Management',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Routine Module Feedback',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Routine Session Feedback',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Routine Feedback Form',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Routine Module Complete',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Routine Training Session',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Routine Trainee Enroll',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Routine Evaluation Criteria',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Certificate Template',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Organization Center',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Organization Department',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Organization',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Evaluation Criteria',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Feedback Management',
    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Roles and Permissions',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Hall Management',
    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Routine Management',
    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Training Management',
    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Modules And Session',
    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Module Category',
    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },

  {
    module_name: 'Attendance Management',

    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Attendance Request',
    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Trainee Management',
    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Trainee Approval',
    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'User Management',
    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  },
  {
    module_name: 'Dashboard',
    module_id: 0,
    can_view: 0,
    can_add: 0,
    can_update: 0,
    can_delete: 0
  }
];

const PermissionForm: FC<IPermissionForm> = ({ formModal, setFormModal, role }) => {
  const [updateRolePermission, { isLoading: isCreating }] = useUpdateRolePermissionMutation();
  const { data } = useListModuleQuery(role.id);
  const [updatedValue, setUpdatedValue] = useState<any>([random]);
  const { data: feedbackData, isLoading, isFetching, status } = useGetRolePermissionQuery(role.id);

  React.useEffect(() => {
    if (feedbackData?.data?.menus || formModal === true) {
      const PermissionOfThisModule = feedbackData?.data?.menus.map((item: any) => {
        return {
          module_name: item.module_name,

          module_id: item?.module_id,
          can_view: item?.can_view === true ? 1 : 0,
          can_add: item?.can_add === true ? 1 : 0,
          can_update: item?.can_update === true ? 1 : 0,
          can_delete: item?.can_delete === true ? 1 : 0
        };
      });

      setUpdatedValue(
        random.map(
          (item) =>
            PermissionOfThisModule?.find((i: any) => i.module_name === item.module_name) ?? item
        )
      );
    }
  }, [feedbackData, formModal]);

  const onSubmitForm = (values: any) => {
    const postData2 = updatedValue
      .filter((item: any) => item?.module_id !== undefined)
      .filter((item: any) => item?.module_id !== 0)
      .map((item: any) => {
        return {
          module_id: item?.module_id,
          can_view: item?.can_view,
          can_add: item?.can_add,
          can_update: item?.can_update,
          can_delete: item?.can_delete
        };
      });

    const updatedData = { role_id: role.id, modules: postData2 };

    updateRolePermission(updatedData)
      .unwrap()
      .then(() => {
        setFormModal();
        toast.success('Updated Permissions Successfully');
        setUpdatedValue([random]);
      })
      .catch((err: any) => {
        toast.error('Failed to update Permissions');
      });
  };
  const handleCheckbox = (name: any) => () => {
    const item: any = updatedValue.find((item: any) => item.module_name === name.name);
    {
      item.can_view == 1
        ? setUpdatedValue([...updatedValue, ((item.can_view = 0), (item.module_id = name.id))])
        : setUpdatedValue([...updatedValue, ((item.can_view = 1), (item.module_id = name.id))]);
    }
  };
  const handleCheckbox2 = (name: any) => () => {
    const item: any = updatedValue.find((item: any) => item.module_name === name.name);
    {
      item.can_add == 1
        ? setUpdatedValue([...updatedValue, ((item.can_add = 0), (item.module_id = name.id))])
        : setUpdatedValue([...updatedValue, ((item.can_add = 1), (item.module_id = name.id))]);
    }
  };
  const handleCheckbox3 = (name: any) => () => {
    const item: any = updatedValue.find((item: any) => item.module_name === name.name);
    {
      item.can_update == 1
        ? setUpdatedValue([...updatedValue, ((item.can_update = 0), (item.module_id = name.id))])
        : setUpdatedValue([...updatedValue, ((item.can_update = 1), (item.module_id = name.id))]);
    }
  };
  const handleCheckbox4 = (name: any) => () => {
    const item: any = updatedValue.find((item: any) => item.module_name === name.name);
    {
      item.can_delete == 1
        ? setUpdatedValue([...updatedValue, ((item.can_delete = 0), (item.module_id = name.id))])
        : setUpdatedValue([...updatedValue, ((item.can_delete = 1), (item.module_id = name.id))]);
    }
  };

  const columns: any = (formikProps: FormikProps<any>) => [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{data.row.index + 1}</>;
      }
    },
    {
      Header: 'Modules',
      accessor: 'name'
    },
    {
      Header: 'Read',
      Cell: (data: any) => {
        return (
          <>
            <input
              type="checkbox"
              name="Can Read"
              checked={
                !!updatedValue.length &&
                updatedValue.find((item: any) => {
                  return item?.module_name === data.row.original.name;
                })?.can_view === 1
                  ? true
                  : false
              }
              onChange={handleCheckbox(data.row.original)}
            />
          </>
        );
      }
    },
    {
      Header: 'Create',
      Cell: (data: any) => {
        return (
          <>
            <input
              type="checkbox"
              name="Can Create"
              checked={
                !!updatedValue.length &&
                updatedValue.find((item: any) => {
                  return item?.module_name === data.row.original.name;
                })?.can_add === 1
                  ? true
                  : false
              }
              onChange={handleCheckbox2(data.row.original)}
            />
          </>
        );
      }
    },

    {
      Header: 'Update',
      Cell: (data: any) => {
        return (
          <>
            <input
              type="checkbox"
              name="Can Update"
              checked={
                !!updatedValue.length &&
                updatedValue.find((item: any) => {
                  return item?.module_name === data.row.original.name;
                })?.can_update === 1
                  ? true
                  : false
              }
              onChange={handleCheckbox3(data.row.original)}
            />
          </>
        );
      }
    },

    {
      Header: 'Delete',
      Cell: (data: any) => {
        return (
          <>
            <input
              type="checkbox"
              name="Can Delete"
              checked={
                !!updatedValue.length &&
                updatedValue.find((item: any) => {
                  return item?.module_name === data.row.original.name;
                })?.can_delete === 1
                  ? true
                  : false
              }
              onChange={handleCheckbox4(data.row.original)}
            />
          </>
        );
      }
    }
  ];

  return (
    <Formik
      enableReinitialize={true}
      initialValues={{ accessWithScreens: [] }}
      onSubmit={(values) => {
        onSubmitForm(values);
      }}
      render={(formik) => (
        <Form>
          <Modal
            title="Permission Configurations"
            isModalOpen={formModal}
            closeModal={setFormModal}
            renderFooter={() => (
              <div className="hstack gap-2 justify-content-end">
                <SecondaryButton
                  onClick={() => {
                    setFormModal();
                    setUpdatedValue([random]);
                  }}
                  title="Close"
                  isLoading={isCreating}
                />
                <PrimaryButton
                  title="Update Permission"
                  iconName="ri-add-line align-bottom me-1"
                  type="submit"
                  isLoading={isCreating}
                />
              </div>
            )}
          >
            {isFetching && status !== 'fulfilled' ? (
              <div className="d-flex align-items-center justify-content-center">
                <div className="spinner-border" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            ) : (
              <DataTable2 columns={columns(formik)} data={data?.data || []} />
            )}
          </Modal>
        </Form>
      )}
    />
  );
};

export default PermissionForm;
