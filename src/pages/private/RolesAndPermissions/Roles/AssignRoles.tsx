import React, { useState } from 'react';
import Container from 'components/ui/Container';
import { useListModuleQuery } from 'redux/reducers/roles';
import FormPermission from './FormPermission';
import { permissionInitialValues } from './schema';
import { useFormik } from 'formik';
import PrimaryButton from 'components/ui/PrimaryButton';
const AssignRoles = () => {
  const { data, isLoading } = useListModuleQuery();
  const [isHover, setIsHover] = useState(data?.data[0].id);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: permissionInitialValues,
    onSubmit: (values) => {
      console.log(values);
    }
  });

  return (
    <Container title="Assign Permission to Roles">
      <div>
        <div className="row">
          <div className="col-12 border-end">
            <div className="col-sm"></div>
            {isLoading ? (
              <div className="d-flex align-items-center justify-content-center">
                <div className="spinner-border" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            ) : (
              data?.data.map((item: any) => (
                <>
                  <div
                    key={item.id}
                    style={{
                      padding: '10px'
                    }}
                    onClick={(e) => {
                      setIsHover(item.id);
                    }}
                  >
                    <h5 style={{ textAlign: 'left', cursor: 'pointer', marginBottom: '15px' }}>
                      <b>{item.name}</b>
                    </h5>
                    {/* <FormPermission formik={formik} /> */}
                    <div className="row">
                      {}
                      <div className="col-2">
                        Create
                        <input style={{ margin: '5px 0 0 10px' }} type="checkbox" />
                      </div>
                      <div className="col-2">
                        Update
                        <input style={{ margin: '5px 0 0 10px' }} type="checkbox" />
                      </div>
                      <div className="col-2">
                        Delete
                        <input
                          style={{ margin: '5px 0 0 10px' }}
                          type="checkbox"
                          // checked={formikProps.values.accessWithScreens[data.row.index].permissions.can_view}
                          //   onChange={() =>
                          //     formikProps.setFieldValue(
                          //       `accessWithScreens.${data.row.index}.permissions.can_view`,
                          //       !formikProps.values.accessWithScreens[data.row.index].permissions.can_view
                          //     )
                          //   }
                        />
                      </div>
                      <div className="col-2">
                        Read
                        <input style={{ margin: '5px 0 0 10px' }} type="checkbox" />
                      </div>
                    </div>
                  </div>
                </>
              ))
            )}
            <PrimaryButton
              title="Submit"
              iconName="ri-add-line align-bottom me-1"
              type="submit"
              // isLoading={isCreating || isUpdating}
            />
          </div>

          {/* <div className="col">
            <div className="card-body">
              <FormPermission formik={formik} />
            </div>
          </div> */}
        </div>

        <hr />
      </div>
    </Container>
  );
};

export default AssignRoles;
