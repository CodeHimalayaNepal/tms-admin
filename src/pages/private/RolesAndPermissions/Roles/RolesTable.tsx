import DataTable from 'components/ui/DataTable/data-table';
import React, { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useListRolesQuery } from 'redux/reducers/roles';
import { ACTIONS_FOR } from '.';
import EditIcon from '../../../../assets/images/icons/edit.png';

interface ITraineeDataTable {
  tableHeaders: () => React.ReactNode;
  onActionClicked: (roleId: string, actionForm: ACTIONS_FOR) => void;
}
const RolesTable: FC<ITraineeDataTable> = ({ tableHeaders, onActionClicked }) => {
  const [page, setPage] = useState(1);
  const [allDataList, setAllDataList] = useState(0);
  const navigate = useNavigate();

  const { data: rolesLists, isLoading } = useListRolesQuery();

  const totalUser = rolesLists?.data;
  const paginatedData = totalUser?.slice(
    page === 1 ? 0 : (page - 1) * 10,
    page === 1 ? 10 : page * 10
  );
  const canPrevious = page === 1 ? false : true;
  const canNext = allDataList === totalUser?.length ? false : true;

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        // return <>{data.row.index + 1}</>;
        setAllDataList(data.row.index + (page - 1) * 10 + 1);
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: ' Role',
      accessor: 'name'
    },
    {
      Header: 'Description',
      accessor: 'desc'
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <div style={{ display: 'flex' }}>
            <img
              style={{ marginRight: '10px' }}
              src={EditIcon}
              alt="icon for config"
              onClick={() => {
                onActionClicked(data.row.original, ACTIONS_FOR.EDIT);
              }}
            />
            {data.row.original.is_routine_role === true ? null : (
              <img
                style={{ marginRight: '10px', height: '20px' }}
                src={require('../Roles/images/Vector.png')}
                alt="icon for config"
                onClick={() => onActionClicked(data.row.original, ACTIONS_FOR.DELETE)}
              />
            )}

            <div className="relative">
              <div className="dropdown d-flex justify-content-end">
                <a
                  href="#"
                  role="button"
                  id="dropdownMenuLink5"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <i className="ri-more-2-fill"></i>
                </a>

                <ul
                  className="dropdown-menu fixed z-[10] position-fixed-important"
                  aria-labelledby="dropdownMenuLink"
                >
                  <li>
                    <a
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#showModal"
                      onClick={() => {
                        onActionClicked(data.row.original, ACTIONS_FOR.PERMISSION);
                        // navigate(`/role/assignroles/roleId=${data.row.original.id}`);
                      }}
                    >
                      Update Permissions
                    </a>
                  </li>
                  {data.row.original.is_routine_role === true ? null : (
                    <li>
                      <a
                        className="dropdown-item"
                        href="#"
                        data-bs-toggle="modal"
                        data-bs-target="#inviteTraineeModal"
                        onClick={() => onActionClicked(data.row.original, ACTIONS_FOR.ADD_USER)}
                      >
                        Add Users
                      </a>
                    </li>
                  )}

                  {/* <li>
                  <a
                    className="dropdown-item"
                    href="#"
                    data-bs-toggle="modal"
                    data-bs-target="#inviteTraineeModal"
                    onClick={() => onActionClicked(data.row.original, ACTIONS_FOR.DELETE)}
                  >
                    Delete
                  </a>
                </li> */}
                </ul>
              </div>
            </div>
          </div>
        );
      }
    }
  ];

  return (
    <div>
      <DataTable
        columns={columns}
        // data={rolesLists?.data || []}
        data={paginatedData || []}
        isLoading={isLoading}
        tableHeaders={tableHeaders}
        pagination={{
          canPrevious,
          canNext,
          prevPage: () => {
            if (totalUser) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (totalUser) {
              totalUser?.length >= allDataList && setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default RolesTable;
