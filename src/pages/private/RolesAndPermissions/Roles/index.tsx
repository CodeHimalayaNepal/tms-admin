import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import useToggle from 'hooks/useToggle';
import React, { useState } from 'react';
import RolesForm from './RolesForm';
import RolesTable from './RolesTable';
import { useFormik } from 'formik';

import { toast } from 'react-toastify';
import { rolesInitialValues, RolesSchemaValidations } from './schema';
import {
  useCreateRolesMutation,
  useDeleteRoleMutation,
  useGetRoleDetailsMutation,
  useUpdateRoleMutation
} from 'redux/reducers/roles';
import PermissionForm from './PermissionForm';
import RolesAssignToUsersModal from './RolesAssignToUsersModal';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import { useNavigate } from 'react-router-dom';
import { useListUsersOfThisroleQuery } from 'redux/reducers/administrator';

export enum ACTIONS_FOR {
  EDIT,
  PERMISSION,
  ADD_USER,
  DELETE
}

const Roles = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const navigate = useNavigate();
  const [permissionFormModal, setPermissionFormModal] = useToggle(false);
  const [inviteUserModal, toggleInviteUserModal] = useToggle(false);

  const [rolesInitialValuesFormState, setRolesInitialValues] = React.useState(rolesInitialValues);
  const [createRoles, { isLoading: isCreating }] = useCreateRolesMutation();
  const [updateRole, { isLoading: isUpdating }] = useUpdateRoleMutation();
  const [deleteRole, { isLoading: isDeleting }] = useDeleteRoleMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: RolesSchemaValidations,
    initialValues: rolesInitialValuesFormState,

    onSubmit: (values) => {
      let executeFunc = createRoles;
      if (values.id) {
        executeFunc = updateRole;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setRolesInitialValues(rolesInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          if (err.data?.data?.name) {
            toast.error('Already assigned role of this name!');
          }
          // const errorKeys = Object.keys(err.data);
          // errorKeys.forEach((item) => {
          //   if (Array.isArray(err.data[item])) {
          //     err.data[item].length && toast.error(err.data[item][0]);
          //   } else {
          //     toast.error(err.data[item]);
          //   }
          // });
        });
    }
  });

  const onActionClicked = (roleId: any, actionFor: ACTIONS_FOR) => {
    setRolesInitialValues(roleId);
    if (actionFor === ACTIONS_FOR.EDIT) {
      setFormModal();
    } else if (actionFor === ACTIONS_FOR.PERMISSION) {
      setPermissionFormModal();
      // navigate('/role/assignroles');
    } else if (actionFor === ACTIONS_FOR.ADD_USER) {
      // const { data } = useListUsersOfThisroleQuery(roleId.id);
      toggleInviteUserModal();
    } else {
      toggleDeleteModal();
    }
  };

  const onDeleteConfirm = () => {
    deleteRole(rolesInitialValuesFormState.id)
      .unwrap()
      .then(() => {
        toast.success('Successfully deleted');
        toggleDeleteModal();
      })
      .catch((err: any) => {
        toast.error('Failed to Delete Role');
      });
  };

  return (
    <Container title="Roles and Permissions" subTitle="List of all roles">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <RolesTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title="Create new role"
              iconName="ri-add-line align-bottom me-1"
              onClick={() => {
                setFormModal();
                setRolesInitialValues(rolesInitialValues);
              }}
              type="button"
            />
          </div>
        )}
        onActionClicked={onActionClicked}
      />
      <RolesAssignToUsersModal
        formModal={inviteUserModal}
        setFormModal={toggleInviteUserModal}
        role={rolesInitialValuesFormState}
      />
      {rolesInitialValuesFormState?.menus && (
        <PermissionForm
          formModal={permissionFormModal}
          setFormModal={setPermissionFormModal}
          role={rolesInitialValuesFormState}
        />
      )}

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={rolesInitialValuesFormState.id ? 'Update Role' : 'Create New Role'}
          isModalOpen={formModal}
          closeModal={setFormModal}
          modalSize="modal-md"
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={setFormModal}
                title="Close"
                isLoading={isCreating || isUpdating}
              />
              <PrimaryButton
                title="Add role"
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={isCreating || isUpdating}
              />
            </div>
          )}
        >
          <RolesForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default Roles;
