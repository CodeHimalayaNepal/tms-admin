import CustomSelect from 'components/ui/Editor/CustomSelect';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import React, { FC } from 'react';
import { toast } from 'react-toastify';
import {
  useListsUsersByRoleQuery,
  useListUsersOfThisroleQuery
} from 'redux/reducers/administrator';
import {
  IRolesInput,
  ROLE_ASSIGNEE_ACTION,
  useAssignRoleToUserMutation
} from 'redux/reducers/roles';

interface IPermissionForm {
  formModal: boolean;
  setFormModal: () => void;
  role: IRolesInput;
}
const RolesAssignToUsersModal: FC<IPermissionForm> = ({ formModal, setFormModal, role }) => {
  if (!role.id) return null;

  const [assignRoleToUser, { isLoading: isCreating }] = useAssignRoleToUserMutation();

  const { data: userListsOfthisRole, isLoading: userLoading } = useListUsersOfThisroleQuery(
    role.id
  );

  const initialUsers = userListsOfthisRole?.data[0].users.map((item: any) => {
    return item.id.toString();
  });

  const { data: userLists } = useListsUsersByRoleQuery();

  const allUsersLists = userLists
    ? [...userLists?.staffs, ...userLists?.trainee].map((item: any) => {
        return {
          value: item.user_id?.toString(),
          label: item.first_name + ' ' + item.last_name
        };
      })
    : [];

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: { user_ids: initialUsers },
    onSubmit: (values) => {
      assignRoleToUser({
        user_ids: values.user_ids.map((item: any) => +item),
        role_id: role.id
        // action: ROLE_ASSIGNEE_ACTION.ASSIGN
      })
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');

          setFormModal();
        })
        .catch((err: any) => {
          const errorKeys = Object.keys(err.data);
          errorKeys.forEach((item) => {
            if (Array.isArray(err.data[item])) {
              err.data[item].length && toast.error(err.data[item][0]);
            } else {
              toast.error(err.data[item]);
            }
          });
        });
    }
  });

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <Modal
          title="Role to User"
          isModalOpen={formModal}
          modalSize="modal-md"
          closeModal={setFormModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton onClick={setFormModal} title="Close" isLoading={isCreating} />
              <PrimaryButton
                title="Add User"
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={isCreating}
              />
            </div>
          )}
        >
          <CustomSelect
            options={allUsersLists}
            placeholder={'Search by name'}
            value={formik.values.user_ids}
            onChange={(value: any) => {
              // setLastClickedItem(value);
              formik.setFieldValue('user_ids', value);
            }}
            isMulti={true}
            label={'Select User to Assign Role'}
            containerClassName="col-md-12"
          />
        </Modal>
      </form>
    </div>
  );
};

export default RolesAssignToUsersModal;
