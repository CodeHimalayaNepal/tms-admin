import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { IRolesInput } from 'redux/reducers/roles';

interface IRolesForm {
  formik: FormikProps<IRolesInput>;
}
const RolesForm = (props: IRolesForm) => {
  const { formik } = props;

  return (
    <div>
      <TextInput
        label="Title"
        name="name"
        type="text"
        onChange={formik.handleChange}
        value={formik.values.name}
        onBlur={formik.handleBlur}
        formik={formik}
        containerClassName="col-md-12"
      />

      <div className="mb-3 mt-3 col-md-12">
        <label htmlFor="Training-detail" className="form-label">
          Detail
        </label>
        <textarea
          className="form-control"
          rows={3}
          placeholder="Training Detail"
          defaultValue={''}
          value={formik.values.desc}
          onChange={(event) => {
            formik.setFieldValue('desc', event.target.value);
          }}
        />
        <FormikValidationError name="desc" errors={formik.errors} touched={formik.touched} />
      </div>
    </div>
  );
};

export default RolesForm;
