import { Field, FormikProps } from 'formik';
import { FC } from 'react';
import { IRolePermissionUpdateInput } from 'redux/reducers/roles';

interface IFeedbackForm {
  formik: FormikProps<IRolePermissionUpdateInput>;
}

const FormPermission: FC<IFeedbackForm> = ({ formik }) => {
  return (
    <form>
      <div className="row">
        <div className="col-2">
          create
          <input style={{ margin: '5px 0 0 10px' }} type="checkbox" />
        </div>

        <div className="col-2">
          Update
          <input style={{ margin: '5px 0 0 10px' }} type="checkbox" />
        </div>
        <div className="col-2">
          Delete
          <input type="checkbox" style={{ margin: '5px 0 0 10px' }} />
        </div>
        <div className="col-2">
          create
          <input style={{ margin: '5px 0 0 10px' }} type="checkbox" />
        </div>
      </div>
    </form>
  );
};

export default FormPermission;
