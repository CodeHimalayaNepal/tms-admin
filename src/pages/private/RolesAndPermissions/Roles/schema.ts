import { IRolesInput, IRolePermissionUpdateInput } from 'redux/reducers/roles';

import * as Yup from 'yup';

export const rolesInitialValues: IRolesInput = {
  name: '',
  desc: '',
  routine_role: '0'
};

export const permissionInitialValues: IRolePermissionUpdateInput = {
  role_id: 0,
  modules: [0],
  can_view: 0,
  can_add: 0,
  can_update: 0,
  can_delete: 0
};

export const RolesSchemaValidations = Yup.object().shape({
  name: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Title is required'),
  desc: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Details is required'),
  routine_role: Yup.string().required('Role is required')
});
