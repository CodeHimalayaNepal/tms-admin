import ActiveInactiveBadge from 'components/ui/ActiveInactiveBage/ActiveInactiveBadge';
import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Cell } from 'react-table';
import { useListRoutineQuery, IRoutineDetail } from 'redux/reducers/routine';
import download from 'utils/downloadFile';
import 'css/style.css';
import Modal from 'components/ui/Modal/modal';
import useToggle from 'hooks/useToggle';
import SecondaryButton from 'components/ui/SecondaryButton';
import SelectTemplateForm from './SelectTemplateForm';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import { useAssignToRoutineMutation } from 'redux/reducers/certification';
import StatusBadge from 'components/ui/StatusBadge';

interface ITOrganizationCentresTable {
  tableHeaders: () => React.ReactNode;
  onViewClicked: (routineId: number) => void;
  onEditClicked: (routineId: number) => void;
  onDeleteClicked: (data: IRoutineDetail) => void;
  routineStatusValue: string;
  training: string;
}
const OrganizationCentresTable: FC<ITOrganizationCentresTable> = ({
  tableHeaders,
  onViewClicked,
  onEditClicked,
  onDeleteClicked,
  routineStatusValue,
  training
}) => {
  const [page, setPage] = useState(1);
  const [routineId, setRoutineId] = useState<number | null>(null);
  const [pageSize, setPageSize] = useState(10);
  const [templateModal, setTemplateModal] = useToggle(false);
  const [assignToRoutine, { isLoading: assigning }] = useAssignToRoutineMutation();

  const navigate = useNavigate();

  const { data: routineData, isLoading } = useListRoutineQuery({
    page,
    pageSize,
    routineStatusValue,
    training
  });
  const closeModal = () => {
    setTemplateModal();
  };
  const initialValues = {
    certificate_template: 0,
    certificate_content: ''
  };
  const formik = useFormik({
    enableReinitialize: true,
    initialValues,
    onSubmit: (values) => {
      const sendValue = {
        ...values,
        certificate_routine: routineId
      };
      assignToRoutine(sendValue);
      formik.resetForm();
      setTemplateModal();
      toast.success('Successfully Assigned');
    }
  });
  const canNext = routineData?.next;
  const canPrevious = routineData?.previous;

  const columns = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: 'Training',
      accessor: 'training_details.training_name'
    },
    {
      Header: 'Trainee Count',
      accessor: 'trainee_count'
    },
    {
      Header: 'Batch',
      accessor: 'batch_name'
    },
    {
      Header: 'Hall',
      accessor: 'hall_details.hall_name'
    },
    {
      Header: 'Start Date',
      accessor: 'start_date'
    },
    {
      Header: 'End Date',
      accessor: 'end_date'
    },
    {
      Header: '  Status',
      accessor: 'is_active',
      Cell: (data: any) => {
        return <StatusBadge status={data.row.original.routine_status} />;
      }
    },
    {
      Header: 'Action',
      Cell: (data: Cell<IRoutineDetail & { routine_master_id: number }>) => {
        return (
          <div style={{ display: 'flex' }}>
            <img
              className="icons"
              style={{ marginRight: '10px', height: '20px' }}
              src={require('../RoutineManagement/Images/view.png')}
              alt="icon for config"
              onClick={() => onViewClicked(data?.row.original.routine_master_id)}
            />
            <img
              className="icons"
              style={{ marginRight: '10px' }}
              src={require('../RoutineManagement/Images/edit.png')}
              alt="icon for c"
              onClick={() => onEditClicked(data?.row.original.routine_master_id)}
            />
            <div className="relative">
              <div className="dropdown d-flex justify-content-end">
                <a
                  href="#"
                  role="button"
                  id="dropdownMenuLink5"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <i className="ri-more-2-fill"></i>
                </a>
                <ul
                  className="dropdown-menu fixed z-[10] position-absolute-important"
                  aria-labelledby="dropdownMenuLink"
                >
                  <li>
                    <a
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#showModal"
                      onClick={() =>
                        navigate(`/feedback/evaluation/${data?.row.original?.routine_master_id}`)
                      }
                    >
                      <img
                        src={require('../RoutineManagement/Images/carbon_result.png')}
                        alt="icon for config"
                      />{' '}
                      Configuration
                    </a>
                  </li>
                  <li>
                    <a
                      style={{ zIndex: 1 }}
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#showModal"
                      onClick={() =>
                        navigate(
                          `/routine-management/${data?.row.original.routine_master_id}/feedback`
                        )
                      }
                    >
                      <img src={require('../RoutineManagement/Images/fluent.png')} alt="view" />{' '}
                      View Feedback
                    </a>
                  </li>
                  <li>
                    <a
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#inviteTraineeModal"
                      onClick={() =>
                        navigate(
                          `/routine-management/trainee-evaluation/${data?.row.original.routine_master_id}`
                        )
                      }
                    >
                      <img
                        src={require('../RoutineManagement/Images/chart.png')}
                        alt="chart image"
                      />{' '}
                      Evaluation Management
                    </a>
                  </li>
                  <li>
                    <a
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#inviteTraineeModal"
                      onClick={() => onDeleteClicked(data.row.original)}
                    >
                      <img
                        src={require('../RoutineManagement/Images/Vector.png')}
                        alt="delete"
                        style={{ marginRight: '10px' }}
                      />{' '}
                      Delete
                    </a>
                  </li>
                  <li>
                    <a
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#showModal"
                      onClick={() =>
                        download(
                          `training/trainee-enrollment-export/${data.row.original.routine_master_id}/`,
                          'Routine.xlsx'
                        )
                      }
                    >
                      <img
                        src={require('../RoutineManagement/Images/download.png')}
                        alt="view"
                        height="25px"
                        max-width="18px"
                      />{' '}
                      Download Trainee Profile
                    </a>
                  </li>

                  <li>
                    <a
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#showModal"
                      onClick={() =>
                        download(
                          `attendance/attendance-export/${data.row.original.routine_master_id}/`,
                          'Attendance.xlsx'
                        )
                      }
                    >
                      <img
                        src={require('../RoutineManagement/Images/download.png')}
                        alt="view"
                        height="25px"
                        max-width="18px"
                      />{' '}
                      Download Trainee Attendance
                    </a>
                  </li>
                  <li>
                    <a
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#showModal"
                      onClick={() => {
                        setRoutineId(data.row.original.routine_master_id);
                        setTemplateModal();
                      }}
                    >
                      <img
                        src={require('../RoutineManagement/Images/certificate.png')}
                        alt="view"
                        height="25px"
                        max-width="18px"
                        style={{ marginRight: '5px' }}
                      />{' '}
                      Assign Certificate
                    </a>
                  </li>
                  <li>
                    <a
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#showModal"
                      onClick={() => {
                        navigate(
                          `/routine-management/generate-report/${data?.row.original.routine_master_id}`
                        );
                      }}
                    >
                      <img
                        src={require('../RoutineManagement/Images/report.png')}
                        alt="view"
                        height="25px"
                        max-width="18px"
                        style={{ marginRight: '5px' }}
                      />{' '}
                      Generate Report
                    </a>
                  </li>
                  {/* /routine-management/trainee-evaluation/:id */}
                </ul>
              </div>
            </div>
          </div>
        );
      }
    }
  ];
  return (
    <div className="list form-check-all">
      <DataTable
        columns={columns}
        data={routineData?.results?.data || []}
        tableHeaders={tableHeaders}
        isLoading={isLoading}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (canPrevious) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (canNext) {
              setPage(page + 1);
            }
          }
        }}
      />
      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={'Assign Template'}
          isModalOpen={templateModal}
          closeModal={closeModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton title="Close" isLoading={false} onClick={() => setTemplateModal()} />
              <PrimaryButton
                title={'Submit'}
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={false}
              />
            </div>
          )}
        >
          <SelectTemplateForm formik={formik} />
        </Modal>
      </form>
    </div>
  );
};

export default OrganizationCentresTable;
