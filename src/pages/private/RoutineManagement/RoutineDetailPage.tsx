import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import Spinner from 'components/ui/Spinner/spinner';
import { useFormik } from 'formik';
import { iteratorSymbol } from 'immer/dist/internal';
import React, { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  IRoutineDetail,
  IRoutineDetailRequest,
  useCreateRoutineMutation,
  useListRoutineByIdQuery,
  usePatchUpdateRoutineMutation
} from 'redux/reducers/routine';

import RoutineManagementForm from './RoutineManagementForm';
import { routineManagementInitialValues, RoutineSchemaValidations } from './schema';

const RoutineDetailPage = () => {
  let { routineId } = useParams();
  let navigate = useNavigate();
  const [courseId, setCourseId] = useState<any>('');
  const isCreate = routineId === 'create';
  const [createRoutine, { isLoading: isCreating }] = useCreateRoutineMutation();
  const [updateRoutine, { isLoading: isUpdating }] = usePatchUpdateRoutineMutation();
  const [routineInitialFormState, setRoutineInitialForm] = useState(routineManagementInitialValues);

  const { data: routineById, isLoading: isFetchingRoutineDetail } = useListRoutineByIdQuery(
    routineId ? routineId : '',
    { skip: isCreate || !routineId }
  );

  React.useEffect(() => {
    if (routineById && !isFetchingRoutineDetail) {
      setRoutineInitialForm({
        ...routineById.data,
        description: routineById.data.description,
        batch_name: routineById.data.batch_name,
        tr_routine_coordinator: routineById?.data?.tr_routine_coordinator
          ?.filter((c: any) => !c.is_module_coordinator)
          ?.map((item: any) => item?.coordinator_details?.coordinator_id.toString()),
        tr_routine_coordinators: routineById?.data?.tr_routine_coordinator
          ?.filter((c: any) => c.is_module_coordinator)
          .reduce((acc: any, d: any) => {
            acc[d.course_id] = {
              coordinator: [
                ...(acc[d.course_id]?.coordinator || []),
                d.coordinator_details?.coordinator_id.toString()
              ],
              is_module_coordinator: true,
              course_id: d.course_id,
              title: d.course_details.course_name
            };
            return acc;
          }, {}),
        training_details: routineById.data.training_details?.training_type_id,
        evaluator: routineById.data.evaluator_details?.map((item: any) => item.id?.toString()),
        duration: +routineById.data.duration,
        hall: routineById?.data.hall_details?.hall_id || 0,
        course: routineById?.data.course_details?.map((item: any) => item.id?.toString()) || [],
        training: routineById?.data.training_details?.id || 0,

        attendance_checkin_time: routineById.data.attendance_checkin_time,
        attendance_checkout_time: routineById.data.attendance_checkout_time,
        buffer_time_before: routineById.data.buffer_time_before?.slice(0, 5),
        buffer_time_after: routineById.data.buffer_time_after?.slice(0, 5)
      });
    }
  }, [routineById, isFetchingRoutineDetail]);

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: RoutineSchemaValidations,
    initialValues: routineInitialFormState,

    onSubmit: (values) => {
      let executeFunc = createRoutine;
      if (routineId) {
        executeFunc = updateRoutine;
      }

      let formValues = { ...values } as Partial<IRoutineDetailRequest>;
      const _coordinator =
        values.tr_routine_coordinator?.map((c) => ({
          coordinator: c,
          is_module_coordinator: false
        })) ?? [];

      const module_coordinates = Object.values(values.tr_routine_coordinators).flatMap((c) =>
        c.coordinator.map((cod: any) => ({
          coordinator: cod,
          is_module_coordinator: c.is_module_coordinator,
          course_id: c.course_id,
          title: c.title
        }))
      );

      formValues.tr_routine_coordinator = [..._coordinator, ...module_coordinates] as any;
      delete formValues.tr_routine_coordinators;
      if (formValues.attendance_type === 'daily_session') {
        delete formValues.attendance_checkin_time;
        delete formValues.attendance_checkout_time;
      }

      executeFunc(formValues)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setRoutineInitialForm(routineManagementInitialValues);
          navigate('/routine-management');
        })
        .catch((err: any) => {
          if (err.originalStatus === 403) {
            toast.error('You donot have permission to create routine,Please contact Admin.');
          } else {
            const errorKeys = Object.keys(err.data);
            errorKeys.forEach((item) => {
              if (Array.isArray(err.data[item])) {
                err.data[item].length && toast.error(err.data[item][0]);
              } else {
                toast.error(err.data[item]);
              }
            });
          }
        });
    }
  });

  return (
    <Container title="Routine Management">
      {isFetchingRoutineDetail && !isCreate ? (
        <Spinner />
      ) : (
        <form onSubmit={formik.handleSubmit}>
          <RoutineManagementForm formik={formik} setCourseId={setCourseId} />

          <div className="hstack gap-2 justify-content-end">
            <SecondaryButton
              onClick={() => navigate('/routine-management')}
              title="Cancel"
              isLoading={isUpdating || isCreating}
            />

            <PrimaryButton
              title="Create"
              iconName="ri-add-line align-bottom me-1"
              type="submit"
              isLoading={isUpdating || isCreating}
            />
          </div>
        </form>
      )}
    </Container>
  );
};

export default RoutineDetailPage;
