import CustomSelect from 'components/ui/Editor/CustomSelect';
import React from 'react';
import { FormikProps, useFormik } from 'formik';
import { useListCertificateTemplateQuery } from 'redux/reducers/certification';
import { useListRoutineQuery } from 'redux/reducers/routine';
import TextInput from 'components/ui/TextInput';

const SelectTemplateForm = ({ formik }: { formik: FormikProps<any> }) => {
  const { data: templateData, isLoading: isLooding } = useListCertificateTemplateQuery();
  const { data: routineData, isLoading } = useListRoutineQuery();
  console.log(templateData, 'templateDataa');
  const templateList = templateData?.map((item: any) => {
    return {
      value: +item.id,
      label: item.template_name
    };
  });
  const routineList = routineData?.results?.data?.map((item: any) => {
    return {
      value: +item.routine_master_id,
      label: item.training_details.training_name
    };
  });

  return (
    <>
      <form>
        <div className="row">
          <CustomSelect
            name="template_name"
            options={templateList}
            label={'Select Template'}
            onChange={(value: any) => {
              formik.setFieldValue('certificate_template', value);
            }}
            value={formik.values.certificate_template}
            onBlur={formik.handleBlur}
            formik={formik}
            size="large"
          />
          <TextInput
            label="Certificate Content"
            name="certificate_content"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.certificate_content}
            onBlur={formik.handleBlur}
            formik={formik}
          />
        </div>
      </form>
    </>
  );
};

export default SelectTemplateForm;
