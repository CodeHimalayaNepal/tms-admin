import CustomSelect from 'components/ui/Editor/CustomSelect';
import EditorComponent from 'components/ui/Editor/editor';
import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { FC, useEffect } from 'react';
import { useListUserQuery } from 'redux/reducers/internal-user';
import { useListAvailableOrganizationHallQuery } from 'redux/reducers/organization-halls';
import { useListTrainingQuery } from 'redux/reducers/training';
import { routineManagementInitialValues } from './schema';

interface IRoutineManagementForm {
  formik: FormikProps<typeof routineManagementInitialValues>;
  setCourseId?: any;
}

function getTimes() {
  let x = 1; //minutes interval
  let times = []; // time array
  let tt = 0; // start time

  //loop to increment the time and push results in array
  for (let i = 0; tt < 24 * 60; i++) {
    let hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
    let mm = tt % 60; // getting minutes of the hour in 0-55 format
    times[i] = ('0' + (hh % 12)).slice(-2) + ':' + ('0' + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
    tt = tt + x;
  }
  return times.map((time) => ({ label: time, value: time }));
}

const RoutineManagementForm: FC<IRoutineManagementForm> = ({ formik, setCourseId }) => {
  const startDate = formik?.values?.start_date;
  const endDate = formik?.values?.end_date;
  const { data: organizationHallData } = useListAvailableOrganizationHallQuery(
    { startDate, endDate },
    { skip: !endDate && !startDate }
  );

  const { data: trainingData } = useListTrainingQuery();
  const { data: userList = [{}] } = useListUserQuery();

  const coordinatorList = userList.map(
    (c: {
      id: string;
      first_name: string;
      last_name: string;
      mobile_number: string;
      role_name: string;
    }) => ({
      label:
        c.role_name == 'External'
          ? c.first_name + ' ' + c.last_name + ' - ' + c.mobile_number + '(external)'
          : c.first_name + ' ' + c.last_name + ' - ' + c.mobile_number + '(internal)',
      value: c?.id?.toString() || ''
    })
  );

  const hallLists = [
    ...(organizationHallData?.data || []),
    ...([formik?.values?.hall_details] || [])
  ]
    .filter((l) => l)
    ?.map((item: any) => {
      return {
        value: item.hall_id.toString(),
        label: item.hall_name
      };
    });

  const trainingLists = trainingData?.map((item: any) => {
    return {
      value: +item.id,
      label: item.training_name
    };
  });

  const setCoursesFun = (value: any, isNew = true) => {
    const newData = (trainingData ?? [])?.filter((el: any) => el.id == value);
    formik.setFieldValue(`tr_routine_coordinators`, {});
    if (newData?.length) {
      newData[0]?.courses.forEach((el: any) => {
        formik.setFieldValue(`tr_routine_coordinators.${el.id}`, {
          coordinator: isNew ? null : formik.values.tr_routine_coordinators[el.id]?.coordinator,
          is_module_coordinator: true,
          course_id: el.id,
          title: el.title
        });
      });
    }
  };

  useEffect(() => {
    if (formik.values.training) {
      setCoursesFun(formik.values.training, false);
    }
  }, [trainingData]);

  return (
    <form>
      <div className="row">
        <CustomSelect
          options={trainingLists}
          placeholder={'Search by name'}
          value={formik.values.training}
          onChange={(value: any) => {
            formik.setFieldValue('training', value);
            setCoursesFun(value);
            setCourseId(trainingData?.find((el: any) => el.id == value));
          }}
          label={'Training'}
        />
        <FormikValidationError name="training" errors={formik.errors} touched={formik.touched} />

        <TextInput
          label="Batch"
          name="batch_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.batch_name}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="Start Date"
          name="start_date"
          type="date"
          onChange={formik.handleChange}
          value={formik.values.start_date}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="End Date"
          name="end_date"
          type="date"
          onChange={formik.handleChange}
          value={formik.values.end_date}
          onBlur={formik.handleBlur}
          formik={formik}
          min={formik.values.start_date}
        />
        <CustomSelect
          options={hallLists}
          placeholder={'Search by name'}
          value={formik.values.hall}
          onChange={(value: any) => formik.setFieldValue('hall', value)}
          label={'Hall'}
        />
        <FormikValidationError name="hall" errors={formik.errors} touched={formik.touched} />

        <CustomSelect
          options={coordinatorList}
          placeholder={'Search by name'}
          value={formik.values.tr_routine_coordinator}
          onChange={(value: any) => formik.setFieldValue('tr_routine_coordinator', value)}
          label={'Coordinator'}
          isMulti
        />

        <CustomSelect
          options={getTimes()}
          placeholder={'hh:mm'}
          value={formik.values.buffer_time_before}
          onChange={(value: any) => formik.setFieldValue('buffer_time_before', value)}
          label={'Early Attendance Buffer'}
        />

        <CustomSelect
          options={getTimes()}
          placeholder={'hh:mm'}
          value={formik.values.buffer_time_after}
          onChange={(value: any) => formik.setFieldValue('buffer_time_after', value)}
          label={'Late Attendance Buffer'}
        />

        <div className=" mt-3 col-12 mb-2">
          <div className="form-check form-switch form-switch-md">
            <label className="form-check-label" htmlFor="attendance_type">
              Enable Check-In Check-Out?
            </label>
            <input
              type="checkbox"
              className="form-check-input"
              checked={formik.values.attendance_type === 'routine'}
              id="attendance_type"
              onChange={(e) =>
                formik.setFieldValue(
                  'attendance_type',
                  e.target.checked ? 'routine' : 'daily_session'
                )
              }
            />
          </div>
        </div>

        {formik.values.attendance_type === 'routine' && (
          <>
            <TextInput
              containerClassName="mt-3 col-4"
              label="Check-In Time"
              name="attendance_checkin_time"
              type="time"
              onChange={(e: any) => {
                formik.setFieldValue('attendance_checkin_time', e.target.value);
              }}
              value={formik.values.attendance_checkin_time}
              onBlur={formik.handleBlur}
              formik={formik}
            />

            <TextInput
              containerClassName="mt-3 col-4"
              label="Check-Out Time"
              name="attendance_checkout_time"
              type="time"
              onChange={(e: any) => {
                formik.setFieldValue('attendance_checkout_time', e.target.value);
              }}
              value={formik.values.attendance_checkout_time}
              onBlur={formik.handleBlur}
              formik={formik}
            />
          </>
        )}

        <div style={{ marginTop: '20px' }}>
          <div style={{ display: 'flex', alignItems: 'center', gap: '1rem' }}>
            <h5>Modules and Module Coordinator</h5>
            <div style={{ height: '2px', width: '900px', background: '#D8D6DE' }}></div>
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column'
            }}
          >
            {Object.entries(formik.values?.tr_routine_coordinators)?.map(
              ([key, value]: any, index: number, array: any[]) => {
                return (
                  <div
                    style={{
                      display: 'grid',
                      alignItems: 'center',
                      gridTemplateColumns: '1fr 3fr',
                      justifyContent: 'start'
                    }}
                  >
                    <b style={{ width: '200px', marginTop: '2rem' }}>{value.title}</b>

                    <div
                      style={{
                        padding: '1rem',
                        alignItems: 'center',
                        justifyContent: 'start'
                      }}
                    >
                      <CustomSelect
                        options={coordinatorList}
                        placeholder={'Search by name'}
                        value={value.coordinator}
                        onChange={(coordinators: any) => {
                          const updatedValue = { ...value, coordinator: coordinators };
                          formik.setFieldValue(`tr_routine_coordinators.${key}`, updatedValue);
                        }}
                        isMulti
                      />
                    </div>
                  </div>
                );
              }
            )}
          </div>
        </div>
        <FormikValidationError
          name="tr_routine_coordinator"
          errors={formik.errors}
          touched={formik.touched}
        />

        <FormikValidationError
          name="tr_routine_coordinator_module"
          errors={formik.errors}
          touched={formik.touched}
        />

        <div className="my-3 col-md-12">
          <EditorComponent
            label="Description"
            containerClassName="mt-3"
            editorValue={formik.values.description}
            onChangeValue={(data) => formik.setFieldValue('description', data)}
          />
          <FormikValidationError
            name="description"
            errors={formik.errors}
            touched={formik.touched}
          />
        </div>
      </div>
    </form>
  );
};

export default RoutineManagementForm;
