import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import Spinner from 'components/ui/Spinner/spinner';
import useToggle from 'hooks/useToggle';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { CoursesResponse, useListCoursesByTrainingQuery } from 'redux/reducers/courses';
import {
  IRoutineInfo,
  IRoutineSessionDetail,
  useCompleteModuleForRoutineMutation,
  useListRoutineByIdQuery,
  useListRoutineSessionQuery,
  useSaveRoutineQuery
} from 'redux/reducers/routine';

import { formatTime, getLocaleDateOnly } from 'utils/date';
import { tConvert } from 'utils/timeConvert';

import InviteUsersModal from './InviteUsersModal';
import DailySessionModal from './Modal/DailySessionModal';
import SessionModal from './Modal/SessionModal';
import RoutineSessionDailyDetail from './RoutineSessionDailyDetail';

type Props = {};

type CoursesProps = {
  routineInfo: IRoutineInfo;
};

function CoursesElement({ routineInfo }: CoursesProps) {
  const [completeModuleForRoutine, { isLoading }] = useCompleteModuleForRoutineMutation();
  const { routineId = '' } = useParams<{ routineId: string }>();
  const handleOnCompleteClick = async (id: string) => {
    try {
      await completeModuleForRoutine({
        routineId: routineId,
        moduleId: id,
        body: { is_module_complete: true }
      }).unwrap();

      toast.success('Module is marked as complete!');
    } catch (error) {
      toast.success('Something went wrong!');
    }
  };
  let uniqueCourseNames: string[] = [];

  return (
    <div>
      {routineInfo?.tr_routine_coordinator?.map((item) => {
        // Check if the current course name has already been rendered
        if (
          item?.course_details?.course_name &&
          !uniqueCourseNames?.includes(item?.course_details?.course_name)
        ) {
          // Add the current course name to the array of unique course names
          uniqueCourseNames.push(item?.course_details?.course_name);
          return (
            <div className="row mb-3 align-items-center">
              <div className="col-4 col-md-3  align-items-start">
                <p>
                  <b>{item?.course_details?.course_name}</b>
                </p>
              </div>
              <div className="col-4 col-md-3 align-items-start">
                {routineInfo.tr_routine_coordinator.map(
                  (coordinator, index) =>
                    coordinator?.is_module_coordinator === true &&
                    coordinator?.course_details?.course_id === item?.course_details?.course_id && (
                      <React.Fragment key={index}>
                        {coordinator?.coordinator_details?.first_name +
                          ' ' +
                          (coordinator?.coordinator_details?.middle_name === null
                            ? ''
                            : coordinator?.coordinator_details?.middle_name) +
                          ' ' +
                          coordinator?.coordinator_details?.last_name}
                        {index !== routineInfo.tr_routine_coordinator.length - 1 && ', '}
                      </React.Fragment>
                    )
                )}
              </div>
              <div className="col-4 col-md-3 align-items-start" style={{ gap: '10px' }}>
                {item?.is_module_coordinator === true ? (
                  <div>
                    <PrimaryButton
                      title="Mark complete"
                      type="submit"
                      onClick={() => {
                        handleOnCompleteClick(item?.course_details?.course_id.toString() ?? '');
                      }}
                    />
                  </div>
                ) : null}
              </div>
            </div>
          );
        }
      })}
    </div>
  );
}

function RoutineSessionDetailPage({}: Props) {
  const { routineId = '' } = useParams<{ routineId: string }>();

  const [sessionId, setSessionId] = useState('');

  const [sessionModalOpen, toggleSessionModal] = useToggle(false);
  const [dailySessionModalOpen, toggleDailySessionModal] = useToggle(false);

  const { data: courseList = [] } = useListCoursesByTrainingQuery(routineId);

  const courseCategory = courseList?.results?.data
    ?.map((course: any) => course.courses_list)
    ?.map((item: any) => {
      return {
        id: item.id?.toString(),
        name: item.title
      };
    });
  const { data: routineData, isLoading: isRoutingLoading } = useListRoutineByIdQuery(routineId, {
    skip: !routineId
  });

  const finalizedStatus = routineData?.data?.save_as_draft;

  const routineInfo = (routineData?.data ?? {}) as IRoutineInfo;
  const [saveRoutineId, setSaveRoutineId] = useState<string | null>();
  const [routineFinalized, setRoutineFinalized] = useState<boolean>(false);

  const {
    data: savedDraft,
    isLoading: isSaving,
    isSuccess
  } = useSaveRoutineQuery(saveRoutineId, { skip: !saveRoutineId });

  useEffect(() => {
    toast.success(savedDraft?.message);
  }, [savedDraft]);

  const {
    data: sessionList = [],
    isLoading: isSessionsLoading
  }: { data: IRoutineSessionDetail[]; isLoading: boolean } = useListRoutineSessionQuery(routineId);

  if (isRoutingLoading) {
    return <Spinner />;
  }
  return (
    <Container title="Routine Info">
      <h4>Training Routine Details</h4>

      <div>
        <div className="row">
          <div className="col-lg-6 col-sm-12 border-end">
            <div className="card-body">
              <h5 className="mb-4">Details</h5>

              <div className="row">
                <div className="col-12 col-lg-6">
                  <h6>Training Name</h6>
                  <p>{routineInfo.training_details?.training_name}</p>
                </div>
                <div className="col-6 col-lg-3">
                  <h6>Training Code</h6>
                  <p>{routineInfo.training_details?.training_code}</p>
                </div>
                <div className="col-6 col-lg-3">
                  <h6>Hall</h6>
                  <p>{routineInfo.hall_details?.hall_name}</p>
                </div>
                <div className="col-6 col-lg-3">
                  <h6>Batch</h6>
                  <p>{routineInfo.batch_name}</p>
                </div>
                <div className="col-6 col-lg-3">
                  <h6>Start Date</h6>
                  <p>{getLocaleDateOnly(new Date(routineInfo.start_date))}</p>
                </div>
                <div className="col-6 col-lg-3">
                  <h6>End Date</h6>
                  <p>{getLocaleDateOnly(new Date(routineInfo.end_date))}</p>
                </div>
                <div className="col-6 col-lg-3">
                  <h6>Coordinator</h6>
                  <p>
                    {routineInfo.tr_routine_coordinator
                      ?.filter((c) => !c.is_module_coordinator)
                      .map(
                        (c) =>
                          c.coordinator_details.first_name + ' ' + c.coordinator_details.last_name
                      )
                      .join(', ') || 'N/A'}
                  </p>
                </div>
                <div className="col-6 col-lg-3">
                  <h6>Start Time</h6>
                  <p>
                    {routineInfo.attendance_checkin_time
                      ? tConvert(routineInfo.attendance_checkin_time)
                      : '-'}
                  </p>
                </div>
                <div className="col-6 col-lg-3">
                  <h6>End Time</h6>
                  <p>
                    {routineInfo.attendance_checkout_time
                      ? tConvert(routineInfo.attendance_checkout_time)
                      : '-'}
                  </p>
                </div>
                <div className="col-6 col-lg-3">
                  <h6>Early Attendance Buffer</h6>
                  <p>{routineInfo.buffer_time_before?.slice(0, 5) || '-'}</p>
                </div>
                <div className="col-6 col-lg-3">
                  <h6>Late Attendance Buffer</h6>
                  <p>{routineInfo.buffer_time_after?.slice(0, 5) || '-'}</p>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-6 col-sm-12">
            <div className="card-body">
              <h5 className="mb-4">TraineeDetails</h5>

              <InviteUsersModal selectedRoutine={routineId} />
            </div>
          </div>
        </div>

        <hr />
        <div className="row">
          <div className="card-body">
            <CoursesElement routineInfo={routineInfo} />
          </div>
        </div>

        <hr />

        <div className="row">
          <div className="col-12">
            <div className="card-body">
              <h5>Training Sessions</h5>
              <div className="row mt-2">
                <div className="col-3" style={{ paddingLeft: '20px', paddingTop: '20px' }}>
                  <PrimaryButton
                    type="button"
                    disabled={finalizedStatus || routineFinalized}
                    title={
                      finalizedStatus || routineFinalized ? 'Routine Finalized' : 'Save Routine'
                    }
                    onClick={() => {
                      if (!finalizedStatus && !routineFinalized) {
                        setSaveRoutineId(routineId);
                        setRoutineFinalized(true);
                      } else {
                        toast.info('Routine Already Finalized');
                      }
                    }}
                  />
                </div>
              </div>
              {isSessionsLoading ? (
                <Spinner />
              ) : (
                <div className="overflow-auto mt-4 p-4">
                  <div className="row flex-nowrap">
                    {sessionList.map((session) => (
                      <div key={session.id} className="col-12 col-md-4 border-end">
                        <div className="d-flex flex-column">
                          <div className="card">
                            <div className="card-body p-2 px-3 bg-light">
                              <h6>{session.session_name}</h6>
                              <p className="m-0">
                                {formatTime(session.start_time)} - {formatTime(session.end_time)}
                              </p>
                            </div>
                          </div>
                          <RoutineSessionDailyDetail
                            sessionId={session?.id?.toString()}
                            routineId={routineId}
                          />

                          <div className="d-flex justify-content-center">
                            <PrimaryButton
                              title="Add a Daily Session Topic"
                              type="button"
                              className="btn-sm"
                              onClick={() => {
                                setSessionId(session.id.toString());
                                toggleDailySessionModal();
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    ))}
                    <div className={`col-4 ${sessionList.length ? '' : 'border-start'}`}>
                      <div className="d-flex">
                        <PrimaryButton
                          title="Add Session"
                          type="button"
                          onClick={toggleSessionModal}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>

      <SessionModal routineId={routineId} open={sessionModalOpen} toggle={toggleSessionModal} />

      <DailySessionModal
        trainingId={routineInfo.training_details?.id.toString()}
        routineId={routineId}
        sessionId={sessionId}
        open={dailySessionModalOpen}
        toggle={toggleDailySessionModal}
      />
    </Container>
  );
}

export default RoutineSessionDetailPage;
