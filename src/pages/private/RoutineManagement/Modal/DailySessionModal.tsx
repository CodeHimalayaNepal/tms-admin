import CustomSelect from 'components/ui/Editor/CustomSelect';
import FormikValidationError from 'components/ui/FormikErrors';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import TextInput from 'components/ui/TextInput';
import { useFormik } from 'formik';
import React, { useState } from 'react';
import { toast } from 'react-toastify';
import { useListCoursesByTrainingQuery, useListCoursesQuery } from 'redux/reducers/courses';
import { useListUserQuery, useGetAvailableUserListQuery } from 'redux/reducers/internal-user';
import {
  useCreateRoutineDailySessionMutation,
  useUpdateRoutineDailySessionMutation
} from 'redux/reducers/routine';
import { useListSessionQuery } from 'redux/reducers/session-resources';
import { useListTrainingCourseTopicsQuery } from 'redux/reducers/training';
import { routineDailySessionSchema, routineDailySessionValidation } from './schema';
import { useListRoutineDailySessionByIdQuery } from 'redux/reducers/routine';

type Props = {
  trainingId?: string;
  routineId?: string;
  sessionId?: string;
  open: boolean;
  toggle: () => void;
  dailySessionId?: any;
};
interface CustomState {
  course_topic_id: string;
  course_id: string;
}

function DailySessionModal({
  trainingId,
  routineId,
  sessionId,
  open,
  toggle,
  dailySessionId
}: Props) {
  const { data: courseList = [] } = useListCoursesByTrainingQuery(routineId);

  const [initialFormState, setTrainingInitialFormState] = useState(routineDailySessionSchema);
  const [routineDate, setRoutineDate] = useState('');

  const { data: trainingCourseTopics = [] } = useListTrainingCourseTopicsQuery(trainingId, {
    skip: !trainingId
  });
  const [createRoutineDailySession, { isLoading }] = useCreateRoutineDailySessionMutation();

  const [updateRoutineDailySession, { isLoading: isUpdating }] =
    useUpdateRoutineDailySessionMutation();
  const { data: userList = [] } = useListUserQuery();

  const courseCategory = courseList?.results?.data
    ?.map((course: any) => course.courses_list)
    ?.map((item: any) => {
      return {
        value: item.id?.toString(),
        label: item.title
      };
    });

  const { data: dailySessionData, isLoading: SessionDataLoading } =
    useListRoutineDailySessionByIdQuery({ dailySessionId, routineId }, { skip: !dailySessionId });

  React.useEffect(() => {
    if (dailySessionId) {
      setTrainingInitialFormState({
        ...dailySessionData,
        routine_session: sessionId,
        course_name: dailySessionData?.course?.course_id,
        course_topic: dailySessionData?.course_topic?.id,
        routine_date: dailySessionData?.routine_date,
        altered_start_time: dailySessionData?.altered_start_time,
        altered_end_time: dailySessionData?.altered_end_time,
        lecturers: dailySessionData?.lecturers?.map((item: any) => item?.id.toString()),
        daily_session_id: dailySessionId
      });
      formik.setFieldValue(
        'lecturers',
        dailySessionData?.lecturers?.map((user: any) => user?.id.toString())
      );
      setRoutineDate(dailySessionData?.routine_date);
    }
  }, [dailySessionData]);

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: routineDailySessionValidation,
    initialValues: initialFormState,

    onSubmit: async (values) => {
      values.routine_session = sessionId;
      let obj = {
        routine_session: sessionId,
        routine_date: routineDate,
        altered_start_time: values.altered_start_time || null,
        altered_end_time: values.altered_end_time || null,
        course_topic: values.course_topic,
        lecturers: values.lecturers,
        daily_session_id: dailySessionId,
        routine: routineId
      };
      let executeFunc = createRoutineDailySession;
      if (dailySessionId) {
        executeFunc = updateRoutineDailySession;
      }
      executeFunc(obj)
        .then((data: any) => {
          if (data?.error?.data?.non_field_errors?.length > 0) {
            data?.error?.data?.non_field_errors.map((value: any) => {
              toast.error(value);
              return;
              return;
            });
            return;
          }
          formik.resetForm({ values: routineDailySessionSchema });
          toggle();
          toast.success('Session created successfully!');
        })
        .catch((err: any) => {
          toast.error('Error !');
        });
    }
  });

  const { data: sessionData } = useListSessionQuery(
    { id: formik?.values?.course_name, page: 1, pageSize: 1000 },
    {
      skip: !formik.values.course_name
    }
  );
  const sessionList =
    sessionData?.data?.results?.map((item: any) => {
      return {
        value: item.course_topic_id?.toString(),
        label: item.title
      };
    }) || [];

  const { data: availableUsers, isLoading: isAvailableUsersLoading } = useGetAvailableUserListQuery(
    { sessionId, routineDate },
    { skip: !routineDate }
  );

  const availableUsersOptions = [
    ...(availableUsers?.data || []),
    ...(dailySessionData?.lecturers || [])
  ]
    .filter((l) => l)
    ?.map((c: { id: string; first_name: string; last_name: string; user_type: string }) => ({
      label:
        c.user_type === '3'
          ? c.first_name + ' ' + c.last_name + '(external)'
          : c.first_name + ' ' + c.last_name + '(internal)',
      value: c.id.toString()
    }));
  return (
    <form onSubmit={formik.handleSubmit}>
      <Modal
        title={`${dailySessionId ? 'Update' : 'Add'} Session`}
        isModalOpen={open}
        closeModal={toggle}
        modalSize="modal-md"
        renderFooter={() => (
          <div className="hstack gap-2 justify-content-end">
            <SecondaryButton onClick={toggle} title="Close" />
            <PrimaryButton
              title={`${dailySessionId ? 'Update' : 'Add'} Session`}
              iconName="align-bottom me-1"
              type="submit"
              isLoading={isLoading}
            />
          </div>
        )}
      >
        <CustomSelect
          containerClassName="col-12"
          options={courseCategory}
          placeholder={'Search by name'}
          value={formik.values.course_name}
          onChange={(value: any) => {
            formik.setFieldValue('course_name', value);
          }}
          label={'Course'}
        />

        <CustomSelect
          containerClassName="mt-3 col-12"
          options={sessionList}
          placeholder={'Search by name'}
          value={formik.values.course_topic}
          onChange={(value: any) => formik.setFieldValue('course_topic', value)}
          label={'Session'}
        />
        <FormikValidationError
          name="course_topic"
          errors={formik.errors}
          touched={formik.touched}
        />

        <TextInput
          containerClassName="mt-3 col-12"
          label="Date"
          name="routine_date"
          type="date"
          onChange={(e: any) => {
            formik.setFieldValue('routine_date', e.target.value);
            setRoutineDate(e.target.value);
          }}
          value={formik.values.routine_date}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <CustomSelect
          containerClassName="mt-3 col-12"
          options={availableUsersOptions}
          placeholder={'Search by name'}
          value={formik.values.lecturers}
          onChange={(value: any) => formik.setFieldValue('lecturers', value)}
          label={'Lecturer'}
          isMulti={true}
        />
        <FormikValidationError name="lecturers" errors={formik.errors} touched={formik.touched} />

        <TextInput
          containerClassName="mt-3 col-12"
          label="Altered Start Time"
          name="altered_start_time"
          type="time"
          onChange={formik.handleChange}
          value={formik.values.altered_start_time}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
        />

        <TextInput
          containerClassName="mt-3 col-12"
          label="Altered End Time"
          name="altered_end_time"
          type="time"
          onChange={formik.handleChange}
          value={formik.values.altered_end_time}
          onBlur={formik.handleBlur}
          formik={formik}
          min={formik.values.altered_start_time}
          disableRequiredValidation
        />
      </Modal>
    </form>
  );
}

export default DailySessionModal;
