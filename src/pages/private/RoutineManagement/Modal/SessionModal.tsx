import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import TextInput from 'components/ui/TextInput';
import { useFormik } from 'formik';
import React from 'react';
import { toast } from 'react-toastify';
import { useCreateRoutineSessionMutation } from 'redux/reducers/routine';

import { routineSessionSchema, routineSessionValidation } from './schema';

type Props = {
  routineId: string;
  open: boolean;
  toggle: () => void;
};

function SessionModal({ routineId, open, toggle }: Props) {
  const [createRoutineSession, { isLoading }] = useCreateRoutineSessionMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: routineSessionValidation,
    initialValues: routineSessionSchema,

    onSubmit: async (values) => {
      try {
        await createRoutineSession({
          routine: routineId,
          session_name: values.session_name,
          start_time: values.start_date,
          end_time: values.end_date
        }).unwrap();
        toggle();
        formik.resetForm({ values: routineSessionSchema });
        toast.success('Session created successfully!');
      } catch (error: any) {
        if (error?.originalStatus === 403) {
          toast.error('You donot have permission to create routine session.Please contact Admin!');
        } else {
          toast.error('Failed to create routine session!');
        }
      }
    }
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Modal
        title="Add Routine Session"
        isModalOpen={open}
        closeModal={toggle}
        modalSize="modal-md"
        renderFooter={() => (
          <div className="hstack gap-2 justify-content-end">
            <SecondaryButton onClick={toggle} title="Close" />
            <PrimaryButton
              title="Add Session"
              iconName="align-bottom me-1"
              type="submit"
              isLoading={isLoading}
            />
          </div>
        )}
      >
        <TextInput
          containerClassName="col-12"
          label="Session Name"
          name="session_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.session_name}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          containerClassName="mt-3 col-12"
          label="Start Time"
          name="start_date"
          type="time"
          onChange={formik.handleChange}
          value={formik.values.start_date}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          containerClassName="mt-3 col-12"
          label="End Time"
          name="end_date"
          type="time"
          onChange={formik.handleChange}
          value={formik.values.end_date}
          onBlur={formik.handleBlur}
          formik={formik}
          min={formik.values.start_date}
        />
      </Modal>
    </form>
  );
}

export default SessionModal;
