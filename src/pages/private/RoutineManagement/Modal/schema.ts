import * as Yup from 'yup';

export const routineSessionSchema = {
  routine_id: null as number | null,
  session_name: '',
  start_date: '',
  end_date: ''
};

export const routineSessionValidation = Yup.object().shape({
  session_name: Yup.string().required('Session name is required'),
  start_date: Yup.string().required('Start Date is required'),
  end_date: Yup.string().required('End Date is required')
});

export const routineDailySessionSchema = {
  course_name: null as any,
  routine_session: null as any,
  course_topic: null,
  routine_date: '',
  altered_start_time: '',
  altered_end_time: '',
  lecturers: []
};

export const routineDailySessionValidation = (isRoutineAttendance: boolean) =>
  Yup.object().shape({
    course_topic: Yup.string().required('Course is required'),
    routine_date: isRoutineAttendance ? Yup.string() : Yup.string().required('Date is required'),
    //   altered_start_time: Yup.string().required('Start time is required'),
    //   altered_end_time: Yup.string().required('End time is required'),
    lecturers: isRoutineAttendance ? Yup.string() : Yup.array().min(1, 'Lecturer is required')
  });
