import useToggle from 'hooks/useToggle';
import { set } from 'immer/dist/internal';
import React, { useState } from 'react';
import { toast } from 'react-toastify';
import {
  IRoutineDailySession,
  useDeleteRoutineDailySessionByIdMutation,
  useListRoutineDailySessionQuery
} from 'redux/reducers/routine';
import { formatTime, getLocaleDateOnly } from 'utils/date';
import editIcon from './Images/edit.png';
import deleteIcon from './Images/Vector.png';
import DailySessionModal from './Modal/DailySessionModal';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';

type Props = {
  sessionId: string;
  routineId: string;
};

function RoutineSessionDailyDetail({ sessionId, routineId }: Props) {
  const {
    data: dailySessionData,
    isLoading: isDailySessionsLoading
  }: { data: IRoutineDailySession; isLoading: boolean } = useListRoutineDailySessionQuery(
    sessionId,
    {
      skip: !sessionId
    }
  );

  const [deleteDailySession, { isLoading: isDeleting }] =
    useDeleteRoutineDailySessionByIdMutation();

  const [dailySessionModalOpen, toggleDailySessionModal] = useToggle(false);
  const [dailySessionId, setDailySessionId] = useState<any>();

  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const onDeleteConfirm = () => {
    deleteDailySession({ dailySessionId, routineId })
      .unwrap()
      .then(() => {
        toast.success('Daily Session removed successfully');
        toggleDeleteModal();
      })
      .catch((err: any) => {
        toast.error('Failed to remove Daily Session');
      });
  };

  return dailySessionData?.daily_sessions?.length ? (
    <>
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      {dailySessionData?.daily_sessions?.map((dailySession, index) => (
        <div className="card" key={index}>
          <div className="card-body p-2 px-3">
            <div className="d-flex flex-wrap justify-content-between">
              <p className="m-0">{getLocaleDateOnly(new Date(dailySession.routine_date))}</p>
              <p className="m-0">
                {formatTime(dailySession.altered_start_time)} -{' '}
                {formatTime(dailySession.altered_end_time)}
              </p>
              <div>
                <div className="dropdown d-flex justify-content-end">
                  <a
                    href="#"
                    role="button"
                    id="dropdownMenuButton1"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <i className="ri-more-2-fill"></i>
                  </a>

                  <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li
                      onClick={() => {
                        setDailySessionId(dailySession?.id);
                        toggleDailySessionModal();
                      }}
                    >
                      <a className="dropdown-item">
                        <img src={editIcon} style={{ padding: '4px' }} />
                        Edit
                      </a>
                    </li>
                    <li
                      onClick={() => {
                        setDailySessionId(dailySession?.id);
                        toggleDeleteModal();
                      }}
                    >
                      <a className="dropdown-item">
                        <img src={deleteIcon} style={{ padding: '4px', marginLeft: '3px' }} />{' '}
                        Delete
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <DailySessionModal
              open={dailySessionModalOpen}
              sessionId={sessionId}
              toggle={toggleDailySessionModal}
              dailySessionId={dailySessionId}
              routineId={routineId}
            />

            <div className="d-flex flex-column">
              <h6 className="text-black mt-3">{dailySession.course_topic?.title}</h6>
              <ul className="text-info">
                {dailySession.lecturers.map((l, i) => (
                  <li key={i}>{l.first_name + ' ' + l.last_name}</li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      ))}
    </>
  ) : (
    <div className="card">
      <div className="card-body p-2 px-3">
        <div className="text-center">No sessions</div>
      </div>
    </div>
  );
}

export default RoutineSessionDailyDetail;
