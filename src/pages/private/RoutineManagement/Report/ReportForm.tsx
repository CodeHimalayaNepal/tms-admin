import React, { useState } from 'react';
import useToggle from 'hooks/useToggle';
import Container from 'components/ui/Container';
import CustomSelect from 'components/ui/Editor/CustomSelect';
import EditorComponent from 'components/ui/Editor/editor';
import { FileDrop } from 'components/ui/FileDrop/FileDrop';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import TextInput from 'components/ui/TextInput';
import { useFormik } from 'formik';
import { useListAdminUsersQuery } from 'redux/reducers/user';
import { initialReportFormValues } from './Schema';
import { useNavigate, useParams } from 'react-router-dom';
import { refreshTokenRef, tokenRef } from 'common/constant';
import { baseApiUrl } from 'redux/reducers/settings';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useDeleteReportMutation } from 'redux/reducers/routine';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';

// import { useDownloadReportQuery } from 'redux/reducers/routine';
// const generateId = (length = 5) => {
//   return new Array(length)
//     .fill(null)
//     .map((_) => Math.round(Math.random() * length).toString(16))
//     .join('');
// };

const ReportForm = () => {
  const { id } = useParams();
  const { data: adminUsers, isLoading: adminLoading } = useListAdminUsersQuery();
  // const { data: report, isLoading: isDownloading } = useDownloadReportQuery(id);
  const [deleteReport, { isLoading: isDeletingReport }] = useDeleteReportMutation();
  const [isDownloadingReport, setIsDownloadingReport] = useState(false);
  const [isCreatingReport, setIsCreatingReport] = useState(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const facultyMember = adminUsers?.map(
    (c: { id: number; first_name: string; last_name: string }) => ({
      label: c.first_name + ' ' + c.last_name,
      value: c.id?.toString() || ''
    })
  );
  const onDeleteConfirm = () => {
    deleteReport(id)
      .unwrap()
      .then((res: unknown) => {
        toast.success('Report deleted Successfully');
      })
      .then(() => toggleDeleteModal())
      .catch((error: any) => {
        toast.error(error?.data?.detail);
      });
  };

  const navigate = useNavigate();
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: initialReportFormValues,
    onSubmit: async (values) => {
      const payload = { ...values, id: id };
      try {
        setIsCreatingReport(true);
        const token = window.localStorage[tokenRef];
        const URL = baseApiUrl + `reports/create_pdf/routine/${id}/`;
        const res = await axios.post(URL, payload, {
          headers: {
            authorization: `Bearer  ${JSON.parse(token)}`
          }
        });
        const fileURL = window.URL.createObjectURL(
          new Blob([res.data], { type: 'application/pdf' })
        );
        let alink = document.createElement('a');
        alink.href = fileURL;
        alink.download = 'SamplePDF.pdf';
        alink.click();
        setIsCreatingReport(false);
        toast.success('Report generated and downloaded Successfully');
      } catch (e: any) {
        setIsCreatingReport(false);
        toast.error(e?.response?.data?.detail || 'Something went Wrong!');
      }
    }
  });

  return (
    <Container title="Report">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeletingReport}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <div className="row">
        <div className="col-9"></div>
        <div className="col-3 d-flex justify-content-around">
          <SecondaryButton title="Delete Report" onClick={() => toggleDeleteModal()} />
          <PrimaryButton
            title="Download Report"
            type="button"
            isLoading={isDownloadingReport}
            onClick={async () => {
              try {
                setIsDownloadingReport(true);
                const token = window.localStorage[tokenRef];
                const URL = baseApiUrl + `reports/download_pdf/routine/${id}/`;
                await axios
                  .get(URL, {
                    headers: {
                      authorization: `Bearer  ${JSON.parse(token)}`
                    }
                  })
                  .then((res: any) => {
                    const fileURL = window.URL.createObjectURL(
                      new Blob([res.data], { type: 'application/pdf' })
                    );
                    let alink = document.createElement('a');
                    alink.href = fileURL;
                    alink.download = 'SamplePDF.pdf';
                    alink.click();
                    setIsDownloadingReport(false);
                    toast.success('Successfully Downloaded');
                  });
              } catch (e: any) {
                setIsDownloadingReport(false);
                toast.error(e.response.data.detail);
              }
            }}
          />
        </div>
      </div>
      <form onSubmit={formik.handleSubmit}>
        <div className="row">
          <label
            htmlFor="faculty"
            style={{
              fontWeight: '500',
              fontSize: '16px',
              letterSpacing: '0.15px',
              color: '#0e548f'
            }}
          >
            Choose Faculty Member
          </label>
          <CustomSelect
            name="faculty"
            onChange={(value: any) => formik.setFieldValue('faculty', value)}
            value={formik.values.faculty}
            onBlur={formik.handleBlur}
            formik={formik}
            options={facultyMember}
            isMulti
          />
          <div className="col-6 d-flex align-items-end pb-2">
            <input
              name="date"
              type="date"
              style={{
                height: '38px',
                width: '200px',
                border: '1px solid hsl(0, 0%, 80%)',
                borderRadius: '4px'
              }}
              onChange={(data) => formik.setFieldValue('date', data.target.value)}
            />
          </div>
          <EditorComponent
            label="Submitted by:"
            containerClassName="mt-3"
            editorValue={formik.values.submitted_by}
            onChangeValue={(data) => formik.setFieldValue('submitted_by', data)}
          />
          <EditorComponent
            label="Submitted to:"
            containerClassName="mt-3"
            editorValue={formik.values.submitted_to}
            onChangeValue={(data) => formik.setFieldValue('submitted_to', data)}
          />
          <EditorComponent
            label="Introduction:"
            containerClassName="mt-3"
            editorValue={formik.values.introduction}
            onChangeValue={(data) => formik.setFieldValue('introduction', data)}
          />
          <EditorComponent
            label="Training Details:"
            containerClassName="mt-3"
            editorValue={formik.values.training_details}
            onChangeValue={(data) => formik.setFieldValue('training_details', data)}
          />
          <EditorComponent
            label="Opening Ceremony:"
            containerClassName="mt-3"
            editorValue={formik.values.opening_ceremony}
            onChangeValue={(data) => formik.setFieldValue('opening_ceremony', data)}
          />
          <EditorComponent
            label="Closing Ceremony:"
            containerClassName="mt-3"
            editorValue={formik.values.closing_ceremony}
            onChangeValue={(data) => formik.setFieldValue('closing_ceremony', data)}
          />
          <EditorComponent
            label="Co-ordinator's Remarks:"
            containerClassName="mt-3"
            editorValue={formik.values.coordinator_remarks}
            onChangeValue={(data) => formik.setFieldValue('coordinator_remarks', data)}
          />
          {/* <div className="row mt-3">
            <p
              style={{
                fontWeight: '500',
                fontSize: '16px',
                letterSpacing: '0.15px',
                color: '#0e548f'
              }}>
              Training Evaluation:
            </p> */}

          {/* <div className="row">
              <div className="col-6">
                <PrimaryButton
                  title="Add +"
                  type="button"
                  onClick={() => {
                    formik.setFieldValue(
                      'training_evaluation',
                      [
                        ...formik.values.training_evaluation,
                        {
                          title: '',
                          image: [],
                          id: generateId()
                        }
                      ],
                      true
                    );
                  }}
                />
              </div>
            </div>
            {formik?.values?.training_evaluation?.map(
              ({ title, image, id }: any, index: number) => {
                return (
                  <div className="col-sx-12 col-md-5 p-4" key={id}>
                    <TextInput
                      containerClassName="mt-3 p-2"
                      label={'Title'}
                      name={`training_evaluation[${index}].title`}
                      type="text"
                      height={20}
                      onChange={formik.handleChange}
                      value={title}
                      onBlur={formik.handleBlur}
                      formik={formik}
                    />
                    <FileDrop
                      files={Array.isArray(image) ? image : []}
                      setFiles={(data) =>
                        formik.setFieldValue(`training_evaluation[${index}].image`, data)
                      }
                      message={`Drop Evaluation Image Here `}
                    />
                    <SecondaryButton
                      title="delete"
                      onClick={() => {
                        formik.setFieldValue(
                          'training_evaluation',
                          [...formik.values.training_evaluation].filter((value) => value.id !== id),
                          true
                        );
                      }}
                    />
                  </div>
                );
              }
            )}
          </div> */}
          {/* </div> */}

          <EditorComponent
            label="Annex:"
            containerClassName="mt-3"
            editorValue={formik.values.annex}
            onChangeValue={(data) => formik.setFieldValue('annex', data)}
          />
          <div className="row mt-4">
            <div className="col-12 d-flex justify-content-end">
              <PrimaryButton title="Submit" type="submit" isLoading={isCreatingReport} />
            </div>
          </div>
        </div>
      </form>
    </Container>
  );
};

export default ReportForm;
