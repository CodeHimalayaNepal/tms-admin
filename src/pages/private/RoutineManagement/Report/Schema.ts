export const initialReportFormValues = {
  faculty: '',
  submitted_by: '',
  submitted_to: '',
  date: '',
  introduction: '',
  training_details: '',
  opening_ceremony: '',
  closing_ceremony: '',
  coordinator_remarks: '',
  // training_evaluation: [{ title: '', image: [], id: -1 }],
  annex: ''
};
