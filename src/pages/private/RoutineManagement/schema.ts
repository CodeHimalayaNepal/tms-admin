import { IRoutineDetail } from 'redux/reducers/routine';
import * as Yup from 'yup';

export const routineManagementInitialValues = {
  routine_id: null as number | null,
  training: null as number | null,
  hall: null as number | null,
  hall_details: null,
  batch_name: '',
  description: '',
  start_date: '',
  end_date: '',
  is_active: true,
  tr_routine_coordinator: [
    // {
    //   routine_coor
    //   is_module_coordinator: true,
    //   course_id: ''
    //, }
  ],
  tr_routine_coordinators: {
    // {
    //   routine_coor
    //   is_module_coordinator: true,
    //   course_id: ''
    //, }
  } as Record<string, any>,
  attendance_type: 'daily_session' as 'daily_session' | 'routine',
  attendance_checkin_time: '',
  attendance_checkout_time: '',
  buffer_time_before: '',
  buffer_time_after: ''
  //  tr_routine_coordinators: []
};

export const RoutineSchemaValidations = Yup.object().shape({
  training: Yup.string().required('Training is required').nullable(),
  hall: Yup.string().required('Hall is required').nullable(),
  batch_name: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Batch is required'),
  start_date: Yup.string().required('Start Date is required'),
  end_date: Yup.string().required('End Date is required'),
  description: Yup.string().required('Description is required'),
  tr_routine_coordinator: Yup.array().min(1, 'Coordinator is required'),

  attendance_type: Yup.string().required('Attendance type is required'),
  attendance_checkin_time: Yup.string().when('attendance_type', {
    is: 'routine',
    then: Yup.string().required('Attendance Check-in time is required'),
    otherwise: Yup.string()
  }),
  attendance_checkout_time: Yup.string().when('attendance_type', {
    is: 'routine',
    then: Yup.string().required('Attendance Check-out time is required'),
    otherwise: Yup.string()
  }),
  buffer_time_before: Yup.string().when('attendance_type', {
    is: 'routine',
    then: Yup.string().required('Buffer time before attendance time is required'),
    otherwise: Yup.string()
  }),
  buffer_time_after: Yup.string().when('attendance_type', {
    is: 'routine',
    then: Yup.string().required('Buffer time after attendance time is required'),
    otherwise: Yup.string()
  })
});

export const inviteTraineeInitialValues = {
  trainee: []
};

export const InviteTraineeSchemaValidations = Yup.object().shape({
  trainee: Yup.array().min(1, 'Please add a trainee')
});
