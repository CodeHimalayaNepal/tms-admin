import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import React, { FC, useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import { inviteTraineeInitialValues, InviteTraineeSchemaValidations } from './schema';
import {
  IRoutineEnrollment,
  useCreateRoutineEnrollmentMutation,
  useDeleteRoutineEnrollmentMutation,
  useEnrollTraineeInRoutineMutation,
  useListRoutineEnrollmentQuery
} from 'redux/reducers/routine-enrollment';
import CustomSelect from 'components/ui/Editor/CustomSelect';
import { useListsUsersByRoleQuery } from 'redux/reducers/administrator';
import FormikValidationError from 'components/ui/FormikErrors';
import { IRoutineDetail } from 'redux/reducers/routine';
import DataTable from 'components/ui/DataTable/data-table';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import useToggle from 'hooks/useToggle';
import { useListTraineeQuery } from 'redux/reducers/auth';

interface IInviteUsersModal {
  selectedRoutine: string;
}
const InviteUsersModal: FC<IInviteUsersModal> = ({ selectedRoutine }) => {
  const [initialFormModal] = React.useState(inviteTraineeInitialValues);
  const [inviteTraineeModal, toggleInviteTraineeModal] = useToggle(false);

  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [deletingEnrollTrainee, setDeletingTrainee] = React.useState<any>();
  const [page, setPage] = useState(1);
  const [allDataList, setAllDataList] = useState(0);
  const { data: routineEnrollUserLists } = useListRoutineEnrollmentQuery({
    routineId: selectedRoutine
  });
  const totalUser = routineEnrollUserLists?.results?.data;
  const paginatedData = totalUser?.slice(
    page === 1 ? 0 : (page - 1) * 5,
    page === 1 ? 5 : page * 5
  );
  const canPrevious = page === 1 ? false : true;
  const canNext = allDataList === totalUser?.length ? false : true;
  const { data: usersLists } = useListTraineeQuery('');
  const [deleteRoutineEnrollment, { isLoading: isDeleting }] = useDeleteRoutineEnrollmentMutation();

  const [enrollTrainee, { isLoading: isAddingTrainee }] = useEnrollTraineeInRoutineMutation({});

  const onEnroll = async (trainee: string[]) => {
    try {
      await enrollTrainee({
        routine: +selectedRoutine,
        trainee: trainee.map((t) => +t)
      }).unwrap();
      toggleInviteTraineeModal();
      toast.success('Trainee added successfully!');
    } catch (error) {
      toast.error('Failed to add trainee!');
    }
  };

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: InviteTraineeSchemaValidations,
    initialValues: initialFormModal,

    onSubmit: (values) => {
      onEnroll(values.trainee);
    }
  });

  const traineeUserLists = usersLists?.data.map((item: any) => {
    return {
      value: item.id?.toString(),
      label:
        item.first_name +
        ' ' +
        item.last_name +
        '  -  ' +
        item.mobile_number +
        '   (' +
        item.user[0].email +
        ')'
    };
  });

  // .filter(
  //   (item: any) =>
  //     !routineEnrollUserLists?.results?.data?.some(
  //       (enroll: any) => enroll?.id?.toString() === item?.value?.toString()
  //     )
  // );

  const onDeleteClicked = (data: any) => {
    toggleDeleteModal();
    setDeletingTrainee(data);
  };

  const onConfirmDelete = () => {
    deleteRoutineEnrollment(deletingEnrollTrainee.id)
      .unwrap()
      .then(() => {
        toast.success('Trainee removed successfully');
        toggleDeleteModal();
      })
      .catch((err: any) => {
        toast.error('Failed to remove trainee');
      });
  };

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{data.row.index + (page - 1) * 5 + 1}</>;
      }
    },
    {
      Header: 'First Name',
      accessor: 'first_name'
    },
    {
      Header: 'Last Name',
      accessor: 'last_name'
    },
    {
      Header: 'Phone',
      accessor: 'mobile_number'
    }
    // {
    //   Header: '  Action',
    //   Cell: (data: any) => {
    //     return (
    //       <div className="relative">
    //         <PrimaryButton
    //           isDanger
    //           title="Remove"
    //           type="button"
    //           className="m-2"
    //           onClick={() => onDeleteClicked(data.row.original)}
    //         />
    //       </div>
    //     );
    //   }
    // }
  ];

  useEffect(() => {
    if (routineEnrollUserLists) {
      formik.setFieldValue(
        'trainee',
        routineEnrollUserLists?.results?.data?.map((user: any) => user?.id.toString())
      );
    }
  }, [routineEnrollUserLists]);
  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <Modal
          title="Add Trainee"
          isModalOpen={inviteTraineeModal}
          closeModal={toggleInviteTraineeModal}
          modalSize="modal-md"
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton onClick={toggleInviteTraineeModal} title="Close" />
              <PrimaryButton
                title="Add Trainee"
                iconName="align-bottom me-1"
                type="submit"
                isLoading={isAddingTrainee}
              />
            </div>
          )}
        >
          <CustomSelect
            containerClassName="col-12"
            options={traineeUserLists}
            placeholder={'Search by name'}
            value={formik.values.trainee}
            onChange={(value: any) => formik.setFieldValue('trainee', value)}
            label={'Select Trainee to enroll'}
            isMulti={true}
          />
          <FormikValidationError name="training" errors={formik.errors} touched={formik.touched} />
        </Modal>
      </form>

      <div className="d-flex justify-content-between">
        <h6 className="mb-4">Trainee Count- {totalUser?.length}</h6>
        <PrimaryButton title="Add Trainee" type="button" onClick={toggleInviteTraineeModal} />
      </div>

      <DataTable
        search={false}
        columns={columns}
        data={paginatedData || []}
        tableHeaders={() => <></>}
        pagination={{
          canPrevious,
          canNext,
          prevPage: () => {
            if (totalUser) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (totalUser) {
              totalUser?.length >= allDataList && setPage(page + 1);
            }
          }
        }}
      />

      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onConfirmDelete}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to trainee from this routine ? </p>
          </div>
        </>
      </ConfirmationModal>
    </div>
  );
};

export default InviteUsersModal;
