import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import CustomSelect from 'components/ui/Editor/CustomSelect';
import PrimaryButton from 'components/ui/PrimaryButton';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { IRoutineDetail, useDeleteRoutineMutation } from 'redux/reducers/routine';

import RoutineManagementTable from './RoutineManagementTable';
import { useListTrainingQuery } from 'redux/reducers/training';

const Routines = () => {
  let navigate = useNavigate();

  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [deletingRoutineState, setDeletingRoutines] = useState<IRoutineDetail | null>(null);

  const [deleteRoutine, { isLoading: isDeleting }] = useDeleteRoutineMutation();
  const { data: trainingData, isLoading } = useListTrainingQuery();

  const [training, setTraining] = useState('');

  const TrainingList = trainingData?.map((item: any) => {
    return {
      value: item.id,
      label: item.training_name
    };
  });
  const [routineStatusValue, setRoutineStatusValue] = useState('');
  const RoutineStatusList = [
    {
      value: 'Running',
      label: 'Running'
    },
    {
      value: 'Completed',
      label: 'Completed'
    },
    {
      value: 'Upcoming',
      label: 'Upcoming'
    }
  ];

  const onViewClicked = (routine_master_id: number) => {
    navigate(`/routine-management/view/${routine_master_id}`);
  };

  const onEditClicked = (routine_master_id: number) => {
    navigate(`/routine-management/edit/${routine_master_id}`);
  };

  const onDeleteConfirm = () => {
    deletingRoutineState &&
      deleteRoutine(deletingRoutineState?.routine_master_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');

          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IRoutineDetail) => {
    setDeletingRoutines(data);
    toggleDeleteModal();
  };

  return (
    <Container title="Routine">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>

      <RoutineManagementTable
        tableHeaders={() => (
          <>
            <div
              style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                width: '40rem'
              }}
            >
              <div style={{ marginTop: '25px' }}>
                <PrimaryButton
                  title={'Create Routine'}
                  iconName="ri-add-line align-bottom me-1"
                  onClick={() => {
                    navigate('/routine-management/create');
                  }}
                  type="button"
                />
              </div>

              <CustomSelect
                name="status"
                options={RoutineStatusList}
                placeholder={'Status'}
                value={routineStatusValue}
                onChange={(e: any) => {
                  setRoutineStatusValue(e);
                }}
              />
              <CustomSelect
                name="trainingname"
                options={TrainingList}
                placeholder={'Training Name'}
                value={training}
                onChange={(e: any) => {
                  setTraining(e);
                }}
              />
            </div>
          </>
        )}
        onViewClicked={onViewClicked}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
        routineStatusValue={routineStatusValue}
        training={training}
      />
    </Container>
  );
};

export default Routines;
