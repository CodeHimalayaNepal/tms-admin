import { IOrganizationHallInput } from 'redux/reducers/organization-halls';
import * as Yup from 'yup';

export const organizationHallInitialValues: IOrganizationHallInput = {
  organization_id: 0,
  hall_name: '',
  hall_name_np: '',
  hall_unique_key: '',
  status: true,
  organization: '' as any
};

export const OrganizationHallSchemaValidations = Yup.object().shape({
  hall_name: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Title is required'),
  hall_name_np: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Hall Name Nepali is required'),
  hall_unique_key: Yup.number()
    .typeError('digit is required')
    .required('Attendance Device Id is required')
  // status: Yup.boolean().required('Required'),
  // organization: Yup.string().required('organization is required')
});
