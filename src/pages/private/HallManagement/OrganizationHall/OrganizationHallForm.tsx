// import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { IOrganizationHallInput } from 'redux/reducers/organization-halls';
import { useListOrganizationQuery } from 'redux/reducers/organizations';

interface ITrainingForm {
  formik: FormikProps<IOrganizationHallInput>;
}
const TrainingForm: FC<ITrainingForm> = ({ formik }) => {
  const { data: organizationData } = useListOrganizationQuery();
  return (
    <form>
      <div className="row">
        <div className="col-md-4 mt-3">
          <div className="mb-3">
            <label htmlFor="trainingType-field" className="form-label">
              Organizations
            </label>
            <select
              id="trainingType-field"
              className="form-select"
              data-choices
              data-choices-sorting="true"
              value={formik?.values?.organization_id}
              onChange={(e) => formik.setFieldValue('organization_id', +e.target.value)}
            >
              <option selected>Choose...</option>
              {organizationData?.data?.results.map((item: any) => {
                return (
                  <>
                    {' '}
                    <option value={+item.tms_organization_id}>{item.name}</option>
                  </>
                );
              })}
            </select>
          </div>
        </div>

        <TextInput
          label="Title"
          name="hall_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.hall_name}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="Title in nepali"
          name="hall_name_np"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.hall_name_np}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="Unique Key"
          name="hall_unique_key"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.hall_unique_key}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              Status
            </label>
            <input
              type="checkbox"
              className="form-check-input"
              id="customSwitchsizesm"
              checked={formik.values.status}
              onChange={(e) => {
                formik.setFieldValue('status', !formik.values.status);
              }}
            />
          </div>
        </div>

        {/* <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              
            </label>
            <input type="checkbox" className="form-check-input" id="customSwitchsizesm" />
          </div>
        </div> */}
      </div>
    </form>
  );
};

export default TrainingForm;
