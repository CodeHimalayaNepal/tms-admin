import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import {
  IOrganizationHallInput,
  useCreateOrganizationHallMutation,
  useDeleteOrganizationHallMutation,
  useUpdateOrganizationHallMutation
} from 'redux/reducers/organization-halls';

import { organizationHallInitialValues, OrganizationHallSchemaValidations } from './schema';

import OrganizationHallForm from './OrganizationHallForm';
import OrganizationHallTable from './OrganizationsHallTable';

const OrganizationHalls = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [hover, setHover] = useState(false);

  const [organizationHallInitialFormState, setOrganizationHallInitialForm] = useState(
    organizationHallInitialValues
  );
  const [deletingOrganizationHallState, setDeletingOrganizationHalls] =
    useState<IOrganizationHallInput | null>(null);

  const [deleteOrganizationHall, { isLoading: isDeleting }] = useDeleteOrganizationHallMutation();
  const [createOrganizationHall, { isLoading: isCreating }] = useCreateOrganizationHallMutation();
  const [updateOrganizationHall, { isLoading: isUpdating }] = useUpdateOrganizationHallMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: OrganizationHallSchemaValidations,
    initialValues: organizationHallInitialFormState,
    onSubmit: (values) => {
      let executeFunc = createOrganizationHall;
      if (values.hall_id) {
        executeFunc = updateOrganizationHall;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Update Successfully');
          setOrganizationHallInitialForm(organizationHallInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          console.log(err?.data?.hall_unique_key[0]);
          if (err?.data?.hall_unique_key[0]) {
            toast.error(err?.data?.hall_unique_key[0]);
          }
          toast.error('Error');
        });
    }
  });

  const onEditClicked = (data: IOrganizationHallInput) => {
    setOrganizationHallInitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingOrganizationHallState &&
      deleteOrganizationHall(deletingOrganizationHallState?.hall_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setOrganizationHallInitialForm(organizationHallInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IOrganizationHallInput) => {
    setDeletingOrganizationHalls(data);
    toggleDeleteModal();
  };

  const closeModal = () => {
    setFormModal();
    setOrganizationHallInitialForm(organizationHallInitialValues);
  };

  return (
    <Container title="Organization Halls" subTitle="List of all halls">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <OrganizationHallTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title={
                organizationHallInitialFormState.hall_id
                  ? 'Edit Organization Hall'
                  : 'Create New  Hall'
              }
              iconName="ri-add-line align-bottom me-1"
              onClick={setFormModal}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={
            organizationHallInitialFormState.hall_id ? 'Edit Organization Hall' : 'Create New  Hall'
          }
          isModalOpen={formModal}
          closeModal={closeModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={closeModal}
                title="Close"
                isLoading={isUpdating || isCreating}
                onMouseOver={() => {
                  setHover(true);
                }}
                onMouseLeave={() => {
                  setHover(false);
                }}
                style={hover ? { color: 'blue' } : {}}
              />
              <PrimaryButton
                title={
                  organizationHallInitialFormState.hall_id
                    ? 'Edit OrganizationHall'
                    : 'Add OrganizationHall'
                }
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <OrganizationHallForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default OrganizationHalls;
