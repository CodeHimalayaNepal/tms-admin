import PrimaryButton from 'components/ui/PrimaryButton';
import TextInput from 'components/ui/TextInput';
import { useFormikContext } from 'formik';
import { FC } from 'react';
import { MCQInitialValues } from './AssignmentSchema';
interface IFeedbackForm {}
const AssignmentEditform: FC<IFeedbackForm> = () => {
  const formik = useFormikContext<typeof MCQInitialValues>();

  return (
    <div>
      <div className="row">
        <div className="col-12">
          <TextInput
            containerClassName="mt-3 "
            label="Question"
            name="question"
            type="text"
            height={20}
            onChange={formik.handleChange}
            value={formik.values?.question}
            onBlur={formik.handleBlur}
            formik={formik}
          />
        </div>
        <div className="col-12">
          <TextInput
            containerClassName="mt-3 "
            label="Difficulty Level"
            name="difficulty_level"
            type="text"
            height={20}
            onChange={formik.handleChange}
            value={formik.values?.difficulty_level}
            onBlur={formik.handleBlur}
            formik={formik}
          />
        </div>
        <div className="col-12">
          <TextInput
            containerClassName="mt-3 "
            label="order"
            name="order"
            type="text"
            height={20}
            onChange={formik.handleChange}
            value={formik.values?.order}
            onBlur={formik.handleBlur}
            formik={formik}
          />
        </div>
        <div className="col-12">
          <TextInput
            containerClassName="mt-3 "
            label="Marks"
            name="marks"
            type="text"
            height={20}
            onChange={formik.handleChange}
            value={formik.values?.marks}
            onBlur={formik.handleBlur}
            formik={formik}
          />
        </div>
        <div className="row" style={{ justifyContent: 'flex-end' }}>
          <div
            className="col-md-3  col-sx-6"
            style={{ marginTop: '10px', display: 'flex', justifyContent: 'flex-end' }}
          >
            <PrimaryButton
              title="Add Options"
              iconName="ri-add-line align-bottom me-1"
              type="button"
              onClick={() => {
                formik.setFieldValue(
                  'options',
                  [...formik.values?.options, { is_correct: false, answer: '' }],
                  true
                );
              }}
            />
          </div>
        </div>
        {formik.values?.options?.map((opt: any, ioption: number) => {
          return (
            <div className="col-sx-12 col-md-6 mt-3" key={ioption}>
              <TextInput
                containerClassName="mt-3"
                label="Option"
                name={`options[${ioption}].answer`}
                type="text"
                height={20}
                onChange={formik.handleChange}
                value={opt.answer}
                onBlur={formik.handleBlur}
                formik={formik}
              />

              <label className="form-check-label" htmlFor="customSwitchsizesm">
                Is true
              </label>
              <input
                type="checkbox"
                className="form-check-input"
                id="customSwitchsizesm"
                checked={opt.is_correct}
                onChange={(e) => {
                  formik.setFieldValue(`options[${ioption}].is_correct`, !opt.is_correct);
                }}
              />
            </div>
          );
        })}
        <hr
          className="border-1 mx-3"
          style={{ backgroundColor: 'black', width: '96%', marginTop: '20px' }}
        />
      </div>
    </div>
  );
};

export default AssignmentEditform;
