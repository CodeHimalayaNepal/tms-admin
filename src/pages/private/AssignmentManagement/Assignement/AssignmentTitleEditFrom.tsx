import { FileDrop } from 'components/ui/FileDrop/FileDrop';
import FormikValidationError from 'components/ui/FormikErrors';
import PrimaryButton from 'components/ui/PrimaryButton';
import TextInput from 'components/ui/TextInput';
import { useFormikContext } from 'formik';
import { FC } from 'react';

import { AssignmentEditValues } from './AssignmentSchema';
interface IFeedbackForm {}
const AssignmentTitleEditfrom: FC<IFeedbackForm> = () => {
  const formik = useFormikContext<typeof AssignmentEditValues>();

  return (
    <div>
      <div className="row">
        <div className="col-12">
          <TextInput
            containerClassName="mt-3 "
            label="Assignment Title"
            name="assignment_title"
            type="text"
            height={20}
            onChange={formik.handleChange}
            value={formik.values?.assignment_title}
            onBlur={formik.handleBlur}
            formik={formik}
          />
        </div>
        <div className="col-12">
          <TextInput
            containerClassName="mt-3 "
            label="Assignment Description"
            name="assignment_description"
            type="text"
            height={20}
            onChange={formik.handleChange}
            value={formik.values?.assignment_description}
            onBlur={formik.handleBlur}
            formik={formik}
          />
        </div>

        <div className="dropzone " style={{ marginTop: '10px' }}>
          <FileDrop
            files={
              Array.isArray(formik.values?.assignment_file) ? formik.values?.assignment_file : []
            }
            setFiles={(data) => formik.setFieldValue('assignment_file', data)}
            message="Drop Assignment here or click to upload."
          />
          <FormikValidationError
            name="assignment_file"
            errors={formik.errors}
            touched={formik.touched}
          />
        </div>
      </div>
    </div>
  );
};

export default AssignmentTitleEditfrom;
