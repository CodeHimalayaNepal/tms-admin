import { FileDrop } from 'components/ui/FileDrop/FileDrop';
import FormikValidationError from 'components/ui/FormikErrors';
import PrimaryButton from 'components/ui/PrimaryButton';
import TextInput from 'components/ui/TextInput';
import { FieldArray, FormikProps, useFormikContext } from 'formik';
import { FC, useCallback, useEffect } from 'react';
import { useState } from 'react';
import { AssignmentInitialValues } from './AssignmentSchema';
interface IFeedbackForm {}
const AssignmentTypeForm: FC<IFeedbackForm> = () => {
  const formik = useFormikContext<typeof AssignmentInitialValues>();

  const generateId = (length = 5) => {
    return new Array(length)
      .fill(null)
      .map((_) => Math.round(Math.random() * length).toString(16))
      .join('');
  };
  return (
    <div className="row">
      <div className="col-12">
        <TextInput
          containerClassName="mt-3 "
          label="Assignment Title"
          name="assignment_title"
          type="text"
          height={20}
          onChange={formik.handleChange}
          value={formik.values.assignment_title}
          onBlur={formik.handleBlur}
          formik={formik}
        />
      </div>
      <div className="col-12">
        <TextInput
          containerClassName="mt-3 "
          label="Assignment Description"
          name="assignment_description"
          type="text"
          height={20}
          onChange={formik.handleChange}
          value={formik.values.assignment_description}
          onBlur={formik.handleBlur}
          formik={formik}
        />
      </div>

      <div className="dropzone " style={{ marginTop: '10px' }}>
        <FileDrop
          files={Array.isArray(formik.values.assignment_file) ? formik.values.assignment_file : []}
          setFiles={(data) => formik.setFieldValue('assignment_file', data)}
          message="Drop Assignment here or click to upload."
        />
        <FormikValidationError
          name="assignment_file"
          errors={formik.errors}
          touched={formik.touched}
        />
      </div>
      <div className="row" style={{ justifyContent: 'flex-end' }}>
        <div
          className="col-md-3  col-sx-6"
          style={{ marginTop: '10px', display: 'flex', justifyContent: 'flex-end' }}
        >
          <PrimaryButton
            title="Add Question"
            iconName="ri-add-line align-bottom me-1"
            type="button"
            onClick={() => {
              formik.setFieldValue(
                'mcq_id',
                [
                  ...formik.values.mcq_id,
                  {
                    question: '',
                    order: 0,
                    difficulty_level: 1,
                    marks: 0,
                    id: generateId(),
                    options: [{ is_correct: false, answer: '' }]
                  }
                ],
                true
              );
            }}
          />
        </div>
      </div>
      {formik.values?.mcq_id?.map(
        ({ question, difficulty_level, order, marks, options, id }: any, index: number) => {
          return (
            <div
              className="row"
              style={{
                border: '1px solid gray',
                borderRadius: '10px',
                margin: '10px 0px',
                padding: '0px'
              }}
            >
              {Object.entries({ question, difficulty_level, order, marks })?.map(
                ([type, value]) => {
                  return (
                    <div className="col-sx-12 col-md-6">
                      <TextInput
                        containerClassName="mt-3"
                        label={type}
                        name={`mcq_id[${index}].${type}`}
                        type="text"
                        height={20}
                        onChange={formik.handleChange}
                        value={value}
                        onBlur={formik.handleBlur}
                        formik={formik}
                      />
                    </div>
                  );
                }
              )}
              <div className="row" style={{ justifyContent: 'flex-end' }}>
                <div
                  className="col-md-3  col-sx-6"
                  style={{ marginTop: '10px', display: 'flex', justifyContent: 'flex-end' }}
                >
                  <PrimaryButton
                    title="Add Options"
                    iconName="ri-add-line align-bottom me-1"
                    type="button"
                    onClick={() => {
                      formik.setFieldValue(
                        `mcq_id[${index}].options`,
                        [...formik.values.mcq_id[index].options, { is_correct: false, answer: '' }],
                        true
                      );
                    }}
                  />
                </div>
              </div>
              {options?.map((opt: any, ioption: number) => {
                return (
                  <div className="mt-3" key={id}>
                    <TextInput
                      containerClassName="mt-3"
                      label="Option"
                      name={`mcq_id[${index}].options[${ioption}].answer`}
                      type="text"
                      height={20}
                      onChange={formik.handleChange}
                      value={opt.answer}
                      onBlur={formik.handleBlur}
                      formik={formik}
                    />

                    <label className="form-check-label" htmlFor="customSwitchsizesm">
                      Is true
                    </label>
                    <input
                      type="checkbox"
                      className="form-check-input"
                      id="customSwitchsizesm"
                      checked={opt.is_correct}
                      onChange={(e) => {
                        formik.setFieldValue(
                          `mcq_id[${index}].options[${ioption}].is_correct`,
                          !opt.is_correct
                        );
                      }}
                    />
                  </div>
                );
              })}
              <div className="row" style={{ justifyContent: 'flex-end' }}>
                <div style={{ marginBottom: '10px', display: 'flex', justifyContent: 'flex-end' }}>
                  <PrimaryButton
                    title="Remove Question"
                    isDanger={true}
                    type="button"
                    onClick={() => {
                      formik.setFieldValue(
                        'mcq_id',
                        [...formik.values?.mcq_id].filter((value) => value.id !== id)
                      );
                    }}
                  />
                </div>
              </div>
            </div>
          );
        }
      )}
    </div>
  );
};

export default AssignmentTypeForm;
