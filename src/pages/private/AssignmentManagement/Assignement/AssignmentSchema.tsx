import { IFeedbackFormInput } from 'redux/reducers/Feedback2';
import * as Yup from 'yup';

export const AssignmentInitialValues: any = {
  assignment_title: '',
  assignment_description: '',
  assignment_file: [],
  mcq_id: [
    {
      id: -1,
      question: '',

      order: 0,
      difficulty_level: 1,
      marks: 0,

      options: [{ is_correct: false, answer: '' }]
    }
  ]
};

export const AssignmentSchemaValidations = Yup.object().shape({
  assignment_title: Yup.string().required('Assignment Title is required'),
  assignment_description: Yup.string().required('Assignment Description is required'),
  mcq_id: Yup.array(
    Yup.object().shape({
      question: Yup.string().required('Question is required'),
      order: Yup.number().required('Order is required'),
      marks: Yup.number().required('Marks is required'),
      difficulty_level: Yup.number().required('Difficulty level is required'),

      options: Yup.array(
        Yup.object().shape({
          is_correct: Yup.boolean(),
          answer: Yup.string()
        })
      )
    })
  )
});

export const AssignmentEditValues: any = {
  assignment_title: '',
  assignment_description: '',
  assignment_file: []
};
export const AssignmentEditSchemaValidations = Yup.object().shape({
  assignment_title: Yup.string().required('Assignment Title is required'),
  assignment_description: Yup.string().required('Assignment Description is required')
});

export const MCQInitialValues: any = {
  mcq_id: [
    {
      question: '',

      order: 0,
      difficulty_level: 1,
      marks: 0,

      options: [{ is_correct: false, answer: '' }]
    }
  ]
};
export const MCQSchemaValidations = Yup.object().shape({
  mcq_id: Yup.array(
    Yup.object().shape({
      question: Yup.string().required('Question is required'),
      order: Yup.number().required('Order is required'),
      marks: Yup.number().required('Marks is required'),
      difficulty_level: Yup.number().required('Difficulty level is required'),
      // .min(1, 'Minimum atleast 0')
      // .max(10, 'Allowed maximum is 10'),
      options: Yup.array(
        Yup.object().shape({
          is_correct: Yup.boolean(),
          answer: Yup.string()
        })
      )
    })
  )
});
export const MCQEditSchemaValidations = Yup.object().shape({
  question: Yup.string().required('Question is required'),
  order: Yup.number().required('Order is required'),
  marks: Yup.number().required('Marks is required'),
  difficulty_level: Yup.number().required('Difficulty level is required'),
  options: Yup.array(
    Yup.object().shape({
      is_correct: Yup.boolean(),
      answer: Yup.string()
    })
  )
});
