import DataTable from 'components/ui/DataTable/data-table';
import React, { FC, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useAssignmentQuery } from 'redux/reducers/assignment';

interface ITraineeDataTable {
  tableHeaders: () => React.ReactNode;
  onDeleteClicked: (data: any) => void;
  onEditClicked: (data: any) => void;
}
const shortener = (string: any) => {
  if (string?.length > 50) {
    string = string.substring(0, 50) + '...';
  }
  return string;
};
const AssignmentTypeTable: FC<ITraineeDataTable> = ({
  tableHeaders,
  onDeleteClicked,
  onEditClicked
}) => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const { data: Assignments, isLoading } = useAssignmentQuery(
    { id, page, pageSize },
    { skip: !id }
  );
  const canNext = Assignments?.data?.next != null;
  const canPrevious = Assignments?.data?.previous != null;

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: 'Assignment Title',
      accessor: 'assignment_title'
    },
    {
      Header: 'Description',
      Cell: (data: any) => {
        return shortener(data.row.original.assignment_description);
      }
    },

    {
      Header: 'Assignment file',
      Cell: (data: any) => {
        return (
          <>
            {data.row.original.assignment_file == null
              ? 'Null'
              : shortener(data.row.original.assignment_file)}
          </>
        );
      }
    },
    {
      Header: 'MCQ',
      Cell: (data: any) => {
        return (
          <>
            {data.row.original.multi_choice_question_details.length !== 0
              ? data.row.original.multi_choice_question_details.length
              : 'Null'}
          </>
        );
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <div style={{ display: 'flex' }}>
            <img
              style={{ marginRight: '10px', height: '20px', cursor: 'pointer' }}
              src={require('../Assignement/images/edit.png')}
              alt="icon for config"
              onClick={() => {
                onEditClicked(data.row.original);
              }}
            />
            <img
              style={{ marginRight: '10px', height: '20px', cursor: 'pointer' }}
              src={require('../Assignement/images/Vector.png')}
              alt="icon for config"
              onClick={() => onDeleteClicked(data.row.original)}
            />
            <div style={{ marginTop: '2px', position: 'relative' }}>
              <div className="dropdown d-flex justify-content-end">
                <a
                  href="#"
                  role="button"
                  id="dropdownMenuLink5"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <i className="ri-more-2-fill"></i>
                </a>

                <ul
                  className="dropdown-menu fixed  position-absolute-important"
                  aria-labelledby="dropdownMenuLink"
                >
                  <li>
                    <a
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#showModal"
                      onClick={() =>
                        navigate(
                          `/modules/session/assignments/assignment/${data.row.original.tms_assignment_id}`
                        )
                      }
                    >
                      Assignment Detail
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        );
      }
    }
  ];
  return (
    <div>
      <DataTable
        columns={columns}
        isLoading={isLoading}
        data={Assignments?.data?.results || []}
        tableHeaders={tableHeaders}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (canPrevious) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (canNext) {
              setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default AssignmentTypeTable;
