import PrimaryButton from 'components/ui/PrimaryButton';
import TextInput from 'components/ui/TextInput';
import { useFormikContext } from 'formik';
import React from 'react';
import { MCQInitialValues } from './AssignmentSchema';

const MCQForm = () => {
  const formik = useFormikContext<typeof MCQInitialValues>();

  return (
    <div>
      {formik.values?.mcq_id?.map(
        ({ question, difficulty_level, order, marks, options }: any, index: number) => {
          return (
            <div>
              {Object.entries({ question, difficulty_level, order, marks })?.map(
                ([type, value]) => {
                  return (
                    <div className="col-12 ">
                      <TextInput
                        containerClassName="mt-3"
                        label={type}
                        name={`mcq_id[${index}].${type}`}
                        type="text"
                        height={20}
                        onChange={formik.handleChange}
                        value={value}
                        onBlur={formik.handleBlur}
                        formik={formik}
                      />
                    </div>
                  );
                }
              )}
              <div className="row" style={{ justifyContent: 'flex-end' }}>
                <div
                  className="col-md-3  col-sx-6"
                  style={{ marginTop: '10px', display: 'flex', justifyContent: 'flex-end' }}
                >
                  <PrimaryButton
                    title="Add Options"
                    iconName="ri-add-line align-bottom me-1"
                    type="button"
                    onClick={() => {
                      formik.setFieldValue(
                        `mcq_id[${index}].options`,
                        [...formik.values.mcq_id[index].options, { is_correct: false, answer: '' }],
                        true
                      );
                    }}
                  />
                </div>
              </div>
              {options?.map((opt: any, ioption: number) => {
                return (
                  <div className="col-sx-12 col-md-6 mt-3" key={ioption}>
                    <TextInput
                      containerClassName="mt-3"
                      label="Option"
                      name={`mcq_id[${index}].options[${ioption}].answer`}
                      type="text"
                      height={20}
                      onChange={formik.handleChange}
                      value={opt.answer}
                      onBlur={formik.handleBlur}
                      formik={formik}
                    />

                    <label className="form-check-label" htmlFor="customSwitchsizesm">
                      Is true
                    </label>
                    <input
                      type="checkbox"
                      className="form-check-input"
                      id="customSwitchsizesm"
                      checked={opt.is_correct}
                      onChange={(e) => {
                        formik.setFieldValue(
                          `mcq_id[${index}].options[${ioption}].is_correct`,
                          !opt.is_correct
                        );
                      }}
                    />
                  </div>
                );
              })}
            </div>
          );
        }
      )}
    </div>
  );
};

export default MCQForm;
