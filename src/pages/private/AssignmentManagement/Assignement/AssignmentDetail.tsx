import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import {
  useAssignmentDetailQuery,
  useCreateMCQAssignmentMutation,
  useDeleteMCQAnswerAssignmentMutation,
  useDeleteMCQAssignmentMutation,
  useUpdateMCQAssignmentMutation
} from 'redux/reducers/assignment';
import { FormikProvider, useFormik, useFormikContext } from 'formik';
import {
  MCQSchemaValidations,
  MCQInitialValues,
  MCQEditSchemaValidations
} from './AssignmentSchema';
import Modal from 'components/ui/Modal/modal';
import SecondaryButton from 'components/ui/SecondaryButton';
import useToggle from 'hooks/useToggle';
import MCQForm from './MCQForm';
import { toast } from 'react-toastify';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import AssignmentEditform from './AssignmentEditform';

const AssignmentDetail = () => {
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [editModal, toggleEditModal] = useToggle(false);
  const [formModal, setFormModal] = useToggle(false);

  const [assignmentInitialFormState, setAssignmentInitialForm] =
    React.useState<any>(MCQInitialValues);
  const [deletingEvaluationState, setDeletingEvaluation] = useState<number | null>(null);
  const [editMCQState, setEditMCQState] = useState<any | null>(null);

  const { id } = useParams();
  const { data: getAssignment, isLoading } = useAssignmentDetailQuery({ id }, { skip: !id });

  const [CreateMCQ, { isLoading: creatingMCQ }] = useCreateMCQAssignmentMutation();
  const [updateMCQ, { isLoading: isUpdatingMCQ }] = useUpdateMCQAssignmentMutation();
  const [deleteMCQ, { isLoading: isDeletingMCQ }] = useDeleteMCQAssignmentMutation();

  const [deleteMCQAns, { isLoading: isDeletingMCQans }] = useDeleteMCQAnswerAssignmentMutation();

  const MCQdelete = (id: number) => {
    setDeletingEvaluation(id);
    toggleDeleteModal();
  };
  const onDeleteConfirm = () => {
    deletingEvaluationState &&
      deleteMCQ(deletingEvaluationState)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setAssignmentInitialForm(MCQInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const setMCQansDeleteID = (id: number) => {
    deleteMCQAns(id)
      .unwrap()
      .then((data: any) => {
        toast.success('Successfully Deleted');
      })
      .catch((err: any) => {
        toast.error('Error');
      });
  };
  const editOption = (mcq: any) => {
    setEditMCQState(mcq);
    toggleEditModal();
  };
  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: MCQSchemaValidations,
    initialValues: assignmentInitialFormState,

    onSubmit: (values, { resetForm }) => {
      const Data = {
        assignment_id: getAssignment?.data.tms_assignment_id,
        question: values.mcq_id[0].question,
        order: values.mcq_id[0].order,
        difficulty_level: values.mcq_id[0].difficulty_level,
        marks: values.mcq_id[0].marks,
        options: values.mcq_id[0].options
      };

      let executeFunc = CreateMCQ;
      executeFunc(Data)
        .unwrap()
        .then((data: any) => {
          setAssignmentInitialForm(MCQInitialValues);
          resetForm();
          toast.success('Created Successfully');
          setFormModal(false);
        })
        .catch((err: any) => {
          if (err.data.difficulty_level[0]) {
            toast.error('Difficulty level should be 1 to 10 !');
          }
        });
    }
  });
  const editformik = useFormik({
    enableReinitialize: true,
    validationSchema: MCQEditSchemaValidations,
    initialValues: editMCQState,

    onSubmit: (values, { resetForm }) => {
      const Data = {
        mcq_id: editMCQState?.mcq_id,
        question: values.question,
        order: values.order,
        difficulty_level: values.difficulty_level,
        marks: values.marks,
        options: values.options
      };

      let executeFunc = updateMCQ;
      executeFunc(Data)
        .unwrap()
        .then((data: any) => {
          setEditMCQState(MCQInitialValues);
          resetForm();
          toast.success('Created Successfully');
          toggleEditModal();
        })
        .catch((err: any) => {
          if (err.data.difficulty_level[0]) {
            toast.error('Difficulty level should be 1 to 10 !');
          }
        });
    }
  });
  return (
    <Container title="Assignment Details">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeletingMCQ}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this MCQ ?</p>
          </div>
        </>
      </ConfirmationModal>
      <FormikProvider value={formik}>
        <form onSubmit={formik.handleSubmit}>
          <Modal
            title="Create New MCQ"
            isModalOpen={formModal}
            closeModal={() => {
              setFormModal();
              formik.resetForm();
            }}
            renderFooter={() => (
              <div className="hstack gap-2 justify-content-end">
                <SecondaryButton onClick={setFormModal} title="Close" />
                <PrimaryButton
                  title="Submit"
                  iconName="ri-add-line align-bottom me-1"
                  type="submit"
                  isLoading={creatingMCQ}
                />
              </div>
            )}
          >
            <MCQForm />
          </Modal>
        </form>
      </FormikProvider>

      <FormikProvider value={editformik}>
        <form onSubmit={editformik.handleSubmit}>
          <Modal
            title="Edit MCQ"
            isModalOpen={editModal}
            closeModal={() => {
              toggleEditModal();
              editformik.resetForm();
            }}
            renderFooter={() => (
              <div className="hstack gap-2 justify-content-end">
                <SecondaryButton onClick={toggleEditModal} title="Close" />
                <PrimaryButton
                  title="Submit"
                  iconName="ri-add-line align-bottom me-1"
                  type="submit"
                  isLoading={isUpdatingMCQ}
                />
              </div>
            )}
          >
            <AssignmentEditform />
          </Modal>
        </form>
      </FormikProvider>

      <div className="row" style={{ justifyContent: 'flex-end' }}>
        <div
          className="col-md-3  col-sx-6"
          style={{ marginTop: '10px', display: 'flex', justifyContent: 'flex-end' }}
        >
          <PrimaryButton
            title="Add MCQ"
            iconName="ri-add-line align-bottom me-1"
            type="button"
            onClick={() => {
              setFormModal();
              //   formik.setFieldValue(
              //     'mcq_id',
              //     [
              //       ...formik.values.mcq_id,
              //       {
              //         question: '',
              //         order: 0,
              //         difficulty_level: 1,
              //         marks: 0,
              //         options: [{ is_correct: false, answer: '' }]
              //       }
              //     ],
              //     true
              //   );
            }}
          />
        </div>
      </div>
      <div className="row">
        <h5>
          <b>Assignment Title:</b>
        </h5>{' '}
        <h5>{getAssignment?.data?.assignment_title}</h5>{' '}
      </div>
      <div className="row">
        <h5>
          <b>Assignment Description:</b>
        </h5>
        <h5>{getAssignment?.data?.assignment_description}</h5>{' '}
      </div>
      <div className="row">
        <h5>
          <b>Assignment File:</b>
        </h5>
        <h5>
          {getAssignment?.data?.assignment_file == null
            ? 'Null'
            : getAssignment?.data?.assignment_file}
        </h5>{' '}
      </div>
      <h5>
        <b>MCQ questions</b>
      </h5>
      {getAssignment?.data?.multi_choice_question_details.map((mcq: any, index: number) => {
        return (
          <>
            <div className="row" key={index}>
              <div className="col-md-12 col-sx-12">
                <div className="row">
                  <div className="col-md-6 col-sx-12">
                    <h5 style={{ marginTop: '15px' }}>
                      <b>Question:</b>
                    </h5>
                  </div>
                  <div
                    className="col-md-6 col-sx-12"
                    style={{ display: 'flex', justifyContent: 'flex-end' }}
                  >
                    <div style={{ marginRight: '5px' }}>
                      <PrimaryButton
                        isDanger={true}
                        title="Delete MCQ"
                        type="button"
                        onClick={() => MCQdelete(mcq.mcq_id)}
                      />
                    </div>
                    <div>
                      <PrimaryButton
                        title="Edit MCQ"
                        type="button"
                        onClick={() => editOption(mcq)}
                      />
                    </div>
                  </div>
                </div>
                <h5>{mcq.question}</h5>
              </div>
            </div>

            <div className="row">
              {mcq.options.map((opt: any, index2: number) => {
                return (
                  <>
                    <div className="col-md-3 col-sx-12" key={index2}>
                      <h5>option:{opt.answer}</h5>
                      <img
                        style={{ marginRight: '10px', height: '15px', width: '18px' }}
                        src={require('../Assignement/images/Vector.png')}
                        alt="icon for config"
                        onClick={() => setMCQansDeleteID(opt.mcq_answer_id)}
                      />
                      <label className="form-check-label" htmlFor="customSwitchsizesm">
                        Is true
                      </label>
                      <input
                        type="checkbox"
                        className="form-check-input"
                        id="customSwitchsizesm"
                        checked={opt.is_correct}
                      />
                    </div>
                  </>
                );
              })}

              <hr
                className="border-1 mx-3"
                style={{ backgroundColor: 'black', width: '96%', marginTop: '20px' }}
              />
            </div>
          </>
        );
      })}
    </Container>
  );
};

export default AssignmentDetail;
