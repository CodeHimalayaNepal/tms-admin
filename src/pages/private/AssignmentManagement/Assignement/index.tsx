import Container from 'components/ui/Container';
import React, { useState } from 'react';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { FormikProvider, useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { toast } from 'react-toastify';
import AssignmentForm from './AssignmentForm';
import AssignmentTable from './AssignmentTable';
import {
  AssignmentSchemaValidations,
  AssignmentInitialValues,
  AssignmentEditSchemaValidations
} from './AssignmentSchema';
import { useParams } from 'react-router-dom';
import {
  useCreateAssignmentMutation,
  useDeleteAssignmentMutation,
  useUpdateAssignmentMutation
} from 'redux/reducers/assignment';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import AssignmentTitleEditfrom from './AssignmentTitleEditFrom';

const Assignment = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [editformModal, setEditFormModal] = useToggle(false);
  const [assignmentInitialFormState, setAssignmentInitialForm] =
    React.useState<any>(AssignmentInitialValues);
  const [assignmentEditFormState, setAssignmentEditForm] = React.useState<any>(null);

  const [deletingEvaluationState, setDeletingEvaluation] = useState<any | null>(null);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [CreateAssignment, { isLoading: creatingAssignment }] = useCreateAssignmentMutation();
  const [updateAssignment, { isLoading: isUpdatingAssignment }] = useUpdateAssignmentMutation();

  const [deleteAssignment, { isLoading: isDeletingAssignment }] = useDeleteAssignmentMutation();

  const onEditClicked = (data: any) => {
    setAssignmentEditForm(data);
    setEditFormModal();
  };
  const onDeleteClicked = (data: any) => {
    setDeletingEvaluation(data);
    toggleDeleteModal();
  };

  const onDeleteConfirm = () => {
    deletingEvaluationState &&
      deleteAssignment(deletingEvaluationState?.tms_assignment_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setAssignmentInitialForm(AssignmentInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const { id } = useParams();

  const editAssignmentformik = useFormik({
    enableReinitialize: true,
    validationSchema: AssignmentEditSchemaValidations,
    initialValues: assignmentEditFormState,

    onSubmit: (values, { resetForm }) => {
      console.log(assignmentEditFormState);
      const data = {
        topic_id: assignmentEditFormState.tms_assignment_id,
        assignment_title: values.assignment_title,
        assignment_description: values.assignment_description,
        assignment_file: values.assignment_file
      };

      let executeFunc = updateAssignment;

      executeFunc(data)
        .unwrap()
        .then((data: any) => {
          resetForm();
          toast.success('Created Successfully');
          setEditFormModal(false);
        })
        .catch((err: any) => {
          if (err.data.mcq_id[0]?.difficulty_level[0]) {
            toast.error('Difficulty level should be 1 or greater !');
          }
        });
    }
  });
  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: AssignmentSchemaValidations,
    initialValues: assignmentInitialFormState,

    onSubmit: (values, { resetForm }) => {
      const data = { ...values, topic_id: id };

      let executeFunc = CreateAssignment;

      executeFunc(data)
        .unwrap()
        .then((data: any) => {
          setAssignmentInitialForm(assignmentInitialFormState);
          resetForm();
          toast.success('Created Successfully');
          setFormModal(false);
        })
        .catch((err: any) => {
          if (err.data.mcq_id[0]?.difficulty_level[0]) {
            toast.error('Difficulty level should be 1 or greater !');
          }
        });
    }
  });
  return (
    <Container title="Assignment">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeletingAssignment}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <AssignmentTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title=" Add Assignment"
              iconName="ri-add-line align-bottom me-1"
              onClick={() => {
                setAssignmentInitialForm(AssignmentInitialValues);
                setFormModal();
              }}
              type="button"
            />
          </div>
        )}
        onDeleteClicked={onDeleteClicked}
        onEditClicked={onEditClicked}
      />

      <FormikProvider value={formik}>
        <form onSubmit={formik.handleSubmit}>
          <Modal
            title="Create New Assignment"
            overflowY="auto"
            isModalOpen={formModal}
            closeModal={() => {
              setFormModal();
              formik.resetForm();
            }}
            height="90vh"
            renderFooter={() => (
              <div className="hstack gap-2 justify-content-end">
                <SecondaryButton onClick={setFormModal} title="Close" />
                <PrimaryButton
                  title="Submit"
                  iconName="ri-add-line align-bottom me-1"
                  type="submit"
                  isLoading={creatingAssignment}
                />
              </div>
            )}
          >
            <AssignmentForm />
          </Modal>
        </form>
      </FormikProvider>

      <FormikProvider value={editAssignmentformik}>
        <form onSubmit={editAssignmentformik.handleSubmit}>
          <Modal
            title="Update Assignment"
            isModalOpen={editformModal}
            closeModal={() => {
              setEditFormModal();
              editAssignmentformik.resetForm();
            }}
            renderFooter={() => (
              <div className="hstack gap-2 justify-content-end">
                <SecondaryButton onClick={setEditFormModal} title="Close" />
                <PrimaryButton title="Update" type="submit" isLoading={isUpdatingAssignment} />
              </div>
            )}
          >
            <AssignmentTitleEditfrom />
          </Modal>
        </form>
      </FormikProvider>
    </Container>
  );
};

export default Assignment;
