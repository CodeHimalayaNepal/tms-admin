import React from 'react';
import { Outlet } from 'react-router-dom';

const AssignmentManagement = () => {
  return (
    <div>
      <Outlet />
    </div>
  );
};

export default AssignmentManagement;
