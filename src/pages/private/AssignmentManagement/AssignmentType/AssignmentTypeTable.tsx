import DataTable from 'components/ui/DataTable/data-table';
import React, { FC } from 'react';

interface ITraineeDataTable {
  tableHeaders: () => React.ReactNode;
}
const AssignmentTypeTable: FC<ITraineeDataTable> = ({ tableHeaders }) => {
  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn'
    },
    {
      Header: ' Type Name',
      accessor: 'name'
    },
    {
      Header: '  Action',
      accessor: 'action'
    }
  ];
  return (
    <div>
      <DataTable columns={columns} data={[]} tableHeaders={tableHeaders} />
    </div>
  );
};

export default AssignmentTypeTable;
