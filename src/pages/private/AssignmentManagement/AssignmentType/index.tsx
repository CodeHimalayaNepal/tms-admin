import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import useToggle from 'hooks/useToggle';
import AssignmentTypeForm from './AssignmentTypeForm';
import AssignmentTypeTable from './AssignmentTypeTable';

const AssignmentType = () => {
  const [formModal, setFormModal] = useToggle(false);

  return (
    <Container title="Assignment Type">
      <AssignmentTypeTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title="Create new Assignment Type"
              iconName="ri-add-line align-bottom me-1"
              onClick={setFormModal}
              type="button"
            />
          </div>
        )}
      />

      <Modal
        title="Create New Assignment Type"
        isModalOpen={formModal}
        closeModal={setFormModal}
        modalSize="modal-md"
        renderFooter={() => (
          <div className="hstack gap-2 justify-content-end">
            <SecondaryButton onClick={setFormModal} title="Close" />
            <PrimaryButton
              title="Add Assignment Type"
              iconName="ri-add-line align-bottom me-1"
              type="submit"
            />
          </div>
        )}
      >
        <AssignmentTypeForm />
      </Modal>
    </Container>
  );
};

export default AssignmentType;
