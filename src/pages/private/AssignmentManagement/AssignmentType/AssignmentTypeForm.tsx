import React from 'react';

const AssignmentTypeForm = () => {
  return (
    <div>
      <div className="row">
        <div className="mb-3 col-lg-12 col-md-12">
          <label htmlFor="role-field" className="form-label">
            Type Name
          </label>
          <input
            type="text"
            id="role-field"
            className="form-control"
            placeholder="Role Name"
            required
          />
        </div>
      </div>
      <div className="row">
        <div className="mb-3 col-md-12">
          <label htmlFor="exampleFormControlTextarea1" className="form-label">
            Description
          </label>
          <textarea
            className="form-control"
            id="exampleFormControlTextarea1"
            rows={3}
            placeholder="Description Here ..."
            defaultValue={''}
          />
        </div>
      </div>
    </div>
  );
};

export default AssignmentTypeForm;
