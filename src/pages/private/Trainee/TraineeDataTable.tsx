import DataTable from 'components/ui/DataTable/data-table';
import React, { FC, useState } from 'react';
import ViewIcon from '../../../assets/images/icons/view.png';
import EditIcon from '../../../assets/images/icons/edit.png';
import { FcUnlock } from 'react-icons/fc';
import { Cell } from 'react-table';
import { useListTraineeQuery } from 'redux/reducers/auth';

interface ITraineeDataTable {
  searchQuery: string;
  onEdit: (traineeData: ITraineeInfo & { id: string; attendance_pin: string }) => void;
  onView: (traineeData: ITraineeInfo & { id: string; attendance_pin: string }) => void;
  onUnblock: (traineeData: ITraineeInfo & { id: string; attendance_pin: string }) => void;
}

export interface ITraineeData {
  first_name: string;
  middle_name: string;
  last_name: string;
  user: [{ id: number; email: string }];
  mobile_number: string;
  attendance_pin: string;
  user__user_login_attempts__is_locked: boolean;
  extra_fields: {
    program: any;
    locations: any;
    'mobile number': string;
  };
}

export interface ITraineeInfo {
  first_name: string;
  middle_name: string;
  last_name: string;
  email: string;
  mobile_number: string;
  user__user_login_attempts__is_locked: boolean;
  userID: any;
}

const TraineeDataTable: FC<ITraineeDataTable> = ({ onEdit, onView, searchQuery, onUnblock }) => {
  const [page, setPage] = useState(1);
  const [allDataList, setAllDataList] = useState(0);
  const { data: userLists, isLoading, isSuccess } = useListTraineeQuery(searchQuery);
  const totalUser = userLists?.data;
  const paginatedData = totalUser?.slice(
    page === 1 ? 0 : (page - 1) * 10,
    page === 1 ? 10 : page * 10
  );
  const canPrevious = page === 1 ? false : true;
  const canNext = allDataList === totalUser?.length ? false : true;
  React.useEffect(() => {
    if (isSuccess) {
      setPage(1);
    }
  }, [isSuccess]);
  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: Cell<ITraineeData>) => {
        // return <>{data.row.index + 1}</>;
        setAllDataList(data.row.index + (page - 1) * 10 + 1);
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: '  First Name',
      accessor: 'first_name'
    },
    {
      Header: '  Middle Name',
      accessor: 'middle_name'
    },
    {
      Header: '  Last Name',
      accessor: 'last_name'
    },
    {
      Header: 'Email',
      accessor: 'user[0].email'
    },
    {
      Header: 'Attendance Pin',
      accessor: 'attendance_pin'
    },

    {
      Header: 'Phone',
      accessor: 'mobile_number'
    },
    // {
    //   Header: 'Attendance PIN',
    //   accessor: 'attendance_pin'
    // },
    {
      Header: 'Actions',
      Cell: (cell: Cell<ITraineeData & { id: string }>) => {
        return (
          <>
            <img
              src={ViewIcon}
              style={{ cursor: 'pointer' }}
              onClick={() =>
                onView({
                  id: cell.row.original.id,
                  userID: cell.row.original.user[0].id,
                  first_name: cell.row.original.first_name,
                  middle_name: cell.row.original.middle_name,
                  last_name: cell.row.original.last_name,
                  email: cell.row.original.user[0].email,
                  mobile_number: cell.row.original.mobile_number,
                  attendance_pin: cell.row.original.attendance_pin,
                  user__user_login_attempts__is_locked:
                    cell.row.original.user__user_login_attempts__is_locked
                })
              }
            />
            <img
              src={EditIcon}
              style={{ cursor: 'pointer' }}
              className="m-2"
              onClick={() =>
                onEdit({
                  id: cell.row.original.id,
                  userID: cell.row.original.user[0].id,
                  first_name: cell.row.original.first_name,
                  middle_name: cell.row.original.middle_name,
                  last_name: cell.row.original.last_name,
                  email: cell.row.original.user[0].email,
                  mobile_number: cell.row.original.mobile_number,
                  attendance_pin: cell.row.original.attendance_pin,
                  user__user_login_attempts__is_locked:
                    cell.row.original.user__user_login_attempts__is_locked
                })
              }
            />
            {cell.row.original.user__user_login_attempts__is_locked === true ? (
              <FcUnlock
                size={20}
                cursor={'pointer'}
                onClick={() =>
                  onUnblock({
                    id: cell.row.original.id,
                    userID: cell.row.original.user[0].id,
                    first_name: cell.row.original.first_name,
                    middle_name: cell.row.original.middle_name,
                    last_name: cell.row.original.last_name,
                    email: cell.row.original.user[0].email,
                    mobile_number: cell.row.original.mobile_number,
                    attendance_pin: cell.row.original.attendance_pin,
                    user__user_login_attempts__is_locked:
                      cell.row.original.user__user_login_attempts__is_locked
                  })
                }
              />
            ) : null}
          </>
        );
      }
    }
  ];

  return (
    <DataTable
      columns={columns}
      data={paginatedData || []}
      isLoading={isLoading}
      search={false}
      pagination={{
        canPrevious,
        canNext,
        prevPage: () => {
          if (totalUser) {
            page > 1 && setPage(page - 1);
          }
        },
        nextPage: () => {
          if (totalUser) {
            totalUser?.length >= allDataList && setPage(page + 1);
          }
        }
      }}
    />
  );
};

export default TraineeDataTable;
