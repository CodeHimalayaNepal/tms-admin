import { SCREENS_ROUTES_ENUMS } from 'common/enum';
import FormikValidationError from 'components/ui/FormikErrors';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import TextInput from 'components/ui/TextInput';
import { useFormik } from 'formik';
import { FC, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { authenticationApi, useSignUpMutation } from 'redux/reducers/auth';
import { useUpdateSomeTraineeDetailMutation, useTraineeRegMutation } from 'redux/reducers/user';
import { useAppDispatch } from 'redux/store/store';
import validations from 'utils/validation';
import * as Yup from 'yup';

import { ITraineeInfo } from './TraineeDataTable';

const SignUpValidationSchema = {
  first_name: Yup.string().required('First Name is required'),
  last_name: Yup.string().required('Last Name is required'),
  email: Yup.string().email('Invalid email').required('Email is required'),
  mobile_number: Yup.string()
    .matches(validations.phone.regex, validations.phone.message)
    .required('Mobile Phone is required.')
};

const initialValue = {
  first_name: '',
  middle_name: '',
  last_name: '',
  email: '',
  mobile_number: '',
  attendance_pin: ''
};

interface ITraineeForm {
  formModal: boolean;
  setFormModal: () => void;
  editDetails: (ITraineeInfo & { id: string; attendance_pin: string }) | null;
}

const TraineeForm: FC<ITraineeForm> = ({ formModal, setFormModal, editDetails }) => {
  const dispatch = useAppDispatch();
  const fetchTraineeList = () =>
    dispatch(authenticationApi.endpoints.listTrainee.initiate(undefined, { forceRefetch: true }));

  const [trainRegistration, { isLoading: isCreating }] = useTraineeRegMutation();
  const [updateTrainee, { isLoading: isUpdating }] = useUpdateSomeTraineeDetailMutation();
  //   const dynamicFieldsValidations: any = {};
  //   traineeDynamicFields?.map((item: any) => {
  //     dynamicFieldsValidations[item.fieldname] =
  //       item.field_type_details.title === 'number'
  //         ? item.is_required
  //           ? Yup.number().required(`${item.fieldname} is required`)
  //           : Yup.number()
  //         : item.is_required
  //         ? Yup.string().required(`${item.fieldname} is required`)
  //         : Yup.string();
  //   });

  const formik = useFormik({
    validationSchema: Yup.object().shape({
      ...SignUpValidationSchema
      //   ...dynamicFieldsValidations
    }),
    initialValues: initialValue,
    onSubmit: async (values: Partial<typeof initialValue>) => {
      if (editDetails) {
        try {
          await updateTrainee({
            traineeId: editDetails.id,
            body: {
              first_name: values.first_name,
              middle_name: values.middle_name,
              last_name: values.last_name,
              email: values.email,
              mobile_number: values.mobile_number,
              attendance_pin: values.attendance_pin
            }
          }).unwrap();
          fetchTraineeList();
          toast.success('Trainee profile updated successfully!');
          setFormModal();
        } catch (error) {
          toast.success('Trainee profile updated failed!');
        }
      } else {
        delete values.attendance_pin;
        trainRegistration(values)
          .unwrap()
          .then(() => {
            toast.success('Trainee Added');
            setFormModal();
          })
          .catch((err: any) => {
            if (err.data.email.length) {
              toast.error(err.data.email[0]);
              return;
            }
            toast.error(err.data?.message);
          });
      }
    }
  });

  useEffect(() => {
    if (editDetails) {
      formik.resetForm({
        values: {
          ...initialValue,
          ...editDetails
        }
      });
    }
  }, [editDetails]);

  return (
    <div className="">
      <div />
      {/* auth-page content */}

      <div>
        <div className="mt-4">
          <form onSubmit={formik.handleSubmit} className="row">
            <Modal
              title={`${editDetails ? 'Update' : 'Create'} Trainee`}
              isModalOpen={formModal}
              closeModal={setFormModal}
              renderFooter={() => (
                <div className="hstack gap-2 justify-content-end">
                  <SecondaryButton onClick={setFormModal} title="Close" />
                  <PrimaryButton
                    title={`${editDetails ? 'Update' : 'Create'}`}
                    iconName={`${editDetails ? '' : 'ri-add-line'} align-bottom me-1`}
                    type="submit"
                    isLoading={isCreating || isUpdating}
                  />
                </div>
              )}
            >
              <div className="row">
                <TextInput
                  label="First Name"
                  name="first_name"
                  type="text"
                  onChange={formik.handleChange}
                  value={formik.values.first_name}
                  onBlur={formik.handleBlur}
                  formik={formik}
                />
                <TextInput
                  label="Middle Name"
                  name="middle_name"
                  type="text"
                  onChange={formik.handleChange}
                  value={formik.values.middle_name}
                  onBlur={formik.handleBlur}
                  formik={formik}
                  disableRequiredValidation
                />

                <TextInput
                  label="Last Name"
                  name="last_name"
                  type="text"
                  onChange={formik.handleChange}
                  value={formik.values.last_name}
                  onBlur={formik.handleBlur}
                  formik={formik}
                />

                <TextInput
                  label="Email"
                  name="email"
                  type="email"
                  onChange={formik.handleChange}
                  value={formik.values.email}
                  onBlur={formik.handleBlur}
                  formik={formik}
                />

                <TextInput
                  label="Mobile No"
                  name="mobile_number"
                  type="text"
                  onChange={formik.handleChange}
                  value={formik.values.mobile_number}
                  onBlur={formik.handleBlur}
                  formik={formik}
                  disableRequiredValidation
                />

                {editDetails && (
                  <TextInput
                    label="PIN Code"
                    name="attendance_pin"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.attendance_pin}
                    onBlur={formik.handleBlur}
                    formik={formik}
                    disableRequiredValidation
                  />
                )}

                {/* {traineeDynamicFields?.map((item: any) => {
                  return (
                    <>
                      <TextInput
                        label={item.fieldname}
                        name={item.fieldname}
                        type={item.field_type_details.title}
                        onChange={formik.handleChange}
                        //@ts-ignore
                        value={formik.values[item.fieldname]}
                        onBlur={formik.handleBlur}
                        formik={formik}
                      />
                    </>
                  );
                })} */}
              </div>
            </Modal>
          </form>
        </div>
      </div>
    </div>
  );
};

export default TraineeForm;
