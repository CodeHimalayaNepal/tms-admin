import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import useToggle from 'hooks/useToggle';
import React, { useState } from 'react';
import { toast } from 'react-toastify';
import { useUnblockTraineeMutation } from 'redux/reducers/auth';
import CsvUploadForm from './CsvUploadForm';
import TraineeDataTable, { ITraineeInfo } from './TraineeDataTable';
import TraineeForm from './TraineeForm';
import TraineeView from './TraineeView';

const Trainee = () => {
  const timerRef = React.useRef<NodeJS.Timeout>();

  const [showQuery, setShowQuery] = useState('');
  const [searchQuery, setSearchQuery] = useState('');
  const [formModal, setFormModal] = useToggle(false);
  const [unblockModal, setUnblockModal] = useToggle(false);
  const [csvFormModal, setCsvFormModal] = useToggle(false);
  const [traineeViewModal, setTraineeViewModal] = useToggle(false);

  const [traineeViewDetail, setTraineeViewDetail] = useState<
    (ITraineeInfo & { id: string; attendance_pin: string }) | null
  >(null);

  const [editDetails, setEditDetails] = useState<
    (ITraineeInfo & { id: string; attendance_pin: string }) | null
  >(null);

  const [unblockDetails, setUnblockDetails] = useState<
    (ITraineeInfo & { id?: string; attendance_pin: string }) | null
  >(null);
  const [confirmationModal, setConfirmationModal] = useToggle(false);

  const [unblockTrainee, { isLoading: isUnblocking }] = useUnblockTraineeMutation();
  const onEdit = (traineeDetail: ITraineeInfo & { id: string; attendance_pin: string }) => {
    setFormModal(true);
    setEditDetails(traineeDetail);
  };

  const onView = (traineeDetail: ITraineeInfo & { id: string; attendance_pin: string }) => {
    setTraineeViewModal(true);
    setTraineeViewDetail(traineeDetail);
  };

  const onUnblock = (traineeDetail: ITraineeInfo & { id: string; attendance_pin: string }) => {
    setUnblockModal(true);
    setUnblockDetails(traineeDetail);
  };

  const onUnblockConfirm = () => {
    unblockDetails &&
      unblockTrainee({ id: unblockDetails?.userID })
        .unwrap()
        .then((data: any) => {
          toast.success('Unblocked Successfully');
          // setFeedbackInitialForm(FeedbackInitialValues);
          setUnblockModal(false);
        })
        .catch((err: any) => {
          toast.error('Error in unblocking');
        });
  };

  return (
    <div>
      <>
        <div className="main-content overflow-hidden">
          <div className="page-content">
            <div className="container-fluid">
              <div className="row">
                <div className="col-lg-12">
                  <div className="card">
                    <div className="card-header">
                      <h4 className="card-title mb-0">Trainee Management</h4>
                      <h6 className="card-title mt-2" style={{ fontSize: '12px' }}>
                        List of Trainee
                      </h6>
                    </div>
                    <div className="card-body">
                      <div id="customerList">
                        <div className="col-sm-auto">
                          <div className="d-flex">
                            <PrimaryButton
                              onClick={() => setCsvFormModal(true)}
                              type="button"
                              className="btn btn-outline-primary add-btn"
                              small="me-2 mb-2"
                              iconName="ri-download-line align-bottom me-1"
                              data-bs-toggle="modal"
                              title="Import Users"
                              isOutline={true}
                            />
                            <PrimaryButton
                              type="button"
                              iconName="ri-add-line align-bottom ms-1"
                              small="mb-2"
                              onClick={() => {
                                setEditDetails(null);
                                setFormModal(true);
                              }}
                              title="Create Trainee"
                            />
                            <div
                              className="justify-content-lg-end"
                              style={{ flexGrow: '1', display: 'flex' }}
                            >
                              <div className="search-box ms-2">
                                <input
                                  type="text"
                                  className="form-control search"
                                  placeholder="Search..."
                                  name="key"
                                  value={showQuery}
                                  onChange={(e) => {
                                    setShowQuery(e.target.value);
                                    clearTimeout(timerRef.current);
                                    timerRef.current = setTimeout(() => {
                                      setSearchQuery(e.target.value);
                                    }, 2000);
                                  }}
                                />
                                <i className="ri-search-line search-icon" />
                              </div>
                            </div>
                          </div>
                        </div>
                        <TraineeDataTable
                          onEdit={onEdit}
                          onView={onView}
                          onUnblock={onUnblock}
                          searchQuery={searchQuery}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <TraineeForm
            formModal={formModal}
            setFormModal={setFormModal}
            editDetails={editDetails}
          />
          <CsvUploadForm csvFormModal={csvFormModal} setCsvFormModal={setCsvFormModal} />

          {traineeViewDetail && (
            <Modal
              title={`${traineeViewDetail.first_name}'s Detail`}
              isModalOpen={traineeViewModal}
              closeModal={setTraineeViewModal}
              modalSize="modal-xl"
            >
              <TraineeView
                traineeId={traineeViewDetail?.id}
                attendance_pin={traineeViewDetail.attendance_pin}
              />
            </Modal>
          )}
          {unblockDetails && (
            <ConfirmationModal
              isOpen={unblockModal}
              onClose={setUnblockModal}
              onConfirm={onUnblockConfirm}
              title={`Unblock ${unblockDetails.first_name}`}
              btnText={'Unblock'}
              isLoading={isUnblocking}
            >
              <>
                <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                  <h4>Are you Sure ?</h4>
                  <p className="text-muted ">
                    you want to unblock{' '}
                    <b>
                      {' '}
                      {unblockDetails.first_name} {unblockDetails.last_name}
                    </b>{' '}
                  </p>
                </div>
              </>
            </ConfirmationModal>
          )}

          <ConfirmationModal
            isOpen={confirmationModal}
            onClose={setConfirmationModal}
            onConfirm={() => console.log()}
            title="Delete Confirmations"
          >
            <>
              <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                <h4>Are you Sure ?</h4>
                <p className="text-muted mx-4 mb-0">
                  Are you Sure You want to Remove this Record ?
                </p>
              </div>
            </>
          </ConfirmationModal>
        </div>
      </>
    </div>
  );
};

export default Trainee;

<div></div>;
