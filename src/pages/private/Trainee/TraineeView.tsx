import { Avatar } from 'common/constant';
import DataTable from 'components/ui/DataTable/data-table';
import React from 'react';
import { useCurrentUserDetailsQuery } from 'redux/reducers/administrator';
import { useTraineeDetailQuery } from 'redux/reducers/user';
import { PlaceType, TraineeProfile } from '../TraineeProfile';

type Props = {
  traineeId: string;
  attendance_pin: string;
};

const expCols = [
  {
    Header: 'Organization',
    accessor: 'organization'
  },
  { Header: 'Department', accessor: 'department' },
  { Header: 'Level', accessor: 'level' },
  { Header: 'From Date', accessor: 'from_date' },
  { Header: 'To Date', accessor: 'to_date' },
  {
    Header: 'Is it your first job ?',
    accessor: (row: TraineeProfile['experience'][0]) => (row.is_first_service ? 'True' : 'False')
  },
  {
    Header: 'Currently working here?',
    accessor: (row: TraineeProfile['experience'][0]) => (row.is_current_service ? 'True' : 'False')
  }
];

const traineeCols = [
  {
    Header: 'Name',
    accessor: 'name'
  },
  { Header: 'Place', accessor: 'place' },
  {
    Header: 'Type',
    accessor: (row: TraineeProfile['previous_training'][0]) => PlaceType[row.place_type]
  },
  { Header: 'From Date', accessor: 'from_date' },
  { Header: 'To Date', accessor: 'to_date' }
];

function TraineeView({ traineeId, attendance_pin }: Props) {
  const { data: traineeDetail } = useTraineeDetailQuery(
    { traineeId: traineeId },
    {
      skip: !traineeId
    }
  );

  const {
    address,
    mobile_number,
    dob,
    email,
    first_name,
    middle_name,
    last_name,
    first_name_np,
    middle_name_np,
    last_name_np,
    experience,
    previous_training
  } = traineeDetail || {};

  const fullName = `${first_name ?? ''} ${middle_name ?? ''} ${last_name ?? ''}`;
  const fullNameNp = `${first_name_np ?? ''} ${middle_name_np ?? ''} ${last_name_np ?? ''}`;

  return (
    <div className="row">
      <div className="col-xxl-4">
        <div className="col-auto d-flex align-items-center justify-content-around">
          <div className="avatar-lg">
            <img
              src={traineeDetail?.image == '' ? Avatar : traineeDetail?.image}
              alt="user-img"
              className="img-thumbnail rounded-circle"
              style={{
                width: '92px',
                height: '92px',
                objectFit: 'cover'
              }}
            />
          </div>
          {/* 
          <div className="avatar-lg">
            <img
              src={traineeDetail?.signature ?? Avatar}
              alt="user-img"
              className="img-thumbnail rounded-circle"
              style={{
                width: '92px',
                height: '92px',
                objectFit: 'cover'
              }}
            />
          </div> */}
        </div>

        <div className="card">
          <div className="card-body">
            <h5 className="card-title mb-3">Info</h5>
            <div className="table-responsive">
              <table className="table table-borderless mb-0">
                <tbody>
                  <tr>
                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                      Attendance PIN:
                    </th>
                    <td className="text-muted">{attendance_pin ?? ' - '}</td>
                  </tr>
                  <tr>
                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                      Full Name:
                    </th>
                    <td className="text-muted">
                      <p className="mb-1">{fullName ?? ' - '}</p>
                      <p className="mb-1">{fullNameNp?.trim() ? `(${fullNameNp})` : ' - '}</p>
                    </td>
                  </tr>
                  <tr>
                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                      Date of Birth:
                    </th>
                    <td className="text-muted">{dob ?? ' - '}</td>
                  </tr>
                  <tr>
                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                      Mobile:
                    </th>
                    <td className="text-muted">(977) {mobile_number ?? ' - '}</td>
                  </tr>
                  <tr>
                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                      E-mail:
                    </th>
                    <td className="text-muted">{email ?? ' - '}</td>
                  </tr>
                  <tr>
                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                      Location:
                    </th>
                    <td className="text-muted">
                      {[
                        address?.tole_street,
                        address?.ward_no,
                        address?.municipality,
                        address?.province?.name_en
                      ]
                        .filter((add) => add)
                        .join(', ')}
                    </td>
                  </tr>
                  <tr>
                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                      Joining Date:
                    </th>
                    <td className="text-muted">24 Nov 2021</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="col-xxl-8">
        <div className="card">
          <div className="card-body">
            <h5 className="card-title mb-3">Experience</h5>
            <DataTable data={experience ?? []} columns={expCols} />
          </div>
        </div>
        <div className="card">
          <div className="card-body">
            <h5 className="card-title mb-3">Training</h5>
            <DataTable data={previous_training ?? []} columns={traineeCols} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default TraineeView;
