import { FileDrop } from 'components/ui/FileDrop/FileDrop';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import TextInput from 'components/ui/TextInput';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import {
  useUploadCsvFileAdminMutation,
  useUploadCsvFileMutation
} from 'redux/reducers/form-fields';
import * as Yup from 'yup';

const CsvValidation = Yup.object().shape({
  csvFile: Yup.mixed().required('required csv file')
});

const initialValue = {
  csvFile: [] as File[]
};

interface ICsvFormModal {
  csvFormModal: boolean;
  setCsvFormModal: () => void;
  isAdmin?: boolean;
}

const CsvUploadForm = ({ csvFormModal, setCsvFormModal, isAdmin }: ICsvFormModal) => {
  const [uploadFile, { isloading }] = isAdmin
    ? useUploadCsvFileAdminMutation()
    : useUploadCsvFileMutation();
  const formik = useFormik({
    validationSchema: CsvValidation,
    initialValues: initialValue,
    onSubmit: async (values: Partial<typeof initialValue>) => {
      console.log(values);
      const formdata = new FormData();
      if (values.csvFile?.[0]) {
        formdata.append('csv_file', values.csvFile?.[0]);
      }
      try {
        await uploadFile({
          data: formdata
        }).unwrap();
        toast.success('CSV File Uploaded Sucessfully');
        setCsvFormModal();
      } catch (e) {
        toast.error('Failed to Upload, Please check the file format');
      }
    }
  });

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <Modal
          title="Upload Csv"
          isModalOpen={csvFormModal}
          closeModal={setCsvFormModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton onClick={setCsvFormModal} title="Close" />
              <PrimaryButton
                title={`Upload`}
                iconName={` ri-add-line`}
                type="submit"
                isLoading={isloading}
              />
            </div>
          )}
        >
          <div className="row">
            <FileDrop
              files={formik.values.csvFile}
              message="click to upload files"
              setFiles={(file) => {
                formik.setFieldValue('csvFile', file);
              }}
            />
          </div>
          <h6 style={{ color: 'red' }}>(files must be .csv, .xlxs and upto 5MB)</h6>
        </Modal>
      </form>
    </div>
  );
};

export default CsvUploadForm;
