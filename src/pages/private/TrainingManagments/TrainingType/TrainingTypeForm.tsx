import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { ITrainingTypeInput } from 'redux/reducers/training-type';

interface ITrainingTypeForm {
  formik: FormikProps<ITrainingTypeInput>;
}
const TrainingTypeForm: FC<ITrainingTypeForm> = ({ formik }) => {
  return (
    <div>
      <div>
        <TextInput
          label="Title"
          name="title"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.title}
          onBlur={formik.handleBlur}
          formik={formik}
          containerClassName="col-md-12"
        />
      </div>
      <div>
        <div className="mb-3 mt-3 col-md-12">
          <label htmlFor="Training-detail" className="form-label">
            Descriptions
          </label>
          <textarea
            className="form-control"
            rows={3}
            placeholder="Descriptions"
            defaultValue={''}
            value={formik.values.description}
            onChange={(event) => {
              formik.setFieldValue('description', event.target.value);
            }}
          />
          <FormikValidationError
            name="description"
            errors={formik.errors}
            touched={formik.touched}
          />
        </div>
      </div>
    </div>
  );
};

export default TrainingTypeForm;
