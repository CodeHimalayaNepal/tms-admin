import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import { FaRegEdit } from 'react-icons/fa';
import { RiDeleteBin5Line } from 'react-icons/ri';
import { ITrainingTypeInput, useListTrainingTypeQuery } from 'redux/reducers/training-type';

interface ITrainingTypeTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: ITrainingTypeInput) => void;
  onDeleteClicked: (data: ITrainingTypeInput) => void;
}
const TrainingTypeTable: FC<ITrainingTypeTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const { data: trainingTypeData, isLoading } = useListTrainingTypeQuery();
  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{data.row.index + 1}</>;
      }
    },
    {
      Header: ' Training Type',
      accessor: 'title'
    },

    {
      Header: 'Descriptions',
      accessor: 'description'
    },

    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <FaRegEdit size={20} onClick={() => onEditClicked(data.row.original)} />
            <RiDeleteBin5Line
              size={20}
              className="m-2"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];

  return (
    <div>
      <DataTable
        columns={columns}
        isLoading={isLoading}
        data={trainingTypeData?.data || []}
        tableHeaders={tableHeaders}
      />
    </div>
  );
};

export default TrainingTypeTable;
