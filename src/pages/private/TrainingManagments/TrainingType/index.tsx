import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import {
  ITrainingTypeInput,
  useCreateTrainingTypeMutation,
  useDeleteTrainingTypeMutation,
  useUpdateTrainingTypeMutation
} from 'redux/reducers/training-type';
import { trainingTypeInitialValues, TrainingTypeSchemaValidations } from './schema';

import TrainingTypeForm from './TrainingTypeForm';
import TrainingTypeTable from './TrainingTypeTable';

const TrainingType = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [trainingTypeInitialFormState, setTrainingTypeInitialForm] =
    useState(trainingTypeInitialValues);
  const [deletingTrainingTypeState, setDeletingTrainingType] = useState<ITrainingTypeInput | null>(
    null
  );

  const [deleteTrainingType, { isLoading: isDeleting }] = useDeleteTrainingTypeMutation();
  const [createTrainingType, { isLoading: isCreating }] = useCreateTrainingTypeMutation();
  const [updateTrainingType, { isLoading: isUpdating }] = useUpdateTrainingTypeMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: TrainingTypeSchemaValidations,
    initialValues: trainingTypeInitialFormState,
    onSubmit: (values) => {
      let executeFunc = createTrainingType;
      if (values.training_type_id) {
        executeFunc = updateTrainingType;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setTrainingTypeInitialForm(trainingTypeInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
    }
  });

  const onEditClicked = (data: ITrainingTypeInput) => {
    setTrainingTypeInitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingTrainingTypeState &&
      deleteTrainingType(deletingTrainingTypeState?.training_type_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setTrainingTypeInitialForm(trainingTypeInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: ITrainingTypeInput) => {
    setDeletingTrainingType(data);
    toggleDeleteModal();
  };

  const closeModal = () => {
    setFormModal();
    setTrainingTypeInitialForm(trainingTypeInitialValues);
  };

  return (
    <Container title="Training Type">
      <TrainingTypeTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title={
                trainingTypeInitialFormState.training_type_id
                  ? 'Update training type'
                  : 'Create new training type'
              }
              iconName="ri-add-line align-bottom me-1"
              onClick={setFormModal}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
      />
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={
            trainingTypeInitialFormState.training_type_id
              ? 'Update training type'
              : 'Create new training type'
          }
          isModalOpen={formModal}
          closeModal={closeModal}
          modalSize="modal-md"
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={closeModal}
                title="Close"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton
                title={
                  trainingTypeInitialFormState.training_type_id
                    ? 'Update Training Type'
                    : 'Add Training Type'
                }
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <TrainingTypeForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default TrainingType;
