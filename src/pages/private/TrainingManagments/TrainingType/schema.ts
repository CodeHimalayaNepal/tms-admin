import { ITrainingTypeInput } from 'redux/reducers/training-type';
import * as Yup from 'yup';

export const trainingTypeInitialValues: ITrainingTypeInput = {
  title: '',
  description: ''
};

export const TrainingTypeSchemaValidations = Yup.object().shape({
  title: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Title is required'),
  description: Yup.string()
    .min(2, 'Too Short!')
    .max(100, 'Too Long!')
    .required('Description is required')
});
