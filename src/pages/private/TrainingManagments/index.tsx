import React from 'react';
import { Outlet } from 'react-router-dom';

const TrainingManagements = () => {
  return (
    <div>
      <Outlet />
    </div>
  );
};

export default TrainingManagements;
