import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import EditIcon from '../../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../../assets/images/icons/delete.png';
import { Cell } from 'react-table';
import { ITrainingInput, useListTrainingQuery } from 'redux/reducers/training';
import 'css/style.css';
interface ITraineeDataTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: ITrainingInput) => void;
  onDeleteClicked: (data: ITrainingInput) => void;
}
const TrainingManagementTable: FC<ITraineeDataTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked
}) => {
  const [page, setPage] = useState(1);
  const [allDataList, setAllDataList] = useState(0);
  const { data: trainingData, isLoading } = useListTrainingQuery();
  console.log(trainingData, 'ttt');
  const totalTraining = trainingData;

  const paginatedData = totalTraining?.slice(
    page === 1 ? 0 : (page - 1) * 10,
    page === 1 ? 10 : page * 10
  );
  const canPrevious = page === 1 ? false : true;
  const canNext = allDataList === totalTraining?.length ? false : true;

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: 'Training Name',
      accessor: 'training_name'
    },
    {
      Header: 'Course',
      accessor: (
        row: Omit<ITrainingInput, 'courses'> & { courses: { id: number; title: string }[] }
      ) => row.courses.map((c) => c.title).join(', '),
      Cell: (
        cell: Cell<Omit<ITrainingInput, 'courses'> & { courses: { id: number; title: string }[] }>
      ) => {
        return (
          <div className="d-flex flex-wrap">
            {cell.row.original.courses.map((c) => (
              <span className="badge bg-info me-1">{c.title}</span>
            ))}
          </div>
        );
      }
    },
    {
      Header: 'Training Code',
      accessor: 'training_code'
    },
    {
      Header: 'Action',
      Cell: (data: Cell<ITrainingInput & { id: number }>) => {
        return (
          <>
            <img
              src={EditIcon}
              className="icons"
              onClick={() => onEditClicked(data.row.original)}
            />
            <img
              src={DeleteIcon}
              className="m-2 icons"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <DataTable
      columns={columns}
      data={paginatedData || []}
      tableHeaders={tableHeaders}
      isLoading={isLoading}
      pagination={{
        canPrevious,
        canNext,
        prevPage: () => {
          if (totalTraining) {
            page > 1 && setPage(page - 1);
          }
        },
        nextPage: () => {
          if (totalTraining) {
            totalTraining?.length >= allDataList && setPage(page + 1);
          }
        }
      }}
    />
  );
};

export default TrainingManagementTable;
