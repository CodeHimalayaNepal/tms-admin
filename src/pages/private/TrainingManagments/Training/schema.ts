import { ITrainingInput } from 'redux/reducers/training';
import { ITrainingTypeInput } from 'redux/reducers/training-type';
import * as Yup from 'yup';

export const trainingInitialValues = {
  training_name: '',
  training_description: '',
  training_code: '',
  //   center: null as number | null,
  courses: [] as string[],
  id: null as number | null,
  image: [] as File[]
};
const SUPPORTED_FORMATS = ['image/jpg', 'image/jpeg', 'image/png'];
export const TrainingSchemaValidations = Yup.object().shape({
  training_name: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Title is required'),
  training_description: Yup.string().min(2, 'Too Short!').required('Description is required'),
  training_code: Yup.string().required('Code is required'),
  courses: Yup.array()
    .min(1, 'course  is required.')
    .required("You can't leave this blank.")
    .nullable()
  // image: Yup.mixed()
  //   .required('A file is required')
  //   .nullable()
  //   .test((value) => !value || (value && value.size <= 1024 * 1024))
  //   .test(
  //     'format',
  //     'upload file',
  //     (value) => !value || (value && SUPPORTED_FORMATS.includes(value.type))
  //   )
  // center: Yup.number().required('Training Center is required')
});
