import Container from 'components/ui/Container';
import CustomSelect from 'components/ui/Editor/CustomSelect';
import EditorComponent from 'components/ui/Editor/editor';
import { FileDrop } from 'components/ui/FileDrop/FileDrop';
import FormikValidationError from 'components/ui/FormikErrors';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import Spinner from 'components/ui/Spinner/spinner';
import TextInput from 'components/ui/TextInput';
import { useFormik } from 'formik';
import React, { FC, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { CoursesResponse, ICoursesInput, useListCoursesQuery } from 'redux/reducers/courses';
import {
  IOrganizationCentreInput,
  useListOrganizationCentreQuery
} from 'redux/reducers/organizations-centre';
import {
  ITrainingInput,
  useCreateTrainingMutation,
  useListTrainingByIdQuery,
  useUpdateTrainingMutation
} from 'redux/reducers/training';
import { useListTrainingTypeQuery } from 'redux/reducers/training-type';
import { boolean } from 'yup';

import { trainingInitialValues, TrainingSchemaValidations } from './schema';

interface ITrainingForm {}
const TrainingForm: FC<ITrainingForm> = () => {
  let { trainingId } = useParams();
  const navigate = useNavigate();

  const { data: trainingType } = useListTrainingTypeQuery();
  const { data: courseList } = useListCoursesQuery({ page: 1, pageSize: 1000 });

  const { data: orgCenters } = useListOrganizationCentreQuery();
  const [createTraining, { isLoading: isCreating }] = useCreateTrainingMutation();
  const [updateTraining, { isLoading: isUpdating }] = useUpdateTrainingMutation();
  const [trainingInitialFormState, setTrainingInitialForm] = useState(trainingInitialValues);
  const [errorLogo, setErrorLogo] = useState(false);
  const [errorLogosize, setErrorLogosize] = useState(false);
  const isCreate = trainingId === 'create';
  const { data: trainingById, isLoading: isFetchingTrainingDetail } = useListTrainingByIdQuery(
    trainingId ? trainingId : '',
    { skip: isCreate || !trainingId }
  );

  React.useEffect(() => {
    return () => {
      setErrorLogo(false);
    };
  }, []);

  React.useEffect(() => {
    if (trainingById && !isFetchingTrainingDetail) {
      setTrainingInitialForm({
        ...trainingById.data,
        training_code: trainingById.data.training_code,
        training_description: trainingById.data.training_description,
        training_name: trainingById.data.training_name,
        courses: trainingById?.data?.courses?.map((item: any) => item.id.toString()),
        image: []
      });
    }
  }, [trainingById, isFetchingTrainingDetail, courseList]);

  const courseListsOptions =
    courseList?.data?.results?.map((item: CoursesResponse) => {
      return {
        label: item.title,
        value: item.id.toString()
      };
    }) || [];

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: TrainingSchemaValidations,
    initialValues: trainingInitialFormState,
    onSubmit: (values) => {
      if (values?.image[0]?.size > 1048576) {
        setErrorLogosize(true);
      } else {
        let executeFunc = createTraining;
        if (trainingId) {
          executeFunc = updateTraining;
        }
        const img = values.image?.[0] || [];
        let selectedValues = {
          ...values,
          id: trainingId,
          courses: values.courses.map((c) => +c),
          // image: new Blob([img],{type:img.type})
          image: img
        };
        // if(values.image.length) {
        //   let data = new FormData();
        //   values.image.map((image,i) => {
        //     data.append(`img${i+1}`)
        //   })
        // }

        const { ...trainingValues } = selectedValues;

        executeFunc(trainingValues)
          .unwrap()
          .then(() => {
            toast.success('Updated Successfully');
            setTrainingInitialForm(trainingInitialValues);
            navigate('/training-mgmt/training');
          })
          .catch(() => {
            toast.error('Error');
          });
      }
    }
  });
  return (
    <Container title={trainingInitialFormState.id ? 'Update Training' : 'Create Training'}>
      <form onSubmit={formik.handleSubmit}>
        <div className="row">
          <div className="col-4">
            <FileDrop
              defaultImage={trainingById?.data?.image ?? ''}
              files={formik.values.image}
              setFiles={(data) => {
                if (data[0].size > 1048576) {
                  setErrorLogosize(false);
                }
                setErrorLogo(false);
                formik.setFieldValue('image', data);
              }}
              preview={true}
              message={'Click to upload the logo'}
            />
            <FormikValidationError name="image" errors={formik.errors} touched={formik.touched} />
          </div>

          <div className="col-8">
            <div className="row">
              <div style={{ display: 'flex', gap: '0.5rem' }}>
                <TextInput
                  containerClassName="mt-3 col-lg-6 col-md-6"
                  label="Training Name"
                  name="training_name"
                  type="text"
                  onChange={formik.handleChange}
                  value={formik.values.training_name}
                  onBlur={formik.handleBlur}
                  formik={formik}
                  disableRequiredValidation
                />
                <TextInput
                  containerClassName="mt-3 col-lg-6 col-md-6"
                  label="Training Code"
                  name="training_code"
                  type="text"
                  onChange={formik.handleChange}
                  value={formik.values.training_code}
                  onBlur={formik.handleBlur}
                  formik={formik}
                  disableRequiredValidation
                />
              </div>

              <CustomSelect
                containerClassName="mt-3 col-lg-6 col-md-6"
                options={courseListsOptions}
                placeholder={'Search by title'}
                value={formik.values.courses}
                onChange={(value: string) => formik.setFieldValue('courses', value)}
                isMulti={true}
                label={'Course'}
              />
              <FormikValidationError
                name="courses"
                errors={formik.errors}
                touched={formik.touched}
              />

              <div className="my-3 col-md-12">
                <EditorComponent
                  label="Training Description"
                  containerClassName="mt-3"
                  editorValue={formik.values.training_description}
                  onChangeValue={(data) => formik.setFieldValue('training_description', data)}
                />
              </div>
              <FormikValidationError
                name="training_description"
                errors={formik.errors}
                touched={formik.touched}
              />
            </div>
          </div>
        </div>

        <div className="hstack gap-2 justify-content-end">
          <SecondaryButton
            onClick={() => navigate('/training-mgmt/training')}
            title="Close"
            isLoading={isUpdating || isCreating}
          />
          <PrimaryButton
            title={trainingInitialFormState.id ? 'Update Training' : 'Create Training'}
            iconName={`${trainingInitialFormState.id ? '' : 'ri-add-line'} align-bottom me-1`}
            type="submit"
            isLoading={isUpdating || isCreating}
          />
        </div>
      </form>
    </Container>
  );
};

export default TrainingForm;
