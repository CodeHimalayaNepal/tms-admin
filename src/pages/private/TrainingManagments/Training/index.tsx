import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { ITrainingInput, useDeleteTrainingMutation } from 'redux/reducers/training';

import TrainingManagementTable from './TrainingManagementTable';

const TrainingManagement = () => {
  const navigate = useNavigate();
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [deletingTrainingState, setDeletingTraining] = useState<ITrainingInput | null>(null);

  const [deleteTraining, { isLoading: isDeleting }] = useDeleteTrainingMutation();

  const onEditClicked = (data: ITrainingInput) => {
    navigate(`/training-mgmt/training/edit/${data.id}`);
    //${data.id}
  };

  const onDeleteConfirm = () => {
    deletingTrainingState &&
      deleteTraining(deletingTrainingState?.id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: ITrainingInput) => {
    setDeletingTraining(data);
    toggleDeleteModal();
  };

  return (
    <Container
      title="Training Management"
      subTitle="List of all trainings
    "
    >
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <TrainingManagementTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title={'Create Training'}
              iconName=" ri-add-line align-bottom me-1"
              onClick={() => navigate('/training-mgmt/training/new')}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
      />
    </Container>
  );
};

export default TrainingManagement;
