import React, { Component } from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import {
  useListCoordinatorRoutineForRosterQuery,
  useListEvaluatorRoutineForRosterQuery
  // uselistCoordinatorRoutineForRosterByIdQuery
} from 'redux/reducers/roster';
// import { useFetchUserRoleQuery } from 'redux/reducers/administrator';

const localizer = momentLocalizer(moment);

const RosterManagement = () => {
  const { data: coordinatorList } = useListCoordinatorRoutineForRosterQuery();
  const { data: evaluatorList } = useListEvaluatorRoutineForRosterQuery();
  // const { data: coordinatorById } = uselistCoordinatorRoutineForRosterByIdQuery();
  // const { data: usersLists } = useFetchUserRoleQuery();
  const coordinatorEvent = !!coordinatorList?.data?.results
    ? coordinatorList?.data?.results
        ?.map((item: any) => {
          return {
            id: item.coordinator_id
          };
          return item?.routine?.map((r: any) => {
            return {
              id: r.routine_id,
              title: r.routine_name,
              allDay: true,
              start: new Date(r.start_date),
              end: new Date(r.end_date)
            };
          });
        })
        .flat()
    : [];
  const evaluatorEvent = !!evaluatorList?.data?.results
    ? evaluatorList?.data?.results
        ?.map((item: any) => {
          return item?.routine?.map((r: any) => {
            return {
              id: r.routine_id,
              title: r.routine_name,
              allDay: true,
              start: new Date(r.start_date),
              end: new Date(r.end_date)
            };
          });
        })
        .flat()
    : [];
  // const coordinatorEventById = !!coordinatorById?.data?.results
  //   ? coordinatorById?.data?.results
  //       ?.map((item: any) => {
  //         return item?.routine?.map((r: any) => {
  //           return {
  //             id: r.routine_id,
  //             title: r.routine_name,
  //             allDay: true,
  //             start: new Date(r.start_date),
  //             end: new Date(r.end_date)
  //           };
  //         });
  //       })
  //       .flat()
  //   : [];

  const eventsdata = [...evaluatorEvent, ...coordinatorEvent];
  // console.log(eventsdata);

  return (
    <div
      className="App"
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Calendar
        localizer={localizer}
        defaultDate={new Date()}
        defaultView="month"
        events={eventsdata}
        style={{
          height: '90vh',
          width: '150vh',
          marginTop: '70px',
          marginLeft: '150px'
        }}
      />
    </div>
  );
};

export default RosterManagement;

// import Container from 'components/ui/Container';
// import TimelineGant from 'components/ui/TimeLineGantChart/TimeLineGantChart';
// import moment from 'moment';
// import React from 'react';
// import { useFetchUserRoleQuery } from 'redux/reducers/administrator';
// import {
//   useListCoordinatorRoutineForRosterQuery,
//   useListEvaluatorRoutineForRosterQuery
// } from 'redux/reducers/roster';

// const groups = [
//   { id: 1, title: 'group 1' },
//   { id: 2, title: 'group 2' }
// ];

// const items = [
//   {
//     id: 1,
//     group: 1,
//     title: 'item 1',
//     start_time: moment(),
//     end_time: moment().add(1, 'hour')
//   },
//   {
//     id: 5,
//     group: 1,
//     title: 'item 5',
//     stackItems: true,
//     start_time: moment(),
//     end_time: moment().add(1, 'hour')
//   },
//   {
//     id: 2,
//     group: 2,
//     title: 'item 2',
//     start_time: moment().add(-0.5, 'hour'),
//     end_time: moment().add(0.5, 'hour')
//   },
//   {
//     id: 3,
//     group: 1,
//     title: 'item 3',
//     start_time: moment().add(2, 'hour'),
//     end_time: moment().add(3, 'hour')
//   }
// ];

// const RosterManagement = () => {
//   const { data: usersLists } = useFetchUserRoleQuery();
//   const filterLists =
//     usersLists?.staffs?.find((word: any) => word.name.toUpperCase() === 'COORDINATOR')?.staff || [];
//   const filterEvalLists =
//     usersLists?.staffs?.find((word: any) => word.name.toUpperCase() === 'EVALUATOR')?.staff || [];

//   const allStaffs = filterEvalLists && filterLists ? [...filterLists, ...filterEvalLists] : [];
//   const { data: coordinatorRoutine } = useListCoordinatorRoutineForRosterQuery();
//   const { data: rosterRoutine } = useListEvaluatorRoutineForRosterQuery();
//   const groups = allStaffs?.map((item: any) => {
//     return {
//       id: item.id,
//       title: item.first_name + ' ' + item.last_name
//     };
//   });

//   return (
//     <Container title="RosterManagement">
//       <TimelineGant groups={groups} items={items} />
//     </Container>
//   );
// };

// export default RosterManagement;
