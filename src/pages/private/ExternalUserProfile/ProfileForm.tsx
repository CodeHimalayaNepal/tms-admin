import { Avatar } from 'common/constant';
import EditorComponent from 'components/ui/Editor/editor';
import { FileDrop } from 'components/ui/FileDrop/FileDrop';
import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { IProfile } from './schema';
interface IProfileForm {
  formik: FormikProps<IProfile>;
}
const getImage = (image: File) => {
  try {
    return URL.createObjectURL(image);
  } catch (error) {
    return '';
  }
};

const ProfileForm: FC<IProfileForm> = ({ formik }) => {
  return (
    <div className="card mt-2 px-4">
      <div className="card-body">
        <div className="text-center">
          <div className="profile-user position-relative d-inline-block mx-auto  mb-4">
            <img
              src={
                formik.values.image
                  ? typeof formik.values.image === 'string'
                    ? formik.values.image
                    : getImage(formik.values.image)
                  : Avatar
              }
              className="rounded-circle avatar-xl img-thumbnail user-profile-image"
              alt="user-profile-image"
            />
            <div className="avatar-xs rounded-circle profile-photo-edit">
              <input
                id="profile-img-file-input"
                type="file"
                className="profile-img-file-input"
                name="image"
                onChange={(e) => {
                  formik.handleChange({
                    target: { name: 'image', value: e.target.files?.[0] ?? null }
                  });
                }}
              />
              <label htmlFor="profile-img-file-input" className="profile-photo-edit avatar-xs">
                <span className="avatar-title rounded-circle bg-blue text-body">
                  <i className="ri-pencil-fill" />
                </span>
              </label>
            </div>
          </div>

          <h5 className="fs-14 mb-1">Profile Image</h5>

          {formik.touched.image && formik.errors.image && (
            <span className="text-danger">{formik.errors.image}</span>
          )}
        </div>
      </div>

      <div className="col-md">
        <EditorComponent
          label="Bio"
          withoutMinHeight
          containerClassName="mt-3"
          editorValue={formik.values.bio}
          onChangeValue={(data) => formik.setFieldValue('bio', data)}
          placeholder="Your bio here...."
        />

        <FormikValidationError name="bio" errors={formik.errors} touched={formik.touched} />
      </div>
      <span className="profile-details-heading mt-4">Personal Details</span>
      <div className="row mb-3">
        <TextInput
          label="First Name"
          name="first_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.first_name}
          onBlur={formik.handleBlur}
          formik={formik}
          required
          disableRequiredValidation
        />
        <TextInput
          label="Middle Name"
          name="middle_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.middle_name}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
        />
        <TextInput
          label="Last Name"
          name="last_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.last_name}
          onBlur={formik.handleBlur}
          formik={formik}
          required
          disableRequiredValidation
        />
        <TextInput
          label="Email"
          name="email"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.email}
          onBlur={formik.handleBlur}
          formik={formik}
          required
          disableRequiredValidation
        />
        <TextInput
          label="Mobile Number"
          name="mobile_number"
          type="number"
          onChange={formik.handleChange}
          value={formik.values.mobile_number}
          onBlur={formik.handleBlur}
          formik={formik}
          required
          disableRequiredValidation
        />
        <TextInput
          label="PAN Number"
          name="pan_number"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.pan_number}
          onBlur={formik.handleBlur}
          formik={formik}
          required
          disableRequiredValidation
        />
      </div>
      <div className="mt-4">
        <span className="profile-details-heading">Professional Details</span>
      </div>
      <div className="row mb-3">
        <TextInput
          label="Current Position"
          name="current_position"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.current_position}
          onBlur={formik.handleBlur}
          formik={formik}
          required
          disableRequiredValidation
        />
        <TextInput
          label="Associated Organization"
          name="associated_organization"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.associated_organization}
          onBlur={formik.handleBlur}
          formik={formik}
          required
          disableRequiredValidation
        />
      </div>
      <div className="mt-4">
        <span className="profile-details-heading">Bank Details</span>
      </div>
      <div className="row mb-3">
        <TextInput
          label="Bank Name"
          name="bank_detail[0].bank_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.bank_detail[0].bank_name}
          onBlur={formik.handleBlur}
          formik={formik}
          required
          disableRequiredValidation
        />
        <TextInput
          label="Branch"
          name="bank_detail[0].bank_branch"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.bank_detail[0].bank_branch}
          onBlur={formik.handleBlur}
          formik={formik}
          required
          disableRequiredValidation
        />
        <TextInput
          label="Account Number"
          name="bank_detail[0].account_number"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.bank_detail[0].account_number}
          onBlur={formik.handleBlur}
          formik={formik}
          required
          disableRequiredValidation
        />
        <TextInput
          label="Account Name"
          name="bank_detail[0].account_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.bank_detail[0].account_name}
          onBlur={formik.handleBlur}
          formik={formik}
          required
          disableRequiredValidation
        />
      </div>
      <div className="col-md mb-3">
        <EditorComponent
          label="Area of experience"
          containerClassName="mt-3"
          withoutMinHeight
          editorValue={formik.values.experience}
          onChangeValue={(data) => formik.setFieldValue('experience', data)}
          placeholder="Your area of experience here..."
        />
        <FormikValidationError name="experience " errors={formik.errors} touched={formik.touched} />
      </div>
      <div className="col-md mb-3">
        <EditorComponent
          label="Expertise"
          containerClassName="mt-3"
          withoutMinHeight
          editorValue={formik.values.area_of_expertise}
          onChangeValue={(data) => formik.setFieldValue('area_of_expertise', data)}
          placeholder="Your area_of_expertise here..."
        />
        <FormikValidationError
          name="area_of_expertise"
          errors={formik.errors}
          touched={formik.touched}
        />
      </div>
      <span className="profile-details-heading mt-3">Upload CV</span>
      <div className="col-3" style={{ cursor: 'pointer' }}>
        <FileDrop
          files={
            formik.values.documents instanceof File
              ? formik.values.documents
              : ([] as unknown as File[])
          }
          setFiles={(data) => formik.setFieldValue('documents', data)}
          message="Add new file"
          preview={true}
        />
        <b> {`*** PDF or WORD < 2Mb `}</b>
      </div>
      {/* <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', marginTop: '10px' }}>
        {formik.values.existing_documents &&
          formik?.values?.existing_documents?.length > 0 &&
          formik.values.existing_documents.map((documents, index) => {
            return (
              <img
                src={documents.document}
                className="rounded-circle avatar-xl img-thumbnail user-profile-image"
                alt="user-profile-image"
              />
            );
          })}
      </div> */}
      <FormikValidationError name="documents" errors={formik.errors} touched={formik.touched} />
    </div>
  );
};

export default ProfileForm;
