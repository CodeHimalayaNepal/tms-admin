import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import React, { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  useCurrentUserDetailsQuery,
  useFetchUserProfileQuery,
  useUpdateUserProfileMutation
} from 'redux/reducers/administrator';
import { useChangePasswordMutation } from 'redux/reducers/auth';
import * as Yup from 'yup';

import ChangePasswordForm from '../Profile/ChangePasswordForm';
import { initialValue, SignUpValidationSchema } from '../Profile/schema';
import ChangePassword from './ChangePassword';
import ProfileForm from './ProfileForm';
import { IProfile, profileInitialValues, profileValidationSchema } from './schema';

const ExternalUserProfile = () => {
  const [value, setValue] = useState<string>('profile');
  const { id } = useParams<{ id: string }>();
  const navigate = useNavigate();
  const { data: userDetails } = useCurrentUserDetailsQuery();
  const { data: profileDetail } = useFetchUserProfileQuery(id);
  const [changethisPassword, isLoading] = useChangePasswordMutation();

  const [updateProfile, { data: updateProfileData }] = useUpdateUserProfileMutation();
  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: profileValidationSchema,
    initialValues: profileInitialValues,

    onSubmit: (values) => {
      let executeFunc = updateProfile;
      if (typeof values.image === 'string') {
        const { image, ...rest } = values;

        executeFunc({ id: profileDetail.id, ...rest })
          .unwrap()
          .then((data: any) => {
            toast.success('Profile Updated Successfully');
            navigate('/');
          })
          .catch((err: any) => {
            console.log(err);
            toast.error(err.data?.image ? err.data?.image[0] : null);
          });
      } else {
        executeFunc({ id: profileDetail.id, ...values })
          .unwrap()
          .then((data: any) => {
            toast.success('Profile Updated Successfully');
            navigate('/');
          })
          .catch((err: any) => {
            console.log(err);
            toast.error(err.data?.image ? err.data?.image[0] : null);
          });
      }
    }
  });

  const passwordFormik = useFormik({
    validationSchema: Yup.object().shape({
      ...SignUpValidationSchema
    }),
    enableReinitialize: true,
    initialValues: initialValue,
    onSubmit: (values) => {
      let executeFunc = changethisPassword;
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          navigate('/');
          toast.success('Password Edited Successfullly');
        })
        .catch((err: any) => {
          if (err.data?.errors) {
            toast.error(Object.values(err.data.errors).join(', '));
            return;
          }
        });
    }
  });

  React.useEffect(() => {
    if (profileDetail) {
      formik.setValues({
        first_name: profileDetail.first_name || '',
        last_name: profileDetail.last_name || '',
        email: profileDetail.email || '',
        area_of_expertise: profileDetail.area_of_expertise || '',
        current_position: profileDetail.current_position || '',
        middle_name: profileDetail.middle_name || '',
        bio: profileDetail?.bio || '',
        experience: profileDetail.experience || '',
        associated_organization: profileDetail.associated_organization || '',
        documents: profileDetail.documents || (null as unknown as File),
        image: profileDetail.image || (null as unknown as File),
        pan_number: profileDetail.pan_number || '',

        mobile_number: profileDetail.mobile_number || '',
        bank_detail: [
          {
            bank_name:
              formik?.values?.bank_detail[0]?.bank_name ||
              profileDetail?.bank_details[0]?.bank_name ||
              '',
            bank_branch:
              formik.values.bank_detail[0].bank_branch ||
              profileDetail.bank_details[0]?.bank_branch ||
              '',
            account_number:
              formik.values.bank_detail[0].account_number ||
              profileDetail.bank_details[0]?.account_number ||
              '',
            account_name:
              formik.values.bank_detail[0].account_name ||
              profileDetail.bank_details[0]?.account_name ||
              ''
          }
        ]
      });
    }
  }, [profileDetail]);

  if (formik.values.documents instanceof File) {
    const files = formik.values.documents;
  }
  // const files =
  return (
    <Container title="Profile">
      <div className="row">
        <div className="col-xxl-2">
          <div className="d-flex  flex-column">
            <div className="d-flex justify-content-start p-2" onClick={() => setValue('profile')}>
              <i className="ri-pencil-line align-bottom"></i>
              <span className="me-1" style={{ cursor: 'pointer' }}>
                Profile
              </span>
            </div>
            <hr />
            <div className="d-flex justify-content- p-2" onClick={() => setValue('password')}>
              <i className="ri-lock-line align-bottom"></i>
              <span style={{ cursor: 'pointer' }}>Password And Security</span>
            </div>
          </div>
        </div>

        <div className="col-xxl-10">
          {value === 'profile' ? (
            <form onSubmit={formik.handleSubmit}>
              <ProfileForm formik={formik} />
              <div className="hstack gap-2 justify-content-end">
                <PrimaryButton
                  title="Save"
                  iconName="ri-add-line align-bottom me-1"
                  type="submit"
                  // isLoading={creatingCourse || updatingCourse}
                />
                <SecondaryButton
                  onClick={() => {
                    //   navigate('/');
                  }}
                  title="Discard"
                  // isLoading={creatingCourse || updatingCourse}
                />
              </div>
            </form>
          ) : (
            <div>
              <form onSubmit={passwordFormik.handleSubmit}>
                <ChangePassword formik={passwordFormik} />
                <div className="row">
                  <div className="col-xxl-4 my-4">
                    <div className="hstack gap-2 justify-content-end">
                      <PrimaryButton
                        title="Save"
                        iconName="ri-add-line align-bottom me-1"
                        type="submit"
                        // isLoading={creatingCourse || updatingCourse}
                      />
                      <SecondaryButton
                        onClick={() => {
                          // navigate('/modules');
                        }}
                        title="Discard"
                        // isLoading={creatingCourse || updatingCourse}
                      />
                    </div>
                  </div>
                </div>
              </form>
            </div>
          )}
        </div>
      </div>
    </Container>
  );
};

export default ExternalUserProfile;
