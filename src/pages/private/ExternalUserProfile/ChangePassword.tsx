import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { IInitialValue } from '../Profile/schema';
interface IChangePasswordForm {
  formik: FormikProps<IInitialValue>;
}

const ChangePassword: FC<IChangePasswordForm> = ({ formik }) => {
  return (
    <>
      <h1>Password and Security</h1>
      <span className="profile-details-heading">Change Password</span>
      <TextInput
        label="Old Password"
        name="old_password"
        type="password"
        onChange={formik.handleChange}
        value={formik.values.old_password}
        onBlur={formik.handleBlur}
        formik={formik}
        placeholder={'Old Password'}
        disableRequiredValidation
      />

      <TextInput
        label="New Password"
        name="new_password"
        type="password"
        onChange={formik.handleChange}
        value={formik.values.new_password}
        onBlur={formik.handleBlur}
        formik={formik}
        placeholder={'New Password'}
        disableRequiredValidation
      />
      <TextInput
        label="Confirm Password"
        name="confirm_password"
        type="password"
        onChange={formik.handleChange}
        value={formik.values.confirm_password}
        onBlur={formik.handleBlur}
        formik={formik}
        placeholder={'Confirm Password'}
        disableRequiredValidation
      />
    </>
  );
};

export default ChangePassword;
