import { useRef } from 'react';
import Container from 'components/ui/Container';
import { AiFillBank } from 'react-icons/ai';
import { GoLocation } from 'react-icons/go';

import { FiPhoneCall } from 'react-icons/fi';
import { BsBagX } from 'react-icons/bs';
import { MdDriveFileRenameOutline } from 'react-icons/md';
import { AiOutlineNumber } from 'react-icons/ai';
import { AiOutlineFileDone } from 'react-icons/ai';

import { BiEnvelope } from 'react-icons/bi';
import { useNavigate, useParams } from 'react-router-dom';
import { useFetchUserProfileQuery, IProfileDetail } from 'redux/reducers/administrator';
import ReactToPrint from 'react-to-print';
import PrimaryButton from 'components/ui/PrimaryButton';

const styles = {
  margin: '3px 5px',
  fontSize: '18px'
};
const bankstyles = {
  margin: '3px 5px',
  fontSize: '18px'
};
const Docstyles = {
  fontSize: '150px'
};
const ExternalProfileDetail = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const { data: UserProfile } = useFetchUserProfileQuery(id) as { data: IProfileDetail };
  const ref = useRef<HTMLDivElement | null>(null);
  return (
    <Container title="Profile">
      <div className="row">
        <div className="col-xxl-12">
          <div className="w-100 d-flex justify-content-between  p-3">
            <p className="h3 fw-bold ">Profile</p>
            <button
              type="button"
              className="btn btn-primary"
              onClick={() => navigate(`/external-user-management/external/${id}`)}
            >
              <p className="h5 text-white">Edit Profile</p>
            </button>
          </div>

          <div ref={ref} style={{ margin: '20px' }}>
            <div className="row">
              <div className="col-md-3 col-lg-3 col-sm-12 p-3 ">
                <div className="row border mb-3">
                  <div className=" pt-2">
                    <div className="text-center">
                      <img
                        style={{ width: '150px', height: '150px', borderRadius: '50%' }}
                        className="rounded-circle  mx-auto d-block"
                        src={UserProfile?.image}
                      />
                      <div className=" align-content-center d-block  mt-2">
                        <p>{UserProfile?.first_name + ' ' + UserProfile?.last_name}</p>
                        <p>Pan Id : {UserProfile?.pan_number}</p>
                      </div>
                    </div>

                    <div className="row " style={{ textAlign: 'center' }}>
                      <p>
                        <BiEnvelope style={bankstyles} />
                        {UserProfile?.email}
                      </p>
                    </div>
                    <div className="row " style={{ textAlign: 'center' }}>
                      <p>
                        <FiPhoneCall style={bankstyles} />
                        {UserProfile?.mobile_number}
                      </p>
                    </div>
                    <div className="row " style={{ textAlign: 'center' }}>
                      <p>
                        <BsBagX style={bankstyles} />
                        {UserProfile?.current_position}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="row border">
                  <div className="text-align">
                    <p
                      className="text-primary text-center"
                      style={{ fontSize: '18px', fontWeight: '500px', marginTop: '20px' }}
                    >
                      Bank Detail
                    </p>
                    {UserProfile?.bank_details?.map((bankDetail, index: number) => {
                      return (
                        <div key={index} className="d-flex justify-content-center flex-column">
                          <div className="row " style={{ textAlign: 'center' }}>
                            <p>
                              <MdDriveFileRenameOutline style={bankstyles} />
                              <b>Account Name:</b>{' '}
                              <p style={{ margin: '0px' }}>{bankDetail?.account_name}</p>
                            </p>
                          </div>

                          <div className="row " style={{ textAlign: 'center' }}>
                            <p>
                              <AiOutlineNumber style={bankstyles} />
                              <b>Account Number:</b>{' '}
                              <p style={{ margin: '0px' }}>{bankDetail?.account_number}</p>
                            </p>
                          </div>

                          <div className="row " style={{ textAlign: 'center' }}>
                            <p>
                              <AiFillBank style={bankstyles} />
                              <b>Bank Name:</b>{' '}
                              <p style={{ margin: '0px' }}>{bankDetail?.bank_name}</p>
                            </p>
                          </div>
                          <div className="row " style={{ textAlign: 'center' }}>
                            <p>
                              <AiFillBank style={bankstyles} />
                              <b>Bank Branch:</b>{' '}
                              <p style={{ margin: '0px' }}>{bankDetail?.bank_branch}</p>
                            </p>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
              <div className="col-md-12 col-lg-9 p-5">
                <div>
                  <p className="text-primary" style={{ fontSize: '18px', fontWeight: '500px' }}>
                    Bio
                  </p>
                  <p
                    style={{ fontSize: '16px' }}
                    dangerouslySetInnerHTML={{ __html: UserProfile?.bio }}
                  />
                </div>
                <div>
                  <p className="text-primary" style={{ fontSize: '18px', fontWeight: '500px' }}>
                    Area Of Experience
                  </p>

                  <ul dangerouslySetInnerHTML={{ __html: UserProfile?.experience }}></ul>
                </div>
                <div>
                  <p className="text-primary" style={{ fontSize: '18px', fontWeight: '500px' }}>
                    Expertise
                  </p>
                  <ul dangerouslySetInnerHTML={{ __html: UserProfile?.area_of_expertise }}></ul>
                </div>
                <div className="my-3">
                  <p className="text-primary" style={{ fontSize: '18px', fontWeight: '500px' }}>
                    Document
                  </p>
                  {UserProfile?.user_document?.map((document: any, index: number) => {
                    return (
                      <div>
                        <a href={document?.document}>
                          <AiOutlineFileDone style={Docstyles} />
                        </a>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ReactToPrint
        content={() => ref.current}
        trigger={() => (
          <div className="row">
            <div className="col-12 d-flex justify-content-end">
              <PrimaryButton title="Print" type="button" />
            </div>
          </div>
        )}
      />
    </Container>
  );
};

export default ExternalProfileDetail;
