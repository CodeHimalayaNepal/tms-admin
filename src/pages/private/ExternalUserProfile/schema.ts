import validations from 'utils/validation';
import * as Yup from 'yup';
import { UserDocument } from 'redux/reducers/administrator';
export interface IProfile {
  first_name: string;
  middle_name: string;
  last_name: string;
  email: string;
  mobile_number: string;
  pan_number: string;
  current_position: string;
  associated_organization: string;
  experience: string;
  area_of_expertise: string;
  documents: File[];
  image?: File | string | null;
  bio: string;
  bank_detail: IBankDetails[];
}
export interface IBankDetails {
  bank_name: string;
  account_number: string;
  account_name: string;
  bank_branch: string;
}

export const profileInitialValues: IProfile = {
  first_name: '',
  image: null as File | null,
  middle_name: '',
  last_name: '',
  email: '',
  mobile_number: '',
  pan_number: '',
  current_position: '',
  associated_organization: '',
  experience: '',
  area_of_expertise: '',
  documents: [] as File[],
  bio: '',
  bank_detail: [{ bank_name: '', account_number: '', account_name: '', bank_branch: '' }]
};
export const profileValidationSchema = Yup.object().shape({
  first_name: Yup.string().required('First Name is required'),
  last_name: Yup.string().required('Last Name is required'),
  middle_name: Yup.string().nullable(),
  email: Yup.string().email('Invalid email').required('Email is required'),
  mobile_number: Yup.string()
    .matches(validations.phone.regex, validations.phone.message)
    .required('Mobile Number is required'),
  pan_number: Yup.string().required('Pan Number is required'),
  current_position: Yup.string().required('Current Position is required'),
  associated_organization: Yup.string().required('Associated Organization is required'),
  experience: Yup.string().required('Area of experience is required'),
  area_of_expertise: Yup.string().required('Expertise is required'),
  image: Yup.mixed().required('Image is required'),
  documents: Yup.mixed().required('Document is required'),
  bio: Yup.string().required('Bio is required')
});
