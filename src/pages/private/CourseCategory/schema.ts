import { ICourseCategoryInput } from './../../../redux/reducers/courseCategory/index';
import * as Yup from 'yup';

export const courseCategoryInitialValues: ICourseCategoryInput = {
  name: '',
  description: ''
};

export const courseCategorySchemaValidation = Yup.object().shape({
  name: Yup.string().required('Name is required'),
  description: Yup.string().min(2, 'Too Short!').required('Description is required')
});
