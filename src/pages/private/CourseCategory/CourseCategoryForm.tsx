import { FC } from 'react';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { ICourseCategoryInput } from 'redux/reducers/courseCategory';
interface ITrainingForm {
  formik: FormikProps<ICourseCategoryInput>;
}
const TrainingForm: FC<ITrainingForm> = ({ formik }) => {
  return (
    <form>
      <div className="row">
        <TextInput
          label="Name"
          name="name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.name}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          containerClassName="mt-3 "
          label="Description"
          name="description"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.description}
          onBlur={formik.handleBlur}
          formik={formik}
        />
      </div>
    </form>
  );
};

export default TrainingForm;
