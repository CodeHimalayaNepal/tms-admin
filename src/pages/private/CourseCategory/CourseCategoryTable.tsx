import React, { FC, useState } from 'react';
import EditIcon from '../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../assets/images/icons/delete.png';
import { RiDeleteBin5Line } from 'react-icons/ri';
import { Cell } from 'react-table';
import { ICourseCategoryInput, useListCourseCategoryQuery } from 'redux/reducers/courseCategory';
import 'css/style.css';
import NonPageDataTable from 'components/ui/DataTable/NonPageDataTable';
interface ICourseCategoryDataTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: ICourseCategoryInput) => void;
  onDeleteClicked: (data: ICourseCategoryInput) => void;
}
const TrainingManagementTable: FC<ICourseCategoryDataTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked
}) => {
  const { data: courseCategoryData, isLoading } = useListCourseCategoryQuery();

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: Cell<ICourseCategoryInput>) => {
        return <>{data.row.index + 1}</>;
      }
    },
    {
      Header: 'Name',
      accessor: 'name'
    },
    {
      Header: 'Description',
      accessor: 'description'
    },
    {
      Header: '  Action',
      Cell: (data: Cell<ICourseCategoryInput>) => {
        return (
          <>
            <img
              src={EditIcon}
              className="icons"
              onClick={() => onEditClicked(data.row.original)}
            />
            <img
              src={DeleteIcon}
              className="m-2 icons"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div>
      <NonPageDataTable
        columns={columns}
        data={courseCategoryData?.data || []}
        tableHeaders={tableHeaders}
        isLoading={isLoading}
      />
    </div>
  );
};

export default TrainingManagementTable;
