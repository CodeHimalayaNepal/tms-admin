import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { courseCategoryInitialValues, courseCategorySchemaValidation } from './schema';
import CourseCategoryTable from './CourseCategoryTable';
import CourseCategoryForm from './CourseCategoryForm';
import {
  ICourseCategoryInput,
  useCreateCourseCategoryMutation,
  useDeleteCourseCategoryMutation,
  useUpdateCourseCategoryMutation
} from 'redux/reducers/courseCategory';

const CourseCategory = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [courseCategoryInitialFormState, setCourseCategoryInitialForm] = useState(
    courseCategoryInitialValues
  );
  const [deletingCourseCategoryState, setDeletingCourseCategory] =
    useState<ICourseCategoryInput | null>(null);

  const [deleteCourseCategory, { isLoading: isDeleting }] = useDeleteCourseCategoryMutation();
  const [createCourseCategory, { isLoading: isCreating }] = useCreateCourseCategoryMutation();
  const [updateCourseCategory, { isLoading: isUpdating }] = useUpdateCourseCategoryMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: courseCategorySchemaValidation,
    initialValues: courseCategoryInitialFormState,
    onSubmit: (values) => {
      let executeFunc = createCourseCategory;
      if (values.course_category_id) {
        executeFunc = updateCourseCategory;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success(data?.message || 'Created Successfully');
          setCourseCategoryInitialForm(courseCategoryInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error(err?.data?.message || 'Error  creating course category');
        });
    }
  });

  const onEditClicked = (data: ICourseCategoryInput) => {
    setCourseCategoryInitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingCourseCategoryState &&
      deleteCourseCategory(deletingCourseCategoryState?.course_category_id)
        .unwrap()
        .then((data: any) => {
          toast.success(data?.message || 'Deleted Successfully');
          setCourseCategoryInitialForm(courseCategoryInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error(err?.data?.message || 'Error deleting course category');
        });
  };

  const onDeleteClicked = (data: ICourseCategoryInput) => {
    setDeletingCourseCategory(data);
    toggleDeleteModal();
  };

  const closeModal = () => {
    setFormModal();
    setCourseCategoryInitialForm(courseCategoryInitialValues);
  };

  return (
    <Container title="Module Category" subTitle="List of all module category">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <CourseCategoryTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title="Create Module Category"
              iconName="ri-add-line align-bottom me-1"
              onClick={setFormModal}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={
            courseCategoryInitialFormState.course_category_id
              ? 'Update Module Category'
              : 'Create Module Category'
          }
          isModalOpen={formModal}
          closeModal={closeModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={closeModal}
                title="Close"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton
                title={courseCategoryInitialFormState.course_category_id ? 'Update' : 'Create'}
                iconName={`${
                  courseCategoryInitialFormState.course_category_id ? '' : 'ri-add-line'
                } align-bottom me-1`}
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <CourseCategoryForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default CourseCategory;
