import React, { FC, ReactNode } from 'react';

import { Column, useGlobalFilter, useTable } from 'react-table';

interface IColumns {
  Header: string;
  accessor?: string | ((row: any) => ReactNode);
  columns?: IColumns[];
}
interface IDataTable {
  columns: Column<any>[];
  data: any;
  tableHeaders?: () => React.ReactNode;
  search?: boolean;
  isLoading?: boolean;

  pagination?: {
    canNext: boolean;
    canPrevious: boolean;
    nextPage: () => void;
    prevPage: () => void;
  };
}
const CustomDataTable: FC<IDataTable> = ({
  columns,
  data,
  tableHeaders,
  search = true,
  isLoading,
  pagination: { canNext, canPrevious, nextPage, prevPage } = {}
}) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    // @ts-ignore
    setGlobalFilter,
    // @ts-ignore
    state: { globalFilter }
  } = useTable(
    {
      columns,
      data
    },
    useGlobalFilter
  );

  return (
    <>
      <div className="col-sm">
        <div className="d-flex justify-content-sm-end">{tableHeaders && tableHeaders()}</div>
      </div>
      <div className="table-responsive table-card mt-3 mb-1">
        <table className="table align-middle table-nowrap" {...getTableProps()}>
          <thead className="table-light">
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th
                    className="sort"
                    data-sort="customer_name"
                    style={{ width: 80 }}
                    {...column.getHeaderProps()}
                  >
                    {column.render('Header')}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody className="list form-check-all" {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row);
              return (
                <tr style={{ fontSize: '15px' }} {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>;
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="noresult" style={{ display: data.length ? 'none' : 'block' }}>
          <div className="text-center">
            <h5 className="mt-2">
              {isLoading ? (
                <div className="spinner-border text-secondary" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              ) : (
                'No records'
              )}
            </h5>
          </div>
        </div>
      </div>
    </>
  );
};
export default CustomDataTable;
