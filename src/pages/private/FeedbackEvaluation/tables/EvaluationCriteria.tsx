import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import { useParams } from 'react-router-dom';
import {
  IEvaluationCriteriaFormInput,
  useListEvaluationCriteriaQuery
} from 'redux/reducers/FeedbackEvaluation';

interface ITraineeDataTable {
  tableHeaders?: () => React.ReactNode;
  onEditClicked: (data: IEvaluationCriteriaFormInput) => void;
  onDeleteClicked: (data: IEvaluationCriteriaFormInput) => void;
  onViewClicked: (data: IEvaluationCriteriaFormInput) => void;
}

const EvaluationCriteria: FC<ITraineeDataTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked,
  onViewClicked
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { id } = useParams();
  const { data: EvaluationQueryData, isLoading } = useListEvaluationCriteriaQuery({
    id,
    page,
    pageSize
  });

  const canNext = EvaluationQueryData?.next;
  const canPrevious = EvaluationQueryData?.previous;

  const columns: any = [
    {
      Header: 'Criteria',
      accessor: 'evaluation_criteria_details.evaluation_criteria_detail.name'
    },
    {
      Header: ' Sub Criteria',
      accessor: 'evaluation_criteria_details.evaluation_criteria_detail.parent_detail.name'
    },
    {
      Header: 'Detail',
      accessor: 'evaluation_criteria_details.name'
    },

    {
      Header: 'Evaluator',
      Cell: (data: any) => {
        return (
          <>
            {data.row.original.routine_evaluation.map((item: any) => {
              return (
                item.evaluator_details.first_name + ' ' + item.evaluator_details.last_name + '   '
              );
            })}
          </>
        );
      }
      // accessor: 'evaluation_criteria_details.routine_evaluation.evaluator_details.first_name'
    },
    {
      Header: 'total Marks',
      accessor: 'max_marks'
    },

    {
      Header: '  Actions',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={require('./edit.png')}
              alt="edit"
              style={{ marginRight: '10px' }}
              onClick={() => onEditClicked(data.row.original)}
            />
            <img
              src={require('../../RoutineManagement/Images/Vector.png')}
              alt="delete"
              onClick={() => onDeleteClicked(data.row.original.id)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div className="list form-check-all">
      <DataTable
        columns={columns}
        isLoading={isLoading}
        data={
          EvaluationQueryData?.results
            ? EvaluationQueryData?.results?.data
            : EvaluationQueryData?.data || []
        }
        tableHeaders={tableHeaders}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (canPrevious) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (canNext) {
              setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default EvaluationCriteria;
