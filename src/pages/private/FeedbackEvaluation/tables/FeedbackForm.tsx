import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import { IFeedbackFormInput, useListFeedbackQuery } from 'redux/reducers/Feedback2';
import CustomDataTable from './CustomDataTable';

interface ITraineeDataTable {
  tableHeaders?: () => React.ReactNode;
  onEditClicked: (data: IFeedbackFormInput) => void;
  onDeleteClicked: (data: IFeedbackFormInput) => void;
  onViewClicked: (data: IFeedbackFormInput) => void;
}

const FeedbackForm: FC<ITraineeDataTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked,
  onViewClicked
}) => {
  const datas = [
    {
      data: 'Daily Session feedback'
    },
    {
      data: 'Module feedback'
    },
    {
      data: 'Lunch feedback'
    },
    {
      data: 'Training feedback'
    }
  ];

  const columns: any = [
    {
      Header: ' Feedbacks (English)',
      accessor: 'data'
    },

    {
      Header: '  Actions',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={require('./View.png')}
              alt="view"
              className="m-2"
              onClick={() => onViewClicked(data.row.original.data)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div className="list form-check-all">
      <CustomDataTable columns={columns} data={datas} tableHeaders={tableHeaders} />
    </div>
  );
};

export default FeedbackForm;
