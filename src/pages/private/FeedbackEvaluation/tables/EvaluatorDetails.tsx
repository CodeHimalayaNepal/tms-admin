import DataTable from 'components/ui/DataTable/data-table';
import React, { FC, useState } from 'react';
import {
  IInviteEvaluatorFormInput,
  useListInviteEvaluatorQuery
} from 'redux/reducers/FeedbackEvaluation';
import CustomDataTable from './CustomDataTable2';

interface ITraineeDataTable {
  tableHeaders?: () => React.ReactNode;
  onViewClicked: (data: IInviteEvaluatorFormInput) => void;
}

const EvaluatorDetails: FC<ITraineeDataTable> = ({ tableHeaders, onViewClicked }) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data: InviteQueryData } = useListInviteEvaluatorQuery({ page, pageSize });
  const canNext = InviteQueryData?.data?.next;
  const canPrevious = InviteQueryData?.data?.previous;

  const columns: any = [
    {
      Header: 'Criteria',
      accessor: 'questin'
    },
    {
      Header: 'Evaluator',
      accessor: 'question_p'
    }
  ];
  return (
    <div className="list form-check-all">
      <CustomDataTable
        columns={columns}
        data={InviteQueryData?.data?.results || []}
        tableHeaders={tableHeaders}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (canPrevious) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (canNext) {
              setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default EvaluatorDetails;
