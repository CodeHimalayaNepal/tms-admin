import CustomSelect from 'components/ui/Editor/CustomSelect';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { FC } from 'react';
import { useListParentDetailsQuery } from 'redux/reducers/evaluationCriteria';
import { IInviteEvaluatorFormInput } from 'redux/reducers/FeedbackEvaluation';

interface IEvaluationCriteriaForm {
  formik: FormikProps<IInviteEvaluatorFormInput>;
}

const InviteEvaluatorForm: FC<IEvaluationCriteriaForm> = ({ formik }) => {
  const { data: parentDetails } = useListParentDetailsQuery();
  const selectedEvaluationCriteria = parentDetails?.data?.map((item: any) => {
    return {
      value: item?.id?.toString(),
      label: item?.name
    };
  });

  return (
    <form>
      <div className="row">
        <CustomSelect
          options={selectedEvaluationCriteria}
          placeholder={'Search by module'}
          value={formik.values.module}
          onChange={(value: any) => formik.setFieldValue('parent', value)}
          label={'Module'}
        />
        <CustomSelect
          options={selectedEvaluationCriteria}
          placeholder={'Search by evaluator'}
          value={formik.values.evaluator}
          onChange={(value: any) => formik.setFieldValue('parent', value)}
          label={'Evaluator'}
        />
      </div>
    </form>
  );
};

export default InviteEvaluatorForm;
