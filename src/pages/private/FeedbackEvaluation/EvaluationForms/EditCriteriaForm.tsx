import CustomSelect from 'components/ui/Editor/CustomSelect';
import Select from 'react-select';
import CustomSelect2 from 'components/ui/Editor/CustomSelect2';
import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { FC, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useListParentDetailsQuery } from 'redux/reducers/evaluationCriteria';
import {
  IEvaluationCriteriaFormInput,
  useListEvalCriteriDetailsQuery
} from 'redux/reducers/FeedbackEvaluation';
import { useListInternalUsersByTypeQuery } from 'redux/reducers/internal-user';

interface IEvaluationCriteriaForm {
  formik: FormikProps<IEvaluationCriteriaFormInput>;
}

const EditCriteriaForm: FC<IEvaluationCriteriaForm> = ({ formik }) => {
  const evaluator1 = formik.values?.routine_evaluation?.map((item: any) => {
    return item;
  });
  const EvDetail =
    {
      value: formik.values?.evaluation_criteria_details?.evaluation_criteria_detail_id?.toString(),
      label: formik.values?.evaluation_criteria_details?.name
    } || {};

  const evaluator2 =
    formik.values?.routine_evaluation?.map((item: any) => {
      return {
        label:
          item.evaluator_details?.first_name +
          ' ' +
          item.evaluator_details?.last_name +
          ' ' +
          9864014315,
        value: item.evaluator_details?.id,
        id: item.id
      };
    }) || [];

  const { data: coordinators = [] } = useListInternalUsersByTypeQuery('coordinator');
  const coordinatorList = coordinators.map(
    (c: { id: string; first_name: string; last_name: string; mobile_number: string }) => ({
      label: c.first_name + ' ' + c.last_name + ' - ' + c.mobile_number,
      value: c.id.toString(),
      id: evaluator1?.find((element: any) => element?.evaluator_details?.id == c.id)?.id
    })
  );

  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(100);
  const { data: Details } = useListEvalCriteriDetailsQuery({ page, pageSize });
  const selectedEvaluationCriteriaDetails = Details?.data?.results.map((item: any) => {
    return {
      value: item?.evaluation_criteria_detail_id?.toString(),
      label: item?.name
    };
  });

  const parentCriteria = Details?.data?.results?.find((el: any, index: number) => {
    return el.evaluation_criteria_detail_id == formik.values.evaluation_criteria_detail;
  });
  const parentCriteriaOption = [
    {
      value: parentCriteria?.evaluation_criteria_detail?.evaluation_criteria_id,
      label: parentCriteria?.evaluation_criteria_detail?.name
    }
  ];
  const CriteriaOption = [
    {
      value: parentCriteria?.evaluation_criteria_detail?.parent_detail?.parent_id,
      label: parentCriteria?.evaluation_criteria_detail?.parent_detail?.name
    }
  ];

  const { id } = useParams();

  useEffect(() => {
    if (id) {
      formik.setFieldValue('routine', id);
    }
  }, [id]);

  return (
    <form>
      <div className="row">
        <CustomSelect
          options={selectedEvaluationCriteriaDetails}
          placeholder={'Select Details'}
          // value={EvDetail}
          value={formik.values?.evaluation_criteria_detail}
          onChange={(value: any) => {
            console.log(value);
            formik.setFieldValue('parentCriteria', null);
            formik.setFieldValue('evaluation_criteria_detail', value);
          }}
          // onChange={(value: any) => {
          //   const newValue = value.map((el: any, index: number) => {
          //     return {
          //       id: el.id,
          //       evaluator_details: {
          //         first_name: el.label.split(' ')[0],
          //         last_name: el.label.split(' ')[1],
          //         id: el.value
          //       }
          //     };
          //   });
          //   formik.setFieldValue('parentCriteria', null);
          //   formik.setFieldValue('evaluation_criteria_detail', newValue);
          // }}
          label={'Detail'}
        />

        <FormikValidationError
          name="evaluation_criteria_detail"
          errors={formik.errors}
          touched={formik.touched}
        />

        <CustomSelect
          options={parentCriteriaOption}
          placeholder={'Select Criteria'}
          value={formik.values.parentCriteria}
          onChange={(value: any) => formik.setFieldValue('parentCriteria', value)}
          label={' Criteria'}
        />
        <FormikValidationError
          name="parentCriteria"
          errors={formik.errors}
          touched={formik.touched}
        />

        <CustomSelect
          options={CriteriaOption}
          placeholder={'Select Sub Criteria'}
          value={formik.values.criteria}
          onChange={(value: any) => formik.setFieldValue('criteria', value)}
          label={' Sub Criteria'}
        />
        <FormikValidationError name="criteria" errors={formik.errors} touched={formik.touched} />

        <div className="mt-3 col-lg-4 col-md-6">
          <label htmlFor="fname-field" className="form-label">
            Evaluator
          </label>
          <Select
            options={coordinatorList}
            placeholder={'Select Details'}
            value={evaluator2}
            // value={[{ label: 'Sia KC - 9876542345', value: '3' }]}
            onChange={(value: any) => {
              const newValue = value.map((el: any, index: number) => {
                return {
                  id: el.id,
                  evaluator_details: {
                    first_name: el.label.split(' ')[0],
                    last_name: el.label.split(' ')[1],
                    id: el.value
                  }
                };
              });

              formik.setFieldValue('routine_evaluation', newValue);
            }}
            isMulti
          />
        </div>

        <FormikValidationError
          name="routine_evaluation"
          errors={formik.errors}
          touched={formik.touched}
        />
        <TextInput
          label="Marks"
          name="max_marks"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.max_marks}
          onBlur={formik.handleBlur}
          formik={formik}
        />
      </div>
    </form>
  );
};

export default EditCriteriaForm;
