import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import React, { useState } from 'react';
import { toast } from 'react-toastify';
import {
  useDeleteEvaluationCriteriaMutation,
  useCreateEvaluationCriteriaMutation,
  useUpdateEvaluationCriteriaMutation
} from 'redux/reducers/evaluationCriteria';
import { IFeedbackFormInput } from 'redux/reducers/Feedback2';
import {
  IEvaluationCriteriaFormInput,
  IInviteEvaluatorFormInput,
  useCreateInviteEvaluatorMutation
} from 'redux/reducers/FeedbackEvaluation';
import AddCriteriaForm from './EvaluationForms/AddCriteriaForm';
import InviteEvaluatorForm from './EvaluationForms/InviteEvaluatorForm';
import {
  EvaluationCriteriaInitialValues,
  EvaluationCriteriaSchemaValidations,
  InviteEvaluatorInitialValues,
  InviteEvaluatorSchemaValidations
} from './schema';
import EvaluationCriteria from './tables/EvaluationCriteria';
import EvaluatorDetails from './tables/EvaluatorDetails';
import FeedbackForm from './tables/FeedbackForm';
import { useNavigate, useParams } from 'react-router-dom';
import { json } from 'stream/consumers';

const FeedbackEvaluation = () => {
  const [feedbackModal, setFeedbackModal] = useToggle(false);
  const [evaluatorModal, setEvaluatorModal] = useToggle(false);
  const [addcriteriaModal, setAddCriteriaModal] = useToggle(false);

  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [feedbackInitialFormState, setFeedbackInitialForm] = useState<IEvaluationCriteriaFormInput>(
    EvaluationCriteriaInitialValues
  );

  const [initialEvaluatorFormState, setInitialEvaluatorForm] = useState<IInviteEvaluatorFormInput>(
    InviteEvaluatorInitialValues
  );

  const [createInviteEvaluator, { isLoading: isCreatingInviteEvaluator }] =
    useCreateInviteEvaluatorMutation();

  const [deleteFeedbackEvaluationCriteria, { isLoading: isDeletingEvaluationCriteria }] =
    useDeleteEvaluationCriteriaMutation();
  const [createFeedbackEvaluationCriteria, { isLoading: isCreatingEvaluationCriteria }] =
    useCreateEvaluationCriteriaMutation();
  const [updateFeedbackEvaluationCriteria, { isLoading: isUpdatingEvaluationCriteria }] =
    useUpdateEvaluationCriteriaMutation();

  const [deletingEvaluationState, setDeletingEvaluation] =
    useState<IEvaluationCriteriaFormInput | null>(null);
  const { id } = useParams();
  const navigate = useNavigate();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: InviteEvaluatorSchemaValidations,
    initialValues: initialEvaluatorFormState,

    onSubmit: (values, { resetForm }) => {
      alert('evaluation submitted');
    }
  });

  const addCriteriaformik = useFormik({
    enableReinitialize: true,
    validationSchema: EvaluationCriteriaSchemaValidations,
    initialValues: feedbackInitialFormState,

    onSubmit: (values, { resetForm }) => {
      let executeFunc = createFeedbackEvaluationCriteria;
      if (values.id) {
        executeFunc = updateFeedbackEvaluationCriteria;
      }
    }
  });
  const onDeleteConfirmEvaluationCriteria = () => {
    deletingEvaluationState &&
      deleteFeedbackEvaluationCriteria(deletingEvaluationState?.id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setFeedbackInitialForm(EvaluationCriteriaInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };
  const onFFEditClicked = (data: any) => {
    alert(data);
  };
  const onFFDeleteClicked = (data: any) => {
    setFeedbackInitialForm(data);
    toggleDeleteModal();
  };
  const onFFView = (data: any) => {
    var boolean;
    if (data === 'Daily Session feedback') {
      boolean = true;
    } else {
      boolean = false;
    }
    navigate(`/feedback-mgmt/routinefeedback/id=${id}/${boolean}`);
  };
  const onEditClicked = (data: IEvaluationCriteriaFormInput) => {
    alert('edit clicked');
  };
  const onDeleteClicked = (data: IEvaluationCriteriaFormInput) => {
    setDeletingEvaluation(data);
    toggleDeleteModal();
  };
  const onView = (data: any) => {
    alert('view clicked');
  };

  return (
    <Container title="">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirmEvaluationCriteria}
        title="Delete Confirmations"
        isLoading={isDeletingEvaluationCriteria}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>

      <div className="row ">
        <div className="col-lg-5" style={{ marginRight: '42px', marginLeft: '35px' }}>
          <div
            style={{
              color: '#1269B3',
              fontWeight: '500',
              fontSize: '16px',
              marginBottom: '30px',
              marginTop: '25px'
            }}
          >
            Feedback Form
          </div>
          <FeedbackForm
            onEditClicked={onFFEditClicked}
            onDeleteClicked={onFFDeleteClicked}
            onViewClicked={onFFView}
          />
        </div>
        <div className="col-lg-6">
          <div
            style={{ color: '#1269B3', fontWeight: '500', fontSize: '16px', marginBottom: '16px' }}
          >
            Evaluator Details
          </div>
          <EvaluatorDetails
            tableHeaders={() => (
              <>
                <PrimaryButton
                  title="Invite Evaluator"
                  iconName="ri-add-line align-bottom me-1"
                  onClick={() => {
                    setEvaluatorModal();
                    setInitialEvaluatorForm(InviteEvaluatorInitialValues);
                  }}
                  type="button"
                />
              </>
            )}
            onViewClicked={onView}
          />
        </div>
      </div>
      <hr style={{ marginTop: '20px', color: 'rgb(114 99 99' }}></hr>
      <div className="row mt-4">
        <div className="col-lg-12" style={{ paddingRight: '50px', paddingLeft: '50px' }}>
          <div
            style={{ color: '#1269B3', fontWeight: '500', fontSize: '24px', marginBottom: '16px' }}
          >
            Evaluation Criteria
          </div>
          <EvaluationCriteria
            tableHeaders={() => (
              <>
                <PrimaryButton
                  title="Add Criteria"
                  iconName="ri-add-line align-bottom me-1"
                  onClick={() => {
                    setAddCriteriaModal();
                    setFeedbackInitialForm(EvaluationCriteriaInitialValues);
                  }}
                  type="button"
                />
              </>
            )}
            onEditClicked={onEditClicked}
            onDeleteClicked={onDeleteClicked}
            onViewClicked={onView}
          />
        </div>
      </div>
      <form onSubmit={formik.handleSubmit}>
        <Modal
          title="Invite Evaluator"
          isModalOpen={evaluatorModal}
          closeModal={setEvaluatorModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <PrimaryButton title="Save" type="submit" isLoading={isCreatingInviteEvaluator} />

              <SecondaryButton
                onClick={setEvaluatorModal}
                title="Discard"
                isLoading={isCreatingInviteEvaluator}
              />
            </div>
          )}
        >
          <InviteEvaluatorForm formik={formik} />
        </Modal>
      </form>
      <form onSubmit={addCriteriaformik.handleSubmit}>
        <Modal
          title="Add Criteria"
          isModalOpen={addcriteriaModal}
          closeModal={setAddCriteriaModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <PrimaryButton
                title="Save"
                type="submit"
                isLoading={isUpdatingEvaluationCriteria || isCreatingEvaluationCriteria}
              />

              <SecondaryButton
                onClick={setAddCriteriaModal}
                title="Discard"
                isLoading={isUpdatingEvaluationCriteria || isCreatingEvaluationCriteria}
              />
            </div>
          )}
        >
          <AddCriteriaForm formik={addCriteriaformik} />
        </Modal>
      </form>
    </Container>
  );
};

export default FeedbackEvaluation;
