import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import {
  useCreateEvaluationCriteriaMutation,
  useUpdateEvaluationCriteriaMutation
} from 'redux/reducers/FeedbackEvaluation';
import { useDeleteEvaluationCriteriaMutation } from 'redux/reducers/FeedbackEvaluation';
import { IFeedbackFormInput } from 'redux/reducers/Feedback2';
import {
  IEvaluationCriteriaFormInput,
  IInviteEvaluatorFormInput,
  useCreateInviteEvaluatorMutation
} from 'redux/reducers/FeedbackEvaluation';
import AddCriteriaForm from './EvaluationForms/AddCriteriaForm';
import InviteEvaluatorForm from './EvaluationForms/InviteEvaluatorForm';
import {
  EvaluationCriteriaInitialValues,
  EvaluationCriteriaSchemaValidations,
  InviteEvaluatorInitialValues,
  InviteEvaluatorSchemaValidations
} from './schema';
import EvaluationCriteria from './tables/EvaluationCriteria';
import EvaluatorDetails from './tables/EvaluatorDetails';
import FeedbackForm from './tables/FeedbackForm';
import { useNavigate, useParams } from 'react-router-dom';
import { json } from 'stream/consumers';
import EditCriteriaForm from './EvaluationForms/EditCriteriaForm';

const FeedbackEvaluation = () => {
  const [feedbackModal, setFeedbackModal] = useToggle(false);
  const [evaluatorModal, setEvaluatorModal] = useToggle(false);
  const [addcriteriaModal, setAddCriteriaModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [feedbackInitialFormState, setFeedbackInitialForm] = useState<IEvaluationCriteriaFormInput>(
    EvaluationCriteriaInitialValues
  );
  const [initialEvaluatorFormState, setInitialEvaluatorForm] = useState<IInviteEvaluatorFormInput>(
    InviteEvaluatorInitialValues
  );

  const [createInviteEvaluator, { isLoading: isCreatingInviteEvaluator }] =
    useCreateInviteEvaluatorMutation();

  const [deleteFeedbackEvaluationCriteria, { isLoading: isDeletingEvaluationCriteria }] =
    useDeleteEvaluationCriteriaMutation();
  const [createFeedbackEvaluationCriteria, { isLoading: isCreatingEvaluationCriteria }] =
    useCreateEvaluationCriteriaMutation();
  const [updateFeedbackEvaluationCriteria, { isLoading: isUpdatingEvaluationCriteria }] =
    useUpdateEvaluationCriteriaMutation();

  const [deletingEvaluationState, setDeletingEvaluation] =
    useState<IEvaluationCriteriaFormInput | null>(null);
  const { id } = useParams();
  const navigate = useNavigate();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: EvaluationCriteriaSchemaValidations,
    initialValues: feedbackInitialFormState,

    onSubmit: (values, { resetForm }) => {
      let executeFunc = updateFeedbackEvaluationCriteria;

      let b = values.routine_evaluation.map((evaluator: any) => {
        return {
          id: evaluator.id === undefined ? null : evaluator.id,
          evaluator: evaluator.evaluator_details.id.toString()
        };
      });

      const newValues = { ...values, routine_evaluation: [...b] };
      const payload = {
        id: newValues.id,
        evaluation_criteria_detail: newValues.evaluation_criteria_detail,
        routine: newValues.routine,
        max_marks: newValues.max_marks,
        routine_evaluation: newValues.routine_evaluation
      };

      executeFunc(payload)
        .unwrap()
        .then((data: any) => {
          toast.success('Updated Successfully');
          resetForm();
          setFeedbackInitialForm(EvaluationCriteriaInitialValues);
          setFeedbackModal();
        })
        .catch((err: any) => {
          setFeedbackInitialForm(EvaluationCriteriaInitialValues);
          resetForm();
          // toast.error(err.data.errors.question);
          // toast.error(err.data.errors.question_np);
        });
      setEvaluatorModal();
      resetForm();
    }
  });

  const addCriteriaformik = useFormik({
    enableReinitialize: true,
    validationSchema: EvaluationCriteriaSchemaValidations,
    initialValues: feedbackInitialFormState,

    onSubmit: (values, { resetForm }) => {
      let executeFunc = createFeedbackEvaluationCriteria;

      let b = values.routine_evaluation.map((evaluator: any) => {
        return { evaluator: evaluator.value };
      });

      const newValues = { ...values, routine_evaluation: [...b] };

      const payload = {
        evaluation_criteria_detail: newValues.evaluation_criteria_detail,
        routine: newValues.routine,
        max_marks: newValues.max_marks,
        routine_evaluation: newValues.routine_evaluation
      };

      executeFunc(payload)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          resetForm();
          setFeedbackInitialForm(EvaluationCriteriaInitialValues);
          setFeedbackModal();
        })
        .catch((err: any) => {
          setFeedbackInitialForm(EvaluationCriteriaInitialValues);
          resetForm();
          // toast.error(err.data.errors.question);
          // toast.error(err.data.errors.question_np);
        });
      setAddCriteriaModal();
      resetForm();
    }
  });
  const onDeleteConfirmEvaluationCriteria = () => {
    deletingEvaluationState &&
      deleteFeedbackEvaluationCriteria(deletingEvaluationState)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setFeedbackInitialForm(EvaluationCriteriaInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };
  const onFFEditClicked = (data: any) => {
    alert(data);
  };
  const onFFDeleteClicked = (data: any) => {
    setFeedbackInitialForm(data);
    toggleDeleteModal();
  };
  const onFFView = (data: any) => {
    var tabValue;
    if (data === 'Daily Session feedback') {
      tabValue = 1;
    } else if (data === 'Module feedback') {
      tabValue = 0;
    } else if (data === 'Lunch feedback') {
      tabValue = 2;
    } else {
      tabValue = 3;
    }
    navigate(`/feedback-mgmt/routinefeedback/${id}/tab=${tabValue}`);
  };
  const onEditClicked = (data: any) => {
    setEvaluatorModal();

    // debugger;

    //   eval.map(c=> {
    //     return {
    //         label: c.evaluator_details.first + ' ' + c.evaluator_details.last_name,
    //         value: c.evaluator_details.id
    // }
    // });
    setFeedbackInitialForm(data);
    // addCriteriaformik.setFieldValue(
    //   'evaluation_criteria_detail',
    //   data.evaluation_criteria_detail.id
    // );
  };
  const onDeleteClicked = (data: IEvaluationCriteriaFormInput) => {
    setDeletingEvaluation(data);
    toggleDeleteModal();
  };
  const onView = (data: any) => {
    alert('view clicked');
  };

  return (
    <Container title="">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirmEvaluationCriteria}
        title="Delete Confirmations"
        isLoading={isDeletingEvaluationCriteria}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>

      <div className="row ">
        <div className="col-lg-12" style={{ paddingRight: '50px', paddingLeft: '50px' }}>
          <div
            style={{
              color: '#1269B3',
              fontWeight: '500',
              fontSize: '16px',
              marginBottom: '10px',
              marginTop: '25px'
            }}
          >
            Feedback Form
          </div>
          <FeedbackForm
            onEditClicked={onFFEditClicked}
            onDeleteClicked={onFFDeleteClicked}
            onViewClicked={onFFView}
          />
        </div>
        {/* <div className="col-lg-6">
          <div
            style={{
              color: '#1269B3',
              fontWeight: '500',
              fontSize: '16px',
              marginBottom: '10px',
              marginTop: '25px'
            }}
          >
            Evaluator Details
          </div>
          <EvaluatorDetails onViewClicked={onView} />
        </div> */}
      </div>
      <hr style={{ marginTop: '20px', color: 'rgb(114 99 99' }}></hr>
      <div className="row mt-4">
        <div className="col-lg-12" style={{ paddingRight: '50px', paddingLeft: '50px' }}>
          <div
            style={{ color: '#1269B3', fontWeight: '500', fontSize: '24px', marginBottom: '16px' }}
          >
            Evaluation Criteria
          </div>
          <EvaluationCriteria
            tableHeaders={() => (
              <>
                <PrimaryButton
                  title="Add Criteria"
                  iconName="ri-add-line align-bottom me-1"
                  onClick={() => {
                    setAddCriteriaModal();
                    setFeedbackInitialForm(EvaluationCriteriaInitialValues);
                  }}
                  type="button"
                />
              </>
            )}
            onEditClicked={onEditClicked}
            onDeleteClicked={onDeleteClicked}
            onViewClicked={onView}
          />
        </div>
      </div>
      <form onSubmit={formik.handleSubmit}>
        <Modal
          title="Edit Criteria"
          isModalOpen={evaluatorModal}
          closeModal={setEvaluatorModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <PrimaryButton
                title="Update"
                type="submit"
                isLoading={isUpdatingEvaluationCriteria}
              />

              <SecondaryButton
                onClick={setEvaluatorModal}
                title="Discard"
                isLoading={isUpdatingEvaluationCriteria}
              />
            </div>
          )}
        >
          <EditCriteriaForm formik={formik} />
        </Modal>
      </form>
      <form onSubmit={addCriteriaformik.handleSubmit}>
        <Modal
          title="Add Criteria"
          isModalOpen={addcriteriaModal}
          closeModal={setAddCriteriaModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <PrimaryButton
                title="Save"
                type="submit"
                isLoading={isUpdatingEvaluationCriteria || isCreatingEvaluationCriteria}
              />

              <SecondaryButton
                onClick={setAddCriteriaModal}
                title="Discard"
                isLoading={isUpdatingEvaluationCriteria || isCreatingEvaluationCriteria}
              />
            </div>
          )}
        >
          <AddCriteriaForm formik={addCriteriaformik} />
        </Modal>
      </form>
    </Container>
  );
};

export default FeedbackEvaluation;
