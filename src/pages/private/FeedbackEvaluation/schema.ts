import {
  IEvaluationCriteriaFormInput,
  IInviteEvaluatorFormInput
} from 'redux/reducers/FeedbackEvaluation';
import * as Yup from 'yup';

export const EvaluationCriteriaInitialValues: IEvaluationCriteriaFormInput = {
  evaluation_criteria_detail: 0,
  evaluation_criteria_details: {
    evaluation_criteria_detail_id: 0,
    name: ''
  },
  criteria: '',
  parentCriteria: '',
  max_marks: 0,
  routine: 0,
  // routine_evaluation: {}
  routine_evaluation: []

  //   routine_evaluation: [{
  //     id: null,
  //     evaluator:""
  // }]
};

export const InviteEvaluatorInitialValues: IInviteEvaluatorFormInput = {
  module: '',
  evaluator: ''
};

export const EvaluationCriteriaSchemaValidations = Yup.object().shape({
  evaluation_criteria_detail: Yup.number().required('Detail is required'),
  max_marks: Yup.number().min(0, 'Too Short!').max(100, 'Too Long!').required('Marks is required'),
  routine_evaluation: Yup.array().min(1, 'Evaluator is required')
  // routine_evaluation: Yup.object().shape({
  //   label: Yup.string().required(),
  //   value: Yup.string().required(),

  // })
});

export const InviteEvaluatorSchemaValidations = Yup.object().shape({
  module: Yup.object().shape({
    label: Yup.string().required('Required'),
    value: Yup.string().required('Required')
  })
});
