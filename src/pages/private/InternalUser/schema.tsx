import validations from 'utils/validation';
import * as Yup from 'yup';
export interface IInternalUserInput {
  id?: string;
  first_name: string;
  middle_name: string;
  last_name: string;
  email: string;
  mobile_number: string;
  role: number;
  // admin_role?: string;
  user_type?: string;
}

export const internalUserInitialValues: IInternalUserInput = {
  first_name: '',
  middle_name: '',
  last_name: '',
  email: '',
  mobile_number: '',
  role: 0
};

export const internalUserSchemaValidations = Yup.object().shape({
  first_name: Yup.string().required('First Name is required'),
  middle_name: Yup.string().nullable(),
  last_name: Yup.string().required('Last Name is required'),
  email: Yup.string().email('Invalid email').required('Email is required'),
  mobile_number: Yup.string()
    .matches(validations.phone.regex, validations.phone.message)
    .required('Mobile Number is required'),
  role: Yup.string().required('Role is required')
});
