import Container from 'components/ui/Container';
import { useFormik } from 'formik';
import React, { useState } from 'react';
import InternalForm from './InternalUserGeneralDetails';
import { IGeneral } from './Schema';
import InternalUserEducationDetails from './InternalUserEducationDetails';
import { RiH1 } from 'react-icons/ri';
import InteralUserExperience from './InternalUserExperienceDetails';
import * as Yup from 'yup';
import { initialValue, SignUpValidationSchema } from '../../Profile/schema';

import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import ChangePassword from './changePassword';
import { useChangePasswordMutation } from 'redux/reducers/auth';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';

const InternalUsersManagement = () => {
  const [tabValue, setTabValue] = useState<string>('1');
  const [changeState, setchangeState] = useState<string>('profile');
  const navigate = useNavigate();

  const [changethisPassword, isLoading] = useChangePasswordMutation();
  const passwordFormik = useFormik({
    validationSchema: Yup.object().shape({
      ...SignUpValidationSchema
    }),
    enableReinitialize: true,
    initialValues: initialValue,
    onSubmit: (values) => {
      let executeFunc = changethisPassword;
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          navigate('/');
          toast.success('Password Edited Successfullly');
        })
        .catch((err: any) => {
          if (err.data?.errors) {
            toast.error(Object.values(err.data.errors).join(', '));
            return;
          }
        });
    }
  });
  const formik = useFormik<IGeneral>({
    initialValues: {
      first_name: '',
      middle_name: '',
      last_name: '',
      first_name_np: '',
      middle_name_np: '',
      last_name_np: '',
      dob_AD: '',
      gender: '',
      marital_status: '',
      blood_group: '',
      religion: '',
      email: '',
      mobile_number: '',
      passport_num: '',
      citizenship_num: '',
      national_id: '',
      pan_number: '',
      father_name: '',
      mother_name: '',
      grandfather_name: '',
      grandmother_name: '',
      temporary_address: {
        province: '',
        district: '',
        ward_no: '',
        tole_street: ''
      },
      permanent_address: {
        province: '',
        district: '',
        ward_no: '',
        tole_street: ''
      },

      image: null as File | null
    },
    onSubmit: () => {}
  });
  return (
    <Container title=" Internal Users">
      <div style={{ flexBasis: '1' }}>
        {/* <div style={{ display: 'flex', gap: '1rem', flexBasis: '1' }}> */}

        <div className="row">
          <div className="col-xxl-2 col-md-12 mb-2">
            <div className="d-flex  flex-column">
              <div
                className="d-flex justify-content-start p-2"
                onClick={() => setchangeState('profile')}
              >
                <i className="ri-pencil-line align-bottom"></i>
                <span className="me-1" style={{ cursor: 'pointer' }}>
                  Profile
                </span>
              </div>
              <hr />
              <div
                className="d-flex justify-content- p-2"
                onClick={() => setchangeState('password')}
              >
                <i className="ri-lock-line align-bottom"></i>
                <span style={{ cursor: 'pointer' }}>Password And Security</span>
              </div>
            </div>
          </div>
          {changeState == 'profile' && (
            <div style={{ width: '100%' }}>
              <div className="col-xxl-10 col-md-12">
                <div className="profile-details-heading mb-2">Profile</div>
                <div className="col-xxl-10">
                  <div className="d-flex ">
                    <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                      <li
                        className="nav-item"
                        onClick={() => {
                          setTabValue('1');
                        }}
                      >
                        <a
                          className={`nav-link fs-14 ${tabValue == '1' ? 'active' : ''}`}
                          data-bs-toggle="tab"
                          href="#General-tab"
                          role="tab"
                        >
                          <i className="ri-airplay-fill d-inline-block d-md-none"></i>{' '}
                          <span className="">General</span>
                        </a>
                      </li>

                      <li
                        className="nav-item"
                        onClick={() => {
                          setTabValue('2');
                        }}
                      >
                        <a
                          className={`nav-link fs-14 ${tabValue == '2' ? 'active' : ''}`}
                          data-bs-toggle="tab"
                          href="#EducationTab"
                          role="tab"
                        >
                          <i className="ri-price-tag-line d-inline-block d-md-none"></i>{' '}
                          <span className="d-none d-md-inline-block">Education</span>
                        </a>
                      </li>
                      <li
                        className="nav-item"
                        onClick={() => {
                          setTabValue('3');
                        }}
                      >
                        <a
                          className={`nav-link fs-14 ${tabValue == '3' ? 'active' : ''}`}
                          data-bs-toggle="tab"
                          href="#Experiencetab"
                          role="tab"
                        >
                          <i className="ri-price-tag-line d-inline-block d-md-none"></i>{' '}
                          <span className="d-none d-md-inline-block">Experience</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              {tabValue == '1' && (
                <InternalForm formik={formik} status={tabValue} setTabValue={setTabValue} />
              )}
              {tabValue === '2' && (
                <InternalUserEducationDetails status={tabValue} setTabValue={setTabValue} />
              )}
              {tabValue === '3' && <InteralUserExperience status={tabValue} />}
            </div>
          )}

          {changeState == 'password' && (
            <div className="col-xxl-10">
              <form onSubmit={passwordFormik.handleSubmit}>
                <ChangePassword formik={passwordFormik} />
                <div className="row">
                  <div className="col-xxl-4 my-4">
                    <div className="hstack gap-2 justify-content-end">
                      <PrimaryButton
                        title="Save"
                        iconName="ri-add-line align-bottom me-1"
                        type="submit"
                        // isLoading={creatingCourse || updatingCourse}
                      />
                      <SecondaryButton
                        onClick={() => {
                          // navigate('/modules');
                        }}
                        title="Discard"
                        // isLoading={creatingCourse || updatingCourse}
                      />
                    </div>
                  </div>
                </div>
              </form>
            </div>
          )}
        </div>
      </div>
    </Container>
  );
};

export default InternalUsersManagement;
