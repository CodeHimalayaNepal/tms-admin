import { Avatar } from 'common/constant';
import CustomSelect from 'components/ui/Editor/CustomSelect';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import TextInput from 'components/ui/TextInput';
import { FormikProps, useFormik } from 'formik';
import React, { FC, useEffect, useMemo, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useListDistrictByProvinceQuery, useListProvinceQuery } from 'redux/reducers/geography';
import {
  useListInternalUserDetailsQuery,
  useUpdateInternalUserGeneralMutation
} from 'redux/reducers/internal-user';
import { convertToNepali } from 'utils/nepali-convert';

import { IGeneral, profileInitialValues, profileValidationSchema } from './Schema';
interface IGeneralForm {
  formik: FormikProps<IGeneral>;
  status: string;
  setTabValue: (value: any) => void;
}
const getImage = (image: File) => {
  try {
    return URL.createObjectURL(image);
  } catch (error) {
    return '';
  }
};

const InternalForm: FC<IGeneralForm> = ({ status, setTabValue }) => {
  const navigate = useNavigate();
  const { id } = useParams<{ id: string }>();
  const isCreate = id === 'create';
  const { data: profileDetail, isLoading: isFetchingprofileDetail } =
    useListInternalUserDetailsQuery(id ? id : '', { skip: isCreate || !id });
  const [updateProfile, { data, isLoading: updatingProfile }] =
    useUpdateInternalUserGeneralMutation();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: profileInitialValues,
    onSubmit: (values) => {
      const temporary_address = JSON.stringify(values.temporary_address);
      const permanent_address = duplicateState
        ? JSON.stringify(values.temporary_address)
        : JSON.stringify(values.permanent_address);
      let executeFunc = updateProfile;
      if (typeof values.image === 'string') {
        const { image, ...rest } = values;
        executeFunc({
          id: profileDetail.id,
          ...rest,
          temporary_address,
          permanent_address
        })
          .unwrap()
          .then((data: any) => {
            toast.success('Profile Updated Successfully');
            setTabValue('2');
          })
          .catch((err: any) => {
            console.log(err);
            toast.error(err.data?.image ? err.data?.image[0] : null);
          });
      } else {
        executeFunc({
          id: profileDetail.id,
          ...values,
          temporary_address,
          permanent_address
        })
          .unwrap()
          .then((data: any) => {
            toast.success('Profile Updated Successfully');
            setTabValue('2');
          })
          .catch((err: any) => {
            console.log(err);
            toast.error(err.data?.image ? err.data?.image[0] : null);
          });
      }
    }
  });

  const [provinceId, setProvince] = useState('');
  const [page, setPage] = useState(1);

  const { data: provinceList = [] } = useListProvinceQuery({});
  const provinces = useMemo(() => {
    return provinceList?.map((item: any) => {
      return {
        value: item.id.toString(),
        label: item.name_en
      };
    });
  }, [provinceList]);
  const { data: TemporarydistrictData } = useListDistrictByProvinceQuery(
    { provinceId: formik.values.temporary_address.province, page },
    { skip: !formik.values.temporary_address.province }
  );
  const { data: permanentdistrictData } = useListDistrictByProvinceQuery(
    { provinceId: formik.values.permanent_address.province, page },
    { skip: !formik.values.permanent_address.province }
  );

  const temporarydistricts = useMemo(
    () =>
      TemporarydistrictData?.results?.map((item: any) => {
        return {
          value: item.id.toString(),
          label: item.name_en
        };
      }),
    [TemporarydistrictData?.results]
  );
  const permanentdistricts = useMemo(
    () =>
      permanentdistrictData?.results?.map((item: any) => {
        return {
          value: item.id.toString(),
          label: item.name_en
        };
      }),
    [permanentdistrictData?.results]
  );
  const option1 = [
    { value: '1', label: 'Male' },
    { value: '2', label: 'Female' }
  ];
  const option2 = [
    { value: '1', label: 'Single' },
    { value: '2', label: 'Married' },
    { value: '3', label: 'Divorced' },
    { value: '4', label: 'Separated' },
    { value: '5', label: 'Widowed' }
  ];
  const option3 = [
    { value: 'A+', label: 'A+' },
    { value: 'A-', label: 'A-' },
    { value: 'AB+', label: 'AB+' },
    { value: 'AB-', label: 'AB-' },
    { value: 'B+', label: 'B+' },
    { value: 'B-', label: 'B-' },
    { value: 'O+', label: 'O+' },
    { value: 'O-', label: 'O-' }
  ];

  useEffect(() => {
    if (profileDetail) {
      formik.setValues({
        first_name: formik.values.first_name || profileDetail.first_name || null,
        last_name: formik.values.last_name || profileDetail.last_name || null,
        email: formik.values.email || profileDetail.email || null,
        middle_name_np: formik.values.middle_name_np || profileDetail.middle_name_np || null,

        first_name_np: formik.values.first_name_np || profileDetail.first_name_np || null,
        middle_name: formik.values.middle_name || profileDetail.middle_name || null,
        last_name_np: formik.values.last_name_np || profileDetail.last_name_np || null,
        citizenship_num: formik.values.citizenship_num || profileDetail.citizenship_num || null,
        passport_num: formik.values.passport_num || profileDetail.passport_num || null,
        national_id: formik.values.national_id || profileDetail.national_id || null,
        blood_group: formik.values.blood_group || profileDetail.blood_group || null,
        religion: formik.values.religion || profileDetail.religion || null,

        dob_AD: formik.values.dob_AD || profileDetail.dob_AD || null,
        father_name: formik.values.father_name || profileDetail.father_name || null,
        mother_name: formik.values.mother_name || profileDetail.mother_name || null,
        grandfather_name: formik.values.grandfather_name || profileDetail.grandfather_name || null,
        grandmother_name: formik.values.grandmother_name || profileDetail.grandmother_name || null,
        image: profileDetail.image || (null as unknown as File),
        pan_number: formik.values.pan_number || profileDetail.pan_number || null,
        gender: formik.values.gender || profileDetail.gender || null,
        marital_status: formik.values.marital_status || profileDetail.marital_status || null,
        temporary_address: {
          province:
            (formik.values.temporary_address.province ||
              provinces?.find((p) => p.label === profileDetail.temporary_address?.province?.name_en)
                ?.value) ??
            null,

          tole_street:
            formik.values.temporary_address.tole_street ||
            profileDetail.temporary_address?.tole_street,
          ward_no:
            formik.values.temporary_address.ward_no || profileDetail.temporary_address?.ward_no,
          district:
            (formik.values.temporary_address.district ||
              temporarydistricts?.find(
                (d) => d.label === profileDetail.temporary_address?.district?.name_en
              )?.value) ??
            null
        },
        permanent_address: {
          province:
            (formik.values.permanent_address.province ||
              provinces?.find((p) => p.label === profileDetail.permanent_address?.province?.name_en)
                ?.value) ??
            null,
          tole_street:
            formik.values.permanent_address.tole_street ||
            profileDetail.permanent_address?.tole_street,
          ward_no:
            formik.values.permanent_address.ward_no || profileDetail.permanent_address?.ward_no,
          district:
            (formik.values.permanent_address.district ||
              permanentdistricts?.find(
                (d) => d.label === profileDetail.permanent_address?.district?.name_en
              )?.value) ??
            null
        },

        mobile_number: profileDetail.mobile_number || ''
      });
    }
  }, [profileDetail, provinces, temporarydistricts, permanentdistricts]);

  const [duplicateState, setDuplicateState] = useState(false);
  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <div className="card mt-1 px-3">
          <div className="card-body p-4">
            <div className="text-center">
              <div className="profile-user position-relative d-inline-block mx-auto  mb-4">
                <img
                  src={
                    formik.values.image
                      ? typeof formik.values.image === 'string'
                        ? formik.values.image
                        : getImage(formik.values.image)
                      : Avatar
                  }
                  className="rounded-circle avatar-xl img-thumbnail user-profile-image"
                  alt="user-profile-image"
                />
                <div className="avatar-xs p-0 rounded-circle profile-photo-edit">
                  <input
                    id="profile-img-file-input"
                    type="file"
                    className="profile-img-file-input"
                    name="image"
                    onChange={(e) => {
                      formik.handleChange({
                        target: { name: 'image', value: e.target.files?.[0] ?? null }
                      });
                    }}
                  />
                  <label htmlFor="profile-img-file-input" className="profile-photo-edit avatar-xs">
                    <span className="avatar-title rounded-circle bg-blue text-body">
                      <i className="ri-pencil-fill" />
                    </span>
                  </label>
                </div>
              </div>

              <h5 className="fs-14 mb-1">Profile Image</h5>

              {formik.touched.image && formik.errors.image && (
                <span className="text-danger">{formik.errors.image}</span>
              )}
            </div>
          </div>
          <span className="profile-details-heading">Personal Details</span>
          <div className="row">
            <TextInput
              label="First Name"
              name="first_name"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.first_name}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="Middle Name"
              name="middle_name"
              type="text"
              onChange={formik.handleChange}
              disableRequiredValidation={true}
              value={formik.values.middle_name}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="Last Name"
              name="last_name"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.last_name}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="First Name(in Devnagari)"
              name="first_name_np"
              type="text"
              onChange={(e) => {
                e.target.value = convertToNepali(e.target.value);
                formik.handleChange(e);
              }}
              value={formik.values.first_name_np}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="Middle Name(in Devnagari)"
              name="middle_name_np"
              type="text"
              onChange={(e) => {
                e.target.value = convertToNepali(e.target.value);
                formik.handleChange(e);
              }}
              disableRequiredValidation={true}
              value={formik.values.middle_name_np}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="Last Name(in Devnagari)"
              name="last_name_np"
              type="text"
              onChange={(e) => {
                e.target.value = convertToNepali(e.target.value);
                formik.handleChange(e);
              }}
              value={formik.values.last_name_np}
              onBlur={formik.handleBlur}
              formik={formik}
            />

            <TextInput
              label="Date-of-Birth(AD)"
              name="dob_AD"
              type="date"
              onChange={formik.handleChange}
              value={formik.values.dob_AD}
              onBlur={formik.handleBlur}
              formik={formik}
            />

            <TextInput
              label="Email"
              name="email"
              type="email"
              onChange={formik.handleChange}
              value={formik.values.email}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="Mobile Number"
              name="mobile_number"
              type="number"
              onChange={formik.handleChange}
              value={formik.values.mobile_number}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <CustomSelect
              options={option3}
              placeholder={'Search by blood group'}
              value={formik.values.blood_group}
              onChange={(value: any) => formik.setFieldValue('blood_group', value)}
              // isMulti={true}
              label={'Blood Group'}
            />
            <TextInput
              label="Religion"
              name="religion"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.religion}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="PAN Number"
              name="pan_number"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.pan_number}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="Passport Number"
              name="passport_num"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.passport_num}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="Citizenship Number"
              name="citizenship_num"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.citizenship_num}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <CustomSelect
              options={option1}
              placeholder={'Search by gender'}
              value={formik.values.gender}
              onChange={(value: any) => {
                formik.setFieldValue('gender', value);
              }}
              // isMulti={true}
              label={'Gender'}
            />
            <CustomSelect
              options={option2}
              placeholder={'Search by marital status'}
              value={formik.values.marital_status}
              onChange={(value: any) => formik.setFieldValue('marital_status', value)}
              // isMulti={true}
              label={'Martial Status'}
            />
          </div>
          <div className="mt-5">
            <span className="profile-details-heading">Family Details</span>
          </div>
          <div className="row">
            <TextInput
              label="Father Name"
              name="father_name"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.father_name}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="Mother Name"
              name="mother_name"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.mother_name}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="GrandFather Name"
              name="grandfather_name"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.grandfather_name}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="GrandMother Name"
              name="grandmother_name"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.grandmother_name}
              onBlur={formik.handleBlur}
              formik={formik}
            />
          </div>

          <span className="profile-details-heading mt-5">Temporary Address</span>
          <div className="row">
            <CustomSelect
              options={provinces}
              placeholder={'Search by Province'}
              value={formik.values.temporary_address.province}
              onChange={(value: any) => {
                formik.setFieldValue('temporary_address.province', value);
              }}
              // isMulti={true}
              label={'Province'}
            />
            <CustomSelect
              options={temporarydistricts}
              placeholder={'Search by district'}
              value={formik.values.temporary_address.district}
              onChange={(value: any) => formik.setFieldValue('temporary_address.district', value)}
              // isMulti={true}
              label={'District'}
            />

            <TextInput
              label="Tole Street"
              name="temporary_address.tole_street"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.temporary_address.tole_street}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="Ward Number"
              name="temporary_address.ward_no"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.temporary_address.ward_no}
              onBlur={formik.handleBlur}
              formik={formik}
            />
          </div>

          <div className="row" style={{ display: 'flex', flexDirection: 'row' }}>
            <div className="col">
              <div className="profile-details-heading mt-5">Permananent Address</div>
            </div>
            <div
              className="form-check col mt-5"
              onChange={() => setDuplicateState(!duplicateState)}
              style={{ display: 'flex', justifyContent: 'end' }}
            >
              <input
                className="form-check-input"
                style={{ marginRight: '4px' }}
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label className="form-check-label mb-3">Same as Temporary Address</label>
            </div>
          </div>
          <div className="row mb-3">
            <CustomSelect
              options={provinces}
              placeholder={'Search by Province'}
              value={
                duplicateState
                  ? formik.values.temporary_address.province
                  : formik.values.permanent_address.province
              }
              onChange={(value: any) => {
                duplicateState
                  ? formik.setFieldValue('temporary_address.province', value)
                  : formik.setFieldValue('permanent_address.province', value);
              }}
              label={'Province'}
            />
            <CustomSelect
              options={duplicateState ? temporarydistricts : permanentdistricts}
              placeholder={'Search by District'}
              value={
                duplicateState
                  ? formik.values.temporary_address.district
                  : formik.values.permanent_address.district
              }
              onChange={(value: any) => formik.setFieldValue('permanent_address.district', value)}
              label={'District'}
            />

            <TextInput
              label="Tole Street"
              name="permanent_address.tole_street"
              type="text"
              onChange={formik.handleChange}
              value={
                duplicateState
                  ? formik.values.temporary_address.tole_street
                  : formik.values.permanent_address.tole_street
              }
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <TextInput
              label="Ward Number"
              name="permanent_address.ward_no"
              type="text"
              onChange={formik.handleChange}
              value={
                duplicateState
                  ? formik.values.temporary_address.ward_no
                  : formik.values.permanent_address.ward_no
              }
              onBlur={formik.handleBlur}
              formik={formik}
            />
          </div>
          <div className="hstack gap-2 justify-content-end" style={{ marginBottom: '20px' }}>
            <PrimaryButton title="Save" iconName="ri-add-line align-bottom me-1" type="submit" />
            <SecondaryButton
              onClick={() => {
                navigate('/internal-user-management');
              }}
              title="Discard"
            />
          </div>
        </div>
      </form>
    </>
  );
};

export default InternalForm;
