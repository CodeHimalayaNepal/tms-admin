import CustomSelect from 'components/ui/Editor/CustomSelect';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import TextInput from 'components/ui/TextInput';
import { useFormik } from 'formik';

import { useEffect, useMemo } from 'react';
import { FC, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  useListInternalUserDetailsQuery,
  useUpdateInternalUserEducationMutation,
  useUpdateInternalUserExperienceMutation
} from 'redux/reducers/internal-user';
import { useListOrganizationDepartmentQuery } from 'redux/reducers/organization-department';
import { useListOrganizationCentreQuery } from 'redux/reducers/organizations-centre';

const genId = (len = 5) => {
  return parseInt(
    new Array(len)
      .fill(null)
      .map((_) => Math.round(Math.random() * 9))
      .join('')
  );
};
interface IExperienceDetails {
  status: string;
}

const InteralUserExperience: FC<IExperienceDetails> = ({ status }) => {
  const inputList = [
    {
      department: '',
      center: '',
      date_from: '',
      date_to: '',
      start_position: '',
      promoted_position: ''
    }
  ];
  const { id } = useParams<{ id: string }>();
  const isCreate = id === 'create';
  const navigate = useNavigate();
  const { data: ExperienceDetail, isLoading: isFetchingExperienceDetail } =
    useListInternalUserDetailsQuery(id ? id : '', { skip: isCreate || !id });
  const experienceIntitalValue = ExperienceDetail.inhouse_experience ?? [];

  const [updateExperience, { data, isLoading: updatingEducation }] =
    useUpdateInternalUserExperienceMutation();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: { inhouse_experience: [...experienceIntitalValue] },
    onSubmit: (values) => {
      try {
        const valuesExtracted = Object.values(values).flat();
        const result = updateExperience({
          id: ExperienceDetail.id,
          inhouse_experience: valuesExtracted
        }).unwrap();
        if (result) {
          toast.success(' Experience Profile Updated Successfully');
          navigate('/internal-user-management');
        }
      } catch (err: any) {
        console.log(err);
      }
    }
  });
  const { data: organizationcenter } = useListOrganizationCentreQuery();

  const { data: organizationdepartment } = useListOrganizationDepartmentQuery();

  const centercategory = useMemo(
    () =>
      organizationcenter?.data?.map((item: any) => {
        return {
          value: item.tms_center_id.toString(),
          label: item.name
        };
      }),
    [organizationcenter]
  );

  const departmentcategory = useMemo(
    () =>
      organizationdepartment?.data?.map((item: any) => {
        return {
          value: item.tms_department_id.toString(),
          label: item.name
        };
      }),
    [organizationdepartment]
  );

  // handle click event of the Remove button
  const handleRemoveClick = (id: any) => {
    formik.setFieldValue(
      'inhouse_experience',
      formik.values.inhouse_experience.filter((item) =>
        item.id ? item.id !== id : item.generatedId !== id
      )
    );
  };

  // handle click event of the Add button
  const handleAddClick = () => {
    formik.setFieldValue('inhouse_experience', [
      ...formik.values.inhouse_experience,
      { ...inputList[0], generatedId: genId() }
    ]);
  };

  return (
    <>
      <div style={{ display: 'flex', flexGrow: '1' }} className="px-3">
        <form
          onSubmit={formik.handleSubmit}
          style={{ display: 'flex', flexDirection: 'column', flexGrow: '1' }}
        >
          <span className="profile-details-heading">Experience Details</span>
          <div style={{ display: 'flex', justifyContent: 'end', marginTop: '20px' }}>
            <button type="button" className="btn btn-primary" onClick={handleAddClick}>
              Add More..
            </button>
          </div>
          {formik.values.inhouse_experience.map((x, i) => {
            return (
              <div>
                <div className="row" style={{}}>
                  <CustomSelect
                    options={departmentcategory}
                    placeholder={'Search by Department'}
                    value={x.department}
                    onChange={(value: any) =>
                      formik.setFieldValue(`inhouse_experience.${i}.department`, value)
                    }
                    label={'Department'}
                  />
                  <CustomSelect
                    options={centercategory}
                    placeholder={'Search by Center'}
                    value={x.center}
                    onChange={(value: any) => {
                      formik.setFieldValue(`inhouse_experience.${i}.center`, value);
                    }}
                    label={'Center'}
                  />
                  <TextInput
                    label="Position"
                    name="start_position"
                    type="text"
                    onChange={(e: any) =>
                      formik.setFieldValue(`inhouse_experience.${i}.start_position`, e.target.value)
                    }
                    value={x.start_position}
                    onBlur={formik.handleBlur}
                    formik={formik}
                  />
                </div>

                <div className="row">
                  <TextInput
                    label="Date_From"
                    name="date_from"
                    type="date"
                    onChange={(e: any) =>
                      formik.setFieldValue(`inhouse_experience.${i}.date_from`, e.target.value)
                    }
                    value={x.date_from}
                    onBlur={formik.handleBlur}
                    formik={formik}
                  />
                  {formik?.values?.inhouse_experience[i].date_to === null ? (
                    ''
                  ) : (
                    <TextInput
                      label="Date_To"
                      name=" date_to "
                      type="date"
                      onChange={(e: any) =>
                        formik.setFieldValue(`inhouse_experience.${i}.date_to`, e.target.value)
                      }
                      value={x.date_to}
                      onBlur={formik.handleBlur}
                      formik={formik}
                    />
                  )}
                </div>
                <div className="row">
                  <div className="form-check col mt-5" style={{ display: 'flex' }}>
                    <input
                      className="form-check-input"
                      style={{ marginRight: '4px' }}
                      type="checkbox"
                      value=""
                      id="flexCheckDefault"
                      onChange={() => {
                        formik.setFieldValue(
                          `inhouse_experience.${i}.date_to`,
                          formik?.values?.inhouse_experience[i].date_to === null ? '' : null
                        );
                      }}
                    />
                    <label className="form-check-label mb-3">Currently Working !</label>
                  </div>
                </div>

                <div>
                  {experienceIntitalValue.length !== 1 && (
                    <button
                      className="btn btn-danger"
                      onClick={() => handleRemoveClick(x.id ? x.id : x.generatedId)}
                    >
                      Remove
                    </button>
                  )}
                </div>
              </div>
            );
          })}
          <div
            className="hstack gap-2 justify-content-end"
            style={{ marginBottom: '20px', marginTop: '20px' }}
          >
            <PrimaryButton title="Save" iconName="ri-add-line align-bottom me-1" type="submit" />
            <SecondaryButton
              onClick={() => {
                navigate('/internal-user-management');
              }}
              title="Discard"
            />
          </div>
        </form>
      </div>
    </>
  );
};
export default InteralUserExperience;

// import { FC } from 'react';

// interface IExperienceDetails {
//   status: string;
// }

// const InteralUserExperience: FC<IExperienceDetails> = ({ status }) => {
//   return <h1>RIshav</h1>;
// };
// export default InteralUserExperience;
