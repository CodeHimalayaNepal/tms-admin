import validations from 'utils/validation';
import * as Yup from 'yup';
// import { UserDocument } from 'redux/reducers/administrator';
export interface IGeneral {
  id?: string;
  first_name: string;
  middle_name: string;
  last_name: string;
  first_name_np: string;
  middle_name_np: string;
  last_name_np: string;
  dob_AD: string;
  gender?: string;
  marital_status?: string;
  blood_group?: string;
  religion: string;
  email: string;
  mobile_number: string;
  passport_num: string;
  citizenship_num: string;
  national_id: string;
  pan_number: string;
  father_name: string;
  mother_name: string;
  grandfather_name: string;
  grandmother_name: string;
  permanent_address: {
    province: string;
    district: string;
    ward_no: string;
    tole_street: string;
  };
  temporary_address: {
    province: string;
    district: string;
    ward_no: string;
    tole_street: string;
  };
  image: File | string | null;
}
export interface IEducation {
  id?: string;
  education: [
    {
      degree: string;
      percentage: string;
      passed_year: string;
      level: string;
      institution_name: string;
    }
  ];
}
export interface IExperience {
  id?: string;
  inhouse_experience: [
    {
      department: string;
      center: string;
      date_from: string;
      date_to: string;
      start_position: string;
    }
  ];
}

export const profileInitialValues: IGeneral = {
  first_name: '',
  middle_name: '',
  last_name: '',
  first_name_np: '',
  middle_name_np: '',
  last_name_np: '',
  image: null as File | null,
  email: '',
  mobile_number: '',
  pan_number: '',
  dob_AD: '',
  gender: '',
  marital_status: '',
  blood_group: '',
  religion: '',
  passport_num: '',
  citizenship_num: '',
  national_id: '',
  father_name: '',
  mother_name: '',
  grandfather_name: '',
  grandmother_name: '',
  permanent_address: {
    district: '',
    ward_no: '',
    province: '',
    tole_street: ''
  },
  temporary_address: {
    district: '',
    ward_no: '',
    province: '',
    tole_street: ''
  }
};
export const EducationInitialValues: IEducation = {
  education: [
    {
      degree: '',
      percentage: '',
      passed_year: '',
      level: '',
      institution_name: ''
    }
  ]
};
export const ExperienceInitialValues: IExperience = {
  inhouse_experience: [
    {
      department: '',
      center: '',
      date_from: '',
      date_to: '',
      start_position: ''
    }
  ]
};

export const profileValidationSchema = Yup.object().shape({
  first_name: Yup.string().required('First Name is required'),
  last_name: Yup.string().required('Last Name is required'),
  middle_name: Yup.string().nullable(),
  first_name_np: Yup.string().required('First Name is required'),
  last_name_np: Yup.string().required('Last Name is required'),
  middle_name_np: Yup.string().nullable(),
  email: Yup.string().email('Invalid email').required('Email is required'),
  mobile_number: Yup.string()
    .matches(validations.phone.regex, validations.phone.message)
    .required('Mobile Number is required'),
  pan_number: Yup.string().required('Pan Number is required'),
  gender: Yup.string().required('Gender is required'),
  date_of_birth_bs: Yup.string().required('Date of birth  is required'),
  date_of_birth_ad: Yup.string().required(' Date of birth is required'),
  marital_status: Yup.string().required('Marital Status is required'),
  image: Yup.mixed().required('Image is required'),
  religion: Yup.string().required('Religion is required'),
  citizenship_number: Yup.string().required('Citizenship Number is required'),
  passport_number: Yup.string().required('Passport Number is required'),
  national_id: Yup.string().required('National Id is required'),
  father_name: Yup.string().required('Father Name is required'),
  mother_name: Yup.string().required('Mother Name is required'),
  grandfather_name: Yup.string().required('Grandfather Name is required'),
  grandmother_name: Yup.string().required('GrandMother Name is required'),
  blood_group: Yup.string().required('Blood Group is required'),
  province: Yup.string().required('Province is required'),
  district: Yup.string().required('District is required'),
  ward_no: Yup.string().required('Ward Number is required'),
  tole_street: Yup.string().required('Street is required')
});
export const EducationValidationSchema = Yup.object().shape({
  degree: Yup.string().required('Name of Degree is required'),
  percantage: Yup.string().required('Percantage Obtained is required'),
  passed_year: Yup.string().required('Passed Year is required'),
  level: Yup.string().required('Level is required')
});
export const ExperienceValidationSchema = Yup.object().shape({
  department: Yup.string().required('Department is required'),
  center: Yup.string().required('Center is required'),
  date_from: Yup.string().required('Date is required'),
  date_to: Yup.string().required('Date is required')
});
