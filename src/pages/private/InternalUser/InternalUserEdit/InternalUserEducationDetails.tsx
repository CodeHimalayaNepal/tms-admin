import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import TextInput from 'components/ui/TextInput';
import { useFormik } from 'formik';
import { FC } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import {
  useListInternalUserDetailsQuery,
  useUpdateInternalUserEducationMutation
} from 'redux/reducers/internal-user';

const genId = (len = 5) => {
  return parseInt(
    new Array(len)
      .fill(null)
      .map((_) => Math.round(Math.random() * 9))
      .join('')
  );
};

interface IEducationDetails {
  status: string;
  setTabValue: (value: any) => void;
}
const InternalUserEducationDetails: FC<IEducationDetails> = ({ status, setTabValue }) => {
  const inputList = [
    { degree: '', percantage: '', passed_year: '', level: '', institution_name: '' }
  ];
  const { id } = useParams<{ id: string }>();
  const isCreate = id === 'create';
  const navigate = useNavigate();
  const { data: EducationDetail, isLoading: isFetchingEducationDetail } =
    useListInternalUserDetailsQuery(id ? id : '', { skip: isCreate || !id });

  const educationIntitalValue = EducationDetail.education ?? [];

  const [updateEducation, { data, isLoading: updatingEducation }] =
    useUpdateInternalUserEducationMutation();

  const formik = useFormik({
    enableReinitialize: true,
    // validationSchema: profileValidationSchema,useUpdateInternalUserEducationMutation
    initialValues: { education: [...educationIntitalValue] },
    onSubmit: (values) => {
      try {
        const valuesExtracted = Object.values(values).flat();
        const result = updateEducation({
          id: EducationDetail.id,
          education: valuesExtracted
        }).unwrap();
        if (result) {
          toast.success(' Education Profile Updated Successfully');
          setTabValue('3');
        }
      } catch (err: any) {
        console.log(err);
      }
    }
  });

  const handleRemoveClick = (id: any) => {
    formik.setFieldValue(
      'education',
      formik.values.education.filter((item) => (item.id ? item.id !== id : item.generatedId !== id))
    );
  };

  const handleAddClick = () => {
    formik.setFieldValue('education', [
      ...formik.values.education,
      { ...inputList[0], generatedId: genId() }
    ]);
  };

  return (
    <>
      <div style={{ display: 'flex', flexGrow: '1' }} className="px-3">
        <form
          onSubmit={formik.handleSubmit}
          style={{ display: 'flex', flexDirection: 'column', flexGrow: '1' }}
        >
          <span className="profile-details-heading">Education Details</span>
          <div style={{ display: 'flex', justifyContent: 'end' }} className="mt-1">
            <button type="button" className="btn btn-primary" onClick={handleAddClick}>
              Add More..
            </button>
          </div>
          {formik.values.education.map((x, i) => {
            return (
              <div>
                <div className="row">
                  <TextInput
                    label="Name of Degree"
                    name="degree"
                    type="text"
                    onChange={(e: any) =>
                      formik.setFieldValue(`education.${i}.degree`, e.target.value)
                    }
                    value={x.degree}
                    onBlur={formik.handleBlur}
                    formik={formik}
                  />
                  <TextInput
                    label="Institution"
                    name="institution_name"
                    type="text"
                    onChange={(e: any) =>
                      formik.setFieldValue(`education.${i}.institution_name`, e.target.value)
                    }
                    value={x.institution_name}
                    onBlur={formik.handleBlur}
                    formik={formik}
                  />
                  <TextInput
                    label="Percentage"
                    name="percentage"
                    type="text"
                    onChange={(e: any) =>
                      formik.setFieldValue(`education.${i}.percentage`, e.target.value)
                    }
                    value={x.percentage}
                    onBlur={formik.handleBlur}
                    formik={formik}
                  />
                </div>
                <div className="row">
                  <TextInput
                    label="Passed Year"
                    name="passed_year"
                    type="text"
                    onChange={(e: any) =>
                      formik.setFieldValue(`education.${i}.passed_year`, e.target.value)
                    }
                    value={x.passed_year}
                    onBlur={formik.handleBlur}
                    formik={formik}
                  />
                  <TextInput
                    label="Level"
                    name="level"
                    type="text"
                    onChange={(e: any) =>
                      formik.setFieldValue(`education.${i}.level`, e.target.value)
                    }
                    value={x.level}
                    onBlur={formik.handleBlur}
                    formik={formik}
                  />
                </div>
                <div>
                  {formik.values.education.length !== 1 && (
                    <button
                      type="button"
                      className="btn btn-danger"
                      onClick={() => handleRemoveClick(x.id ? x.id : x.generatedId)}
                    >
                      Remove
                    </button>
                  )}
                </div>
              </div>
            );
          })}
          <div
            className="hstack gap-2 justify-content-end"
            style={{ marginBottom: '20px', marginTop: '20px' }}
          >
            <PrimaryButton title="Save" iconName="ri-add-line align-bottom me-1" type="submit" />
            <SecondaryButton
              onClick={() => {
                navigate('/internal-user-management');
              }}
              title="Discard"
            />
          </div>
        </form>
      </div>
    </>
  );
};
export default InternalUserEducationDetails;
