import Container from 'components/ui/Container';
import { useFormik } from 'formik';
import React, { useState } from 'react';
import InternalForm from './InternalUserGeneralDetails';
import { IGeneral } from './Schema';
import InternalUserEducationDetails from './InternalUserEducationDetails';
import { RiH1 } from 'react-icons/ri';
import InteralUserExperience from './InternalUserExperienceDetails';
import * as Yup from 'yup';
import { initialValue, SignUpValidationSchema } from '../../Profile/schema';
import { toast } from 'react-toastify';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import ChangePassword from './changePassword';
import { useChangePasswordMutation } from 'redux/reducers/auth';
import { useNavigate } from 'react-router-dom';

const AdminPasswordChange = () => {
  const [changethisPassword, isLoading] = useChangePasswordMutation();

  const navigate = useNavigate();

  const passwordFormik = useFormik({
    validationSchema: Yup.object().shape({
      ...SignUpValidationSchema
    }),
    enableReinitialize: true,
    initialValues: initialValue,
    onSubmit: (values) => {
      let executeFunc = changethisPassword;
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          navigate('/');
          toast.success('Password Edited Successfullly');
        })
        .catch((err: any) => {
          if (err.data?.errors) {
            toast.error(Object.values(err.data.errors).join(', '));
            return;
          }
        });
    }
  });

  return (
    <Container title="Change Password">
      <div style={{ flexBasis: '1' }}>
        <div className="row">
          <div className="col-xxl-10">
            <form>
              <ChangePassword formik={passwordFormik} />
              <div className="row">
                <div className="col-xxl-4 my-4">
                  <div className="hstack gap-2 justify-content-end">
                    <PrimaryButton
                      title="Save"
                      iconName="ri-add-line align-bottom me-1"
                      type="submit"
                      // isLoading={creatingCourse || updatingCourse}
                    />
                    <SecondaryButton
                      onClick={() => {
                        // navigate('/modules');
                      }}
                      title="Discard"
                      // isLoading={creatingCourse || updatingCourse}
                    />
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default AdminPasswordChange;
