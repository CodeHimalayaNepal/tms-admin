import CustomSelect from 'components/ui/Editor/CustomSelect';
import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { FC, useState } from 'react';
import { useListRolesQuery } from 'redux/reducers/roles';
import { IInternalUserInput } from './schema';

interface IInternalUserForm {
  formik: FormikProps<IInternalUserInput>;
}

const intrepretRole = (role: number) => {
  switch (role?.toString()) {
    case '1':
      return 'INTERNAL';
    case '3':
      return 'EXTERNAL';
    default:
      return role;
  }
};

const InternalUserForm: FC<IInternalUserForm> = ({ formik }) => {
  const { data: rolesData } = useListRolesQuery();
  const roleList = [
    {
      value: 'INTERNAL',
      label: 'Internal'
    },
    {
      value: 'EXTERNAL',
      label: 'External'
    }
  ];
  // const roleList = rolesData?.data?.filter((item: any) => {
  //   return {
  //     value: item?.id?.toString(),
  //     label: item?.name
  //   };
  // });
  return (
    <form>
      <div className="row">
        <TextInput
          label="First Name"
          name="first_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.first_name}
          onBlur={formik.handleBlur}
          formik={formik}
          required
        />
        <TextInput
          label="Middle Name"
          name="middle_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.middle_name}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <TextInput
          label="Last Name"
          name="last_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.last_name}
          onBlur={formik.handleBlur}
          formik={formik}
          required
        />
        <TextInput
          label="Email"
          name="email"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.email}
          onBlur={formik.handleBlur}
          formik={formik}
          required
        />
        <TextInput
          label="Mobile Number"
          name="mobile_number"
          type="number"
          onChange={formik.handleChange}
          value={formik.values.mobile_number}
          onBlur={formik.handleBlur}
          formik={formik}
          required
        />
        <CustomSelect
          name="role"
          options={roleList}
          placeholder={'Search by name'}
          value={intrepretRole(formik.values.role)}
          onChange={(value: any) => formik.setFieldValue('role', value)}
          label={'Select User'}
        />
        <FormikValidationError name="role" errors={formik.errors} touched={formik.touched} />
      </div>
    </form>
  );
};

export default InternalUserForm;
