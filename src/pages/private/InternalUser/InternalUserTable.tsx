import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import ViewIcon from '../../../assets/images/icons/view.png';
import { Cell } from 'react-table';
import { useListInternalUsersQuery } from 'redux/reducers/internal-user';
import { IInternalUserInput } from './schema';
import editIcon from '../../../assets/images/icons/edit.png';
import { FcUnlock } from 'react-icons/fc';

interface IInternalUserTable {
  onEditClicked: (data: IInternalUserInput) => void;
  onViewClick: (data: IInternalUserInput) => void;
  searchQuery: string;
  onUnBlock: (id: number) => void;
}
const InternalUserManagementTable: FC<IInternalUserTable> = ({
  onEditClicked,
  onViewClick,
  searchQuery,
  onUnBlock
}) => {
  const [page, setPage] = useState(1);
  const [allDataList, setAllDataList] = useState(0);
  const { data: internalUserData, isLoading, isSuccess } = useListInternalUsersQuery(searchQuery);
  const totalUser = internalUserData?.data ?? [];
  const paginatedData = totalUser?.slice(
    page === 1 ? 0 : (page - 1) * 10,
    page === 1 ? 10 : page * 10
  );
  const canPrevious = page === 1 ? false : true;
  const canNext = page === Math.ceil(totalUser?.length / 10) ? false : true;
  React.useEffect(() => {
    if (isSuccess) {
      setPage(1);
    }
  }, [isSuccess]);
  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: Cell<IInternalUserInput>) => {
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: 'First Name',
      accessor: 'first_name'
    },
    {
      Header: 'Middle Name',
      accessor: 'middle_name'
    },
    {
      Header: 'Last Name',
      accessor: 'last_name'
    },
    {
      Header: 'Email',
      accessor: 'email'
    },
    {
      Header: 'Mobile No',
      accessor: 'mobile_number'
    },
    {
      Header: 'User Type',
      Cell: (data: any) => {
        return <>{data.row.original.user_type == '1' ? 'Internal' : 'External'}</>;
      }
      //accessor: 'user_type'
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={ViewIcon}
              style={{ cursor: 'pointer' }}
              onClick={() => onViewClick({ ...data.row.original })}
            />

            <img
              src={editIcon}
              style={{ cursor: 'pointer' }}
              className="m-2"
              onClick={() => onEditClicked({ ...data.row.original, role: data.row.original.role })}
            />
            {data.row?.original?.user__is_blocked === true ? (
              <FcUnlock
                size={20}
                cursor={'pointer'}
                onClick={() => onUnBlock(data.row.original.main_user_id)}
              />
            ) : null}
          </>
        );
      }
    }
  ];
  return (
    <div>
      <DataTable
        columns={columns}
        // data={internalUserData?.data || []}
        data={paginatedData || []}
        search={false}
        isLoading={isLoading}
        pagination={{
          canPrevious,
          canNext,
          prevPage: () => {
            if (totalUser) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (totalUser) {
              totalUser?.length >= allDataList && setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default InternalUserManagementTable;
