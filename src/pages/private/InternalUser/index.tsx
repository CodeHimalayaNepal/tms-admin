import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import React from 'react';
import {
  IInternalUserInput,
  internalUserInitialValues,
  internalUserSchemaValidations
} from './schema';

import InternalUserForm from './InternalUserForm';
import InternalUserTable from './InternalUserTable';
import {
  useCreateInternalUserMutation,
  useUnBlockInternalUserMutation,
  useUpdateInternalUserMutation
} from 'redux/reducers/internal-user';
import CsvUploadForm from '../Trainee/CsvUploadForm';

const InternalUser = () => {
  const timerRef = React.useRef<NodeJS.Timeout>();
  const navigate = useNavigate();
  const [showQuery, setShowQuery] = useState('');
  const [searchQuery, setSearchQuery] = useState('');
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [csvFormModal, setCsvFormModal] = useToggle(false);
  const [unblockModal, setUnblockModal] = useToggle(false);
  const [unBlockUserId, setUnBlockUserId] = useState<number>();
  const [unblockUser, { isLoading: isUnblocking }] = useUnBlockInternalUserMutation();

  const onUnblock = (id: number) => {
    setUnblockModal(true);
    setUnBlockUserId(id);
  };

  const onUnblockConfirm = () => {
    unBlockUserId &&
      unblockUser(unBlockUserId)
        .unwrap()
        .then(() => {
          toast.success('Unblocked Successfully');
          setUnblockModal(false);
        })
        .catch((err: any) => {
          toast.error('Error in unblocking');
        });
  };
  const [internalUserInitialFormState, setinternalUserInitialForm] =
    useState(internalUserInitialValues);
  const [deleteInternalUserState, setDeletingInternalUser] = useState<IInternalUserInput | null>(
    null
  );

  const [createInternalUser, { isLoading: isCreating }] = useCreateInternalUserMutation();
  const [updateInternalUser, { isLoading: isUpdating }] = useUpdateInternalUserMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: internalUserSchemaValidations,
    initialValues: internalUserInitialFormState,
    onSubmit: (values) => {
      let executeFunc = createInternalUser;
      if (values.id) {
        executeFunc = updateInternalUser;
      }
      executeFunc({
        ...values,
        middle_name: values.middle_name === '' ? null : values.middle_name,
        mobile_number: values.mobile_number.toString(),
        role: values.role === 1 ? 'INTERNAL' : 'EXTERNAL'
      })
        .unwrap()
        .then((data: any) => {
          toast.success(data.message);
          setinternalUserInitialForm(internalUserInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error(err?.data?.message);
        });
    }
  });

  const onEditClicked = (data: IInternalUserInput) => {
    setinternalUserInitialForm({ ...data, role: data?.user_type as unknown as number });
    setFormModal();
  };

  const onViewClick = (data: IInternalUserInput) => {
    const userType = data?.user_type;
    userType == '1'
      ? navigate(`/internal-user-management/internal/${data.id}`)
      : navigate(`/external-user-management/external-profile/${data.id}`);
  };

  const closeModal = () => {
    setFormModal();
    setinternalUserInitialForm(internalUserInitialValues);
  };

  return (
    <Container title="User Management" subTitle="List of all admin users">
      <div className="col-sm-auto">
        <div className="d-flex">
          <PrimaryButton
            onClick={() => setCsvFormModal(true)}
            type="button"
            className="btn btn-outline-primary add-btn"
            small="me-2 mb-2"
            iconName="ri-download-line align-bottom me-1"
            data-bs-toggle="modal"
            title="Import Users"
            isOutline={true}
          />

          <PrimaryButton
            small="mb-2"
            title={internalUserInitialFormState.id ? 'Edit Internal User' : 'Create Internal User'}
            iconName="ri-add-line align-bottom me-1"
            onClick={setFormModal}
            type="button"
          />
          <div className="justify-content-lg-end" style={{ flexGrow: '1', display: 'flex' }}>
            <div className="search-box ms-2">
              <input
                type="text"
                className="form-control search"
                placeholder="Search..."
                name="key"
                value={showQuery}
                onChange={(e) => {
                  setShowQuery(e.target.value);
                  clearTimeout(timerRef.current);
                  timerRef.current = setTimeout(() => {
                    setSearchQuery(e.target.value);
                  }, 2000);
                }}
              />
              <i className="ri-search-line search-icon" />
            </div>
          </div>
        </div>
      </div>

      <InternalUserTable
        onEditClicked={onEditClicked}
        onViewClick={onViewClick}
        searchQuery={searchQuery}
        onUnBlock={onUnblock}
      />
      <CsvUploadForm csvFormModal={csvFormModal} setCsvFormModal={setCsvFormModal} isAdmin={true} />
      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={internalUserInitialFormState.id ? 'Edit Internal User' : 'Create Internal User'}
          isModalOpen={formModal}
          closeModal={closeModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={closeModal}
                title="Close"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton
                title={internalUserInitialFormState.id ? 'Update' : 'Create'}
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <InternalUserForm formik={formik} />
        </Modal>
      </form>
      {unBlockUserId && (
        <ConfirmationModal
          isOpen={unblockModal}
          onClose={setUnblockModal}
          onConfirm={onUnblockConfirm}
          title={`Unblock User`}
          btnText={'Unblock'}
          isLoading={isUnblocking}
        >
          <>
            <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
              <h4>Are you Sure ?</h4>
              <p className="text-muted ">you want to unblock this user?</p>
            </div>
          </>
        </ConfirmationModal>
      )}
    </Container>
  );
};

export default InternalUser;
