import { FormikProps } from 'formik';
import { FC, useRef } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import PrimaryButton from 'components/ui/PrimaryButton';
import Container from 'components/ui/Container';
import { IProfile } from './schema';
import { Avatar } from 'common/constant';
import { useListInternalUserDetailsQuery } from 'redux/reducers/internal-user';
import { useListOrganizationCentreQuery } from 'redux/reducers/organizations-centre';
import { useListOrganizationDepartmentQuery } from 'redux/reducers/organization-department';
import ReactToPrint from 'react-to-print';
interface IProfileForm {
  formik?: FormikProps<IProfile>;
}
const getImage = (image: File) => {
  try {
    return URL.createObjectURL(image);
  } catch (error) {
    return '';
  }
};

const InternalUser: FC<IProfileForm> = ({ formik }) => {
  const navigate = useNavigate();
  const { id } = useParams<{ id: string }>();
  const { data: internaluser } = useListInternalUserDetailsQuery(id);
  const { data: organizationcenter } = useListOrganizationCentreQuery();

  const { data: organizationdepartment } = useListOrganizationDepartmentQuery();

  const ref = useRef<HTMLDivElement | null>(null);
  return (
    <Container title="Profile">
      <div className="row">
        {/* <div className="col-xxl-2">
          <div className="d-flex  flex-column">
            <div className="d-flex justify-content-start p-2" onClick={() => {}}>
              <i className="ri-pencil-line align-bottom"></i>
              <span className="me-1" style={{ cursor: 'pointer' }}>
                Profile
              </span>
            </div>
            <hr />
          </div>
        </div> */}
        <div className="col-xxl-12" style={{ marginLeft: '10px' }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginBottom: '50px'
            }}
          >
            <div className="profile-details-heading">Profile Information</div>
            <div>
              <PrimaryButton
                title="Edit Profile"
                type="button"
                onClick={() => navigate(`/internal-user-management/internal/edit/${id}`)}
              />
            </div>
          </div>

          <div style={{ margin: '20px 15px' }} ref={ref}>
            <div className="card mt-n5">
              <div className="card-body p-4">
                <div className="text-center">
                  <div className="profile-user position-relative d-inline-block mx-auto  mb-4">
                    <img
                      src={
                        formik?.values.image
                          ? typeof formik.values.image === 'string'
                            ? formik.values.image
                            : getImage(formik.values.image)
                          : internaluser?.image === null
                          ? Avatar
                          : internaluser?.image
                      }
                      className="rounded-circle avatar-xl img-thumbnail user-profile-image"
                      alt="user-profile-image"
                    />
                    <div className="avatar-xs p-0 rounded-circle profile-photo-edit">
                      <input
                        id="profile-img-file-input"
                        type="file"
                        className="profile-img-file-input"
                        name="image"
                        src={typeof internaluser?.image === 'string' ? internaluser?.image : Avatar}
                        onChange={(e) => {
                          formik?.handleChange({
                            target: { name: 'image', value: e.target.files?.[0] ?? null }
                          });
                        }}
                      />
                    </div>
                  </div>

                  {formik?.touched.image && formik.errors.image && (
                    <span className="text-danger">{formik.errors.image}</span>
                  )}
                </div>
              </div>
            </div>

            <div>
              <h2 style={{ textDecoration: 'underline' }}>Personal Information</h2>
              <div
                style={{
                  display: 'flex',
                  gap: '2rem',
                  flexWrap: 'wrap',
                  justifyContent: 'space-between',
                  marginRight: '50px'
                }}
              >
                <div>
                  <p>
                    <b>First Name</b>
                    <p>{internaluser?.first_name || 'NA'}</p>
                  </p>
                </div>
                <div>
                  <p>
                    <b>Middle Name</b>
                    <p>{internaluser?.middle_name || 'NA'}</p>
                  </p>
                </div>
                <div>
                  <p>
                    <b>Last Name</b>
                    <p>{internaluser?.last_name || 'any'} </p>
                  </p>
                </div>

                <div>
                  <p>
                    <b>Email</b>
                    <p>{internaluser?.email || 'NA'}</p>
                  </p>
                </div>
                <div>
                  <p>
                    <b>Pan Number</b>
                    <p>{internaluser?.pan_number || 'NA'}</p>
                  </p>
                </div>
                <div>
                  <p>
                    <b>Citizenship Number</b>
                    <p>{internaluser?.citizenship_num || 'NA'}</p>
                  </p>
                </div>
                <div>
                  <p>
                    <b>Gender</b>
                    <p>{internaluser?.gender == 1 ? 'Male' : 'Female'}</p>
                  </p>
                </div>

                {/* <div>
                  <p>
                    <b>National ID</b>
                    <p>{internaluser?.national_id || 'NA'}</p>
                  </p>
                </div> */}
              </div>
              <div
                style={{
                  display: 'flex',
                  gap: '2rem',
                  flexWrap: 'wrap',
                  color: '#212121',
                  marginTop: '5px',
                  justifyContent: 'space-between',
                  marginRight: '20px'
                }}
              >
                <div>
                  <p>
                    <b>Passport Number</b>
                    <p>{internaluser?.passport_num || 'NA'}</p>
                  </p>
                </div>
                <div>
                  <p>
                    <b>Marital Status</b>
                    <p>{internaluser?.marital_status == 1 ? 'Single' : 'Married'}</p>
                  </p>
                </div>
                <div>
                  <p>
                    <b>Date Of Birth(AD)</b>
                    <p>{internaluser?.dob_AD || 'NA'}</p>
                  </p>
                </div>
                <div>
                  <p>
                    <b>Blood Group</b>
                    <p>{internaluser?.blood_group || 'NA'}</p>
                  </p>
                </div>
                <div>
                  <p>
                    <b>Religion</b>
                    <p>{internaluser?.religion || 'NA'}</p>
                  </p>
                </div>
                <div>
                  <p>
                    <b>Mobile Number</b>
                    <p>{internaluser?.mobile_number || 'NA'}</p>
                  </p>
                </div>
              </div>
            </div>
            <div>
              <div>
                <h2 style={{ textDecoration: 'underline' }}>Family Information</h2>
                <div
                  style={{
                    display: 'flex',
                    gap: '2rem',
                    flexWrap: 'wrap',
                    justifyContent: 'space-between',
                    marginRight: '50px'
                  }}
                >
                  <div>
                    <p>
                      <b>Father's Name</b>
                      <p>{internaluser?.father_name || 'NA'}</p>
                    </p>
                  </div>
                  <div>
                    <p>
                      <b>Mother's Name</b>
                      <p>{internaluser?.mother_name || 'NA'}</p>
                    </p>
                  </div>
                  <div>
                    <p>
                      <b>GrandFather's Name</b>
                      <p>{internaluser?.grandfather_name || 'any'} </p>
                    </p>
                  </div>

                  <div>
                    <p>
                      <b>GrandMothers's Name</b>
                      <p>{internaluser?.grandmother_name || 'NA'}</p>
                    </p>
                  </div>
                </div>
              </div>
              <div>
                <h2 style={{ textDecoration: 'underline' }}>Address Details</h2>
                <div className="row">
                  <div className="col-lg-12 col-md-6 col-sm-6">
                    <p>
                      <b style={{ fontSize: 16 }}>Temporary Address</b>
                    </p>
                    <div
                      style={{
                        display: 'flex',
                        gap: '2rem',
                        flexWrap: 'wrap',
                        justifyContent: 'space-between',
                        marginRight: '50px'
                      }}
                    >
                      <div className="col-lg-3">
                        <p>
                          <b>Province</b>
                          <p>{internaluser?.temporary_address?.province?.name_en}</p>
                        </p>
                      </div>
                      <div className="col-lg-3">
                        <p>
                          <b>District</b>
                          <p>{internaluser?.temporary_address?.district?.name_en}</p>
                        </p>
                      </div>
                      <div className="col-lg-3">
                        <p>
                          <b>Ward Number</b>
                          <p>{internaluser?.temporary_address?.ward_no}</p>
                        </p>
                      </div>
                      <div className="col-lg-3">
                        <p>
                          <b>Street Name</b>
                          <p>{internaluser?.temporary_address?.tole_street}</p>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-12 col-md-6 col-sm-6">
                    <p>
                      <b style={{ fontSize: 16 }}>Permanent Address</b>
                    </p>
                    <div
                      style={{
                        display: 'flex',
                        gap: '2rem',
                        flexWrap: 'wrap',
                        justifyContent: 'space-between',
                        marginRight: '50px'
                      }}
                    >
                      <div className="col-lg-3">
                        <p>
                          <b>Province</b>
                          <p>{internaluser?.permanent_address?.province?.name_en}</p>
                        </p>
                      </div>
                      <div className="col-lg-3">
                        <p>
                          <b>District</b>
                          <p>{internaluser?.permanent_address?.district?.name_en}</p>
                        </p>
                      </div>
                      <div className="col-lg-3">
                        <p>
                          <b>Ward Number</b>
                          <p>{internaluser?.permanent_address?.ward_no}</p>
                        </p>
                      </div>
                      <div className="col-lg-3">
                        <p>
                          <b>Street Name</b>
                          <p>{internaluser?.permanent_address?.tole_street}</p>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h2 style={{ textDecoration: 'underline' }}>Experience</h2>
              <div
                style={{
                  display: 'flex',
                  gap: '2rem',
                  flexWrap: 'wrap',
                  justifyContent: 'space-between',
                  marginRight: '50px'
                }}
              >
                {internaluser?.inhouse_experience.map((item: any, index: number) => {
                  return (
                    <div key={index}>
                      <b>Department: </b>
                      <p>
                        {organizationdepartment?.data.find(
                          (center: any) => center.tms_department_id == item.department
                        )?.name || 'NA'}
                      </p>
                      <b>Center: </b>
                      <p>
                        {organizationcenter?.data.find(
                          (center: any) => center.tms_center_id == item.center
                        )?.name || 'NA'}
                      </p>

                      <b>Position:</b>
                      <p>{item?.start_position || 'NA'}</p>
                      <b>Date From:</b>
                      <p>{item?.date_from || 'NA'}</p>
                      <p>
                        {item?.date_to !== null ? (
                          <>
                            <b>Date To:</b>
                            <p>{item?.date_to}</p>
                          </>
                        ) : null}
                      </p>
                    </div>
                  );
                })}
              </div>

              <div>
                <h2 style={{ textDecoration: 'underline' }}>Education</h2>
                <div
                  style={{
                    display: 'flex',
                    gap: '2rem',
                    flexWrap: 'wrap',
                    justifyContent: 'space-between',
                    marginRight: '50px'
                  }}
                >
                  {internaluser?.education.map((edu: any, index: number) => {
                    return (
                      <div key={index}>
                        <p>
                          <b>Level: </b> {edu?.level}
                        </p>
                        <p>
                          <b>Degree: </b> {edu?.degree}
                        </p>
                        <p>
                          <b>Institution: </b>
                          {edu?.institution_name || 'NA'}
                        </p>
                        <p>
                          <b>Percentage: </b>
                          {edu?.percentage || 'NA'}%
                        </p>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ReactToPrint
        content={() => ref.current}
        trigger={() => (
          <div className="row">
            <div className="col-12 d-flex justify-content-end">
              <PrimaryButton title="Print" type="button" />
            </div>
          </div>
        )}
      />
    </Container>
  );
};

export default InternalUser;
