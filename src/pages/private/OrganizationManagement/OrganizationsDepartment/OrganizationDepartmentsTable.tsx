import ActiveInactiveBadge from 'components/ui/ActiveInactiveBage/ActiveInactiveBadge';
import React, { FC, useState } from 'react';
import EditIcon from '../../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../../assets/images/icons/delete.png';
import {
  IOrganizationDepartmentInput,
  useListOrganizationDepartmentQuery
} from 'redux/reducers/organization-department';
import { useListOrganizationQuery } from 'redux/reducers/organizations';
import 'css/style.css';
import NonPageDataTable from 'components/ui/DataTable/NonPageDataTable';
interface IOrganizationDepartmentTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: IOrganizationDepartmentInput) => void;
  onDeleteClicked: (data: IOrganizationDepartmentInput) => void;
}
const OrganizationDepartmentTable: FC<IOrganizationDepartmentTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked
}) => {
  const { data: organizationData, isLoading } = useListOrganizationDepartmentQuery();

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{data.row.index + 1}</>;
      }
    },
    {
      Header: 'Department Name',
      accessor: 'name'
    },
    {
      Header: '  Status',
      accessor: 'status',
      Cell: (data: any) => {
        return (
          <>
            {' '}
            <ActiveInactiveBadge isActive={data.row.original.status} />
          </>
        );
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={EditIcon}
              className="icons"
              onClick={() => onEditClicked(data.row.original)}
            />
            <img
              src={DeleteIcon}
              className="m-2 icons"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div>
      <NonPageDataTable
        columns={columns}
        data={organizationData?.data || []}
        tableHeaders={tableHeaders}
        isLoading={isLoading}
      />
    </div>
  );
};

export default OrganizationDepartmentTable;
