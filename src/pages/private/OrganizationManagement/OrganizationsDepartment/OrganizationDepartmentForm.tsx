import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { IOrganizationDepartmentInput } from 'redux/reducers/organization-department';
import { IOrganizationInput, useListOrganizationQuery } from 'redux/reducers/organizations';

interface IOrganizationForm {
  formik: FormikProps<IOrganizationDepartmentInput>;
}
const OrganizationForm: FC<IOrganizationForm> = ({ formik }) => {
  const { data: organizationData } = useListOrganizationQuery();

  // console.log('jkhkjhkjh', formik.errors.organization);

  return (
    <form>
      <div className="row">
        <div className="col-md-4 mt-3">
          <div className="mb-3">
            <label htmlFor="trainingType-field" className="form-label">
              Organizations
            </label>
            <select
              id="trainingType-field"
              className="form-select"
              data-choices
              data-choices-sorting="true"
              value={formik?.values?.organization}
              onChange={(e) => formik.setFieldValue('organization', +e.target.value)}
            >
              <option selected>Choose...</option>
              {organizationData?.data?.map((item: any) => {
                return (
                  <>
                    {' '}
                    <option value={+item.tms_organization_id}>{item.name}</option>
                  </>
                );
              })}
            </select>

            {formik.errors && (
              <span style={{ color: '#e74c3c' }}>{formik.errors.organization}</span>
            )}
          </div>
        </div>

        {/*end col*/}
        <TextInput
          label="Title"
          name="name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.name}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="Email"
          name="email"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.email}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              Status
            </label>
            <input
              type="checkbox"
              className="form-check-input"
              id="customSwitchsizesm"
              checked={formik.values.status}
              onChange={(e) => {
                formik.setFieldValue('status', !formik.values.status);
              }}
            />
          </div>
        </div>

        {/* <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              
            </label>
            <input type="checkbox" className="form-check-input" id="customSwitchsizesm" />
          </div>
        </div> */}
      </div>
    </form>
  );
};

export default OrganizationForm;
