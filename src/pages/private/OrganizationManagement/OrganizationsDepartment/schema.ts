import { IOrganizationDepartmentInput } from 'redux/reducers/organization-department';
import { IOrganizationInput } from 'redux/reducers/organizations';

import * as Yup from 'yup';

export const organizationDepartmentInitialValues: IOrganizationDepartmentInput = {
  name: '',
  // phone_no: '',
  email: '',
  status: true,
  organization: ''
};

export const OrganizationDepoartmentSchemaValidations = Yup.object().shape({
  name: Yup.string().min(2, 'Too Short!').max(100, 'Too Long!').required('Title is required'),
  // phone_no: Yup.number().required('Phone number is required'),
  organization: Yup.number().required('Organization is required'),
  // email: Yup.string().email('Invalid email').required('Email is required'),
  status: Yup.boolean().required('Required')
});
