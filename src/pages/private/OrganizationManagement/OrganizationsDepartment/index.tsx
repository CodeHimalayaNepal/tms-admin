import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import {
  IOrganizationDepartmentInput,
  useCreateOrganizationDepartmentMutation,
  useDeleteOrganizationDepartmentMutation,
  useUpdateOrganizationDepartmentMutation
} from 'redux/reducers/organization-department';

import {
  organizationDepartmentInitialValues,
  OrganizationDepoartmentSchemaValidations
} from './schema';

import OrganizationDepartmentForm from './OrganizationDepartmentForm';
import OrganizationDepartmentTable from './OrganizationDepartmentsTable';

const OrganizationDepartments = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [organizationDepartmentInitialFormState, setOrganizationDepartmentInitialForm] = useState(
    organizationDepartmentInitialValues
  );
  const [deletingOrganizationDepartmentState, setDeletingOrganizationDepartments] =
    useState<IOrganizationDepartmentInput | null>(null);

  const [deleteOrganizationDepartment, { isLoading: isDeleting }] =
    useDeleteOrganizationDepartmentMutation();
  const [createOrganizationDepartment, { isLoading: isCreating }] =
    useCreateOrganizationDepartmentMutation();
  const [updateOrganizationDepartment, { isLoading: isUpdating }] =
    useUpdateOrganizationDepartmentMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: OrganizationDepoartmentSchemaValidations,
    initialValues: organizationDepartmentInitialFormState,
    onSubmit: (values) => {
      let executeFunc = createOrganizationDepartment;
      if (values.tms_department_id) {
        executeFunc = updateOrganizationDepartment;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Update Successfully');
          setOrganizationDepartmentInitialForm(organizationDepartmentInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
    }
  });

  const onEditClicked = (data: IOrganizationDepartmentInput) => {
    setOrganizationDepartmentInitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingOrganizationDepartmentState &&
      deleteOrganizationDepartment(deletingOrganizationDepartmentState?.tms_department_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setOrganizationDepartmentInitialForm(organizationDepartmentInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IOrganizationDepartmentInput) => {
    setDeletingOrganizationDepartments(data);
    toggleDeleteModal();
  };

  const closeModal = () => {
    setFormModal();
    setOrganizationDepartmentInitialForm(organizationDepartmentInitialValues);
  };

  return (
    <Container title="Organization Departments" subTitle="List of all departments">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <OrganizationDepartmentTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title="Create Organization Department"
              iconName="ri-add-line align-bottom me-1"
              onClick={setFormModal}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={
            organizationDepartmentInitialFormState.tms_department_id
              ? 'Update Organization Department'
              : 'Create Organization Department'
          }
          isModalOpen={formModal}
          closeModal={closeModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={closeModal}
                title="Close"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton
                title={
                  organizationDepartmentInitialFormState.tms_department_id ? 'Update ' : 'Create'
                }
                iconName={`${
                  organizationDepartmentInitialFormState.tms_department_id ? '' : 'ri-add-line'
                } align-bottom me-1`}
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <OrganizationDepartmentForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default OrganizationDepartments;
