import React from 'react';
import { Outlet } from 'react-router-dom';

const OrganizationManagement = () => {
  return (
    <div>
      <Outlet />
    </div>
  );
};

export default OrganizationManagement;
