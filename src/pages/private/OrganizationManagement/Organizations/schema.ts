import { IOrganizationInput } from 'redux/reducers/organizations';

import * as Yup from 'yup';

export const organizationInitialValues: IOrganizationInput = {
  name: '',
  location: '',
  phone_no: '',
  email: '',
  status: true
};

export const OrganizationSchemaValidations = Yup.object().shape({
  name: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Title is required'),
  location: Yup.string().required('Location is required'),
  phone_no: Yup.string().required('Phone number is required'),
  // .min(10, 'Phone number is not valid')
  // .max(10, 'Phone number is not valid'),
  email: Yup.string().email('Invalid email').required('Email is required'),
  status: Yup.boolean().required('Required')
});
