import ActiveInactiveBadge from 'components/ui/ActiveInactiveBage/ActiveInactiveBadge';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import EditIcon from '../../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../../assets/images/icons/delete.png';
import { Cell } from 'react-table';
import { IOrganizationInput, useListOrganizationQuery } from 'redux/reducers/organizations';
import { ITrainingInput, useListTrainingQuery } from 'redux/reducers/training';
import 'css/style.css';
import NonPageDataTable from 'components/ui/DataTable/NonPageDataTable';
interface ITraineeDataTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: IOrganizationInput) => void;
  onDeleteClicked: (data: IOrganizationInput) => void;
}
const TrainingManagementTable: FC<ITraineeDataTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked
}) => {
  const [page, setPage] = useState(1);
  const [allDataList, setAllDataList] = useState(0);

  const { data: organizationData, isLoading } = useListOrganizationQuery();
  const totalUser = organizationData;

  const paginatedData = totalUser?.data;

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: Cell<IOrganizationInput>) => {
        // return <>{data.row.index + 1}</>;
        setAllDataList(data.row.index + (page - 1) * 10 + 1);
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: 'Organization Name',
      accessor: 'name'
    },
    {
      Header: '  Status',
      accessor: 'status',
      Cell: (data: any) => {
        return (
          <>
            {' '}
            <ActiveInactiveBadge isActive={data.row.original.status} />
          </>
        );
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={EditIcon}
              className="icons"
              onClick={() => onEditClicked(data.row.original)}
            />
            <img
              src={DeleteIcon}
              className="m-2 icons"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div>
      <NonPageDataTable
        columns={columns}
        data={paginatedData || []}
        tableHeaders={tableHeaders}
        isLoading={isLoading}
      />
    </div>
  );
};
export default TrainingManagementTable;
