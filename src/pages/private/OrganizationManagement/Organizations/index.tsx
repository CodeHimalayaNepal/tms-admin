import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import {
  IOrganizationInput,
  useCreateOrganizationMutation,
  useDeleteOrganizationMutation,
  useUpdateOrganizationMutation
} from 'redux/reducers/organizations';

import { organizationInitialValues, OrganizationSchemaValidations } from './schema';

import OrganizationForm from './OrganizationForm';
import OrganizationTable from './OrganizationsTable';

const Organizations = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [organizationInitialFormState, setOrganizationInitialForm] =
    useState(organizationInitialValues);
  const [deletingOrganizationState, setDeletingOrganizations] = useState<IOrganizationInput | null>(
    null
  );

  const [deleteOrganization, { isLoading: isDeleting }] = useDeleteOrganizationMutation();
  const [createOrganization, { isLoading: isCreating }] = useCreateOrganizationMutation();
  const [updateOrganization, { isLoading: isUpdating }] = useUpdateOrganizationMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: OrganizationSchemaValidations,
    initialValues: organizationInitialFormState,
    onSubmit: (values) => {
      let executeFunc = createOrganization;
      if (values.tms_organization_id) {
        executeFunc = updateOrganization;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setOrganizationInitialForm(organizationInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
    }
  });

  const onEditClicked = (data: IOrganizationInput) => {
    setOrganizationInitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingOrganizationState &&
      deleteOrganization(deletingOrganizationState?.tms_organization_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setOrganizationInitialForm(organizationInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IOrganizationInput) => {
    setDeletingOrganizations(data);
    toggleDeleteModal();
  };

  const closeModal = () => {
    setFormModal();
    setOrganizationInitialForm(organizationInitialValues);
  };

  return (
    <Container title="Organizations">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <OrganizationTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title="Create New Organization"
              iconName="ri-add-line align-bottom me-1"
              onClick={setFormModal}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={
            organizationInitialFormState.tms_organization_id
              ? 'Update Organization'
              : 'Create New Organization'
          }
          isModalOpen={formModal}
          closeModal={closeModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={closeModal}
                title="Close"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton
                title={organizationInitialFormState.tms_organization_id ? 'Update ' : 'Create '}
                iconName={`${
                  organizationInitialFormState.tms_organization_id ? '' : 'ri-add-line'
                } align-bottom me-1`}
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <OrganizationForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default Organizations;
