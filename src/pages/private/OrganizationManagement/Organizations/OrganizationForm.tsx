import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { IOrganizationInput } from 'redux/reducers/organizations';

interface ITrainingForm {
  formik: FormikProps<IOrganizationInput>;
}
const TrainingForm: FC<ITrainingForm> = ({ formik }) => {
  return (
    <form>
      <div className="row">
        {/*end col*/}
        <TextInput
          label="Title"
          name="name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.name}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="Location"
          name="location"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.location}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="Email"
          name="email"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.email}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="Phone no"
          name="phone_no"
          type="number"
          onChange={formik.handleChange}
          value={formik.values.phone_no}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              Status
            </label>
            <input
              type="checkbox"
              className="form-check-input"
              id="customSwitchsizesm"
              checked={formik.values.status}
              onChange={(e) => {
                formik.setFieldValue('status', !formik.values.status);
              }}
            />
          </div>
        </div>

        {/* <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              
            </label>
            <input type="checkbox" className="form-check-input" id="customSwitchsizesm" />
          </div>
        </div> */}
      </div>
    </form>
  );
};

export default TrainingForm;
