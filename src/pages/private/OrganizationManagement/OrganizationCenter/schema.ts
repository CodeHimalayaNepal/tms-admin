import { IOrganizationInput } from 'redux/reducers/organizations';
import { IOrganizationCentreInput } from 'redux/reducers/organizations-centre';

import * as Yup from 'yup';

export const organizationCentreInitialValues: IOrganizationCentreInput = {
  department: '' as any,
  name: '',
  resource_person: '',
  resource_email: '',
  resource_phone: '',
  status: true
};

export const OrganizationCentreSchemaValidations = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Name is too short!')
    .max(100, 'Name is too long!')
    .required('Title is required'),
  department: Yup.string().required('Department is required')
  // resource_person: Yup.string().required('Resource person name is required'),
  //   resource_phone: Yup.string()
  //     .required('Phone number is required')
  //     .min(10, 'Phone number is not valid')
  // .max(10, 'Phone number is not valid'),
  // resource_email: Yup.string().email('Invalid email').required('Email is required'),
  // status: Yup.boolean().required('Required')
  // department: Yup.string().required('Department is required')
});
