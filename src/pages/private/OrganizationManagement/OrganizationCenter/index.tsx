import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import {
  IOrganizationCentreInput,
  useCreateOrganizationCentreMutation,
  useDeleteOrganizationCentreMutation,
  useUpdateOrganizationCentreMutation
} from 'redux/reducers/organizations-centre';
import { OrganizationCentreSchemaValidations } from './schema';

import { organizationCentreInitialValues } from './schema';

import OrganizationCentreForm from './OrganizationCentreForm';
import OrganizationCentreTable from './OrganizationCentresTable';

const OrganizationCentres = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [organizationCentreInitialFormState, setOrganizationCentreInitialForm] = useState(
    organizationCentreInitialValues
  );
  const [deletingOrganizationCentreState, setDeletingOrganizationCentres] =
    useState<IOrganizationCentreInput | null>(null);

  const [deleteOrganizationCentre, { isLoading: isDeleting }] =
    useDeleteOrganizationCentreMutation();
  const [createOrganizationCentre, { isLoading: isCreating }] =
    useCreateOrganizationCentreMutation();
  const [updateOrganizationCentre, { isLoading: isUpdating }] =
    useUpdateOrganizationCentreMutation();
  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: OrganizationCentreSchemaValidations,
    initialValues: organizationCentreInitialFormState,
    onSubmit: (values) => {
      let executeFunc = createOrganizationCentre;
      if (values.tms_center_id) {
        executeFunc = updateOrganizationCentre;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          if (values.tms_center_id) {
            toast.success('Created Successfully');
          } else {
            toast.success('Created Successfully');
          }
          setOrganizationCentreInitialForm(organizationCentreInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
    }
  });

  const onEditClicked = (data: IOrganizationCentreInput) => {
    setOrganizationCentreInitialForm(data);
    setFormModal();
  };
  const onDeleteConfirm = () => {
    deletingOrganizationCentreState &&
      deleteOrganizationCentre(deletingOrganizationCentreState?.tms_center_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setOrganizationCentreInitialForm(organizationCentreInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IOrganizationCentreInput) => {
    setDeletingOrganizationCentres(data);
    toggleDeleteModal();
  };

  const closeModal = () => {
    setFormModal();
    setOrganizationCentreInitialForm(organizationCentreInitialValues);
  };
  return (
    <Container title="Department Centers" subTitle="List of all centers">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <OrganizationCentreTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title="Create Organization Center"
              iconName="ri-add-line align-bottom me-1"
              onClick={setFormModal}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={
            organizationCentreInitialFormState.tms_center_id
              ? 'Update Organization Center'
              : 'Create Organization Center'
          }
          isModalOpen={formModal}
          closeModal={closeModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                title="Close"
                onClick={() => setFormModal()}
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton
                title={organizationCentreInitialFormState.tms_center_id ? 'Update ' : 'Create'}
                iconName={`${
                  organizationCentreInitialFormState.tms_center_id ? '' : 'ri-add-line'
                } align-bottom me-1`}
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <OrganizationCentreForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default OrganizationCentres;
