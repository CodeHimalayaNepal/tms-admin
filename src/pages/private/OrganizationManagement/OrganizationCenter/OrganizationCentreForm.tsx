import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { useListOrganizationDepartmentQuery } from 'redux/reducers/organization-department';
import { IOrganizationInput } from 'redux/reducers/organizations';
import { IOrganizationCentreInput } from 'redux/reducers/organizations-centre';

interface IOrganizationCentreForm {
  formik: FormikProps<IOrganizationCentreInput>;
}
const OrganizationCentreForm: FC<IOrganizationCentreForm> = ({ formik }) => {
  const { data: departmentData } = useListOrganizationDepartmentQuery({ page: 1, pageSize: 10 });

  return (
    <form>
      <div className="row">
        <div className="col-md-6 mt-3">
          <div className="mb-3">
            <label htmlFor="trainingType-field" className="form-label">
              Department
            </label>
            <select
              id="trainingType-field"
              className="form-select"
              name="department"
              data-choices
              data-choices-sorting="true"
              value={formik?.values?.department}
              onChange={(e) => formik.setFieldValue('department', +e.target.value)}
            >
              <option selected value=""></option>
              {departmentData?.data?.map((item: any) => {
                return (
                  <>
                    <option value={+item.tms_department_id}>{item.name}</option>
                  </>
                );
              })}
            </select>
            {/* <FormikValidationError
              name="organization"
              errors={formik.errors}
              touched={formik.touched}
            /> */}
            {formik.errors && <span style={{ color: '#e74c3c' }}>{formik.errors.department}</span>}
          </div>
        </div>

        {/*end col*/}

        <TextInput
          label="Title"
          containerClassName="mt-3 col-lg-6  mb-2"
          name="name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.name}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              Status
            </label>
            <input
              type="checkbox"
              className="form-check-input"
              id="customSwitchsizesm"
              checked={formik.values.status}
              onChange={(e) => {
                formik.setFieldValue('status', !formik.values.status);
              }}
            />
          </div>
        </div>

        {/* <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              
            </label>
            <input type="checkbox" className="form-check-input" id="customSwitchsizesm" />
          </div>
        </div> */}
      </div>
    </form>
  );
};

export default OrganizationCentreForm;
