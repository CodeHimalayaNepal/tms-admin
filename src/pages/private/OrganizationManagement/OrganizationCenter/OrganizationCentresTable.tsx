import ActiveInactiveBadge from 'components/ui/ActiveInactiveBage/ActiveInactiveBadge';
import DataTable from 'components/ui/DataTable/data-table';
import React, { FC, useState } from 'react';
import EditIcon from '../../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../../assets/images/icons/delete.png';
import { Cell } from 'react-table';

import {
  IOrganizationCentreInput,
  useListOrganizationCentreQuery
} from 'redux/reducers/organizations-centre';
import { ITrainingInput, useListTrainingQuery } from 'redux/reducers/training';
import 'css/style.css';
interface ITOrganizationCentresTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: IOrganizationCentreInput) => void;
  onDeleteClicked: (data: IOrganizationCentreInput) => void;
}
const OrganizationCentresTable: FC<ITOrganizationCentresTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked
}) => {
  const { data: organizationData, isLoading } = useListOrganizationCentreQuery();
  const [page, setPage] = useState(1);
  const [allDataList, setAllDataList] = useState(0);
  const totalUser = organizationData?.data ?? [];
  const paginatedData = totalUser?.slice(
    page === 1 ? 0 : (page - 1) * 10,
    page === 1 ? 10 : page * 10
  );
  const canPrevious = page === 1 ? false : true;
  const canNext = page === Math.ceil(totalUser?.length / 10) ? false : true;

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: 'Organization Center',
      accessor: 'name'
    },
    {
      Header: '  Status',
      accessor: 'status',
      Cell: (data: any) => {
        return (
          <>
            <ActiveInactiveBadge isActive={data.row.original.status} />
          </>
        );
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={EditIcon}
              className="icons"
              onClick={() => onEditClicked(data.row.original)}
            />
            <img
              src={DeleteIcon}
              className="m-2 icons"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div>
      <DataTable
        columns={columns}
        data={paginatedData || []}
        tableHeaders={tableHeaders}
        isLoading={isLoading}
        pagination={{
          canPrevious,
          canNext,
          prevPage: () => {
            if (totalUser) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (totalUser) {
              totalUser?.length >= allDataList && setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default OrganizationCentresTable;
