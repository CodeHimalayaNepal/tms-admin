import { useNavigate } from 'react-router-dom';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import React, { useState } from 'react';
import { toast } from 'react-toastify';
import {
  useCreateEvaluationCriteriaMutation,
  useUpdateEvaluationCriteriaMutation,
  useDeleteEvaluationCriteriaMutation,
  IEvaluationCriteriaDetails
} from 'redux/reducers/evaluationCriteria';
import { IEvaluationFormInuput } from 'redux/reducers/evaluationCriteria';

import { evaluationCriteriaInitialValues, EvaluationCriteriaSchemaValidations } from './schema';

import EvaluationCriteriaForm from './EvaluationCriteriaForm';
import EvaluationCriteriaTable from './EvaluationCriteriaTable';

const EvaluationCriteria = () => {
  const navigate = useNavigate();
  const timerRef = React.useRef<NodeJS.Timeout>();
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [showQuery, setShowQuery] = useState('');
  const [searchQuery, setSearchQuery] = useState('');

  const [evaluationInitialFormState, setEvaluationInitialForm] = useState<IEvaluationFormInuput>(
    evaluationCriteriaInitialValues
  );

  const [deletingEvaluationState, setDeletingEvaluation] = useState<IEvaluationFormInuput | null>(
    null
  );

  const [deleteEvaluation, { isLoading: isDeleting }] = useDeleteEvaluationCriteriaMutation();
  const [createEvaluation, { isLoading: isCreating }] = useCreateEvaluationCriteriaMutation();
  const [updateEvaluation, { isLoading: isUpdating }] = useUpdateEvaluationCriteriaMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: EvaluationCriteriaSchemaValidations,
    initialValues: evaluationInitialFormState,

    onSubmit: (values, { resetForm }) => {
      let executeFunc = createEvaluation;
      if (values.evaluation_criteria_id) {
        executeFunc = updateEvaluation;
      }
      if (values.parent === 0) {
        delete values.parent;
      }
      //{ ...values, parent: values?.parent === 0 ? null : values.parent }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setEvaluationInitialForm(evaluationCriteriaInitialValues);
          resetForm();
          setFormModal();
        })
        .catch((err: any) => {
          toast.error('Error');
          const errorKeys = Object.keys(err.data);
          console.log('errr', err.data.errors);
          errorKeys.forEach((item) => {
            if (Array.isArray(err.data[item])) {
              err.data[item].length && toast.error(err.data[item][0]);
            } else {
              toast.error(err.data[item]);
            }
          });
        });
    }
  });

  const onEditClicked = (data: IEvaluationFormInuput) => {
    setEvaluationInitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingEvaluationState &&
      deleteEvaluation(deletingEvaluationState?.evaluation_criteria_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setEvaluationInitialForm(evaluationCriteriaInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IEvaluationFormInuput) => {
    setDeletingEvaluation(data);
    toggleDeleteModal();
  };

  const onView = (data: any) => {
    navigate('/EvaluationDetails/EvaluationCriteriaDetails', {
      state: {
        name: data.name,
        parent_detail: data.parent_detail,
        evaluation_criteria_id: data.evaluation_criteria_id
      }
    });
  };

  return (
    <Container title="Evaluation Criteria">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <div className="col-sm-auto">
        <div className="d-flex">
          <PrimaryButton
            title="Add Criteria"
            iconName="ri-add-line align-bottom me-1"
            onClick={() => {
              setFormModal();
              setEvaluationInitialForm(evaluationCriteriaInitialValues);
            }}
            type="button"
          />
          <div className="justify-content-lg-end" style={{ flexGrow: '1', display: 'flex' }}>
            <div className="search-box ms-2">
              <input
                type="text"
                className="form-control search"
                placeholder="Search..."
                name="key"
                value={showQuery}
                onChange={(e) => {
                  setShowQuery(e.target.value);
                  clearTimeout(timerRef.current);
                  timerRef.current = setTimeout(() => {
                    setSearchQuery(e.target.value);
                  }, 2000);
                }}
              />
              <i className="ri-search-line search-icon" />
            </div>
          </div>
        </div>
      </div>
      <EvaluationCriteriaTable
        searchQuery={searchQuery}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
        onViewClicked={onView}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={!formik.values.evaluation_criteria_id ? 'Add Criteria' : 'Edit Criteria'}
          isModalOpen={formModal}
          closeModal={setFormModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={setFormModal}
                title="Discard"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton title="Save" type="submit" isLoading={isUpdating || isCreating} />
            </div>
          )}
        >
          <EvaluationCriteriaForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default EvaluationCriteria;
