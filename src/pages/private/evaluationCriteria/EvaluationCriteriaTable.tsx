import DataTable from 'components/ui/DataTable/data-table';
import React, { FC, useState } from 'react';
import { IEvaluationFormInuput } from 'redux/reducers/evaluationCriteria';
import { useListEvaluationCriteriaQuery } from 'redux/reducers/evaluationCriteria';
import ViewIcon from '../../../assets/images/icons/view.png';
import EditIcon from '../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../assets/images/icons/delete.png';
import 'css/style.css';
interface ITraineeDataTable {
  searchQuery: string;
  onEditClicked: (data: IEvaluationFormInuput) => void;
  onDeleteClicked: (data: IEvaluationFormInuput) => void;
  onViewClicked: (data: IEvaluationFormInuput) => void;
}
const EvaluationCriteriaTable: FC<ITraineeDataTable> = ({
  searchQuery,
  onEditClicked,
  onDeleteClicked,
  onViewClicked
}) => {
  const [page, setPage] = useState(1);
  const allDataList = 0;
  const [pageSize, setPageSize] = useState(10);
  const {
    data: routineData,
    isLoading,
    isFetching,
    isSuccess
  } = useListEvaluationCriteriaQuery(searchQuery);
  const totalEvaluationCriteria = routineData?.data?.results ?? [];
  const paginatedData = totalEvaluationCriteria?.slice(
    page === 1 ? 0 : (page - 1) * 10,
    page === 1 ? 10 : page * 10
  );
  const canPrevious = page === 1 ? false : true;
  const canNext = page === Math.ceil(totalEvaluationCriteria?.length / 10) ? false : true;

  React.useEffect(() => {
    if (isSuccess) {
      setPage(1);
    }
  }, [isSuccess]);
  const columns: any = [
    {
      Header: 'Criteria Name',
      accessor: 'name'
    },
    {
      Header: ' Sub Criteria',
      accessor: 'Parents',
      Cell: (data: any) => {
        return <>{data.row.original?.parent_detail?.name}</>;
      }
    },

    {
      Header: 'Code',
      accessor: 'code'
    },

    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={EditIcon}
              className="icons"
              onClick={() =>
                onEditClicked({
                  ...data.row.original,
                  parent: data.row.original.parent_detail?.parent_id || null
                })
              }
            />
            <img
              src={DeleteIcon}
              className="m-2 icons"
              onClick={() => onDeleteClicked(data.row.original)}
            />
            <img
              src={ViewIcon}
              className="icons"
              onClick={() => onViewClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div className="list form-check-all">
      <DataTable
        columns={columns}
        isLoading={isFetching}
        search={false}
        data={paginatedData || []}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (totalEvaluationCriteria) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (totalEvaluationCriteria) {
              totalEvaluationCriteria?.length >= allDataList && setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default EvaluationCriteriaTable;
