import CustomSelect from 'components/ui/Editor/CustomSelect';
import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { FC } from 'react';
import { IEvaluationFormInuput } from 'redux/reducers/evaluationCriteria';
import { useListParentDetailsQuery } from 'redux/reducers/evaluationCriteria';

interface IEvaluationCriteriaForm {
  formik: FormikProps<IEvaluationFormInuput>;
}

const EvaluationCriteriaForm: FC<IEvaluationCriteriaForm> = ({ formik }) => {
  const { data: parentDetails } = useListParentDetailsQuery();
  const selectedEvaluationCriteria = parentDetails?.data?.map((item: any) => {
    return {
      value: item?.id?.toString(),
      label: item?.name
    };
  });

  return (
    <form>
      <div className="row">
        {/*end col*/}
        <TextInput
          label="Code"
          name="code"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.code}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="Criteria Name"
          name="name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.name}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <CustomSelect
          options={selectedEvaluationCriteria}
          placeholder={'Search by criteria'}
          value={formik.values.parent}
          onChange={(value: any) => formik.setFieldValue('parent', value)}
          label={'Sub Criteria'}
        />
      </div>
    </form>
  );
};

export default EvaluationCriteriaForm;
