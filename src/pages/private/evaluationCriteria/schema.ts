//import { IEvaluationInuput } from 'redux/reducers/evaluation';
import { IEvaluationFormInuput } from 'redux/reducers/evaluationCriteria';
import * as Yup from 'yup';

export const evaluationCriteriaInitialValues: IEvaluationFormInuput = {
  code: '',
  name: '',
  parent: 0
};

export const EvaluationCriteriaSchemaValidations = Yup.object().shape({
  code: Yup.string().required('Code is required'),
  name: Yup.string().required('Criteria Name is required')
});
