import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { IAttendanceInput } from 'redux/reducers/attendance';
import { useListOrganizationHallQuery } from 'redux/reducers/organization-halls';
import CustomSelect from 'components/ui/Editor/CustomSelect';
import { useListRoutineQuery } from 'redux/reducers/routine';
import { useFetchUserRoleQuery } from 'redux/reducers/administrator';
import moment from 'moment';
interface ITrainingForm {
  formik: FormikProps<IAttendanceInput>;
}

const TrainingForm: FC<ITrainingForm> = ({ formik }) => {
  const { data: organizationData } = useListOrganizationHallQuery();
  const { data: usersLists } = useFetchUserRoleQuery();
  const filteRoleCoodinator =
    usersLists?.staffs?.filter((word: any) => word.name.toUpperCase() === 'COORDINATOR') || [];
  const staffsUserLists = filteRoleCoodinator[0]?.staff?.map((item: any) => {
    return {
      value: item.id.toString(),
      label: item.first_name + ' ' + item.last_name
    };
  });
  return (
    <form>
      <div className="row">
        <FormikValidationError name="routine" errors={formik.errors} touched={formik.touched} />
        <TextInput
          label="User Pin"
          name="UserPin"
          type="number"
          onChange={formik.handleChange}
          value={formik.values.UserPin}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <FormikValidationError name="trainee" errors={formik.errors} touched={formik.touched} />

        <TextInput
          label="Device Sn"
          name="DeviceSn"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.DeviceSn}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <FormikValidationError name="source" errors={formik.errors} touched={formik.touched} />

        <TextInput
          label="Verify Time"
          name="VerifyTime"
          type="date"
          onChange={formik.handleChange}
          value={formik.values.VerifyTime}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <FormikValidationError name="hall" errors={formik.errors} touched={formik.touched} />
        <TextInput
          label="Reason"
          name="reason"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.reason}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <FormikValidationError name="reason" errors={formik.errors} touched={formik.touched} />
        <TextInput
          label="Check Out"
          name="check_out"
          type="date"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <FormikValidationError name="check_out" errors={formik.errors} touched={formik.touched} />
        <TextInput
          label="Check In"
          name="check_in"
          type="date"
          onChange={formik.handleChange}
          value={formik.values.check_in}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <FormikValidationError name="check_in" errors={formik.errors} touched={formik.touched} />
        <TextInput
          label="Requested Date"
          name="requested_date"
          type="date"
          onChange={formik.handleChange}
          value={formik.values.requested_date}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <FormikValidationError
          name="requested_date"
          errors={formik.errors}
          touched={formik.touched}
        />
        <TextInput
          label="Branch Code "
          name="BranchCode"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.BranchCode}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <FormikValidationError
          name="requested_date"
          errors={formik.errors}
          touched={formik.touched}
        />
        <TextInput
          label="Temperature "
          name="Temperature"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.Temperature}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <FormikValidationError
          name="requested_date"
          errors={formik.errors}
          touched={formik.touched}
        />

        <CustomSelect
          options={staffsUserLists}
          placeholder={'Search by name'}
          value={formik.values.approved_by}
          onChange={(value: any) => formik.setFieldValue('approved_by', value)}
          label={'Select Coordinator'}
        />
        <FormikValidationError name="approved_by" errors={formik.errors} touched={formik.touched} />
        <TextInput
          label="Approved Date"
          name="approved_date"
          type="date"
          onChange={formik.handleChange}
          value={formik.values.approved_date}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <FormikValidationError
          name="approved_date"
          errors={formik.errors}
          touched={formik.touched}
        />
        <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              Verify
            </label>
            <input
              type="checkbox"
              className="form-check-input"
              id="customSwitchsizesm"
              checked={formik.values.VerifyType}
              onChange={(e) => {
                formik.setFieldValue('VerifyType', !formik.values.VerifyType);
              }}
            />
          </div>
        </div>
        <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              Status
            </label>
            <input
              type="checkbox"
              className="form-check-input"
              id="customSwitchsizesm"
              checked={formik.values.status}
              onChange={(e) => {
                formik.setFieldValue('status', !formik.values.status);
              }}
            />
          </div>
        </div>

        {/* <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              
            </label>
            <input type="checkbox" className="form-check-input" id="customSwitchsizesm" />
          </div>
        </div> */}
      </div>
    </form>
  );
};

export default TrainingForm;
