import ActiveInactiveBadge from 'components/ui/ActiveInactiveBage/ActiveInactiveBadge';
import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import { useLocalStorage } from 'hooks/useStorage';
import React, { FC, useState } from 'react';
import EditIcon from '../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../assets/images/icons/delete.png';
import { IAttendanceInput, useListAttendanceQuery } from 'redux/reducers/attendance';
import { useListAdminUsersQuery, userApi } from 'redux/reducers/user';
import 'css/style.css';
interface ITraineeDataTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: IAttendanceInput) => void;
  onDeleteClicked: (data: IAttendanceInput) => void;
  startDate: moment.Moment;
  endDate: moment.Moment;
}
const AttendanceTable: FC<ITraineeDataTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked,
  startDate,
  endDate
}) => {
  const start_date = startDate.isSame(endDate) ? '' : startDate.format('YYYY-MM-DD');
  const end_date = endDate.isSame(startDate) ? '' : endDate.format('YYYY-MM-DD');
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data: adminUsers, isLoading: adminLoading } = useListAdminUsersQuery();
  const { data: attendanceData, isLoading } = useListAttendanceQuery({
    page: page,
    pageSize: pageSize,
    start_date,
    end_date
  });

  const totalUser = attendanceData?.results?.data ?? [];
  const canPrevious = attendanceData?.previous;
  const canNext = attendanceData?.next;

  const columns = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: 'User',
      accessor: (row: any) =>
        row.user_details
          ? `${row.user_details?.[0]?.first_name} ${row.user_details?.[0]?.last_name}`
          : ''
    },
    {
      Header: 'Check in',

      accessor: 'check_in'
    },
    {
      Header: 'Approved By',
      Cell: (data: any) => {
        if (data.row.original.approved_by) {
          const user = adminUsers?.find((val) => val.id === data.row.original.approved_by);
          return <>{user?.first_name + ' ' + user?.middle_name + ' ' + user?.last_name}</>;
        }
        return <></>;
      }
    },
    {
      Header: 'Approved Date',
      Cell: (data: any) => {
        return <>{data.row.original.approved_date ? data.row.original.approved_date : 'none'}</>;
      }
    },
    {
      Header: '  Status',
      accessor: 'status',
      Cell: (data: any) => {
        return (
          <>
            {' '}
            <ActiveInactiveBadge isActive={data.row.original.status} />
          </>
        );
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={EditIcon}
              className="icons"
              onClick={() =>
                onEditClicked({
                  ...data.row.original,
                  id: data.row.original
                })
              }
            />
            <img
              src={DeleteIcon}
              className="m-2 icons"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];

  return (
    <div>
      <DataTable
        columns={columns}
        data={totalUser || []}
        tableHeaders={tableHeaders}
        isLoading={isLoading}
        pagination={{
          canPrevious,
          canNext,
          prevPage: () => {
            if (canPrevious) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (canNext) {
              setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default AttendanceTable;
