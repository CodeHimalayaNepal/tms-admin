import { IAttendanceInput } from 'redux/reducers/attendance';

import * as Yup from 'yup';

export const attendanceInitialValues: IAttendanceInput = {
  UserPin: 0,
  VerifyType: false,
  Temperature: '',
  DeviceSn: '',
  BranchCode: '',
  VerifyTime: '',
  status: true,
  check_in: '',
  check_out: '',
  reason: '',
  requested_date: '',
  approved_by: 0,
  approved_date: ''
};

export const AttendanceSchemaValidations = Yup.object().shape({
  UserPin: Yup.number().min(2, 'Too Short!').required('Routine is required'),
  VerifyType: Yup.boolean().required('Trainee is required'),
  DeviceSn: Yup.string().min(2, 'Too Short!').required('Source is required'),
  VerifyTime: Yup.date().required('Check in is required'),
  BranchCode: Yup.string().min(2, 'Too Short!').required('Check in is required'),
  Temperature: Yup.string().min(2, 'Too Short!').required('Check in is required'),
  check_in: Yup.string().min(2, 'Too Short!').required('Check out is required'),
  reason: Yup.string().min(2, 'Too Short!').required('Reson is required'),
  requested_date: Yup.string().required('Requested Date is required'),
  approved_by: Yup.number().required('Approved by is required'),
  approved_date: Yup.string().required('Date is required'),
  status: Yup.boolean().required('status is required')
});
