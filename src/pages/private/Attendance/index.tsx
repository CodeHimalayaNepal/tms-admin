import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import React, { useState } from 'react';
import { toast } from 'react-toastify';
import {
  IAttendanceInput,
  useCreateAttendanceMutation,
  useUpdateAttendanceMutation,
  useDeleteAttendanceMutation
} from 'redux/reducers/attendance';
import { AiOutlineFilter } from 'react-icons/ai';

import { attendanceInitialValues, AttendanceSchemaValidations } from './schema';

import OrganizationForm from './AttendanceForm';
import AttendanceTable from './AttendanceTable';
import moment from 'moment';
import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { DateRangePicker } from 'react-date-range';

const Attendance = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [attendanceInitialFormState, setAttendancenitialForm] = useState(attendanceInitialValues);
  const [deletingAttendanceState, setDeletingAttendance] = useState<IAttendanceInput | null>(null);

  const [deleteAttendance, { isLoading: isDeleting }] = useDeleteAttendanceMutation();
  const [createAttendance, { isLoading: isCreating }] = useCreateAttendanceMutation();
  const [updateAttendance, { isLoading: isUpdating }] = useUpdateAttendanceMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: AttendanceSchemaValidations,
    initialValues: attendanceInitialFormState,
    onSubmit: (values) => {
      let executeFunc = createAttendance;
      if (values.attendance_id) {
        executeFunc = updateAttendance;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setAttendancenitialForm(attendanceInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
    }
  });
  const [show, setShow] = useState(false);

  const selectionRange = {
    startDate: new Date(),
    endDate: new Date(),
    key: 'selection'
  };

  const [state, setState] = useState<Array<typeof selectionRange>>([selectionRange]);
  const handleSelect = (date: any) => {
    setState([date.selection]);
  };
  const start_date = moment(state[0].startDate);
  const end_date = moment(state[0].endDate);

  React.useEffect(() => {
    if (!start_date.isSame(end_date)) {
      setShow(false);
    }
  }, [state[0]]);

  const onEditClicked = (data: IAttendanceInput) => {
    setAttendancenitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingAttendanceState &&
      deleteAttendance(deletingAttendanceState?.attendance_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setAttendancenitialForm(attendanceInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IAttendanceInput) => {
    setDeletingAttendance(data);
    toggleDeleteModal();
  };

  return (
    <Container title="Attendance Management" subTitle="List of all attendance">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <AttendanceTable
        tableHeaders={() => (
          <div style={{ display: 'flex', justifyContent: 'end' }}>
            <div className="dropdown justify-content-end">
              <a aria-expanded="false">
                <AiOutlineFilter
                  size={30}
                  title="Filter by date range."
                  style={{
                    cursor: 'pointer'
                  }}
                  onClick={() => setShow(!show)}
                />
              </a>
              <ul
                style={{ display: show ? 'flex' : 'none' }}
                className="dropdown-menu fixed z-[10] position-fixed-important"
                aria-labelledby="dropdownMenuLink"
              >
                <DateRangePicker ranges={state} onChange={handleSelect} />
              </ul>
            </div>
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
        startDate={start_date}
        endDate={end_date}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title="Create New Attendance"
          isModalOpen={formModal}
          closeModal={setFormModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={setFormModal}
                title="Close"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton
                title="Add Attendance"
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <OrganizationForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default Attendance;
