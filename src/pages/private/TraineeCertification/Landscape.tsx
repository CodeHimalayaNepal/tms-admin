import React, { forwardRef, useRef } from 'react';
import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
// import logo from './images/logo.png'
import { useParams } from 'react-router-dom';
import { useGetCertificateDetailsQuery } from 'redux/reducers/certification';
import moment from 'moment';

interface IinputData {
  inputData: any;
}

const LandscapeCertificate = forwardRef<HTMLDivElement, IinputData>((props, ref) => {
  const { traineeId } = useParams<{ traineeId: any }>();
  const { id } = useParams<{ id: any }>();
  const { data: CertificateDetails } = useGetCertificateDetailsQuery({ id, traineeId });
  const details = CertificateDetails?.data;
  const routine = CertificateDetails?.data[0]?.certificate_routine_details;
  const template =
    CertificateDetails?.data[CertificateDetails?.data?.length - 1]?.certificate_template_details;
  const certificateLogo = template?.images[0]?.certificate_logo;
  const division =
    props.inputData.inputData.division == 1
      ? 'First'
      : props.inputData.inputData.division == 2
      ? 'Second'
      : 'Third';
  const status = props.inputData.inputData.status;
  const evaluatorList = CertificateDetails?.data[0]?.evaluator_list;
  const completion = props.inputData.inputData.status;
  const evaluator1 = props.inputData.inputData.evaluator1;
  const evaluator2 = props.inputData.inputData.evaluator2;
  const fisrtEvaluator = evaluatorList?.find((item: any) => item.id === evaluator1);
  const secondEvaluator = evaluatorList?.find((item: any) => item.id === evaluator2);
  const traineeDetails = CertificateDetails?.trainee_details[0];
  const trainee =
    traineeDetails?.first_name +
    ' ' +
    traineeDetails?.middle_name +
    ' ' +
    traineeDetails?.last_name;
  const date = moment(new Date()).format('YYYY/MM/DD');

  return (
    <div ref={ref}>
      <div
        className="printable"
        style={{
          width: '1400px',
          height: '800px',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          textAlign: 'center',
          border: '2px solid #d16b54'
        }}
      >
        <div
          style={{
            width: '1390px',
            height: '790px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
            border: '5px solid #d16b54'
          }}
        >
          <div
            style={{
              width: '1375px',
              height: '780px',
              border: '2px solid #d16b54'
            }}
          >
            <div className="row">
              <div className="col-2">
                <img
                  src={certificateLogo}
                  alt=""
                  style={{ width: '150px', height: '150px', objectFit: 'contain', margin: 'auto' }}
                />
              </div>

              <div className="col-8">
                <span
                  style={{
                    fontSize: '40px',
                    fontWeight: 'bold',
                    flexGrow: 1
                  }}
                >
                  {details[0]?.organization_details?.name}
                </span>
                <br />
                <span style={{ fontSize: '25px' }}>
                  <b>Public Service Training Department</b>
                </span>
                <br />
                <span style={{ fontSize: '25px' }}>
                  <b>{routine?.center}</b>
                </span>
                <br />
                <span style={{ fontSize: '20px' }}>
                  {details[0]?.organization_details?.location}
                </span>
              </div>
              <div className="col-2"></div>
            </div>
            <div style={{ textAlign: 'center', margin: '50px 0' }}>
              <p
                style={{
                  fontWeight: 600,
                  fontSize: '40px',
                  color: '#d96431'
                }}
              >
                CERTIFICATE
              </p>
              <p style={{ fontSize: '26px', color: 'red' }}>{trainee}</p>
              <p style={{ fontSize: '25px', color: '#395ea8' }}>
                {completion == 1
                  ? 'successfully completed the training programme '
                  : completion == 2
                  ? 'completed training programme'
                  : 'participated on training programme'}
              </p>
              <p
                style={{
                  fontWeight: 600,
                  fontSize: '40px',
                  color: '#d96431'
                }}
              >
                {routine?.training?.training_name}
              </p>
              <p style={{ fontSize: '25px', color: '#395ea8' }}>
                {status == 3
                  ? details[details?.length - 1]?.certificate_content
                  : `${
                      details[details?.length - 1]?.certificate_content
                    } and was placed in ${division} Division.`}
              </p>
            </div>
            <br />
            <div className="row mt-3">
              <div className="col-12 d-flex justify-content-around" style={{ marginTop: '60px' }}>
                <div style={{ width: '300px', paddingTop: '5px', borderTop: '1px dotted black' }}>
                  <h5>
                    {' '}
                    {fisrtEvaluator?.first_name} {fisrtEvaluator?.middle_name}{' '}
                    {fisrtEvaluator?.last_name}
                  </h5>
                </div>
                <div style={{ width: '300px', paddingTop: '5px', borderTop: '1px dotted black' }}>
                  <h5>
                    {' '}
                    {secondEvaluator?.first_name} {secondEvaluator?.middle_name}{' '}
                    {secondEvaluator?.last_name}
                  </h5>
                </div>
              </div>
              <div className="col-12 mt-0 pt-0 d-flex justify-content-around">
                <div style={{ width: '300px' }}>
                  <h6>{fisrtEvaluator?.current_position}</h6>
                </div>
                <div style={{ width: '300px' }}>
                  <p>{secondEvaluator?.current_position}</p>
                </div>
              </div>
            </div>
            <div>
              <div className="col-12 d-flex justify-content-end">
                <p style={{ marginRight: '5px' }}>Date of issuee: {date}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

const Landscape = (inputData: any) => {
  const ref = useRef<HTMLDivElement | null>(null);
  return (
    <>
      <div className="row">
        <div className="col-12 d-flex justify-content-center align-items-center">
          <LandscapeCertificate ref={ref} inputData={inputData} />
        </div>
      </div>
      <ReactToPrint
        content={() => ref.current}
        trigger={() => (
          <div className="row">
            <div className="col-12 d-flex justify-content-end">
              <button style={{ marginBottom: '10px' }}>Print</button>
            </div>
          </div>
        )}
      />
    </>
  );
};

export default Landscape;

LandscapeCertificate.displayName = 'LandscapeCertificate';
