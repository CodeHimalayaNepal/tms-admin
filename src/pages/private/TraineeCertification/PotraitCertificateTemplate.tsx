import React from 'react';
import { useParams } from 'react-router-dom';
import logo from './images/logo.png';
import { useViewTemplateByIdQuery } from 'redux/reducers/certification';

const PotraitCertificate = () => {
  const { templateId = null } = useParams<{ templateId: any }>();
  const { data, isLoading } = useViewTemplateByIdQuery(templateId);
  const signNo = data?.no_of_signature;

  return (
    <>
      <div className="row">
        <div className="col-12 d-flex justify-content-center align-items-center">
          <div
            //           //   ref={printRef}
            className="printable"
            style={{
              width: '800px',
              height: '1000px',
              // padding: "20
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              textAlign: 'center',
              border: '2px solid #d16b54'
            }}
          >
            <div
              style={{
                width: '775px',
                height: '980px',
                //   padding: "6
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                textAlign: 'center',
                border: '5px solid #d16b54'
              }}
            >
              <div
                style={{
                  // margin: "10px",
                  width: '765px',
                  height: '960px'
                  // border: '2px solid #d16b54'
                }}
              >
                <div className="row">
                  <div className="col-2">
                    <img src={logo} alt="" style={{ width: '150px' }} />
                  </div>

                  <div className="col-8">
                    <span
                      style={{
                        fontSize: '40px',
                        fontWeight: 'bold',
                        flexGrow: 1
                      }}
                    >
                      Nepal Administrative Staff College
                    </span>
                    <br />
                    <span style={{ fontSize: '25px' }}>
                      <b>Public Service Training Department</b>
                    </span>
                    <br />
                    <span style={{ fontSize: '25px' }}>
                      <b>Center of Induction Training</b>
                    </span>
                    <br />
                    <span style={{ fontSize: '20px' }}>Jawalakhel, Lalitpur, Nepal</span>
                  </div>
                  <div className="col-2">
                    {/* <img src={logo} alt="" style={{ width: "150px" }} /> */}
                  </div>
                </div>
                {/* <br />
            <br />
            <br /> */}
                <div style={{ textAlign: 'center', margin: '50px 0' }}>
                  <p
                    style={{
                      fontWeight: 600,
                      fontSize: '40px',
                      color: '#d96431'
                    }}
                  >
                    CERTIFICATE
                  </p>
                  <p style={{ fontSize: '25px', color: '#395ea8' }}>Sucessfully Completed</p>
                  <p
                    style={{
                      fontWeight: 600,
                      fontSize: '30px',
                      color: '#d96431',
                      paddingLeft: '20px',
                      paddingRight: '20px'
                    }}
                  >
                    35 <sup>th</sup> Basic Administration Training
                  </p>
                  <p
                    style={{
                      fontSize: '25px',
                      color: '#395ea8',
                      paddingLeft: '20px',
                      paddingRight: '20px'
                    }}
                  >
                    organised for Newly Appointed Class III Officers of the Government of Nepal from
                    2078 Falgun to 2079 Shrawan 06 (14 <sup>th</sup> Feburary - 22 <sup>nd</sup>{' '}
                    July, 2020 ).
                  </p>
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <div className="row">
                  <div className="col-12 d-flex justify-content-around">
                    {signNo == 3 ? (
                      <>
                        <div>
                          <p>_____________________</p>
                          <b>
                            <p>Er. Abinash Gaire</p>
                          </b>
                          <p>Senior Director of Studies</p>
                        </div>
                        <div>
                          <p>_____________________</p>
                          <b>
                            <p>Dr. Abinash Gaire</p>
                          </b>
                          <p>Executive Director</p>
                        </div>
                        <div>
                          <p>_____________________</p>
                          <b>
                            <p>Dr. Abinash Gaire</p>
                          </b>
                          <p>Executive Director</p>
                        </div>
                      </>
                    ) : (
                      <>
                        <div>
                          <p>_____________________</p>
                          <b>
                            <p>Er. Abinash Gaire</p>
                          </b>
                          <p>Senior Director of Studies</p>
                        </div>
                        <div>
                          <p>_____________________</p>
                          <b>
                            <p>Dr. Abinash Gaire</p>
                          </b>
                          <p>Executive Director</p>
                        </div>
                      </>
                    )}
                  </div>
                </div>
                <div className="row d-flex align-items-end" style={{ height: '100px' }}>
                  <div className="col-12 d-flex justify-content-end">
                    <p style={{ marginRight: '5px' }}>Date of issuree: 2079/04/06</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PotraitCertificate;
