import React, { FC, useState } from 'react';
import DataTable from 'components/ui/DataTable/data-table';
import { useListCertificateTemplateQuery } from 'redux/reducers/certification';
import { Cell } from 'react-table';
import { useNavigate } from 'react-router-dom';
import { useDeleteTemplateMutation } from 'redux/reducers/certification';
import { toast } from 'react-toastify';
import useToggle from 'hooks/useToggle';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';

interface ITemplateTable {
  tableHeaders: () => React.ReactNode;
  onViewClicked: (id: number) => void;
  onEditClicked: (id: number) => void;
  onDeleteClicked: (data: any) => void;
}

const TemplateTable: FC<ITemplateTable> = ({
  tableHeaders,
  onViewClicked,
  onEditClicked,
  onDeleteClicked
}) => {
  const { data: tableData, isLoading } = useListCertificateTemplateQuery();
  const [deleteTemplate, { isLoading: isDeleting }] = useDeleteTemplateMutation();
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [templateId, setTemplateId] = useState('');
  const navigate = useNavigate();

  const onDeleteConfirm = () => {
    deleteTemplate(templateId).then((data: any) => {
      if (data?.data?.status === 'Success') {
        toast.success(data?.data?.message);
        toggleDeleteModal();
      } else if (data?.error) {
        toast.error(data?.error?.data?.detail);
      }
    });
  };

  const columns = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{data.row.index + 1}</>;
      }
    },
    {
      Header: 'Template Name',
      accessor: 'template_name'
    },
    {
      Header: 'No. of Signature',
      accessor: 'no_of_signature'
    },
    {
      Header: 'No. of Slides',
      accessor: 'no_of_slides'
    },
    {
      Header: 'Template Type',
      accessor: 'templateType',
      Cell: (data: any) => {
        return <>{data.row.original.template_type == 1 ? 'Potrait' : 'Landscape'}</>;
      }
    },
    {
      Header: 'Action',
      Cell: (data: Cell<any & { id: number }>) => {
        return (
          <div style={{ display: 'flex' }}>
            <img
              style={{ marginRight: '10px', height: '20px', cursor: 'pointer' }}
              src={require('./images/view.png')}
              alt="icon for config"
              onClick={() => {
                navigate(`/create-certificate-template/view/${data.row.original.id}`);
              }}
            />
            {/* <img
              style={{ marginRight: '10px', height: '20px' }}
              src={require('./images/edit.png')}
              alt="icon for config"
              onClick={() => onDeleteClicked(``)}
            /> */}
            <img
              style={{ marginRight: '10px', height: '20px', cursor: 'pointer' }}
              src={require('./images/Vector.png')}
              alt="icon for config"
              onClick={() => {
                setTemplateId(data.row.original.id);
                toggleDeleteModal();
              }}
            />
          </div>
        );
      }
    }
  ];
  return (
    <>
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <DataTable columns={columns} data={tableData || []} isLoading={isLoading} />
    </>
  );
};
export default TemplateTable;
