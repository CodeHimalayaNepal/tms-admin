import Container from 'components/ui/Container';
import CustomSelect from 'components/ui/Editor/CustomSelect';
import PrimaryButton from 'components/ui/PrimaryButton';
import { useFormik } from 'formik';
import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { useGetCertificateDetailsQuery } from 'redux/reducers/certification';
import { useNavigate } from 'react-router-dom';
import Landscape from './Landscape';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useListTraineeQuery } from 'redux/reducers/auth';
import PotraitCertificate from './PotraitCertificateTemplate';
import Potrait from './Potrait';

const GenerateCertificate = () => {
  const navigate = useNavigate();
  const [inputData, setInputData] = useState({
    evaluator1: '',
    evaluator2: '',
    division: null,
    status: null
  });
  const { traineeId } = useParams<{ traineeId: any }>();
  const [showCertificate, setShowCertificate] = useState(false);
  const { id } = useParams<{ id: any }>();
  const { data: CertificateDetails } = useGetCertificateDetailsQuery({ id, traineeId });
  const templateType =
    CertificateDetails?.data[CertificateDetails?.data.length - 1]?.certificate_template_details
      ?.template_type;

  const { data: traineeDetails, isLoading } = useListTraineeQuery();
  var trainee = traineeDetails?.data?.find((item: any) => item.id == traineeId);

  const evaluatorOptions = CertificateDetails?.data[0]?.evaluator_list?.map(
    (x: any, index: any) => ({
      label: x.first_name + ' ' + x.middle_name + ' ' + x.last_name,
      value: x.id,
      key: index
    })
  );

  const divisionOptions = [
    {
      label: 'First',
      value: 1
    },
    {
      label: 'Second',
      value: 2
    },
    {
      label: 'Third',
      value: 3
    }
  ];
  const statusOptions = [
    {
      label: 'Successfully Completed',
      value: 1
    },
    {
      label: 'Completed',
      value: 2
    },
    {
      label: 'Participated',
      value: 3
    }
  ];
  const initialValues = {
    evaluator1: '',
    evaluator2: '',
    division: null,
    status: null
  };
  const formik = useFormik({
    enableReinitialize: true,
    initialValues,
    onSubmit: function (values) {
      setShowCertificate(!showCertificate);
      setInputData({
        evaluator1: values.evaluator1,
        evaluator2: values.evaluator2,
        division: values.division,
        status: values.status
      });
    }
  });

  return (
    <Container title="Generate Certificate">
      {/* <h1>Here you find Certificate</h1>
       */}
      <div className="row">
        <form onSubmit={formik.handleSubmit}>
          <div className="row">
            <CustomSelect
              name="division"
              label={'Division'}
              options={divisionOptions}
              onChange={(value: any) => formik.setFieldValue('division', value)}
              value={formik.values.division}
              onBlur={formik.handleBlur}
              formik={formik}
            />

            <CustomSelect
              name="status"
              label={'Status'}
              options={statusOptions}
              onChange={(value: any) => formik.setFieldValue('status', value)}
              value={formik.values.status}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <CustomSelect
              name="evaluator1"
              label={'Signature 1'}
              options={evaluatorOptions}
              onChange={(value: any) => formik.setFieldValue('evaluator1', value)}
              value={formik.values.evaluator1}
              onBlur={formik.handleBlur}
              formik={formik}
              // isMulti={true}
            />
            <CustomSelect
              name="evaluator2"
              label={'Signature 2'}
              options={evaluatorOptions}
              onChange={(value: any) => formik.setFieldValue('evaluator2', value)}
              value={formik.values.evaluator2}
              onBlur={formik.handleBlur}
              formik={formik}
              // isMulti={true}
            />
          </div>
          <div className="row mt-4">
            <div className="col-8">
              {showCertificate ? (
                <SecondaryButton title="Hide" onClick={() => setShowCertificate(false)} />
              ) : (
                <PrimaryButton type="submit" title="Generate" />
              )}
            </div>
          </div>
        </form>
      </div>
      {showCertificate ? (
        <div className="row mt-4">
          {templateType == '2' ? (
            <Landscape inputData={inputData} trainee={trainee} />
          ) : (
            <Potrait inputData={inputData} trainee={trainee} />
          )}
        </div>
      ) : (
        ''
      )}
    </Container>
  );
};
export default GenerateCertificate;
