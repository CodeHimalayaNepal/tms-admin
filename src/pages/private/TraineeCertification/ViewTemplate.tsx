import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import { useParams } from 'react-router-dom';
import { useViewTemplateByIdQuery } from 'redux/reducers/certification';
import LandScapeCertificate from './LandScapeCertificateTemplate';
import PotraitCertificate from './PotraitCertificateTemplate';

const ViewTemplate = () => {
  const { templateId = null } = useParams<{ templateId: any }>();
  const { data: template, isLoading } = useViewTemplateByIdQuery(templateId);

  const Potrait = template?.template_type == 1;

  return (
    <>
      <Container title="View Template">
        {Potrait ? <PotraitCertificate /> : <LandScapeCertificate />}
      </Container>
    </>
  );
};

export default ViewTemplate;
