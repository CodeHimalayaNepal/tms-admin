import React from 'react';
import Container from 'components/ui/Container';
import CertificateTemplateForm from './CertificateTemplateForm';

const TraineeCertification = () => {
  return <CertificateTemplateForm />;
};

export default TraineeCertification;
