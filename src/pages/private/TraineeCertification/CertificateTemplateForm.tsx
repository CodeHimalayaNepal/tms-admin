import CustomSelect from 'components/ui/Editor/CustomSelect';
import TextInput from 'components/ui/TextInput';
import { FileDrop } from 'components/ui/FileDrop/FileDrop';
import { CertificateFormInitValues } from './Schema';
import { useFormik } from 'formik';
import React, { useState } from 'react';
import PrimaryButton from 'components/ui/PrimaryButton';
import { useCreateTemplateMutation } from 'redux/reducers/certification';
import Container from 'components/ui/Container';
import TemplateTable from './TemplateTable';
import { toast } from 'react-toastify';

const CertificateTemplateForm = () => {
  const [CertificateTemplateForm, setCertificateTemplateForm] = useState(CertificateFormInitValues);
  const [submitTemplate, { isLoading: isCreating }] = useCreateTemplateMutation();

  const templateList = [
    {
      value: '1',
      label: 'Potrait'
    },
    {
      value: '2',
      label: 'Landscape'
    }
  ];

  const signList = [
    {
      value: 2,
      label: '2'
    }
  ];
  const slideList = [
    {
      value: '1',
      label: '1'
    }
  ];
  const onViewClicked = () => {};

  const onEditClicked = () => {};

  const onDeleteClicked = () => {};
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: CertificateFormInitValues,
    onSubmit: (values) => {
      setCertificateTemplateForm(CertificateTemplateForm);
      submitTemplate(values).then((data: any) => {
        if (data?.error) {
          toast.error('Error!');
        }
        toast.success('Certificate Template Created Successfully.');
        formik.resetForm();
      });
    }
  });
  return (
    <>
      <Container title="Create Certificate Template">
        <form onSubmit={formik.handleSubmit}>
          <div className="row">
            <TextInput
              name="template_name"
              label={'Template Name'}
              onChange={formik.handleChange}
              value={formik.values.template_name}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <CustomSelect
              name="template_type"
              options={templateList}
              label={'Select Template Type'}
              onChange={(value: any) => formik.setFieldValue('template_type', value)}
              value={formik.values.template_type}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <CustomSelect
              name="no_of_signatures"
              options={signList}
              label={'Select number of Signatures'}
              onChange={(value: any) => formik.setFieldValue('no_of_signature', value)}
              value={formik.values.no_of_signature}
              onBlur={formik.handleBlur}
              formik={formik}
            />
            <CustomSelect
              name="no_of_slides"
              options={slideList}
              label={'Select number of Slides'}
              onChange={(value: any) => formik.setFieldValue('no_of_slides', value)}
              value={formik.values.no_of_slides}
              onBlur={formik.handleBlur}
              formik={formik}
            />
          </div>
          <div className="row mt-4">
            <div className="col-3">
              <FileDrop
                files={formik.values.uploaded_images}
                setFiles={(data) => formik.setFieldValue('uploaded_images', data)}
                message="Click to upload the logo"
                multiple={true}
                accept={{ 'image/png': ['.png', '.jgp', '.jpeg', '.gif'] }}
                preview={true}
              />
            </div>
            <div className="col-9"></div>
          </div>
          <div className="row mt-4 mb-4  d-flex justify-content-end">
            <div className="col-12 d-flex justify-content-start">
              <PrimaryButton type="submit" title="Generate" />
            </div>
          </div>
        </form>
        <hr />
        <div className="table">
          <TemplateTable
            tableHeaders={submitTemplate}
            onViewClicked={onViewClicked}
            onEditClicked={onEditClicked}
            onDeleteClicked={onDeleteClicked}
          />
        </div>
      </Container>
    </>
  );
};

export default CertificateTemplateForm;
