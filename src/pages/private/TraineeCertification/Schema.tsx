export const CertificateFormInitValues = {
  template_name: '',
  template_type: '',
  no_of_signature: 0,
  no_of_slides: '',
  uploaded_images: [] as File[]
};
