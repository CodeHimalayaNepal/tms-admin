import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import {
  IAttendanceRequestInput,
  useCreateAttendanceMutation,
  useUpdateAttendanceMutation,
  useDeleteAttendanceMutation,
  useApproveAttendanceMutation
} from 'redux/reducers/attendance/attendanceRequest';

import { attendanceInitialValues, AttendanceSchemaValidations } from './schema';

import AttendanceRequestForm from './AttendanceRequestForm';
import AttendanceRequestTable from './AttendanceRequestTable';
import { useCurrentUserDetailsQuery } from 'redux/reducers/administrator';

const AttendanceRequest = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [approvalModal, toggleApprovalModal] = useToggle(false);

  const [attendanceInitialFormState, setAttendancenitialForm] = useState(attendanceInitialValues);
  const [deletingAttendanceState, setDeletingAttendance] = useState<
    (IAttendanceRequestInput & { attendance_id?: string }) | null
  >(null);
  const [approveAttendanceState, setApproveAttendance] = useState<
    | (IAttendanceRequestInput & {
        user_details?: {
          first_name: string;
        }[];
        attendance_id?: string;
      })
    | null
  >(null);

  const [deleteAttendance, { isLoading: isDeleting }] = useDeleteAttendanceMutation();
  const [createAttendance, { isLoading: isCreating }] = useCreateAttendanceMutation();
  const [updateAttendance, { isLoading: isUpdating }] = useUpdateAttendanceMutation();
  const [approveAttendance, { isLoading: isApproving }] = useApproveAttendanceMutation();
  const { data: userDetails } = useCurrentUserDetailsQuery();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: AttendanceSchemaValidations,
    initialValues: attendanceInitialFormState,
    onSubmit: (values) => {
      let executeFunc = createAttendance;
      if (values.id) {
        executeFunc = updateAttendance;
      }
      executeFunc({ ...values, user: userDetails?.data?.user_details?.id })
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setAttendancenitialForm(attendanceInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
    }
  });

  const onEditClicked = (data: IAttendanceRequestInput) => {
    setAttendancenitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingAttendanceState &&
      deleteAttendance(deletingAttendanceState?.attendance_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setAttendancenitialForm(attendanceInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };
  const onDeleteClicked = (data: IAttendanceRequestInput) => {
    setDeletingAttendance(data);
    toggleDeleteModal();
  };

  const onApproveAttendance = async () => {
    if (approveAttendanceState) {
      try {
        await approveAttendance(approveAttendanceState.attendance_id).unwrap();
        toast.success('Attendance approved successfully');
        toggleApprovalModal();
      } catch (error) {
        toast.success('Attendance approved successfully');
        toggleApprovalModal();
        // toast.error('Error');
      }
    }
  };

  const onApproveClicked = (data: IAttendanceRequestInput) => {
    setApproveAttendance(data);
    toggleApprovalModal();
  };

  return (
    <Container title="Requested Attendance" subTitle="List of requested attendance">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>

      <ConfirmationModal
        isOpen={approvalModal}
        onClose={toggleApprovalModal}
        onConfirm={onApproveAttendance}
        title="Approve Trainee's Attendance"
        isLoading={isApproving}
        btnText="Approve"
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>
              Are you sure you want to approve{' '}
              {approveAttendanceState?.user_details?.[0]?.first_name} attendance?
            </h4>
          </div>
        </>
      </ConfirmationModal>

      <AttendanceRequestTable
        tableHeaders={() => (
          <div>
            {/* <PrimaryButton
              title="Request Attendance"
              iconName="ri-add-line align-bottom me-1"
              onClick={setFormModal}
              type="button"
            /> */}
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
        onApproveClicked={onApproveClicked}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title="Create New Attendance"
          isModalOpen={formModal}
          closeModal={setFormModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={setFormModal}
                title="Close"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton
                title="Add Attendance"
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <AttendanceRequestForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default AttendanceRequest;
