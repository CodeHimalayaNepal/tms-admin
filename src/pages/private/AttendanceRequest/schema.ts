import { IAttendanceRequestInput } from 'redux/reducers/attendance/attendanceRequest';

import * as Yup from 'yup';
import moment from 'moment';
const date = new Date();
export const attendanceInitialValues: IAttendanceRequestInput = {
  check_in: '',
  check_out: '',
  reason: '',
  approved_by: 0,
  source: 'web'
};

export const AttendanceSchemaValidations = Yup.object().shape({
  check_in: Yup.string().min(2, 'Too Short!').required('Check in is required'),
  check_out: Yup.string().min(2, 'Too Short!').required('Check out is required'),
  reason: Yup.string().min(2, 'Too Short!').required('Reason is required'),
  source: Yup.string(),
  approved_by: Yup.string()
});
