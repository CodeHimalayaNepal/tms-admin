import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import {
  IAttendanceRequestInput,
  useListAttendanceQuery
} from 'redux/reducers/attendance/attendanceRequest';

interface ITraineeDataTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: IAttendanceRequestInput) => void;
  onDeleteClicked: (data: IAttendanceRequestInput) => void;
  onApproveClicked: (data: IAttendanceRequestInput) => void;
}
const AttendanceRequestTable: FC<ITraineeDataTable> = ({
  tableHeaders,
  onDeleteClicked,
  onApproveClicked
}) => {
  const { data: attendanceData, isLoading } = useListAttendanceQuery();
  const totalUser = attendanceData?.data ?? [];
  const [page, setPage] = useState(1);
  const [allDataList, setAllDataList] = useState(0);
  const paginatedData = totalUser?.slice(
    page === 1 ? 0 : (page - 1) * 10,
    page === 1 ? 10 : page * 10
  );
  const canPrevious = page === 1 ? false : true;
  const canNext = page === Math.ceil(totalUser?.length / 10) ? false : true;

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: 'User',
      accessor: (row: any) =>
        row.user_details
          ? `${row.user_details?.[0]?.first_name} ${row.user_details?.[0]?.last_name}`
          : ''
    },
    {
      Header: 'Check in',
      accessor: 'check_in'
    },
    {
      Header: 'Attendance PIN',
      accessor: 'user'
    },
    {
      Header: 'DeviceSn',
      accessor: (data: any) => {
        return <>{data?.row?.original?.source === 'web' ? '' : 'Web'}</>;
      }
    },
    {
      Header: 'Approved By',
      Cell: (data: any) => {
        return <>{data.row.original.approved_date ? data.row.original.approved_by : 'Pending'}</>;
      }
    },
    {
      Header: 'Approved Date',
      Cell: (data: any) => {
        return <>{data.row.original.approved_date ? data.row.original.approved_date : 'one'}</>;
      }
    },

    {
      Header: '  Status',
      accessor: 'status',
      Cell: (data: any) => {
        return (
          <>
            <p style={{ color: 'red ' }}>{data.row.original.status ? 'Verified' : 'Unverified'}</p>
          </>
        );
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <PrimaryButton
              margin="m-2"
              title="Approve"
              type="button"
              small="btn-sm"
              onClick={() =>
                onApproveClicked({
                  ...data.row.original
                })
              }
            />

            <PrimaryButton
              isDanger
              title="Reject"
              small="btn-sm"
              type="button"
              className="m-2"
              onClick={() => {
                console.log('deletedddd');
                onDeleteClicked(data.row.original);
              }}
            />
          </>
        );
      }
    }
  ];
  return (
    <div>
      <DataTable
        columns={columns}
        data={paginatedData || []}
        tableHeaders={tableHeaders}
        isLoading={isLoading}
        pagination={{
          canPrevious,
          canNext,
          prevPage: () => {
            if (totalUser) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (totalUser) {
              totalUser?.length >= allDataList && setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default AttendanceRequestTable;
