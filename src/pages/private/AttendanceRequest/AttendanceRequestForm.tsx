import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { IAttendanceRequestInput } from 'redux/reducers/attendance/attendanceRequest';
import { useListOrganizationHallQuery } from 'redux/reducers/organization-halls';
import CustomSelect from 'components/ui/Editor/CustomSelect';
import { useListRoutineQuery } from 'redux/reducers/routine';
import { useFetchUserRoleQuery } from 'redux/reducers/administrator';
interface IAttendanceRequest {
  formik: FormikProps<IAttendanceRequestInput>;
}

const AttendanceRequest: FC<IAttendanceRequest> = ({ formik }) => {
  const { data: organizationData } = useListOrganizationHallQuery();
  const { data: usersLists } = useFetchUserRoleQuery();
  const filteRoleCoodinator =
    usersLists?.staffs?.filter((word: any) => word.name.toUpperCase() === 'COORDINATOR') || [];
  const staffsUserLists = filteRoleCoodinator[0]?.staff?.map((item: any) => {
    return {
      value: item.id.toString(),
      label: item.first_name + ' ' + item.last_name
    };
  });
  return (
    <form>
      <div className="row">
        {/* <FormikValidationError name="UserPin" errors={formik.errors} touched={formik.touched} /> */}
        {/* <FormikValidationError name="user" errors={formik.errors} touched={formik.touched} /> */}

        {/* <FormikValidationError name="reason" errors={formik.errors} touched={formik.touched} /> */}

        <TextInput
          label="Reason"
          name="reason"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.reason}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        {/* <FormikValidationError name="check_out" errors={formik.errors} touched={formik.touched} /> */}
        <TextInput
          label="Check In"
          name="check_in"
          type="time"
          onChange={formik.handleChange}
          value={formik.values.check_in}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <TextInput
          label="Check Out"
          name="check_out"
          type="time"
          onChange={formik.handleChange}
          value={formik.values.check_out}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        {/* <FormikValidationError name="check_out" errors={formik.errors} touched={formik.touched} /> */}

        {/* <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              
            </label>
            <input type="checkbox" className="form-check-input" id="customSwitchsizesm" />
          </div>
        </div> */}
      </div>
    </form>
  );
};

export default AttendanceRequest;
