import { tokenRef } from 'common/constant';
import { useLocalStorage } from 'hooks/useStorage';
import { Navigate, Outlet } from 'react-router-dom';
import Spinner from 'components/ui/Spinner/spinner';
import Footer from './Footer';
import Header from './Header';
import SideNavBar from './SideNavBar';
import useWidth from '../../../hooks/useWidth';
import { useEffect } from 'react';

import {
  useCurrentUserDetailsQuery,
  useListAdministratorQuery
} from 'redux/reducers/administrator';
import useToggle from 'hooks/useToggle';

const SideBar = () => {
  const [isMobileWidth] = useWidth();
  const [sidebarToggle, setsidebarToggle] = useToggle(!isMobileWidth);
  const { data: userDetails, isLoading } = useCurrentUserDetailsQuery();
  const { data: adminData, isLoading: isListingAdministrators } = useListAdministratorQuery();
  const { value: token } = useLocalStorage(tokenRef);

  useEffect(() => {
    if (isMobileWidth) {
      setsidebarToggle(false);
    } else {
      setsidebarToggle(true);
    }
  }, [isMobileWidth]);

  const handleToggle = () => {
    isMobileWidth && setsidebarToggle();
  };
  if (!token) {
    return <Navigate to="/sign-in" replace={true} />;
  }

  return (
    <div>
      <div className="App">
        <div id="layout-wrapper">
          <Header toggle={handleToggle} />
          <SideNavBar sidebarToggle={sidebarToggle} toggle={handleToggle} />

          {isLoading ? (
            <>
              <div className="position-absolute top-50 start-50 translate-middle">
                <Spinner />{' '}
              </div>
            </>
          ) : (
            <Outlet />
          )}
          <Footer />
        </div>
      </div>
    </div>
  );
};

export default SideBar;
