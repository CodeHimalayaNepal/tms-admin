import CustomNavlink from 'components/ui/CustomNavlink';
import NestedNavLink from 'components/ui/NestedNavLink';
import Spinner from 'components/ui/Spinner/spinner';
import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import {
  useCurrentUserDetailsQuery,
  useListAdministratorQuery
} from 'redux/reducers/administrator';
import { privateRoutes } from 'routes/private';
import Logo from '../../../assets/images/logo-narc.png';
const SideNavBar = ({ sidebarToggle, toggle }: { sidebarToggle: boolean; toggle: () => void }) => {
  const navigate = useNavigate();
  const { data: userDetails, isLoading } = useCurrentUserDetailsQuery();
  const { data: adminData, isLoading: isListingAdministrators } = useListAdministratorQuery();
  const currentUserRole = adminData?.results?.find(
    (item: any) => item.id === userDetails?.data?.role_details?.id
  );
  const isAdmin = 'Supervisor' === currentUserRole?.name || true;

  const filterRoute = privateRoutes[0].children.filter((item: any) => isAdmin || item.isForTrainee);
  const navigatefunc = () => {
    navigate('/');
  };
  return (
    <div>
      <div
        className="app-menu navbar-menu"
        style={{
          marginLeft: sidebarToggle ? '0%' : '-100%',
          transition: 'all 0.8s ease'
        }}
      >
        <div
          className="navbar-brand-box py-3"
          style={{
            display: sidebarToggle ? 'flex' : 'flex',
            paddingLeft: sidebarToggle ? '75px' : '75px'
          }}
        >
          {/* <a href="index.html" className="logo logo-light"> */}
          <span className="logo-lg">
            <div onClick={() => navigatefunc()}>
              <img src={Logo} alt="" style={{ height: 100, borderRadius: '50%' }} />
            </div>
          </span>
          {/* </a> */}
          <button
            type="button"
            className="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover"
            id="vertical-hover"
          >
            <i className="ri-record-circle-line" />
          </button>
        </div>

        <div id="scrollbar" style={{ height: 'calc(100vh - 150px)', overflowY: 'auto' }}>
          <div className="container-fluid">
            <div id="two-column-menu"></div>
            {
              // isLoading || isListingAdministrators ? (
              //   <Spinner />
              // ) :
              <ul className="navbar-nav">
                {filterRoute
                  ?.filter((item: any) => !item.hideInSideBar)
                  .map((item: any, index: number) => {
                    if (item?.children) {
                      return (
                        <NestedNavLink
                          isDivider={item.isDivider}
                          routes={item}
                          id={index.toString()}
                          toggle={toggle}
                        />
                      );
                    }
                    return (
                      <CustomNavlink isDivider={item.isDivider} routes={item} toggle={toggle} />
                    );
                  })}
              </ul>
            }
          </div>
        </div>
      </div>
    </div>
  );
};

export default SideNavBar;
