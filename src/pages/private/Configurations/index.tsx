import Container from 'components/ui/Container';
import React from 'react';

const Configurations = () => {
  return <Container title="Configurations" />;
};

export default Configurations;
