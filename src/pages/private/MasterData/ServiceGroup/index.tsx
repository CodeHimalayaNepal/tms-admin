import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import { useNavigate } from 'react-router-dom';
import MasterDataTable from './MasterDataTable';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import useToggle from 'hooks/useToggle';
import { useDeleteServiceMutation } from 'redux/reducers/master';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { IServiceGroup, ServiceGroupInitialValues } from './Schema';

const ServiceGroup = () => {
  const navigate = useNavigate();
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [serviceInitialFormState, setServiceInitialForm] = useState(ServiceGroupInitialValues);
  const [deletingServiceState, setDeletingService] = useState<IServiceGroup>();

  const [deleteService, { isLoading: isDeletingService }] = useDeleteServiceMutation();

  const onEditClicked = (id: any) => {
    navigate(`/master-data/service-group/edit/${id}`);
  };

  const onDeleteConfirm = () => {
    if (!deletingServiceState?.id) return;
    deletingServiceState &&
      deleteService(+deletingServiceState?.id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setServiceInitialForm(ServiceGroupInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };
  const onDeleteClicked = (data: any) => {
    setDeletingService(data);
    toggleDeleteModal();
  };

  return (
    <Container title="Service Group">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        isLoading={isDeletingService}
        title="Delete Confirmations"
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <MasterDataTable
        tableHeaders={() => (
          <>
            <div>
              <PrimaryButton
                title={'Create Service Group'}
                iconName="ri-add-line align-bottom me-1"
                onClick={() => {
                  navigate('/master-data/service-group/create');
                }}
                type="button"
              />
            </div>
          </>
        )}
        onDeleteClicked={onDeleteClicked}
        onEditClicked={onEditClicked}
      />
    </Container>
  );
};

export default ServiceGroup;
