import * as Yup from 'yup';
export interface IServiceGroup {
  name: string;
  status: boolean;
  id?: string;
}

export const ServiceGroupInitialValues: IServiceGroup = {
  name: '',
  status: false
};

export const ServiceGroupSchemaValidations = Yup.object().shape({
  name: Yup.string().required('Service Group Name is required.').nullable()
});
