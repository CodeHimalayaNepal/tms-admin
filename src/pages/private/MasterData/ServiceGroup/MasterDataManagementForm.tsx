import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { ServiceGroupInitialValues } from './Schema';
import React, { FC } from 'react';
import { useGetServiceGroupByIdQuery } from 'redux/reducers/master';
import { useParams } from 'react-router-dom';

interface IServiceGroupForm {
  formik: FormikProps<typeof ServiceGroupInitialValues>;
}
const MasterDataManagementForm: FC<IServiceGroupForm> = ({ formik }) => {
  const { id } = useParams();

  const { data: serviceGroupData, isSuccess } = useGetServiceGroupByIdQuery(id ?? '');

  React.useEffect(() => {
    if (isSuccess) {
      formik.resetForm({
        values: {
          name: serviceGroupData?.data?.[0]?.name,
          status: serviceGroupData?.data?.[0]?.status
        }
      });
    }
  }, [isSuccess]);
  return (
    <form>
      <div className="row">
        <TextInput
          label="Name"
          formik={formik}
          name="name"
          onChange={formik.handleChange}
          value={formik.values.name}
          onBlur={formik.handleBlur}
        />
        <div className=" mt-3 col-12 mb-2">
          <div className="form-check form-switch form-switch-md">
            <label className="form-check-label" htmlFor="attendance_type">
              Service Status
            </label>
            <input
              type="checkbox"
              className="form-check-input"
              checked={formik.values.status}
              id="attendance_type"
              onChange={(e) => formik.setFieldValue('status', e.target.checked ? true : false)}
            />
          </div>
        </div>
      </div>
    </form>
  );
};
export default MasterDataManagementForm;
