import DataTable from 'components/ui/DataTable/data-table';
import { FC, useState } from 'react';
import { BiLastPage } from 'react-icons/bi';
import { useListServiceQuery } from 'redux/reducers/master';
import EditIcon from '../../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../../assets/images/icons/delete.png';
import { useParams } from 'react-router-dom';
import ServiceGroupBadge from 'components/ui/ServiceGroupBadge';

interface ITMasterDataTable {
  tableHeaders: () => React.ReactNode;
  onDeleteClicked: (data: any) => void;
  onEditClicked: (data: any) => void;
}

const MasterDataTable: FC<ITMasterDataTable> = ({
  tableHeaders,
  onDeleteClicked,
  onEditClicked
}) => {
  const [page, setPage] = useState(1);
  const { data: masterData, isLoading } = useListServiceQuery();
  const [allDataList, setAllDataList] = useState(0);
  const totalUser = masterData ?? [];
  const paginatedData = totalUser?.slice(
    page === 1 ? 0 : (page - 1) * 10,
    page === 1 ? 10 : page * 10
  );

  const canPrevious = page === 1 ? false : true;
  const canNext = page === Math.ceil(totalUser?.length / 10) ? false : true;

  const columns = [
    {
      Header: 'Name',
      accessor: 'name'
    },
    {
      Header: 'Status',
      accessor: (data: any) => {
        return <ServiceGroupBadge status={data?.status} />;
      }
    },
    {
      Header: 'Action',
      accessor: (data: any) => {
        return (
          <>
            <img src={EditIcon} className="icons" onClick={() => onEditClicked(data?.id)} />
            <img src={DeleteIcon} className="m-2 icons" onClick={() => onDeleteClicked(data)} />
          </>
        );
      }
    }
  ];
  return (
    <div className="list form-check-all">
      <DataTable
        columns={columns}
        data={paginatedData || []}
        tableHeaders={tableHeaders}
        isLoading={isLoading}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (totalUser) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (totalUser) {
              totalUser?.length >= allDataList && setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};
export default MasterDataTable;
