import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import MasterDataManagementForm from './MasterDataManagementForm';
import { useFormik } from 'formik';
import { ServiceGroupInitialValues, ServiceGroupSchemaValidations } from './Schema';
import { useNavigate, useParams } from 'react-router-dom';
import { useCreateServiceMutation, useUpdateServiceMutation } from 'redux/reducers/master';
import { toast } from 'react-toastify';

const MasterDetailsPage = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [createServiceGroup, { isLoading: isCreating }] = useCreateServiceMutation();
  const [updateServiceGroup, { isLoading: isUpdating }] = useUpdateServiceMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: ServiceGroupSchemaValidations,
    initialValues: ServiceGroupInitialValues,
    onSubmit: async function (values) {
      let executeFunc = createServiceGroup;
      let sendValue = { ...values };
      if (id) {
        executeFunc = updateServiceGroup;
        sendValue = { ...values, id: id };
      }
      try {
        await executeFunc(sendValue);
        toast.success(`Service Group ${id ? 'Updated' : 'Created'} Successfully`);
        navigate(`/master-data/service-group`);
      } catch (err) {
        console.log(err);
      }
    }
  });
  return (
    <Container title={id ? 'Service Group Update' : 'Service Group Create'}>
      <form onSubmit={formik.handleSubmit}>
        <MasterDataManagementForm formik={formik} />
        <div className="hstack gap-2 justify-content-end">
          <SecondaryButton title="Cancel" onClick={() => navigate('/master-data/service-group')} />
          <PrimaryButton
            title={id ? 'Update' : 'Create'}
            iconName="ri-add-line align-bottom me-1"
            type="submit"
            isLoading={id ? isUpdating : isCreating}
          />
        </div>
      </form>
    </Container>
  );
};
export default MasterDetailsPage;
