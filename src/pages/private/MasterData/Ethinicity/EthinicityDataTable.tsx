import DataTable from 'components/ui/DataTable/data-table';
import ServiceGroupBadge from 'components/ui/ServiceGroupBadge';
import EditIcon from '../../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../../assets/images/icons/delete.png';
import { FC, useState } from 'react';
import { useListEthnicityQuery } from 'redux/reducers/master';

interface ITEthinicityDataTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: any) => void;
  onDeleteClicked: (data: any) => void;
}

const EthinicityDataTable: FC<ITEthinicityDataTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const { data: ethnicityData, isLoading } = useListEthnicityQuery({ page, pageSize });

  const canNext = ethnicityData?.next;
  const canPrevious = ethnicityData?.previous;

  const columns = [
    {
      Header: 'Ethnicity Name',
      accessor: 'name'
    },
    {
      Header: 'Status',
      accessor: (data: any) => {
        return <ServiceGroupBadge status={data?.status} />;
      }
    },
    {
      Header: 'Action',
      accessor: (data: any) => {
        return (
          <>
            <img src={EditIcon} className="icons" onClick={() => onEditClicked(data?.id)} />
            <img src={DeleteIcon} className="m-2 icons" onClick={() => onDeleteClicked(data)} />
          </>
        );
      }
    }
  ];
  return (
    <div className="list form-check-all">
      <DataTable
        columns={columns}
        data={ethnicityData?.results || []}
        tableHeaders={tableHeaders}
        isLoading={isLoading}
        pagination={{
          canNext: !!canNext,
          canPrevious: !!canPrevious,
          prevPage: () => {
            if (canPrevious) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (canNext) {
              setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};
export default EthinicityDataTable;
