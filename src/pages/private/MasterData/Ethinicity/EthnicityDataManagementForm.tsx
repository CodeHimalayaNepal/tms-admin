import { EthnicityInitialValues } from './Schema';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { useParams } from 'react-router-dom';
import FormikValidationError from 'components/ui/FormikErrors';
import { useGetEthnicityByIdQuery } from 'redux/reducers/master';

interface IEthnicitytGroupForm {
  formik: FormikProps<typeof EthnicityInitialValues>;
}

const EthnicityDataManagementForm: FC<IEthnicitytGroupForm> = ({ formik }) => {
  const { id } = useParams();

  const { data: ethnicityDetails, isLoading, isSuccess } = useGetEthnicityByIdQuery(id ?? '');

  React.useEffect(() => {
    if (isSuccess) {
      formik.resetForm({
        values: {
          name: ethnicityDetails?.data?.[0]?.name,
          status: ethnicityDetails?.data?.[0]?.status
        }
      });
    }
  }, [isSuccess]);

  return (
    <form>
      <div className="row">
        <TextInput
          label="Name"
          formik={formik}
          name="name"
          onChange={formik.handleChange}
          value={formik.values.name}
          onBlur={formik.handleBlur}
        />

        <div className=" mt-3 col-12 mb-2">
          <div className="form-check form-switch form-switch-md">
            <label className="form-check-label" htmlFor="attendance_type">
              Service Status
            </label>
            <input
              type="checkbox"
              className="form-check-input"
              checked={formik.values.status}
              id="attendance_type"
              onChange={(e) => formik.setFieldValue('status', e.target.checked ? true : false)}
            />
            <FormikValidationError
              name="ethnicity_status"
              errors={formik.errors}
              touched={formik.touched}
            />
          </div>
        </div>
      </div>
    </form>
  );
};
export default EthnicityDataManagementForm;
