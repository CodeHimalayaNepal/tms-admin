import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import { useCreateEthnicityMutation, useUpdateEthnicityMutation } from 'redux/reducers/master';
import { EthnicityInitialValues, EthnicitySchemaValidations } from './Schema';
import { toast } from 'react-toastify';
import { useNavigate, useParams } from 'react-router-dom';
import EthnicityDataManagementForm from './EthnicityDataManagementForm';

const EthnicityDetailsPage = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [createEthnicityGroup, { isLoading: isCreating }] = useCreateEthnicityMutation();
  const [updateEthnicityGroup, { isLoading: isUpdating }] = useUpdateEthnicityMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: EthnicitySchemaValidations,
    initialValues: EthnicityInitialValues,
    onSubmit: (values) => {
      if (id) {
        updateEthnicityGroup({ ...values, id: id }).then((data: any) => {
          toast.success(data?.data?.message);
          navigate('/master-data/ethnicity');
        });
      } else {
        createEthnicityGroup(values).then((data: any) => {
          toast.success(data?.data?.message);
          navigate('/master-data/ethnicity');
        });
      }
    }
  });

  return (
    <Container title={id ? 'Ethnicity Update' : 'Ethnicity Create'}>
      <form onSubmit={formik.handleSubmit}>
        <EthnicityDataManagementForm formik={formik} />
        <div className="hstack gap-2 justify-content-end">
          <SecondaryButton title="Cancel" onClick={() => navigate('/master-data/ethnicity')} />
          <PrimaryButton
            title={id ? 'Update' : 'Create'}
            iconName="ri-add-line align-bottom me-1"
            type="submit"
            isLoading={id ? isUpdating : isCreating}
          />
        </div>
      </form>
    </Container>
  );
};
export default EthnicityDetailsPage;
