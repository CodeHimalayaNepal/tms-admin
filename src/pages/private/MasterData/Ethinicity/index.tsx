import Container from 'components/ui/Container';
import EthinicityDataTable from './EthinicityDataTable';
import PrimaryButton from 'components/ui/PrimaryButton';
import { useNavigate } from 'react-router-dom';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import useToggle from 'hooks/useToggle';
import { IEthnicityGroup, EthnicityInitialValues } from './Schema';
import { useState } from 'react';
import { useDeleteEthnicityMutation } from 'redux/reducers/master';
import { toast } from 'react-toastify';

const Ethnicity = () => {
  const navigate = useNavigate();
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [ethnicityInitialFormState, setEthnicityInitialForm] = useState(EthnicityInitialValues);
  const [deletingEthnicityState, setDeletingEthnicity] = useState<IEthnicityGroup>();
  const [deleteEthnicity, { isLoading: isDeletingEthnicity }] = useDeleteEthnicityMutation();

  const onEditClicked = (id: any) => {
    navigate(`/master-data/ethnicity/edit/${id}`);
  };

  const onDeleteConfirm = () => {
    if (!deletingEthnicityState?.id) return;
    deletingEthnicityState &&
      deleteEthnicity(+deletingEthnicityState?.id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setEthnicityInitialForm(EthnicityInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };
  const onDeleteClicked = (data: any) => {
    setDeletingEthnicity(data);
    toggleDeleteModal();
  };
  return (
    <Container title="Ethnicity">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        isLoading={isDeletingEthnicity}
        title="Delete Confirmations"
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>

      <EthinicityDataTable
        tableHeaders={() => (
          <>
            <div>
              <PrimaryButton
                title={'Create Ethnicity'}
                iconName="ri-add-line align-bottom me-1"
                onClick={() => {
                  navigate('/master-data/ethnicity/create');
                }}
                type="button"
              />
            </div>
          </>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
      />
    </Container>
  );
};
export default Ethnicity;
