import * as Yup from 'yup';

export interface IEthnicityGroup {
  name: string;
  status: boolean;
  id?: string;
}

export const EthnicityInitialValues: IEthnicityGroup = {
  name: '',
  status: false
};

export const EthnicitySchemaValidations = Yup.object().shape({
  name: Yup.string().required('Ethinicity Name is required').nullable()
});
