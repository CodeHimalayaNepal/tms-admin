import { Outlet } from 'react-router-dom';

const MasterData = () => {
  return (
    <div>
      <Outlet />
    </div>
  );
};

export default MasterData;
