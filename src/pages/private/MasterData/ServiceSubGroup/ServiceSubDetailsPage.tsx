import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import { useNavigate, useParams } from 'react-router-dom';
import {
  useCreateServiceSubGroupMutation,
  useUpdateServiceSubGroupMutation
} from 'redux/reducers/master';
import { ServiceSubGroupInitialValues, ServiceSubGroupSchemaValidation } from './Schema';
import ServiceSubGroupManagementForm from './ServiceSubGroupManagementForm';
import { toast } from 'react-toastify';

const ServiceSubDetailsPage = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [createServiceSubGroup, { isLoading: isCreating }] = useCreateServiceSubGroupMutation();
  const [updateServiceSubGroup, { isLoading: isUpdating }] = useUpdateServiceSubGroupMutation();
  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: ServiceSubGroupSchemaValidation,
    initialValues: ServiceSubGroupInitialValues,
    onSubmit: async function (values) {
      let executeFunc = createServiceSubGroup;
      let sendValue = { ...values };
      if (id) {
        executeFunc = updateServiceSubGroup;
        sendValue = { ...values, id: id };
      }
      try {
        await executeFunc(sendValue);
        toast.success(`Service Group ${id ? 'Updated' : 'Created'} Successfully`);
        navigate(`/master-data/service-sub-group`);
      } catch (err) {
        console.log(err);
      }
    }
  });
  return (
    <Container title={id ? 'Service Sub Group Update' : 'Service Sub Group Create'}>
      <form onSubmit={formik.handleSubmit}>
        <ServiceSubGroupManagementForm formik={formik} />
        <div className="hstack gap-2 justify-content-end">
          <SecondaryButton
            title="Cancel"
            onClick={() => navigate('/master-data/service-sub-group')}
          />
          <PrimaryButton
            title={id ? 'Update' : 'Create'}
            iconName="ri-add-line align-bottom me-1"
            type="submit"
            isLoading={id ? isUpdating : isCreating}
          />
        </div>
      </form>
    </Container>
  );
};
export default ServiceSubDetailsPage;
