import * as Yup from 'yup';

export interface IServiceSubGroup {
  name: string;
  service_group_master_id: string;
  id?: string;
}

export const ServiceSubGroupInitialValues: IServiceSubGroup = {
  name: '',
  service_group_master_id: '',
  id: ''
};

export const ServiceSubGroupSchemaValidation = Yup.object().shape({
  name: Yup.string().required('Service Sub Group Name is required').nullable()
});
