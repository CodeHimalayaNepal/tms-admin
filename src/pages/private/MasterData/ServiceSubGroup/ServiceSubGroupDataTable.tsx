import DataTable from 'components/ui/DataTable/data-table';
import EditIcon from '../../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../../assets/images/icons/delete.png';
import { FC, useState } from 'react';
import { useListServiceSubGroupQuery } from 'redux/reducers/master';

interface ITServiceSubGroupTableData {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: any) => void;
  onDeleteClicked: (data: any) => void;
}

const ServiceSubGroupDataTable: FC<ITServiceSubGroupTableData> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked
}) => {
  const [page, setPage] = useState(1);
  const { data: serviceSubGroupData, isLoading } = useListServiceSubGroupQuery();
  const [allDataList, setAllDataList] = useState(0);
  const totalServiceSubGroup = serviceSubGroupData?.data ?? [];
  const paginatedData = totalServiceSubGroup?.slice(
    page === 1 ? 0 : (page - 1) * 10,
    page === 1 ? 10 : page * 10
  );
  const canPrevious = page === 1 ? false : true;
  const canNext = page === Math.ceil(totalServiceSubGroup?.length / 10) ? false : true;

  const columns = [
    {
      Header: 'Service Sub Group Name',
      accessor: 'name'
    },
    {
      Header: 'Service Group ',
      accessor: 'service_group_master.name'
    },
    {
      Header: 'Action',
      accessor: (data: any) => {
        return (
          <>
            <img src={EditIcon} className="icons" onClick={() => onEditClicked(data?.id)} />
            <img src={DeleteIcon} className="m-2 icons" onClick={() => onDeleteClicked(data)} />
          </>
        );
      }
    }
  ];
  return (
    <div className="list form-check-all">
      <DataTable
        columns={columns}
        data={paginatedData || []}
        tableHeaders={tableHeaders}
        isLoading={isLoading}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (totalServiceSubGroup) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (totalServiceSubGroup) {
              totalServiceSubGroup?.length >= allDataList && setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};
export default ServiceSubGroupDataTable;
