import Container from 'components/ui/Container';
import ServiceSubGroupDataTable from './ServiceSubGroupDataTable';
import { useNavigate } from 'react-router-dom';
import PrimaryButton from 'components/ui/PrimaryButton';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { IServiceSubGroup, ServiceSubGroupInitialValues } from './Schema';
import { useDeleteServiceSubGroupMutation } from 'redux/reducers/master';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';

const ServiceSubGroup = () => {
  const navigate = useNavigate();
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [serviceSubInitialFormState, setServiceSubInitialForm] = useState(
    ServiceSubGroupInitialValues
  );
  const [deletingServiceSubState, setDeletingServiceSub] = useState<IServiceSubGroup>();
  const [deleteServiceSubGroup, { isLoading: isDeletingServiceSubGroup }] =
    useDeleteServiceSubGroupMutation();

  const onEditClicked = (id: any) => {
    navigate(`/master-data/service-sub-group/edit/${id}`);
  };

  const onDeleteConfirm = () => {
    if (!deletingServiceSubState?.id) return;
    deletingServiceSubState &&
      deleteServiceSubGroup(+deletingServiceSubState?.id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setServiceSubInitialForm(ServiceSubGroupInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };
  const onDeleteClicked = (data: any) => {
    setDeletingServiceSub(data);
    toggleDeleteModal();
  };
  return (
    <>
      <Container title="Service Sub Group">
        <ConfirmationModal
          isOpen={deleteModal}
          onClose={toggleDeleteModal}
          onConfirm={onDeleteConfirm}
          isLoading={isDeletingServiceSubGroup}
          title="Delete Confirmations"
        >
          <>
            <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
              <h4>Are you Sure ?</h4>
              <p className="text-muted ">You want to Remove this Record ?</p>
            </div>
          </>
        </ConfirmationModal>
        <ServiceSubGroupDataTable
          tableHeaders={() => (
            <div>
              <PrimaryButton
                title={'Create Service Sub Group'}
                iconName="ri-add-line align-bottom me-1"
                onClick={() => {
                  navigate('/master-data/service-sub-group/create');
                }}
                type="button"
              />
            </div>
          )}
          onDeleteClicked={onDeleteClicked}
          onEditClicked={onEditClicked}
        />
      </Container>
    </>
  );
};
export default ServiceSubGroup;
