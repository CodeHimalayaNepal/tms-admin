import TextInput from 'components/ui/TextInput';
import { ServiceSubGroupInitialValues } from './Schema';
import { FormikProps } from 'formik';
import { FC } from 'react';
import CustomSelect from 'components/ui/Editor/CustomSelect';
import { useListServiceQuery } from 'redux/reducers/master';

interface IServiceSubGroupForm {
  formik: FormikProps<typeof ServiceSubGroupInitialValues>;
}

const ServiceSubGroupManagementForm: FC<IServiceSubGroupForm> = ({ formik }) => {
  const { data: ServiceGroup } = useListServiceQuery();
  const ServiceGroupList = ServiceGroup?.map((item: any) => {
    return {
      value: item.id,
      label: item.name
    };
  });

  return (
    <form>
      <div className="row">
        <CustomSelect
          name="service_group_master_id"
          placeholder="Service Group"
          options={ServiceGroupList}
          label={'Service Group'}
          value={formik.values.service_group_master_id}
          onChange={(e: any) => formik.setFieldValue('service_group_master_id', e)}
        />
        <TextInput
          label="Service Sub Group Name"
          formik={formik}
          name="name"
          onChange={formik.handleChange}
          value={formik.values.name}
          onBlur={formik.handleBlur}
        />
      </div>
    </form>
  );
};
export default ServiceSubGroupManagementForm;
