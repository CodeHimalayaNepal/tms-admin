import React from 'react';
import { Outlet } from 'react-router-dom';

const FormsCreations = () => {
  return (
    <div>
      <Outlet />
    </div>
  );
};

export default FormsCreations;
