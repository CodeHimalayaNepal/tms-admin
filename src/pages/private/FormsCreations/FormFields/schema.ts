import { IFormFieldsInput } from 'redux/reducers/form-fields';
import { IOrganizationInput } from 'redux/reducers/organizations';
import { IOrganizationCentreInput } from 'redux/reducers/organizations-centre';

import * as Yup from 'yup';

export const organizationCentreInitialValues: IFormFieldsInput = {
  fieldname: '',
  status: true,
  assign_field: 'TRAINEE',
  field_type: 0,
  is_required: true,
  order_by: 0,
  option: []
};

export const FormFieldSchemaValidations = Yup.object().shape({
  fieldname: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Title is required'),
  assign_field: Yup.string().required('Phone number is required'),

  status: Yup.boolean().required('Required'),
  is_required: Yup.boolean().required('Required'),

  field_type: Yup.number().required('department is required'),
  order_by: Yup.number().required('department is required')
});
