import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import {
  IFormFieldsInput,
  useCreateFormFieldsMutation,
  useDeleteFormFieldsMutation,
  useUpdateFormFieldsMutation
} from 'redux/reducers/form-fields';

import { organizationCentreInitialValues, FormFieldSchemaValidations } from './schema';

import FormFieldsForm from './FormFieldsForm';
import FormFieldsTable from './FormFieldsTable';

const FormFields = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [organizationCentreInitialFormState, setFormFieldsInitialForm] = useState(
    organizationCentreInitialValues
  );
  const [deletingFormFieldsState, setDeletingFormFieldss] = useState<IFormFieldsInput | null>(null);

  const [deleteFormFields, { isLoading: isDeleting }] = useDeleteFormFieldsMutation();
  const [createFormFields, { isLoading: isCreating }] = useCreateFormFieldsMutation();
  const [updateFormFields, { isLoading: isUpdating }] = useUpdateFormFieldsMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: FormFieldSchemaValidations,
    initialValues: organizationCentreInitialFormState,
    onSubmit: (values) => {
      let executeFunc = createFormFields;
      if (values.dynamic_field_id) {
        executeFunc = updateFormFields;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setFormFieldsInitialForm(organizationCentreInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
    }
  });

  const onEditClicked = (data: IFormFieldsInput) => {
    setFormFieldsInitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingFormFieldsState &&
      deleteFormFields(deletingFormFieldsState?.dynamic_field_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setFormFieldsInitialForm(organizationCentreInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IFormFieldsInput) => {
    setDeletingFormFieldss(data);
    toggleDeleteModal();
  };

  return (
    <Container title="Form fields">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <FormFieldsTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title="Create New Form fields"
              iconName="ri-add-line align-bottom me-1"
              onClick={setFormModal}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title="Create New Form fields"
          isModalOpen={formModal}
          closeModal={setFormModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={setFormModal}
                title="Close"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton
                title="Add Form fields"
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <FormFieldsForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default FormFields;
