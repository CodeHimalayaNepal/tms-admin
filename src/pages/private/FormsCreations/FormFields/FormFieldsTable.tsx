import ActiveInactiveBadge from 'components/ui/ActiveInactiveBage/ActiveInactiveBadge';
import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import { FaRegEdit } from 'react-icons/fa';
import { RiDeleteBin5Line } from 'react-icons/ri';
import { IFormFieldsInput } from 'redux/reducers/form-fields';

import { useListFormFieldsQuery } from 'redux/reducers/form-fields';

interface ITFormFieldsTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: IFormFieldsInput) => void;
  onDeleteClicked: (data: IFormFieldsInput) => void;
}
const FormFieldsTable: FC<ITFormFieldsTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const { data: formFieldsData } = useListFormFieldsQuery({ page, pageSize });

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{data.row.index + 1}</>;
      }
    },
    {
      Header: 'Fields Name',
      accessor: 'fieldname'
    },
    {
      Header: 'Dynamic Form for',
      accessor: 'assign_field'
    },

    {
      Header: 'Order',
      accessor: 'order_by'
    },
    {
      Header: 'Status',
      accessor: 'status',
      Cell: (data: any) => {
        return (
          <>
            {' '}
            <ActiveInactiveBadge isActive={data.row.original.status} />
          </>
        );
      }
    },
    {
      Header: 'Is Required?',
      accessor: 'is_required',
      Cell: (data: any) => {
        return <>{data.row.original.is_required ? 'Yes' : 'No'}</>;
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <FaRegEdit
              size={20}
              onClick={() =>
                onEditClicked({
                  ...data.row.original,
                  field_type: data.row.original.field_type_details.id,
                  option: data.row.original.option_details
                })
              }
            />
            <RiDeleteBin5Line
              size={20}
              className="m-2"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div>
      <DataTable columns={columns} data={formFieldsData || []} tableHeaders={tableHeaders} />
    </div>
  );
};

export default FormFieldsTable;
