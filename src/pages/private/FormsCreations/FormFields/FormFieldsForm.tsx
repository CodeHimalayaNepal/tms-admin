import CustomSelect from 'components/ui/Editor/CustomSelect';
import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { IFormFieldsInput } from 'redux/reducers/form-fields';
import { useListFormTypeQuery } from 'redux/reducers/form-types';
import { useListOrganizationDepartmentQuery } from 'redux/reducers/organization-department';
import { IOrganizationInput } from 'redux/reducers/organizations';
import { IOrganizationCentreInput } from 'redux/reducers/organizations-centre';

interface IFormFieldForm {
  formik: FormikProps<IFormFieldsInput>;
}
const FormFieldForm: FC<IFormFieldForm> = ({ formik }) => {
  const { data: formTypeFields } = useListFormTypeQuery();

  const formFieldType =
    formTypeFields?.data.map((item: any) => {
      return {
        value: item.id,
        label: item.title
      };
    }) || [];

  return (
    <form>
      <div className="row">
        <CustomSelect
          options={formFieldType}
          placeholder={'Search by title'}
          value={formik.values.field_type}
          onChange={(value: any) => formik.setFieldValue('field_type', value)}
          isMulti={false}
          label={'Select Field Type'}
        />
        {/*end col*/}
        <TextInput
          label="Title"
          name="fieldname"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.fieldname}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <CustomSelect
          options={[
            { label: 'Trainee', value: 'TRAINEE' },
            { label: 'Assignment', value: 'ASSIGNMENT' }
          ]}
          placeholder={'Search by title'}
          value={formik.values.assign_field}
          onChange={(value: any) => formik.setFieldValue('assign_field', value)}
          isMulti={false}
          label={'Assign Field to'}
        />
        <TextInput
          label="Order"
          name="order_by"
          type="number"
          onChange={formik.handleChange}
          value={formik.values.order_by}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <div className="row">
          <div className="col-4 mb-3 mt-4 d-flex align-items-end">
            <div className="form-check form-switch form-switch-md mb-3">
              <label className="form-check-label" htmlFor="customSwitchsizesm">
                Status
              </label>
              <input
                type="checkbox"
                className="form-check-input"
                id="customSwitchsizesm"
                checked={formik.values.status}
                onChange={(e) => {
                  formik.setFieldValue('status', !formik.values.status);
                }}
              />
            </div>
          </div>

          <div className="col-4 mb-3 mt-4 d-flex align-items-end">
            <div className="form-check form-switch form-switch-md mb-3">
              <label className="form-check-label" htmlFor="customSwitchsizesm">
                Is Required?
              </label>
              <input
                type="checkbox"
                className="form-check-input"
                id="customSwitchsizesm"
                checked={formik.values.is_required}
                onChange={(e) => {
                  formik.setFieldValue('is_required', !formik.values.is_required);
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default FormFieldForm;
