import { IFormTypeInput } from 'redux/reducers/form-types';
import { IOrganizationInput } from 'redux/reducers/organizations';
import { IOrganizationCentreInput } from 'redux/reducers/organizations-centre';

import * as Yup from 'yup';

export const formTypeInitialValues: IFormTypeInput = {
  is_required: true,
  title: ''
};

export const FormTypeSchemaValidations = Yup.object().shape({
  title: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Title is required'),
  is_required: Yup.boolean().required('Required')
});
