import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC } from 'react';
import { IFormTypeInput } from 'redux/reducers/form-types';
import { useListOrganizationDepartmentQuery } from 'redux/reducers/organization-department';
import { IOrganizationInput } from 'redux/reducers/organizations';
import { IOrganizationCentreInput } from 'redux/reducers/organizations-centre';

interface IOrganizationCentreForm {
  formik: FormikProps<IFormTypeInput>;
}
const OrganizationCentreForm: FC<IOrganizationCentreForm> = ({ formik }) => {
  const { data: departmentData } = useListOrganizationDepartmentQuery({ page: 1, pageSize: 10 });
  return (
    <form>
      <div className="row">
        {/*end col*/}
        <TextInput
          label="Title"
          name="title"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.title}
          onBlur={formik.handleBlur}
          formik={formik}
          containerClassName="col-md-12"
        />
        <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              IsRequired
            </label>
            <input
              type="checkbox"
              className="form-check-input"
              id="customSwitchsizesm"
              checked={formik.values.is_required}
              onChange={(e) => {
                formik.setFieldValue('is_required', !formik.values.is_required);
              }}
            />
          </div>
        </div>

        {/* <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              
            </label>
            <input type="checkbox" className="form-check-input" id="customSwitchsizesm" />
          </div>
        </div> */}
      </div>
    </form>
  );
};

export default OrganizationCentreForm;
