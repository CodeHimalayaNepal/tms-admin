import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import {
  IFormTypeInput,
  useCreateFormTypeMutation,
  useDeleteFormTypeMutation,
  useUpdateFormTypeMutation
} from 'redux/reducers/form-types';

import { formTypeInitialValues, FormTypeSchemaValidations } from './schema';

import FormTypeForm from './FormTypeForm';
import FormTypeTable from './FormTypeTable';

const FormTypes = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [formTypeInitialFormState, setForTypeInitialForm] = useState(formTypeInitialValues);
  const [deletingFormTypeState, setDeletingFormTypes] = useState<IFormTypeInput | null>(null);

  const [deleteFormType, { isLoading: isDeleting }] = useDeleteFormTypeMutation();
  const [createFormType, { isLoading: isCreating }] = useCreateFormTypeMutation();
  const [updateFormType, { isLoading: isUpdating }] = useUpdateFormTypeMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: FormTypeSchemaValidations,
    initialValues: formTypeInitialFormState,
    onSubmit: (values) => {
      let executeFunc = createFormType;
      if (values.id) {
        executeFunc = updateFormType;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setForTypeInitialForm(formTypeInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
    }
  });

  const onEditClicked = (data: IFormTypeInput) => {
    setForTypeInitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingFormTypeState &&
      deleteFormType(deletingFormTypeState?.id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setForTypeInitialForm(formTypeInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IFormTypeInput) => {
    setDeletingFormTypes(data);
    toggleDeleteModal();
  };

  return (
    <Container title="Form Type">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <FormTypeTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title={'Create New Form Type'}
              iconName="ri-add-line align-bottom me-1"
              onClick={() => {
                setFormModal();
                setForTypeInitialForm(formTypeInitialValues);
              }}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={formTypeInitialFormState.id ? 'Update Form Type' : 'Create New Form Type'}
          isModalOpen={formModal}
          closeModal={setFormModal}
          modalSize="modal-sm"
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={setFormModal}
                title="Close"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton
                title="Add Form Type"
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <FormTypeForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default FormTypes;
