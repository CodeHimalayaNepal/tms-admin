import ActiveInactiveBadge from 'components/ui/ActiveInactiveBage/ActiveInactiveBadge';
import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import { FaRegEdit } from 'react-icons/fa';
import { RiDeleteBin5Line } from 'react-icons/ri';

import { IFormTypeInput, useListFormTypeQuery } from 'redux/reducers/form-types';

interface ITFormTypesTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: IFormTypeInput) => void;
  onDeleteClicked: (data: IFormTypeInput) => void;
}
const FormTypesTable: FC<ITFormTypesTable> = ({ tableHeaders, onEditClicked, onDeleteClicked }) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const { data: organizationData } = useListFormTypeQuery({ page, pageSize });

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{data.row.index + 1}</>;
      }
    },
    {
      Header: 'Title',
      accessor: 'title'
    },

    {
      Header: '  Status',
      accessor: 'status',
      Cell: (data: any) => {
        return (
          <>
            {' '}
            <ActiveInactiveBadge isActive={data.row.original.status} />
          </>
        );
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <FaRegEdit size={20} onClick={() => onEditClicked(data.row.original)} />
            <RiDeleteBin5Line
              size={20}
              className="m-2"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div>
      <DataTable
        columns={columns}
        data={organizationData?.data || []}
        tableHeaders={tableHeaders}
      />
    </div>
  );
};

export default FormTypesTable;
