import { useLocation, useNavigate } from 'react-router-dom';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import {
  useCreateEvaluationCriteriaDetailsMutation,
  useUpdateEvaluationCriteriaDetailsMutation,
  useDeleteEvaluationCriteriaDetailsMutation,
  ParentDetail
} from 'redux/reducers/evaluationCriteriaDetails';
import { IEvaluationDetailsFormInuput } from 'redux/reducers/evaluationCriteriaDetails';

import {
  evaluationCriteriaDetailsInitialValues,
  EvaluationCriteriaSchemaValidations
} from './schema';

import EvaluationCriteriaDetailsForm from './EvaluationCriteriaDetailsForm';
import EvaluationCriteriaDetailsTable from './EvaluationCriteriaDetailsTable';

interface CustomState {
  name: string;
  parent_detail: ParentDetail;
  evaluation_criteria_id: number;
}
const EvaluationCriteriaDetails = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const location = useLocation();
  const state = location.state as CustomState;

  const [evaluationInitialFormState, setEvaluationInitialForm] =
    useState<IEvaluationDetailsFormInuput>(evaluationCriteriaDetailsInitialValues);

  const [deletingEvaluationState, setDeletingEvaluation] =
    useState<IEvaluationDetailsFormInuput | null>(null);

  const [deleteEvaluation, { isLoading: isDeleting }] =
    useDeleteEvaluationCriteriaDetailsMutation();
  const [createEvaluation, { isLoading: isCreating }] =
    useCreateEvaluationCriteriaDetailsMutation();
  const [updateEvaluation, { isLoading: isUpdating }] =
    useUpdateEvaluationCriteriaDetailsMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: EvaluationCriteriaSchemaValidations,
    initialValues: evaluationInitialFormState,

    onSubmit: (values) => {
      let executeFunc = createEvaluation;
      if (values.evaluation_criteria_detail_id) {
        executeFunc = updateEvaluation;
      }

      executeFunc({ ...values, evaluation_criteria: state.evaluation_criteria_id })
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setEvaluationInitialForm(evaluationCriteriaDetailsInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error('Error');
          const errorKeys = Object.keys(err.data);
          errorKeys.forEach((item) => {
            if (Array.isArray(err.data[item])) {
              err.data[item].length && toast.error(err.data[item][0]);
            } else {
              toast.error(err.data[item]);
            }
          });
        });
    }
  });

  const onEditClicked = (data: IEvaluationDetailsFormInuput) => {
    setEvaluationInitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingEvaluationState &&
      deleteEvaluation(deletingEvaluationState?.evaluation_criteria_detail_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setEvaluationInitialForm(evaluationCriteriaDetailsInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IEvaluationDetailsFormInuput) => {
    setDeletingEvaluation(data);
    toggleDeleteModal();
  };

  return (
    <Container title="Participation and Learning">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <EvaluationCriteriaDetailsTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title="Add Criteria"
              iconName="ri-add-line align-bottom me-1"
              onClick={() => {
                setFormModal();
                setEvaluationInitialForm(evaluationCriteriaDetailsInitialValues);
              }}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
        evaluation_criteria_id={state?.evaluation_criteria_id}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title="Add Criteria"
          isModalOpen={formModal}
          closeModal={setFormModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={setFormModal}
                title="Discard"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton title="Save" type="submit" isLoading={isUpdating || isCreating} />
            </div>
          )}
        >
          <EvaluationCriteriaDetailsForm formik={formik} state={state} />
        </Modal>
      </form>
    </Container>
  );
};

export default EvaluationCriteriaDetails;
