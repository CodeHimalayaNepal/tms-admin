import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import EditIcon from '../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../assets/images/icons/delete.png';
import { IEvaluationDetailsFormInuput } from 'redux/reducers/evaluationCriteriaDetails';
import { useListEvaluationCriteriaDetailsQuery } from 'redux/reducers/evaluationCriteriaDetails';

interface IEvaluationDataTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: IEvaluationDetailsFormInuput) => void;
  onDeleteClicked: (data: IEvaluationDetailsFormInuput) => void;
  evaluation_criteria_id: number;
}
const EvaluationCriteriaDetailsTable: FC<IEvaluationDataTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked,
  evaluation_criteria_id
}) => {
  const [page, setPage] = useState(1);
  const routineData = useListEvaluationCriteriaDetailsQuery(evaluation_criteria_id);
  const canNext = routineData?.data?.next;
  const canPrevious = routineData?.data?.previous;

  const columns: any = [
    {
      Header: 'Criteria',
      Cell: (data: any) => {
        return <>{data.row.original?.evaluation_criteria_detail?.name}</>;
      }
    },
    {
      Header: 'Sub Criteria',
      Cell: (data: any) => {
        return <>{data.row.original?.evaluation_criteria_detail?.parent_detail?.name}</>;
      }
    },

    {
      Header: 'Details',
      Cell: (data: any) => {
        return <>{data.row.original?.name}</>;
      }
    },

    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={EditIcon}
              onClick={() =>
                onEditClicked({
                  ...data.row.original
                })
              }
            />
            <img
              src={DeleteIcon}
              className="m-2"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];

  return (
    <div className="list form-check-all">
      <DataTable
        columns={columns}
        data={routineData?.data?.data ? routineData?.data?.data : []}
        tableHeaders={tableHeaders}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (canPrevious) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (canNext) {
              setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default EvaluationCriteriaDetailsTable;
