//import { IEvaluationInuput } from 'redux/reducers/evaluation';
import { IEvaluationDetailsFormInuput } from 'redux/reducers/evaluationCriteriaDetails';
import * as Yup from 'yup';

export const evaluationCriteriaDetailsInitialValues: IEvaluationDetailsFormInuput = {
  name: ''
};

export const EvaluationCriteriaSchemaValidations = Yup.object().shape({
  name: Yup.string().required('Details  is required')
});
