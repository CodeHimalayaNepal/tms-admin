import CustomSelect from 'components/ui/Editor/CustomSelect';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { FC } from 'react';
import {
  IEvaluationDetailsFormInuput,
  ParentDetail
} from 'redux/reducers/evaluationCriteriaDetails';

interface IEvaluationCriteriaForm {
  formik: FormikProps<IEvaluationDetailsFormInuput>;
  state: any;
  // state: {
  //   name: string;
  //   parent_detail: ParentDetail;
  // };
}

const EvaluationCriteriaDetailsForm: FC<IEvaluationCriteriaForm> = ({ formik, state }) => {
  return (
    <form>
      <div className="row">
        {/*end col*/}
        <TextInput
          label="Sub Criteria"
          name="parent_criteria"
          type="text"
          // onChange={formik.handleChange}
          value={state?.parent_detail?.name ?? state.name}
          // onBlur={formik.handleBlur}
          formik={formik}
          disabled
        />
        <TextInput
          label="Criteria Name"
          name="criteria_name"
          type="text"
          // onChange={formik.handleChange}
          value={state?.name}
          // onBlur={formik.handleBlur}
          formik={formik}
          disabled
        />
        <TextInput
          label="Details"
          name="name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.name}
          onBlur={formik.handleBlur}
          formik={formik}
        />
      </div>
    </form>
  );
};

export default EvaluationCriteriaDetailsForm;
