import { Avatar } from 'common/constant';
import { USERTYPE } from 'common/enum';
import DataTable from 'components/ui/DataTable/data-table';
import {
  useCurrentUserDetailsQuery,
  useListAdministratorQuery
} from 'redux/reducers/administrator';

import { PlaceType, TraineeProfile } from '../TraineeProfile';
import { useListTraineeAttendanceQuery, useTraineeDetailQuery } from 'redux/reducers/user';
import { useNavigate } from 'react-router-dom';
import { IAttendance } from 'redux/reducers/attendance';
import { getLocaleDate } from 'utils/date';
import useToggle from 'hooks/useToggle';
import ChangePasswordForm from './ChangePasswordForm';
import PrimaryButton from 'components/ui/PrimaryButton';
import Modal from 'components/ui/Modal/modal';
import { useFormik } from 'formik';
import { useChangePasswordMutation } from 'redux/reducers/auth';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { IInitialValue, initialValue, SignUpValidationSchema } from './schema';
import * as Yup from 'yup';

const expCols = [
  {
    Header: 'Organization',
    accessor: 'organization'
  },
  { Header: 'Department', accessor: 'department' },
  { Header: 'Level', accessor: 'level' },
  { Header: 'From Date', accessor: 'from_date' },
  { Header: 'To Date', accessor: 'to_date' },
  {
    Header: 'Is it your first job ?',
    accessor: (row: TraineeProfile['experience'][0]) => (row.is_first_service ? 'True' : 'False')
  },
  {
    Header: 'Currently working here?',
    accessor: (row: TraineeProfile['experience'][0]) => (row.is_current_service ? 'True' : 'False')
  }
];

const traineeCols = [
  {
    Header: 'Name',
    accessor: 'name'
  },
  { Header: 'Place', accessor: 'place' },
  {
    Header: 'Type',
    accessor: (row: TraineeProfile['previous_training'][0]) => PlaceType[row.place_type]
  },
  { Header: 'From Date', accessor: 'from_date' },
  { Header: 'To Date', accessor: 'to_date' }
];

const attendanceCols = [
  {
    Header: 'Check In',
    accessor: (row: IAttendance) => (row.check_in ? getLocaleDate(row.check_in) : 'N/A')
  },
  {
    Header: 'Check Out',
    accessor: (row: IAttendance) => (row.check_out ? getLocaleDate(row.check_out) : 'N/A')
  },
  { Header: 'Reason', accessor: 'reason' },
  {
    Header: 'Requested Date',
    accessor: (row: IAttendance) => (row.requested_date ? getLocaleDate(row.requested_date) : 'N/A')
  },
  {
    Header: 'Approved Date',
    accessor: (row: IAttendance) => (row.approved_date ? getLocaleDate(row.approved_date) : 'N/A')
  },
  { Header: 'Approved_By', accessor: 'approved_by' },
  { Header: 'Branch Code', accessor: 'branch_code' },
  { Header: 'Source', accessor: 'source' },
  { Header: 'Status', accessor: 'status' },
  { Header: 'Temperature', accessor: 'temperature' }
];

export const Profile = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [changePassword, { isLoading }] = useChangePasswordMutation();
  const [initialForm, setInitialForm] = useState<IInitialValue>(initialValue);
  const navigate = useNavigate();
  const { data: userDetails } = useCurrentUserDetailsQuery();
  const { data: adminData } = useListAdministratorQuery();

  const { data: traineeAttendance = [] } = useListTraineeAttendanceQuery();

  const isSuperAdmin = adminData
    ? !!adminData?.results[0].roleadmin?.find(
        (item: any) => item.id?.toString() === userDetails?.user?.toString()
      )
    : false;

  const { data: traineeDetail } = useTraineeDetailQuery(
    { traineeId: userDetails?.data?.id },
    {
      skip: +userDetails?.data?.user_details?.user_type === USERTYPE.admin || !userDetails?.data?.id
    }
  );

  const {
    address,
    mobile_number,
    dob,
    first_name,
    middle_name,
    last_name,
    first_name_np,
    middle_name_np,
    last_name_np,
    experience,
    previous_training
  } = traineeDetail || {};

  const fullName = `${first_name ?? ''} ${middle_name ?? ''} ${last_name ?? ''}`;
  const fullNameNp = `${first_name_np ?? ''} ${middle_name_np ?? ''} ${last_name_np ?? ''}`;
  const closeModal = () => {
    setFormModal();
    setInitialForm(initialValue);
  };

  const formik = useFormik({
    validationSchema: Yup.object().shape({
      ...SignUpValidationSchema
    }),
    enableReinitialize: true,
    initialValues: initialForm,
    onSubmit: (values) => {
      changePassword(values)
        .unwrap()
        .then((data: any) => {
          toast.success(data.message);
          setInitialForm(initialValue);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error(err?.data?.errors?.old_password || 'Failed to change password');
        });
    }
  });

  return (
    <div className="main-content overflow-hidden">
      <div className="page-content">
        <div className="container-fluid">
          <div className="profile-foreground position-relative mx-n4 mt-n4">
            <div className="profile-wid-bg" style={{ height: '21vh' }}>
              <img src="/assets/images/profile-bg.jpg" alt="" className="profile-wid-img" />
            </div>
          </div>

          <div className="pt-4 mb-4 mb-lg-3 pb-lg-4">
            <div className="row g-4">
              <div className="col-auto">
                <div className="avatar-lg">
                  <img
                    src={traineeDetail?.image ?? Avatar}
                    alt="user-img"
                    className="img-thumbnail rounded-circle"
                    style={{
                      width: '92px',
                      height: '92px',
                      objectFit: 'cover'
                    }}
                  />
                </div>
              </div>

              <div className="col">
                <div className="p-2">
                  <h3 className="text-white mb-1">{fullName}</h3>
                  <p className="text-white-75"></p>
                  <div className="hstack text-white-50">
                    <div>
                      <i className="ri-building-line me-2 text-white-75 fs-16 align-middle"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <div>
                <div className="d-flex">
                  <ul
                    className="nav nav-pills animation-nav profile-nav gap-2 gap-lg-3 flex-grow-1"
                    role="tablist"
                  >
                    <li className="nav-item">
                      <a
                        className="nav-link fs-14 active"
                        data-bs-toggle="tab"
                        href="#overview-tab"
                        role="tab"
                      >
                        <i className="ri-airplay-fill d-inline-block d-md-none"></i>{' '}
                        <span className="d-none d-md-inline-block">Overview</span>
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        className="nav-link fs-14"
                        data-bs-toggle="tab"
                        href="#attendance"
                        role="tab"
                      >
                        <i className="ri-list-unordered d-inline-block d-md-none"></i>{' '}
                        <span className="d-none d-md-inline-block">Attendance</span>
                      </a>
                    </li>
                  </ul>

                  <div className="flex-shrink-0">
                    <a className="btn btn-link text-success" onClick={() => setFormModal()}>
                      <i className="ri-lock-password-line align-bottom"></i> Change Password ?
                    </a>
                    <a
                      href="#"
                      className="btn btn-success"
                      onClick={(e) => {
                        e.preventDefault();
                        navigate('/trainee/profile');
                      }}
                    >
                      <i className="ri-edit-box-line align-bottom"></i> Edit Profile
                    </a>
                  </div>
                </div>

                <div className="tab-content pt-4 text-muted">
                  <div className="tab-pane active" id="overview-tab" role="tabpanel">
                    <div className="row">
                      <div className="col-xxl-3">
                        <div className="card">
                          <div className="card-body">
                            <h5 className="card-title mb-3">Info</h5>
                            <div className="table-responsive">
                              <table className="table table-borderless mb-0">
                                <tbody>
                                  <tr>
                                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                                      Full Name:
                                    </th>
                                    <td className="text-muted">
                                      <p className="mb-1">{fullName ?? ' - '}</p>
                                      <p className="mb-1">
                                        {fullNameNp?.trim() ? `(${fullNameNp})` : ' - '}
                                      </p>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                                      Date of Birth:
                                    </th>
                                    <td className="text-muted">{dob ?? ' - '}</td>
                                  </tr>
                                  <tr>
                                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                                      Mobile:
                                    </th>
                                    <td className="text-muted">(977) {mobile_number ?? ' - '}</td>
                                  </tr>
                                  <tr>
                                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                                      E-mail:
                                    </th>
                                    <td className="text-muted">
                                      {userDetails?.data?.user_details?.email ?? ' - '}
                                    </td>
                                  </tr>
                                  <tr>
                                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                                      Location:
                                    </th>
                                    <td className="text-muted">
                                      {[
                                        address?.tole_street,
                                        address?.ward_no,
                                        address?.municipality,
                                        address?.province?.name_en
                                      ]
                                        .filter((add) => add)
                                        .join(', ')}
                                    </td>
                                  </tr>
                                  <tr>
                                    <th className="ps-0" scope="row" style={{ width: '40%' }}>
                                      Joining Date:
                                    </th>
                                    <td className="text-muted">24 Nov 2021</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="col-xxl-9">
                        <div className="card">
                          <div className="card-body">
                            <h5 className="card-title mb-3">Experience</h5>
                            <DataTable data={experience ?? []} columns={expCols} />
                          </div>
                        </div>
                        <div className="card">
                          <div className="card-body">
                            <h5 className="card-title mb-3">Training</h5>
                            <DataTable data={previous_training ?? []} columns={traineeCols} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="tab-pane fade" id="attendance" role="tabpanel">
                    <div className="row">
                      <div className="col-xxl-12">
                        <div className="card">
                          <div className="card-body">
                            <h5 className="card-title mb-3">Attendance</h5>
                            <DataTable columns={attendanceCols} data={traineeAttendance} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="tab-pane fade" id="completedTraining" role="tabpanel">
                    <div className="row">
                      <div className="col-xxl-12 ">
                        <div className="bg-white p-3 rounded mb-3">
                          <h5 className="m-0">Completed Training</h5>
                        </div>
                        <div className="row">
                          <div className="col-md-3">
                            <div className="bg-light px-3 py-2 card">
                              <h5 className="text-center">22233433</h5>
                              <div className="border-top pt-3 pb-2">
                                <h5 className="text-center">Introduction To Project Management</h5>
                                <span>Duration: 3 working Days</span>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="bg-light px-3 py-2 card">
                              <h5 className="text-center">22233433</h5>
                              <div className="border-top pt-3 pb-2">
                                <h5 className="text-center">Introduction To Project Management</h5>
                                <span>Duration: 3 working Days</span>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="bg-light px-3 py-2 card">
                              <h5 className="text-center">22233433</h5>
                              <div className="border-top pt-3 pb-2">
                                <h5 className="text-center">Introduction To Project Management</h5>
                                <span>Duration: 3 working Days</span>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="bg-light px-3 py-2 card">
                              <h5 className="text-center">22233433</h5>
                              <div className="border-top pt-3 pb-2">
                                <h5 className="text-center">Introduction To Project Management</h5>
                                <span>Duration: 3 working Days</span>
                              </div>
                            </div>
                          </div>
                          <div className="col-md-3">
                            <div className="bg-light px-3 py-2 card">
                              <h5 className="text-center">22233433</h5>
                              <div className="border-top pt-3 pb-2">
                                <h5 className="text-center">Introduction To Project Management</h5>
                                <span>Duration: 3 working Days</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={'Are you sure you want to change password?'}
          isModalOpen={formModal}
          closeModal={closeModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <PrimaryButton title={'Change Password'} type="submit" className="btn btn-success" />
            </div>
          )}
        >
          <ChangePasswordForm formik={formik} />
        </Modal>
      </form>
    </div>
  );
};
