import React from 'react';

const Experience = () => {
  return (
    <div>
      <form>
        <div id="newlink">
          <div id={'1'}>
            <div className="row">
              <div className="col-lg-12">
                <div className="mb-3">
                  <label htmlFor="organizationTitle" className="form-label">
                    Orgaization
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="organizationTitle"
                    placeholder="Job title"
                    defaultValue="MOFA"
                  />
                </div>
              </div>
              {/*end col*/}
              <div className="col-lg-6">
                <div className="mb-3">
                  <label htmlFor="jobTitle" className="form-label">
                    Designation
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="jobTitle"
                    placeholder="Job title"
                    defaultValue="Secretary"
                  />
                </div>
              </div>
              {/*end col*/}
              <div className="col-lg-6">
                <div className="mb-3">
                  <label htmlFor="dateOfJoining" className="form-label">
                    Date of Joining
                  </label>
                  <input type="date" className="form-control" id="dateOfJoining" />
                </div>
              </div>
              {/*end col*/}
              <div className="col-lg-6">
                <div className="mb-3">
                  <label htmlFor="newjobTitle" className="form-label">
                    New Designation
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="newjobTitle"
                    placeholder="New Job title"
                    defaultValue="President"
                  />
                </div>
              </div>
              {/*end col*/}
              <div className="col-lg-6">
                <div className="mb-3">
                  <label htmlFor="dateOfPromotion" className="form-label">
                    Date of Promotion
                  </label>
                  <input type="date" className="form-control" id="dateOfPromotion" />
                </div>
              </div>
              {/*end col*/}
              <div className="col-lg-6">
                <div className="mb-3">
                  <label htmlFor="scoreTraining" className="form-label">
                    Score on New Training
                  </label>
                  <input
                    type="number"
                    className="form-control"
                    id="scoreTraining"
                    defaultValue={70}
                  />
                </div>
              </div>
              {/*end col*/}
            </div>
            {/*end row*/}
          </div>
        </div>
        <div id="newForm" style={{ display: 'none' }}></div>
        <div className="col-lg-12">
          <div className="hstack gap-2 justify-content-end">
            <button type="submit" className="btn btn-primary">
              Update
            </button>
          </div>
        </div>
        {/*end col*/}
      </form>
    </div>
  );
};

export default Experience;
