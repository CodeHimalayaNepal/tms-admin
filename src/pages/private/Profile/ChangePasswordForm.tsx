import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { FC } from 'react';

import { IInitialValue } from './schema';

interface IChangePasswordForm {
  formik: FormikProps<IInitialValue>;
}

const ChangePasswordForm: FC<IChangePasswordForm> = ({ formik }) => {
  return (
    <div className="row">
      <TextInput
        label="Old Password"
        name="old_password"
        type="password"
        onChange={formik.handleChange}
        value={formik.values.old_password}
        onBlur={formik.handleBlur}
        formik={formik}
        placeholder={'Old Password'}
        disableRequiredValidation
      />

      <TextInput
        label="New Password"
        name="new_password"
        type="password"
        onChange={formik.handleChange}
        value={formik.values.new_password}
        onBlur={formik.handleBlur}
        formik={formik}
        placeholder={'New Password'}
        disableRequiredValidation
      />
      <TextInput
        label="Confirm Password"
        name="confirm_password"
        type="password"
        onChange={formik.handleChange}
        value={formik.values.confirm_password}
        onBlur={formik.handleBlur}
        formik={formik}
        placeholder={'Confirm Password'}
        disableRequiredValidation
      />
    </div>
  );
};

export default ChangePasswordForm;
