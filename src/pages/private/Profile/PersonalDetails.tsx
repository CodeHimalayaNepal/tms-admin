import React from 'react';
import { useCurrentUserDetailsQuery } from 'redux/reducers/administrator';

const PersonalDetails = () => {
  const { data: currentUserDetails } = useCurrentUserDetailsQuery();

  return (
    <div>
      <form action="javascript:void(0);">
        <div className="row">
          <div className="col-lg-6">
            <div className="mb-3">
              <label htmlFor="fullnameInput" className="form-label">
                Full Name
              </label>
              <input
                type="text"
                className="form-control"
                id="fullnameInput"
                placeholder="Enter your fullname"
                defaultValue={currentUserDetails?.data.first_name}
              />
            </div>
          </div>
          {/*end col*/}
          <div className="col-lg-6">
            <div className="mb-3">
              <label htmlFor="fullnameInput" className="form-label">
                Full Name (Nepali)
              </label>
              <input
                type="text"
                className="form-control"
                id="fullnameInput"
                placeholder="Enter your fullname"
                defaultValue={currentUserDetails?.data.last_name}
              />
            </div>
          </div>
          {/*end col*/}
          <div className="col-lg-6">
            <div className="mb-3">
              <label htmlFor="genderInput" className="form-label">
                Gender
              </label>
              {/* <div> */}
              <select
                className="form-control"
                data-choices
                data-choices-search-false
                id="genderInput"
              >
                <option value="">Select Gender</option>
                <option value="male" selected>
                  Male
                </option>
                <option value="female">Female</option>
                <option value="others">Others</option>
              </select>
              {/* </div> */}
            </div>
          </div>
          {/*end col*/}
          <div className="col-lg-6">
            <div className="mb-3">
              <label htmlFor="employeeIdInput" className="form-label">
                Employee ID
              </label>
              <input
                type="text"
                className="form-control"
                id="employeeIdInput"
                placeholder="Enter Employee ID"
              />
            </div>
          </div>
          {/*end col*/}
          <div className="col-lg-6">
            <div className="mb-3">
              <label htmlFor="OrganizationInput" className="form-label">
                Orgaization
              </label>
              <input
                type="text"
                className="form-control"
                id="OrganizationInput"
                placeholder="Enter Employee ID"
              />
            </div>
          </div>
          {/*end col*/}
          {/*end col*/}
          <div className="col-lg-6">
            <div className="mb-3">
              <label htmlFor="phonenumberInput" className="form-label">
                Phone Number
              </label>
              <input
                type="text"
                className="form-control"
                id="phonenumberInput"
                placeholder="Enter your phone number"
              />
            </div>
          </div>
          {/*end col*/}
          <div className="col-lg-6">
            <div className="mb-3">
              <label htmlFor="emailInput" className="form-label">
                Email Address
              </label>
              <input
                type="email"
                className="form-control"
                id="emailInput"
                placeholder="Enter your email"
              />
            </div>
          </div>
          {/*end col*/}
          <div className="col-lg-6">
            <div className="mb-3">
              <label htmlFor="designationInput" className="form-label">
                Designation
              </label>
              <input
                type="text"
                className="form-control"
                id="designationInput"
                placeholder="Designation"
              />
            </div>
          </div>
          {/*end col*/}
          <div className="col-lg-6">
            <div className="mb-3">
              <label htmlFor="websiteInput1" className="form-label">
                Website
              </label>
              <input
                type="text"
                className="form-control"
                id="websiteInput1"
                placeholder="www.example.com"
              />
            </div>
          </div>
          {/*end col*/}
          <div className="col-lg-12">
            <div className="hstack gap-2 justify-content-end">
              <button type="submit" className="btn btn-primary">
                Updates
              </button>
              <button type="button" className="btn btn-soft-success">
                Cancel
              </button>
            </div>
          </div>
          {/*end col*/}
        </div>
        {/*end row*/}
      </form>
    </div>
  );
};

export default PersonalDetails;
