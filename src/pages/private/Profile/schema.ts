import * as Yup from 'yup';
export interface IInitialValue {
  old_password: string;
  new_password: string;
  confirm_password: string;
}
export const initialValue: IInitialValue = {
  old_password: '',
  new_password: '',
  confirm_password: ''
};
export const SignUpValidationSchema = {
  old_password: Yup.string()
    .min(6, 'Old Password is greater than 6 characters')
    .max(50, 'Too Long!')
    .required('Old Password is required'),
  new_password: Yup.string()
    .min(6, 'New Password must be greater than 6 characters!')
    .max(50, 'Too Long!')
    .required('New password is required'),
  confirm_password: Yup.string()
    .oneOf([Yup.ref('new_password'), null], "Password doesn't Match")
    .required('Confirm Password is required')
};
