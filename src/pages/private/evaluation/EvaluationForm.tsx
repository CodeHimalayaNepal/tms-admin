import CustomSelect from 'components/ui/Editor/CustomSelect';
import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import { FC } from 'react';
import { useListAdministratorQuery, useFetchUserRoleQuery } from 'redux/reducers/administrator';
import { ICoursesInput, useListCoursesQuery } from 'redux/reducers/courses';
import { useListOrganizationDepartmentQuery } from 'redux/reducers/organization-department';
import { useListOrganizationHallQuery } from 'redux/reducers/organization-halls';
import { IEvaluationInuput } from 'redux/reducers/evaluation';
import { useListTrainingQuery } from 'redux/reducers/training';
import { useListTrainingTypeQuery } from 'redux/reducers/training-type';
import { useListRoutineEnrollmentQuery } from 'redux/reducers/routine-enrollment';

interface IEvaluationForm {
  formik: FormikProps<IEvaluationInuput>;
}
const EvaluationForm: FC<IEvaluationForm> = ({ formik }) => {
  const { data: trainingType } = useListTrainingTypeQuery();
  const { data: enrollmentUser } = useListRoutineEnrollmentQuery();

  const selectedRoutineTraineeUsers = enrollmentUser?.results?.map((item: any) => {
    return {
      value: item?.id?.toString(),
      label: item?.trainee_details?.first_name + ' ' + item?.trainee_details?.last_name
    };
  });

  return (
    <form>
      <div className="row">
        {/*end col*/}

        <TextInput
          label="Evaluator Marks"
          name="evaluator_marks"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.evaluator_marks}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="Evaluator Remarks"
          name="evaluator_remarks"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.evaluator_remarks}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="Coordinator Marks"
          name="coordinator_marks"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.coordinator_marks}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        <TextInput
          label="Coordinator Remarks"
          name="coordinator_remarks"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.coordinator_remarks}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <CustomSelect
          options={selectedRoutineTraineeUsers}
          placeholder={'Search by name'}
          value={formik.values.enrollment}
          onChange={(value: any) => formik.setFieldValue('enrollment', value)}
          label={'Select User'}
        />
        <TextInput
          label="Assignment"
          name="assignment"
          type="number"
          onChange={formik.handleChange}
          value={formik.values.assignment}
          onBlur={formik.handleBlur}
          formik={formik}
        />
        <TextInput
          label="Evaluation Detail"
          name="evaluation_detail"
          type="number"
          onChange={formik.handleChange}
          value={formik.values.evaluation_detail}
          onBlur={formik.handleBlur}
          formik={formik}
        />

        {/* {} */}

        <div className="col-12 mb-3 mt-4 d-flex align-items-end">
          <div className="form-check form-switch form-switch-md mb-3">
            <label className="form-check-label" htmlFor="customSwitchsizesm">
              Status
            </label>
            <input
              type="checkbox"
              className="form-check-input"
              id="customSwitchsizesm"
              checked={formik.values.status}
              onChange={(e) => {
                formik.setFieldValue('status', !formik.values.status);
              }}
            />
          </div>
        </div>
      </div>
    </form>
  );
};

export default EvaluationForm;
