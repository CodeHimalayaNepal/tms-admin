import { IEvaluationInuput } from 'redux/reducers/evaluation';
import * as Yup from 'yup';

export const evaluationInitialValues: IEvaluationInuput = {
  enrollment: '',
  evaluation_detail: 0,
  assignment: 0,
  evaluator_marks: '',
  evaluator_remarks: '',
  coordinator_marks: '',
  coordinator_remarks: '',
  status: true
};

export const EvaluationSchemaValidations = Yup.object().shape({
  coordinator_remarks: Yup.string().required('Start Date is required'),
  evaluator_remarks: Yup.string().required('End Date is required'),
  status: Yup.boolean().required('Status is required'),
  enrollment: Yup.string().required('Training is required'),
  evaluation_detail: Yup.number().required('Training Type is required'),
  assignment: Yup.number().required('Coordinator is required'),
  evaluator_marks: Yup.string().required('Hall is required'),
  coordinator_marks: Yup.string().required('Course is required')
});
