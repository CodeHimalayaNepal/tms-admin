import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import { FaRegEdit } from 'react-icons/fa';
import { RiDeleteBin5Line } from 'react-icons/ri';
import { IEvaluationInuput } from 'redux/reducers/evaluation';
import { useListEvaluationQuery } from 'redux/reducers/evaluation';

interface ITraineeDataTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: IEvaluationInuput) => void;
  onDeleteClicked: (data: IEvaluationInuput) => void;
}
const OrganizationCentresTable: FC<ITraineeDataTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked
}) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const { data: routineData, isLoading } = useListEvaluationQuery({ page, pageSize });
  // const { data: evaluationSingleData } = uselistEvaluationSingleQuery();
  // const { data: usersLists } = useListSingleQuery();
  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{data.row.index + 1}</>;
      }
    },
    {
      Header: 'coordinator_marks',
      accessor: 'coordinator_marks'
    },

    {
      Header: 'Completed Date',
      accessor: 'completion_date'
    },
    {
      Header: 'evaluator_marks ',
      accessor: 'evaluator_marks'
    },
    {
      Header: 'evaluator_remarks ',
      accessor: 'evaluator_remarks'
    },
    {
      Header: 'tms_evaluation_id ',
      accessor: 'tms_evaluation_id'
    },
    {
      Header: '  Status',
      accessor: 'status',
      Cell: (data: any) => {
        return <>{data.row.original.status ? 'Yes' : 'No'}</>;
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <PrimaryButton
              title="Edit"
              type="button"
              onClick={() =>
                onEditClicked({
                  ...data.row.original,
                  id: data.row.original.tms_evaluation_id,
                  evaluation_detail: data.row.original.evaluation_details.evaluation_detail_id,
                  assignment: data.row.original.assignment_details.tms_assignment_id,
                  enrollment: data.row.original.enrollment_details.id
                })
              }
            />
            <RiDeleteBin5Line
              size={20}
              className="m-2"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div className="list form-check-all">
      <DataTable
        columns={columns}
        isLoading={isLoading}
        data={routineData?.data?.results || []}
        tableHeaders={tableHeaders}
      />
    </div>
  );
};

export default OrganizationCentresTable;
