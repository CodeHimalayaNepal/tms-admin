import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import {
  IEvaluationInuput,
  useCreateEvaluationMutation,
  useUpdateEvaluationMutation,
  useDeleteEvaluationMutation
} from 'redux/reducers/evaluation';

import { evaluationInitialValues, EvaluationSchemaValidations } from './schema';

import EvaluationForm from './EvaluationForm';
import EvaluationTable from './EvaluationTable';

const Evaluation = () => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [evaluationInitialFormState, setEvaluationInitialForm] =
    useState<IEvaluationInuput>(evaluationInitialValues);

  const [deletingEvaluationState, setDeletingEvaluation] = useState<IEvaluationInuput | null>(null);

  const [deleteEvaluation, { isLoading: isDeleting }] = useDeleteEvaluationMutation();
  const [createEvaluation, { isLoading: isCreating }] = useCreateEvaluationMutation();
  const [updateEvaluation, { isLoading: isUpdating }] = useUpdateEvaluationMutation();
  // const [getEvaluation, { isLoading: isGetting }] = useListSingleQuery();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: EvaluationSchemaValidations,
    initialValues: evaluationInitialFormState,

    onSubmit: (values) => {
      let executeFunc = createEvaluation;
      if (values.tms_evaluation_id) {
        executeFunc = updateEvaluation;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setEvaluationInitialForm(evaluationInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          const errorKeys = Object.keys(err.data);
          errorKeys.forEach((item) => {
            if (Array.isArray(err.data[item])) {
              err.data[item].length && toast.error(err.data[item][0]);
            } else {
              toast.error(err.data[item]);
            }
          });
        });
    }
  });

  const onEditClicked = (data: IEvaluationInuput) => {
    setEvaluationInitialForm(data);
    setFormModal();
  };

  const onDeleteConfirm = () => {
    deletingEvaluationState &&
      deleteEvaluation(deletingEvaluationState?.tms_evaluation_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setEvaluationInitialForm(evaluationInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: IEvaluationInuput) => {
    setDeletingEvaluation(data);
    toggleDeleteModal();
  };

  return (
    <Container title="Evaluation">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <EvaluationTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title={
                evaluationInitialFormState.tms_evaluation_id
                  ? 'Edit Evaluation'
                  : 'Create New Evaluation'
              }
              iconName="ri-add-line align-bottom me-1"
              onClick={() => {
                setFormModal();
                setEvaluationInitialForm(evaluationInitialValues);
              }}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={
            evaluationInitialFormState.tms_evaluation_id
              ? 'Edit Evaluation'
              : 'Create New Evaluation'
          }
          isModalOpen={formModal}
          closeModal={setFormModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={setFormModal}
                title="Close"
                isLoading={isUpdating || isCreating}
              />
              <PrimaryButton
                title="Submit"
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={isUpdating || isCreating}
              />
            </div>
          )}
        >
          <EvaluationForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default Evaluation;
