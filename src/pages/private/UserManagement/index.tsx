import Container from 'components/ui/Container';
import React, { useState } from 'react';
import UserManagementTable from './UserManagementTable';
import './style.css';
import PrimaryButton from 'components/ui/PrimaryButton';
import Modal from 'components/ui/Modal/modal';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useBulkApproveTraineeMutation } from 'redux/reducers/user';
import { toast } from 'react-toastify';

const UserManagement = () => {
  const [tabValue, setTabValue] = useState<string>('pending');
  const [isActivePrimary, setIsActivePrimary] = useState(true);
  const [isActiveSecondary, setIsActiveSecondary] = useState(false);
  const [allID, setAllID] = useState<Array<string>>([]);

  const [bulkApproveModal, setbulkApproveModal] = useState(false);

  const [approveBulkTrainee, { isLoading }] = useBulkApproveTraineeMutation();

  const handleClickPrimary = () => {
    setIsActivePrimary(true);
    setIsActiveSecondary(false);
  };

  const handleClickSecondary = () => {
    setIsActivePrimary(false);
    setIsActiveSecondary(true);
  };
  const onApproveUser = async () => {
    if (allID.length) {
      await approveBulkTrainee({ allID })
        .unwrap()
        .then((success) => {
          setAllID([]);
          setbulkApproveModal(false);
          toast.success('Trainee Bulk approved successfully!');
        })
        .catch((err) => {
          setbulkApproveModal(false);

          toast.error('Failed to approve trainee!');
        });
    }
  };
  return (
    <Container title="Trainee Approval">
      <div className="d-flex flex-column">
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <ul className="nav nav-pills gap-2 gap-lg-3 flex-grow-1" role="tablist">
            <li className="nav-item">
              <button
                type="button"
                className={isActivePrimary ? 'btn btn-primary' : 'btn btn-light'}
                onClick={() => {
                  handleClickPrimary();
                  setTabValue('pending');
                }}
              >
                Pending
              </button>
            </li>
            <li>
              <button
                type="button"
                onClick={() => {
                  handleClickSecondary();
                  setTabValue('rejected');
                  handleClickSecondary();
                }}
                className={isActiveSecondary ? 'btn-custom btn-rejected' : 'btn btn-light'}
              >
                Rejected
              </button>
            </li>
          </ul>

          <div>
            <PrimaryButton
              title={`Bulk Approve (${allID.length})`}
              type="button"
              small="btn-sm mx-2"
              onClick={() => setbulkApproveModal(true)}
            />
          </div>
        </div>

        <UserManagementTable status={tabValue} allID={allID} setAllID={setAllID} />
      </div>
      <Modal
        title="Approve Trainee in Bulk"
        isModalOpen={bulkApproveModal}
        closeModal={() => setbulkApproveModal(false)}
        modalSize="modal-md"
        renderFooter={() => (
          <div className="hstack gap-2 justify-content-end">
            <SecondaryButton onClick={() => setbulkApproveModal(false)} title="Cancel" />
            <PrimaryButton
              type="button"
              title="Approve"
              onClick={() => onApproveUser()}
              isLoading={isLoading}
            />
          </div>
        )}
      >
        <h4 className="fw-bold">Are you sure you want to approve {`${allID.length}`} users?</h4>
      </Modal>
    </Container>
  );
};

export default UserManagement;
