import DataTable from 'components/ui/DataTable/data-table';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import React, { FC, useState } from 'react';
import { Cell } from 'react-table';
import { toast } from 'react-toastify';
import {
  ITraineeDetail,
  ITraineeStatus,
  useApproveTraineeMutation,
  useListPendingTraineeQuery,
  useListRejectedTraineeQuery,
  useRejectTraineeMutation
} from 'redux/reducers/user';
import { boolean } from 'yup';

interface IUserManagementTable {
  tableHeaders?: () => React.ReactNode;
  status: string;
  allID: Array<string>;
  setAllID: React.Dispatch<React.SetStateAction<string[]>>;
}

const UserManagementTable: FC<IUserManagementTable> = ({
  tableHeaders,
  status,
  allID,
  setAllID
}) => {
  const [userDetail, setUserDetail] = useState<ITraineeDetail | null>(null);
  const [isReject, setIsReject] = useState<boolean>(false);

  const { data: userList = [], isLoading: isPendingLoading } = useListPendingTraineeQuery({});
  const { data: rejectedUserList = [], isLoading: isRejectedLoading } = useListRejectedTraineeQuery(
    {}
  );
  const [approveTrainee, { isLoading }] = useApproveTraineeMutation();
  const [rejectTrainee, { isLoading: rejectLoading }] = useRejectTraineeMutation();

  const onApproveUser = async () => {
    if (!userDetail) return;

    try {
      await approveTrainee({ id: userDetail.user_details.id.toString() }).unwrap();
      setUserDetail(null);
      toast.success('Trainee approved successfully!');
    } catch (error) {
      toast.error('Failed to approve trainee!');
    }
  };
  const onRejectUser = async () => {
    if (!userDetail) return;

    try {
      await rejectTrainee({ id: userDetail.user_details.id.toString() }).unwrap();
      setIsReject(false);
      setUserDetail(null);
      toast.success('Trainee rejected successfully!');
    } catch (error) {
      toast.error('Failed to reject trainee!');
    }
  };

  const handleCheckbox = (id: string) => () => {
    const item = allID.find((item) => item == id);

    item
      ? setAllID((prev) => prev.filter((item) => item != id))
      : setAllID((prev) => [...prev, id]);
  };
  const columns = [
    {
      Header: 'C.B',
      Cell: (cell: Cell<ITraineeDetail>) => {
        return (
          <input
            type="checkbox"
            className="form-check-input"
            checked={
              !!allID.length &&
              allID?.some((item: any) => item == cell.row.original.user_details.id)
                ? true
                : false
            }
            id="mastercheck"
            onChange={handleCheckbox(cell.row.original.user_details.id.toString())}
          />
        );
      }
    },
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (cell: Cell) => {
        return cell.row.index + 1;
      }
    },
    {
      Header: ' First Name',
      accessor: 'first_name'
    },
    {
      Header: ' Last Name',
      accessor: 'last_name'
    },
    {
      Header: 'Email',
      accessor: 'user_details.email'
    },
    {
      Header: 'Mobile Number',
      accessor: 'mobile_number'
    },

    {
      Header: '  Action',
      Cell: (data: Cell<ITraineeDetail>) => {
        return +data.row.original?.user_details.status === ITraineeStatus.Pending ? (
          <>
            <PrimaryButton
              title="Approve"
              type="button"
              small="btn-sm mx-2"
              onClick={() => setUserDetail(data.row.original)}
            />
            <PrimaryButton
              isDanger
              title="Reject"
              type="button"
              small="btn-sm"
              onClick={() => {
                setIsReject(true);
                setUserDetail(data.row.original);
              }}
            />
          </>
        ) : +data.row.original?.user_details.status === ITraineeStatus.Rejected ? (
          <PrimaryButton
            title="Approve"
            type="button"
            small="btn-sm mx-2"
            onClick={() => setUserDetail(data.row.original)}
          />
        ) : (
          ''
        );
      }
    }
  ];

  return (
    <div>
      <DataTable
        columns={columns}
        data={status === 'pending' ? userList : rejectedUserList}
        isLoading={status === 'pending' ? isPendingLoading : isRejectedLoading}
        tableHeaders={tableHeaders}
      />

      <Modal
        title="Approve Trainee"
        isModalOpen={!isReject && !!userDetail}
        closeModal={() => setUserDetail(null)}
        modalSize="modal-md"
        renderFooter={() => (
          <div className="hstack gap-2 justify-content-end">
            <SecondaryButton onClick={() => setUserDetail(null)} title="Cancel" />
            <PrimaryButton
              type="button"
              title="Approve"
              onClick={onApproveUser}
              isLoading={isLoading}
            />
          </div>
        )}
      >
        <h4 className="fw-bold">
          Are you sure you want to approve{' '}
          {`${userDetail?.first_name ?? ''} ${userDetail?.last_name ?? ''}`}?
        </h4>
      </Modal>
      <Modal
        title="Reject Trainee"
        isModalOpen={isReject && !!userDetail}
        closeModal={() => {
          setIsReject(false);
          setUserDetail(null);
        }}
        modalSize="modal-md"
        renderFooter={() => (
          <div className="hstack gap-2 justify-content-end">
            <SecondaryButton
              onClick={() => {
                setIsReject(false);
                setUserDetail(null);
              }}
              title="Cancel"
            />
            <PrimaryButton
              type="button"
              title="Reject"
              onClick={onRejectUser}
              isLoading={rejectLoading}
            />
          </div>
        )}
      >
        <h4 className="fw-bold">
          Are you sure you want to reject{' '}
          {`${userDetail?.first_name ?? ''} ${userDetail?.last_name ?? ''}`}?
        </h4>
      </Modal>
    </div>
  );
};

export default UserManagementTable;
