import { ISessionInput } from 'redux/reducers/session-resources';
import * as Yup from 'yup';

export const sessionInitialValues: ISessionInput = {
  title: '',
  description: '',
  learning_objective: '',
  course: '',
  course_topic_files: [],
  duration_hr: 0,
  order: 1,
  methodology: '',
  course_topic_id: 0
};

export const SessionSchemaValidations = Yup.object().shape({
  title: Yup.string().required('Title is required'),
  description: Yup.string().required('Description is required'),
  learning_objective: Yup.string().required('Learning Objective is required'),
  // course: Yup.string().required('Course is required'),
  duration_hr: Yup.string().required('Duration Hour is required'),
  order: Yup.number().required('Order is required'),
  methodology: Yup.string().required('Methodology is required')
  //course_topic_id: Yup.string().required('Course Id is required')
});
