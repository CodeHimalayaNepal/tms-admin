import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { ISessionInput, useDeleteSessionMutation } from 'redux/reducers/session-resources';
import { IfStatement } from 'typescript';

import { sessionInitialValues } from './schema';
import SessionTable from './SessionTable';

const Sessions = () => {
  const [formModal, setFormModal] = useToggle(false);
  const navigate = useNavigate();
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [sessionInitialFormState, setSessionInitialForm] = useState(sessionInitialValues);
  const [deletingSessionState, setDeletingSessions] = useState<ISessionInput | null>(null);

  const [deleteSession, { isLoading: isDeleting }] = useDeleteSessionMutation();

  const onDeleteConfirm = () => {
    deletingSessionState &&
      deleteSession(deletingSessionState?.course)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setSessionInitialForm(sessionInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error transactions');
        });
  };

  const onDeleteClicked = (data: ISessionInput) => {
    setDeletingSessions(data);
    toggleDeleteModal();
  };
  const onEditClicked = (data: ISessionInput) => {
    navigate(`/modules/sessionForm/edit/${data?.course_topic_id}`);
  };

  const closeModal = () => {
    setFormModal();
    setSessionInitialForm(sessionInitialValues);
  };

  return (
    <Container title="Sessions">
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <SessionTable onDeleteClicked={onDeleteClicked} />
    </Container>
  );
};

export default Sessions;
