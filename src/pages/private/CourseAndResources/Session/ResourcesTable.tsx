import ActiveInactiveBadge from 'components/ui/ActiveInactiveBage/ActiveInactiveBadge';
import Container from 'components/ui/Container';
import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import useToggle from 'hooks/useToggle';
import React, { FC, useState } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import {
  ISessionInput,
  useDeleteResourcesMutation,
  useListResourcesQuery
} from 'redux/reducers/session-resources';
import EditIcon from '../../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../../assets/images/icons/delete.png';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Modal from 'components/ui/Modal/modal';
import { toast } from 'react-toastify';
import { useDeleteSessionMutation } from 'redux/reducers/session-resources';
import { FaFileCsv, FaFileVideo } from 'react-icons/fa';
import { sessionInitialValues } from './schema';

interface ISessionTable {
  tableHeaders?: () => React.ReactNode;
  onDeleteClicked?: (data: any) => void;
  onEditClicked: (data: any) => void;
}
interface CustomState {
  course_topic_id: string;
  course_id: string;
}
const ResourcesTable: FC<ISessionTable> = ({ tableHeaders, onEditClicked }) => {
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [sessionInitialFormState, setSessionInitialForm] = useState(sessionInitialValues);
  const [deletingSessionState, setDeletingSessions] = useState<any | null>(null);

  const [deleteResources, { isLoading: isDeleting }] = useDeleteResourcesMutation();

  const onDeleteConfirm = () => {
    deletingSessionState &&
      deleteResources(deletingSessionState?.course_topic_resource_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          // setSessionInitialForm(sessionInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error transactions');
        });
  };
  const onDeleteClicked = (data: ISessionInput) => {
    setDeletingSessions(data);
    toggleDeleteModal();
  };

  const closeModal = () => {
    setFormModal();
    setSessionInitialForm(sessionInitialValues);
  };
  let location = useLocation();
  const { id } = useParams();
  const state = location.state as CustomState;

  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const { data: sessionData, isLoading } = useListResourcesQuery({ id, page, pageSize });
  const canNext = sessionData?.data?.next;
  const canPrevious = sessionData?.data?.previous;

  const columns: any = [
    {
      Header: 'Name',
      accessor: 'name'
    },
    {
      Header: 'Type',
      accessor: 'resource_type'
    },
    {
      Header: 'Order',
      accessor: 'order'
    },
    {
      Header: 'File',
      Cell: (data: any) => {
        return (
          <>
            {data.row.original.resource_type == 'pdf' ? (
              <div style={{ display: 'flex' }}>
                <FaFileCsv size={20} className="m-2" />
              </div>
            ) : (
              <div style={{ display: 'flex' }}>
                <FaFileVideo size={20} className="m-2" />
              </div>
            )}
          </>
        );
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <img src={EditIcon} className="m-2" onClick={() => onEditClicked(data.row.original)} />
            <img
              src={DeleteIcon}
              className="m-2 icons"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <>
      <DataTable
        columns={columns}
        isLoading={isLoading}
        data={sessionData?.data?.results || []}
        tableHeaders={tableHeaders}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (canPrevious) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (canNext) {
              setPage(page + 1);
            }
          }
        }}
      />

      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Resource ?</p>
          </div>
        </>
      </ConfirmationModal>
    </>
  );
};

export default ResourcesTable;
