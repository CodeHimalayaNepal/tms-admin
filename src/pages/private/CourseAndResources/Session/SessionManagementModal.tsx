import React, { FC, useState } from 'react';
import { FormikProps, useFormik } from 'formik';
import TextInput from 'components/ui/TextInput';
import { SessionSchemaValidations } from '../Session/schema';
import { sessionInitialValues } from './schema';
import FormikValidationError from 'components/ui/FormikErrors';
import { FileDrop } from 'components/ui/FileDrop/FileDrop';
import EditorComponent from 'components/ui/Editor/editor';
import { useCreateCourseMutation, useListCoursesQuery } from 'redux/reducers/courses';
import {
  useCreateCourseResourceMutation,
  useUpdateCourseResourceMutation
} from 'redux/reducers/course-resource';
import useToggle from 'hooks/useToggle';
import { toast } from 'react-toastify';
import Modal from 'components/ui/Modal/modal';
import SecondaryButton from 'components/ui/SecondaryButton';
import PrimaryButton from 'components/ui/PrimaryButton';
import Container from 'components/ui/Container';
import { Navigate, useLocation, useNavigate, useParams } from 'react-router-dom';
import {
  useCreateSessionMutation,
  useListSessionByIdQuery,
  useUpdateSessionMutation
} from 'redux/reducers/session-resources';
import CustomSelect from 'components/ui/Editor/CustomSelect';

interface CustomState {
  course_id: string;
}

const SessionManagementForm = () => {
  let location = useLocation();
  const state = location.state as CustomState;
  const tms_course_id = state?.course_id;
  const navigate = useNavigate();
  const { sessionId, courseId } = useParams<{ sessionId: string; courseId: string }>();
  const [sessionIntiialValueState, setSessionInitialValue] = useState(sessionInitialValues);
  const isCreate = sessionId === 'create';
  const [createSession, { isLoading: creatingSession }] = useCreateSessionMutation();
  const [updateSession, { isLoading: updatingSession }] = useUpdateSessionMutation();
  const { data: sessionById, isLoading: isFetchingSessionDetail } = useListSessionByIdQuery(
    sessionId ? sessionId : '',
    { skip: isCreate || !sessionId }
  );

  const [sessionModal, toggleSessionModal] = useToggle(false);
  const [formModal, setFormModal] = useToggle(false);
  const closeModal = () => {
    setFormModal();
    setSessionInitialValue(sessionInitialValues);
  };

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: SessionSchemaValidations,
    initialValues: {
      title: '',
      description: '',
      learning_objective: '',
      order: 1,
      course: courseId,
      // course_topic_files: [],
      duration_hr: 0,
      methodology: '',
      course_topic_id: 0
    },
    onSubmit: (values) => {
      let executeFunc = createSession;
      let cid: any = tms_course_id;
      if (sessionId) {
        executeFunc = updateSession;
        cid = courseId;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setFormModal();
          console.log(courseId, 'pop');
          navigate(`/modules/session/${courseId}`, {
            state: { course_id: cid }
          });
        })
        .catch((err: any) => {
          toast.error('Error transactions');
        });
    }
  });

  React.useEffect(() => {
    if (sessionById) {
      formik.setValues({
        course_topic_id: sessionById.data.id,
        title: sessionById.data.title,
        description: sessionById.data.description,
        duration_hr: sessionById.data.duration_hr,
        order: sessionById.data.order,
        // course_topic_files: [],
        learning_objective: sessionById.data.learning_objective,
        methodology: sessionById.data.methodology,
        course: sessionById.data.course
      });
    }
  }, [sessionById, isFetchingSessionDetail]);

  return (
    <>
      <Container title="Session">
        <label>{sessionId ? 'Update Session' : 'Create Session'}</label>
        <form onSubmit={formik.handleSubmit}>
          <div style={{ display: 'flex', gap: '6rem' }}>
            <TextInput
              label="Session Title"
              name="title"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.title}
              onBlur={formik.handleBlur}
              formik={formik}
              disableRequiredValidation
            />

            <TextInput
              label="Duration In Hours"
              name="duration_hr"
              type="number"
              onChange={formik.handleChange}
              value={formik.values.duration_hr}
              onBlur={formik.handleBlur}
              formik={formik}
            />
          </div>
          <div style={{ display: 'flex', gap: '6rem' }}>
            <TextInput
              label="Order"
              name="order"
              type="number"
              onChange={formik.handleChange}
              value={formik.values.order}
              onBlur={formik.handleBlur}
              formik={formik}
            />

            <TextInput
              label="Methodology"
              name="methodology"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.methodology}
              onBlur={formik.handleBlur}
              formik={formik}
              disableRequiredValidation
            />
          </div>

          {/* <CustomSelect
            options={courseCategory}
            placeholder={'Search by title'}
            value={formik.values.course}
            onChange={(value: any) => formik.handleChange({ target: { name: 'course', value } })}
            onBlur={(e: any) =>
              formik.handleBlur({ target: { name: 'course', value: e.target.value } })
            }
            // isMulti={true}
            label={'Select Course '}
          /> */}
          <FormikValidationError name="course" errors={formik.errors} touched={formik.touched} />

          <EditorComponent
            label="Learning Objective"
            containerClassName="mt-3"
            editorValue={formik.values.learning_objective}
            onChangeValue={(data) => formik.setFieldValue('learning_objective', data)}
          />
          <FormikValidationError
            name="learning_objective"
            errors={formik.errors}
            touched={formik.touched}
          />
          <EditorComponent
            label="Description"
            containerClassName="mt-3"
            editorValue={formik.values.description}
            onChangeValue={(data) => formik.setFieldValue('description', data)}
          />
          <FormikValidationError
            name="description"
            errors={formik.errors}
            touched={formik.touched}
          />

          {/* <div className="dropzone" style={{ marginTop: '5px', marginBottom: '10px' }}>
            <FileDrop
              multiple={true}
              files={formik.values.course_topic_files}
              setFiles={(data) => formik.setFieldValue('course_topic_files', data)}
              message="Drop resources here or click to upload."
            />
          </div> */}

          <div className="hstack gap-2 justify-content-end mt-4">
            <SecondaryButton
              onClick={() => {
                navigate('/modules/session');
              }}
              title="Close"
              isLoading={creatingSession || updatingSession}
            />
            <PrimaryButton
              title={!formik.values.course_topic_id ? 'Create Session' : 'Update Session'}
              iconName={`${formik.values.course_topic_id ? '' : 'ri-add-line'} align-bottom me-1`}
              type="submit"
              isLoading={creatingSession || updatingSession}
            />
          </div>
        </form>
      </Container>
    </>
  );
};

export default SessionManagementForm;
