import ActiveInactiveBadge from 'components/ui/ActiveInactiveBage/ActiveInactiveBadge';
import Container from 'components/ui/Container';
import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import useToggle from 'hooks/useToggle';
import React, { FC, useEffect, useState } from 'react';
import { useLocation, useNavigate, useParams, useSearchParams } from 'react-router-dom';
import { ISessionInput, useListSessionQuery } from 'redux/reducers/session-resources';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Modal from 'components/ui/Modal/modal';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import { useDeleteSessionMutation } from 'redux/reducers/session-resources';
import { IfStatement } from 'typescript';
import { sessionInitialValues } from './schema';
import EditIcon from '../../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../../assets/images/icons/delete.png';
import { skipToken } from '@reduxjs/toolkit/dist/query';

interface ISessionTable {
  tableHeaders?: () => React.ReactNode;
  onDeleteClicked?: (data: ISessionInput) => void;
  onEditClicked?: () => void;
}
interface CustomState {
  course_topic_id: string;
  course_id: string;
}
const SessionTable: FC<ISessionTable> = ({ tableHeaders, onEditClicked }) => {
  // const { id } = useParams<{ id: string}>();
  const [formModal, setFormModal] = useToggle(false);
  const [deleteModal, toggleDeleteModal] = useToggle(false);

  const [sessionInitialFormState, setSessionInitialForm] = useState(sessionInitialValues);
  const [deletingSessionState, setDeletingSessions] = useState<ISessionInput | null>(null);

  const [deleteSession, { isLoading: isDeleting }] = useDeleteSessionMutation();

  const onDeleteConfirm = () => {
    deletingSessionState &&
      deleteSession(deletingSessionState?.course_topic_id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setSessionInitialForm(sessionInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error transactions');
        });
  };
  const onDeleteClicked = (data: ISessionInput) => {
    setDeletingSessions(data);
    toggleDeleteModal();
  };

  const closeModal = () => {
    setFormModal();
    setSessionInitialForm(sessionInitialValues);
  };
  let location = useLocation();

  const state = location.state as CustomState;
  const course_id = state?.course_id;
  const navigate = useNavigate();
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data: sessionData, isLoading } = useListSessionQuery(
    {
      id: course_id,
      page,
      pageSize
    },
    { skip: !course_id }
  );

  const canNext = sessionData?.data?.next;
  const canPrevious = sessionData?.data?.previous;
  console.log({ canNext, canPrevious });
  const [sessionInitialValueState, setCourseInitialValue] = useState(sessionInitialValues);
  const onSessionResourseClicked = (data: string) => {
    window.open(data, '_blank', 'noopener,noreferrer');
  };

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: ' Session Name',
      accessor: 'title'
    },

    {
      Header: 'Order',
      accessor: 'order'
    },
    {
      Header: 'Session Duration (In Hrs)',
      accessor: 'duration_hr'
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <div style={{ display: 'flex' }}>
            <img
              src={EditIcon}
              style={{ marginRight: '10px', height: '20px', cursor: 'pointer' }}
              onClick={() => {
                navigate(
                  `/modules/sessionForm/edit/${data?.row?.original?.course_topic_id}/course_id/${course_id}`
                );
              }}
            />
            <img
              src={DeleteIcon}
              style={{ marginRight: '10px', height: '20px', cursor: 'pointer' }}
              onClick={() => {
                onDeleteClicked(data.row.original);
                toggleDeleteModal(true);
              }}
            />

            <div style={{ marginTop: '2px', position: 'relative' }}>
              <div className="dropdown d-flex justify-content-end">
                <a
                  href="#"
                  role="button"
                  id="dropdownMenuLink5"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <i className="ri-more-2-fill"></i>
                </a>

                <ul
                  className="dropdown-menu fixed z-[10]  position-fixed-important"
                  aria-labelledby="dropdownMenuLink"
                >
                  <li>
                    <a
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#showModal"
                      onClick={() =>
                        navigate(
                          `/modules/session/resources/${data?.row?.original?.course_topic_id}`
                        )
                      }
                    >
                      Resources
                    </a>
                  </li>
                  <li>
                    <a
                      className="dropdown-item"
                      href="#"
                      data-bs-toggle="modal"
                      data-bs-target="#showModal"
                      onClick={() =>
                        navigate(
                          `/modules/session/assignments/${data?.row?.original?.course_topic_id}`
                        )
                      }
                    >
                      Assignments
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        );
      }
    }
  ];
  return (
    <>
      <Container title="Sessions" subTitle="List of all sessions">
        <DataTable
          columns={columns}
          isLoading={isLoading}
          data={sessionData?.data?.results || []}
          tableHeaders={() => (
            <>
              <PrimaryButton
                title={sessionInitialValues.course_topic_id ? 'Update Session' : 'Create Session'}
                iconName="ri-add-line align-bottom me-1"
                onClick={() => {
                  setCourseInitialValue(sessionInitialValues);
                  // setFormModal();
                  navigate(`/modules/sessionForm/${course_id}`, {
                    state: { course_id: course_id }
                  });
                }}
                type="button"
              />
            </>
          )}
          pagination={{
            canNext,
            canPrevious,
            prevPage: () => {
              if (canPrevious) {
                page > 1 && setPage(page - 1);
              }
            },
            nextPage: () => {
              if (canNext) {
                setPage(page + 1);
              }
            }
          }}
        />

        <ConfirmationModal
          isOpen={deleteModal}
          onClose={toggleDeleteModal}
          onConfirm={onDeleteConfirm}
          title="Delete Confirmations"
          isLoading={isDeleting}
        >
          <>
            <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
              <h4>Are you Sure ?</h4>
              <p className="text-muted ">You want to Remove this Record ?</p>
            </div>
          </>
        </ConfirmationModal>
      </Container>
    </>
  );
};

export default SessionTable;
