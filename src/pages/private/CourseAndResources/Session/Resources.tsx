import React from 'react';
import Container from 'components/ui/Container';
import ResourcesTable from './ResourcesTable';
import useToggle from 'hooks/useToggle';
import PrimaryButton from 'components/ui/PrimaryButton';
import Modal from 'components/ui/Modal/modal';
import SecondaryButton from 'components/ui/SecondaryButton';
import ResourceForm from './ResourceForm';
import { useFormik } from 'formik';
import { ResourceInitialValues, ResourceSchemaValidations } from './ResourceSchema';
import {
  useCreateResourcesMutation,
  useUpdateResourceMutation
} from 'redux/reducers/session-resources';
import { toast } from 'react-toastify';

const Resources = () => {
  const [feedbackModal, setFeedbackModal] = useToggle(false);
  const [feedbackInitialFormState, setFeedbackInitialForm] =
    React.useState<any>(ResourceInitialValues);
  const [createResource, { isLoading: creatingResources }] = useCreateResourcesMutation();
  const [updateResource, { isLoading: updatingResources }] = useUpdateResourceMutation();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: ResourceSchemaValidations,
    initialValues: feedbackInitialFormState,

    onSubmit: (values, { resetForm }) => {
      let executeFunc = createResource;
      if (values.course_topic_resource_id) {
        executeFunc = updateResource;
      }

      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          setFeedbackInitialForm(ResourceInitialValues);
          resetForm();

          toast.success('Created Successfully');
          //setSessionInitialValue(sessionInitialValues);
          setFeedbackModal();
        })
        .catch((err: any) => {
          resetForm();

          toast.error('Error ');
          setFeedbackInitialForm(ResourceInitialValues);

          toast.error(err?.data?.file[0]);
        });
    }
  });

  const onEditClicked = (data: any) => {
    setFeedbackInitialForm(data);
    setFeedbackModal();
  };
  return (
    <Container title="Resources">
      <ResourcesTable
        tableHeaders={() => (
          <>
            <PrimaryButton
              //   title={sessionInitialValues.course_topic_id ? 'Update Session' : 'Create new Resources'}
              title="Create Resources"
              iconName="ri-add-line align-bottom me-1"
              onClick={() => {
                setFeedbackModal();
                setFeedbackInitialForm(ResourceInitialValues);
              }}
              type="button"
            />
          </>
        )}
        onEditClicked={onEditClicked}
      />
      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={
            feedbackInitialFormState?.course_topic_resource_id
              ? 'Update Resources'
              : 'Create Resources'
          }
          isModalOpen={feedbackModal}
          closeModal={setFeedbackModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <PrimaryButton
                title="Update"
                iconName={'ri-add-line align-bottom me-1'}
                type="submit"
                isLoading={creatingResources || updatingResources}
              />

              <SecondaryButton
                onClick={setFeedbackModal}
                title="Discard"
                // isLoading={creatingResources}
              />
            </div>
          )}
        >
          <ResourceForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default Resources;
