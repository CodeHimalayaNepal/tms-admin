//import { IEvaluationInuput } from 'redux/reducers/evaluation';
import { IFeedbackFormInput } from 'redux/reducers/Feedback2';
import * as Yup from 'yup';

export const ResourceInitialValues: any = {
  name: '',
  resource_type: '',
  order: 0,
  topic: 0,
  file: []
};

export const ResourceSchemaValidations = Yup.object().shape({
  name: Yup.string().required('Name is required'),
  resource_type: Yup.string().required('Type is required'),
  order: Yup.number().required('Order is required'),
  topic: Yup.number().required('Topic is required')
  // question_type: Yup.string().required('feedback type is required')
});
