import CustomSelect from 'components/ui/Editor/CustomSelect';
import TextInput from 'components/ui/TextInput';
import { Field, FormikProps } from 'formik';
import { FC } from 'react';
import FormikValidationError from 'components/ui/FormikErrors';
import { useEffect } from 'react';
import { FileDrop } from 'components/ui/FileDrop/FileDrop';
import { useParams } from 'react-router-dom';

interface IFeedbackForm {
  formik: FormikProps<any>;
}

const ResourceForm: FC<IFeedbackForm> = ({ formik }) => {
  const { id } = useParams();
  const courseCategory = [
    { value: 'video', label: 'video' },
    {
      value: 'pdf',
      label: 'pdf'
    }
  ];
  useEffect(() => {
    formik.setFieldValue('topic', id);
  }, [id]);

  return (
    <form>
      <div className="row">
        {/*end col*/}
        <div className="col-6">
          <TextInput
            containerClassName="mt-3 "
            label="Name"
            name="name"
            type="text"
            height={20}
            onChange={formik.handleChange}
            value={formik.values.name}
            onBlur={formik.handleBlur}
            formik={formik}
          />
        </div>

        <div className="col-6">
          <TextInput
            containerClassName="mt-3 "
            label="Order"
            name="order"
            type="number"
            height={20}
            onChange={formik.handleChange}
            value={formik.values.order}
            onBlur={formik.handleBlur}
            formik={formik}
          />
        </div>
        <CustomSelect
          options={courseCategory}
          placeholder={'Resource Type'}
          value={formik.values.resource_type}
          onChange={(value: any) =>
            formik.handleChange({ target: { name: 'resource_type', value } })
          }
          onBlur={(e: any) =>
            formik.handleBlur({ target: { name: 'course', value: e.target.value } })
          }
          label={'Select Type '}
        />
        <FormikValidationError
          name="resource_type"
          errors={formik.errors}
          touched={formik.touched}
        />

        <div className="dropzone" style={{ marginTop: '5px', marginBottom: '10px' }}>
          <FileDrop
            files={Array.isArray(formik.values.file) ? formik.values.file : []}
            setFiles={(data) => formik.setFieldValue('file', data)}
            message="Drop resources here or click to upload."
          />
        </div>
      </div>
    </form>
  );
};

export default ResourceForm;
