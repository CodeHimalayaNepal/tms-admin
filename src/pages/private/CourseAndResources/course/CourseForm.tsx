import React from 'react';
import CustomSelect from 'components/ui/Editor/CustomSelect';
import EditorComponent from 'components/ui/Editor/editor';
import { FileDrop } from 'components/ui/FileDrop/FileDrop';
import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';

import {
  useCreateCourseMutation,
  useListCoursesCategoryQuery,
  useListCoursesEditQuery,
  useUpdateCourseMutation
} from 'redux/reducers/courses';

import { useFormik } from 'formik';
import { courseInitialValues, CourseSchemaValidations } from './schema';
import useToggle from 'hooks/useToggle';
import { toast } from 'react-toastify';
import SecondaryButton from 'components/ui/SecondaryButton';
import PrimaryButton from 'components/ui/PrimaryButton';
import Container from 'components/ui/Container';
import { useNavigate, useParams } from 'react-router-dom';

const CourseForm = () => {
  const navigate = useNavigate();
  const { courseId } = useParams<{ courseId: string }>();

  const [createCourse, { isLoading: creatingCourse }] = useCreateCourseMutation();
  const isCreate = courseId === 'create';

  const [courseUpdate, { isLoading: updatingCourse }] = useUpdateCourseMutation();
  const { data: coursesById, isLoading: isFetchingCoursesDetail } = useListCoursesEditQuery(
    courseId ? courseId : '',
    { skip: isCreate || !courseId }
  );

  const { data: coursecategory } = useListCoursesCategoryQuery();

  const [formModal, setFormModal] = useToggle(false);

  const categoryLists = coursecategory?.data?.map((item: any) => {
    return {
      value: item.course_category_id.toString(),
      label: item.name
    };
  });
  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: CourseSchemaValidations,
    initialValues: courseInitialValues,
    onSubmit: (values) => {
      let executeFunc = createCourse;

      if (courseId) {
        executeFunc = courseUpdate;
      }
      const sendValue = { ...values, id: courseId, price: 0 };
      if (typeof sendValue.image === 'string') {
        const { image, ...rest } = sendValue;
        executeFunc({ ...rest })
          .unwrap()
          .then((data: any) => {
            toast.success('Created Successfully');
            setFormModal();
            navigate('/modules');
          })
          .catch((err: any) => {
            console.log(err);
            if (err?.data?.course_files) {
              toast.error('Course files is required');
            }
          });
      } else {
        executeFunc(sendValue)
          .unwrap()
          .then((data: any) => {
            toast.success('Created Successfully');
            setFormModal();
            navigate('/modules');
          })
          .catch((err: any) => {
            console.log(err);
            if (err?.data?.course_files) {
              toast.error('Course files is required');
            }
          });
      }
    }
  });

  React.useEffect(() => {
    if (coursesById) {
      formik.setValues({
        code: coursesById.data.code,
        id: coursesById.data.course_id,
        image: coursesById.data.image,
        title: coursesById.data.title,
        description: coursesById.data.description,
        duration_hr: coursesById.data.duration_hr,
        publish: coursesById.data.publish ? coursesById.data.publish : false,
        course_files: [],
        category: coursesById.data.course_category_details.course_category_id || 0
      });
    }
  }, [coursesById, isFetchingCoursesDetail]);

  return (
    <>
      <Container title="Module Form">
        <div className="container">
          <form onSubmit={formik.handleSubmit}>
            <div className="card">
              <div style={{ marginLeft: '20px' }}>
                <h4>Please kindly fill up the form*</h4>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <h5>Module Information</h5>
                </div>
                <div className="row">
                  <div className="dropzone ">
                    <FileDrop
                      files={formik.values.image}
                      setFiles={(data) => formik.setFieldValue('image', data)}
                      message="Click to upload the logo"
                      multiple={true}
                      // accept={{ 'image/png': ['.png', '.jgp', '.jpeg', '.gif'] }}
                      preview={true}
                    />
                    <b> {`*** Image Size < 5Mb `}</b>
                    <FormikValidationError
                      name="image"
                      errors={formik.errors}
                      touched={formik.touched}
                    />
                  </div>
                  <div className="col-9">
                    <div style={{ display: 'flex', gap: '2rem', justifyContent: 'flex-end' }}>
                      <TextInput
                        label="Module Title"
                        name="title"
                        type="text"
                        onChange={formik.handleChange}
                        value={formik.values.title}
                        onBlur={formik.handleBlur}
                        formik={formik}
                        disableRequiredValidation
                      />

                      <TextInput
                        label=" Module Code"
                        name="code"
                        type="text"
                        onChange={formik.handleChange}
                        value={formik.values.code}
                        onBlur={formik.handleBlur}
                        formik={formik}
                        disableRequiredValidation
                      />
                    </div>
                    <div style={{ display: 'flex', gap: '2rem', justifyContent: 'flex-end' }}>
                      <TextInput
                        label="Duration In Hours"
                        name="duration_hr"
                        type="number"
                        onChange={formik.handleChange}
                        value={formik.values.duration_hr}
                        onBlur={formik.handleBlur}
                        formik={formik}
                        disableRequiredValidation
                      />
                      <CustomSelect
                        name="category"
                        options={categoryLists}
                        label={'Module Category'}
                        // placeholder={'Search by title'}
                        value={formik.values.category}
                        onChange={(value: any) => formik.setFieldValue('category', value)}
                        formik={formik}
                      />
                      <FormikValidationError
                        name="category"
                        errors={formik.errors}
                        touched={formik.touched}
                      />
                    </div>
                  </div>
                </div>

                <div className="dropzone">
                  <FileDrop
                    defaultImage={coursesById?.data?.course_files ?? ''}
                    files={formik.values.course_files}
                    setFiles={(data) => formik.setFieldValue('course_files', data)}
                    message="Drop resources here  or click to upload."
                    multiple={true}
                  />
                  <b> {`*** File Size < 200Mb`}</b>
                </div>

                <EditorComponent
                  label="Descriptions"
                  containerClassName="mt-3"
                  editorValue={formik.values.description}
                  onChangeValue={(data) => formik.setFieldValue('description', data)}
                />
                <FormikValidationError
                  name="description"
                  errors={formik.errors}
                  touched={formik.touched}
                />
                <div className="col-12 mb-3 mt-4 d-flex align-items-end">
                  <div className="form-check form-switch form-switch-md mb-3">
                    <label className="form-check-label" htmlFor="customSwitchsizesm">
                      LMS Display
                    </label>
                    <input
                      type="checkbox"
                      className="form-check-input"
                      id="customSwitchsizesm"
                      checked={formik.values.publish}
                      onChange={(e) => {
                        formik.setFieldValue('publish', !formik.values.publish);
                      }}
                    />
                  </div>
                </div>

                <div className="hstack gap-2 justify-content-end">
                  <SecondaryButton
                    onClick={() => {
                      navigate('/modules');
                    }}
                    title="Close"
                    isLoading={creatingCourse || updatingCourse}
                  />
                  <PrimaryButton
                    title={courseId ? 'Update Module' : 'Create Module'}
                    iconName={`${courseId ? '' : 'ri-add-line'} align-bottom me-1`}
                    type="submit"
                    isLoading={creatingCourse || updatingCourse}
                  />
                </div>
              </div>
            </div>
          </form>
        </div>
      </Container>
    </>
  );
};

export default CourseForm;
