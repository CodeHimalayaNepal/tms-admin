import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import React, { useState } from 'react';
import { toast } from 'react-toastify';
import {
  CoursesResponse,
  ICoursesInput,
  useCreateCourseMutation,
  useDeleteCourseMutation,
  useListCoursesQuery,
  useUpdateCourseMutation
} from 'redux/reducers/courses';
import { useCreateCourseResourceMutation } from 'redux/reducers/course-resource';
import CourseForm from './CourseForm';
import CourseListTable from './CourseListsTable';

import {
  courseInitialValues,
  CourseSchemaValidations,
  resourceInitialValues,
  ResourceSchemaValidations,
  SessionSchemaValidations
} from './schema';
import { sessionInitialValues } from '../Session/schema';
import { useNavigate } from 'react-router-dom';
import SessionManagementModal from '../Session/SessionManagementModal';

const CoursesAndResources = () => {
  const [formModal, setFormModal] = useToggle(false);

  const [courseInitialValueState, setCourseInitialValue] = useState(courseInitialValues);
  const [sessionIntiialValueState, setSessionInitialValue] = useState(sessionInitialValues);
  const [deletingCourseState, setDeleteCourse] = useState<ICoursesInput | null>(null);
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [resourceModal, toggleResourceModal] = useToggle(false);
  const [sessionModal, toggleSessionModal] = useToggle(false);

  const [createCourse, { isLoading: creatingCourse }] = useCreateCourseMutation();
  const [updateCourse, { isLoading: updatingCourse }] = useUpdateCourseMutation();
  const [deleteCourse, { isLoading: isDeleting }] = useDeleteCourseMutation();

  const navigate = useNavigate();

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: CourseSchemaValidations,
    initialValues: courseInitialValueState,
    onSubmit: (values) => {
      let executeFunc = createCourse;
      if (values.tms_course_id) {
        executeFunc = updateCourse;
      }
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Created Successfully');
          setCourseInitialValue(courseInitialValues);
          setFormModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
    }
  });

  const onResourceClicked = (data: ICoursesInput) => {
    setCourseInitialValue(data);
    toggleResourceModal();
  };

  const onCourseResourseClicked = (data: string) => {
    window.open(data, '_blank', 'noopener,noreferrer');
  };

  const onEditClicked = (data: ICoursesInput) => {
    navigate(`/modules/modal/edit/${data?.id}`);

    // //${data.id}
  };
  const onDeleteConfirm = () => {
    deletingCourseState &&
      deleteCourse(deletingCourseState?.id)
        .unwrap()
        .then((data: any) => {
          toast.success('Deleted Successfully');
          setCourseInitialValue(courseInitialValues);
          toggleDeleteModal();
        })
        .catch((err: any) => {
          toast.error('Error');
        });
  };

  const onDeleteClicked = (data: ICoursesInput) => {
    setDeleteCourse(data);
    toggleDeleteModal();
  };

  const closeModal = () => {
    setFormModal();
    setCourseInitialValue(courseInitialValues);
  };

  return (
    <Container title="Modules" subTitle="List of all modules">
      <CourseListTable
        tableHeaders={() => (
          <div>
            <PrimaryButton
              title={courseInitialValueState.tms_course_id ? 'Update Module' : 'Create Module'}
              iconName="ri-add-line align-bottom me-1"
              onClick={() => {
                setCourseInitialValue(courseInitialValues);
                navigate('/modules/modal');
              }}
              type="button"
            />
          </div>
        )}
        onEditClicked={onEditClicked}
        onDeleteClicked={onDeleteClicked}
        setFormModal={setFormModal}
        onResourceClicked={onResourceClicked}
        onCourseResourseClicked={onCourseResourseClicked}
      />
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Record ?</p>
          </div>
        </>
      </ConfirmationModal>
      <form onSubmit={formik.handleSubmit}>
        <Modal
          title={courseInitialValueState.tms_course_id ? 'Update ' : 'Add Course '}
          isModalOpen={formModal}
          closeModal={closeModal}
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton
                onClick={closeModal}
                title="Close"
                isLoading={creatingCourse || updatingCourse}
              />
              <PrimaryButton
                title={courseInitialValueState.tms_course_id ? 'Update Course' : 'Add Course'}
                iconName="ri-add-line align-bottom me-1"
                type="submit"
                isLoading={creatingCourse || updatingCourse}
              />
            </div>
          )}
        ></Modal>
      </form>
    </Container>
  );
};

export default CoursesAndResources;
