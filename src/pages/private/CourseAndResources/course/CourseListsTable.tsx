import ActiveInactiveBadge from 'components/ui/ActiveInactiveBage/ActiveInactiveBadge';
import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC, useState } from 'react';
import { FaRegEdit } from 'react-icons/fa';
import { RiDeleteBin5Line } from 'react-icons/ri';
import { useNavigate } from 'react-router-dom';
import { ICoursesInput, useListCoursesQuery } from 'redux/reducers/courses';
import session from './assets1/seminar.png';
import EditIcon from '../../../../assets/images/icons/edit.png';
import DeleteIcon from '../../../../assets/images/icons/delete.png';

interface ITraineeDataTable {
  tableHeaders: () => React.ReactNode;
  onEditClicked: (data: ICoursesInput) => void;
  onDeleteClicked: (data: ICoursesInput) => void;
  setFormModal: () => void;
  onResourceClicked: (data: ICoursesInput) => void;
  onCourseResourseClicked: (data: string) => void;
}
const CourseListTable: FC<ITraineeDataTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked,
  setFormModal,
  onResourceClicked,
  onCourseResourseClicked
}) => {
  const navigate = useNavigate();
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data: coursesdata, isLoading } = useListCoursesQuery({ page: page, pageSize: 10 });

  const canNext = coursesdata?.data?.next;
  const canPrevious = coursesdata?.data?.previous;

  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{page === 1 ? data.row.index + 1 : data.row.index + (page - 1) * 10 + 1}</>;
      }
    },
    {
      Header: ' Module Name',
      accessor: 'title'
    },
    {
      Header: 'Module code',
      accessor: 'code'
    },
    // {
    //   Header: 'Module Resource',
    //   Cell: (data: any) => {
    //     return (
    //       <div className="relative pe-auto">
    //         {data.row.original?.course_resource_list?.map((item: any, index: number) => {
    //           let filename = item.file.substring(item.file.lastIndexOf('/') + 1);
    //           return (
    //             <a
    //               key={index}
    //               onClick={() => onCourseResourseClicked(item.file)}
    //               className={`pe-auto badge badge-soft-info p-2 bg-light text-dark`}
    //             >
    //               {filename}
    //             </a>
    //           );
    //         })}
    //       </div>
    //     );
    //   }
    // },
    {
      Header: 'Action',
      Cell: (data: any) => {
        return (
          <>
            <img
              src={EditIcon}
              className="icons"
              onClick={() => onEditClicked(data.row.original)}
            />
            {/* <PrimaryButton
              title="Session"
              type="button"
              onClick={() => {
                navigate('/modules/session', {
                  state: { course_id: data.row.original.course_id }
                });
              }} */}
            <img
              src={session}
              className="icons"
              style={{ height: '20px', width: '20px', marginLeft: '10px', cursor: 'pointer' }}
              onClick={() => {
                navigate(`/modules/session/${data.row.original.id}`, {
                  state: { course_id: data.row.original.id }
                });
              }}
            />

            {/* <PrimaryButton
              isDanger
              title="Delete"
              type="button"
              className="m-2"
              onClick={() => {
                onDeleteClicked(data.row.original);
              }} */}
            <img
              src={DeleteIcon}
              className="m-2 icons"
              onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div>
      <DataTable
        columns={columns}
        data={coursesdata?.data?.results || []}
        isLoading={isLoading}
        tableHeaders={tableHeaders}
        pagination={{
          canNext,
          canPrevious,
          prevPage: () => {
            if (canPrevious) {
              page > 1 && setPage(page - 1);
            }
          },
          nextPage: () => {
            if (canNext) {
              setPage(page + 1);
            }
          }
        }}
      />
    </div>
  );
};

export default CourseListTable;
