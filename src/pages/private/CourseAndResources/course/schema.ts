import { STATUS } from 'common/enum';
import { ICoursesInput } from 'redux/reducers/courses';
import { ISessionInput } from 'redux/reducers/session-resources';
import * as Yup from 'yup';

export const courseInitialValues: ICoursesInput = {
  title: '',
  code: '',
  description: '',
  duration_hr: 0,
  image: [],
  publish: true,
  category: null,
  course_files: [],
  id: 0
};

// export const courseInitialValuesSchema = {
//   title: '',
//   code: '',
//   description: '',
//   duration_hr: '',
//   category: []
// };

export const CourseSchemaValidations = Yup.object().shape({
  title: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Title is required'),
  code: Yup.string().min(2, 'Too Short!').max(8, 'Too Long!').required('Code is required'),
  description: Yup.string().required('Description is required'),
  duration_hr: Yup.number().required('Duration is required'),
  category: Yup.number().min(1, 'category is required')
});
export const resourceInitialValues = {
  course: 0,
  file: []
};

export const ResourceSchemaValidations = Yup.object().shape({
  file: Yup.array().required('Description is required'),
  course: Yup.number().required('Duration is required')
});
export const SessionSchemaValidations = Yup.object().shape({
  // title: Yup.array().required('title is required'),
  // description: Yup.array().required('description is required'),
  // code: Yup.array().required('code is required'),
  // duration: Yup.array().required('duration is required'),
  // file: Yup.array().required('file is required')
});
