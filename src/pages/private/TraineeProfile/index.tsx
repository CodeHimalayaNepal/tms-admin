import { Avatar } from 'common/constant';
import { USERSTATUS } from 'common/enum';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { FormikProvider, useFormik } from 'formik';
import { FC, ReactNode, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useCurrentUserDetailsQuery } from 'redux/reducers/administrator';
import { useListProvinceQuery } from 'redux/reducers/geography';
import { useTraineeDetailQuery, useUpdateTraineeDetailMutation } from 'redux/reducers/user';
import * as Yup from 'yup';

import Experience from './Experience';
import PersonalInfo from './PersonalInfo';
import Training from './Training';

export enum PlaceType {
  'Nepal' = 1,
  'Foreign Country' = 2
}

export interface TraineeProfile {
  first_name: string;
  first_name_np: string;
  last_name: string;
  last_name_np: string;
  middle_name: string;
  middle_name_np: string;
  dob: string;
  mobile_number: string;

  address: {
    province: string;
    district: string;
    municipality: string;
    ward_no: string;
    block_no: string;
    house_num: string;
    tole_street: string;
  };

  previous_training: {
    name: string;
    from_date: string;
    to_date: string;
    place_type: PlaceType;
    place: string;
  }[];

  experience: {
    from_date: string;
    to_date: string;
    organization: string;
    department: string;
    level: string;
    is_first_service: 'True' | 'False';
    is_current_service: 'False' | 'True';
  }[];

  signature: File | null;
  image: File | null;
}

const initialValues: TraineeProfile = {
  first_name: '',
  first_name_np: '',
  middle_name: '',
  middle_name_np: '',
  last_name: '',
  last_name_np: '',
  mobile_number: '',
  dob: '',
  address: {
    province: '',
    district: '',
    municipality: '',
    ward_no: '',
    block_no: '',
    house_num: '',
    tole_street: ''
  },
  previous_training: [],
  experience: [],
  signature: null as File | null,
  image: null as File | null
};

const validationSchema = Yup.object().shape({
  first_name: Yup.string().required('First Name is required'),
  first_name_np: Yup.string().required('First Name (Nepali) is required'),
  middle_name: Yup.string(),
  middle_name_np: Yup.string(),
  last_name: Yup.string().required('Last Name is required'),
  last_name_np: Yup.string().required('Last Name (Nepali) is required'),
  mobile_number: Yup.string().required('Mobile Number is required'),
  dob: Yup.string().required('Date of birth is required'),
  address: Yup.object().shape({
    province: Yup.string().required('Province is required'),
    district: Yup.string().required('District is required'),
    municipality: Yup.string().required('Municipality is required'),
    ward_no: Yup.string().required('Ward No is required'),
    block_no: Yup.string().required('Block No is required'),
    house_num: Yup.string().required('House No is required'),
    tole_street: Yup.string().required('Street is required')
  }),
  previous_training: Yup.array(),
  experience: Yup.array()
});

const fileValidationSchema = Yup.object().shape({
  image: Yup.mixed().required('Image is required'),
  signature: Yup.mixed().required('Signature is required')
});

const TraineeProfile = () => {
  const [activeStep, setActiveStep] = useState(1);
  const [stepCompleted, setStepCompleted] = useState({ 1: false, 2: false });

  const navigate = useNavigate();

  const { data: provinceList = [] } = useListProvinceQuery({});
  const { data: userData } = useCurrentUserDetailsQuery();
  const userId = userData?.data?.id || '';

  const { data: traineeDetail } = useTraineeDetailQuery(
    { traineeId: userId },
    {
      skip: !!userId
    }
  );

  const disableEdit = userData?.data.user_details.status
    ? parseInt(userData?.data.user_details.status) === USERSTATUS.Accepted
    : false;

  const [updateTraineeDetail, { isLoading }] = useUpdateTraineeDetailMutation();

  const getValidations = (step: number) => {
    if (step === 3) return fileValidationSchema;
    return validationSchema;
  };

  const getImage = (image: File) => {
    try {
      return URL.createObjectURL(image);
    } catch (error) {
      return '';
    }
  };

  const updateProfile = async (values: TraineeProfile) => {
    if (activeStep === 3) {
      try {
        if (userId) {
          await updateTraineeDetail({
            traineeId: userId,
            body: {
              ...values,
              first_name: disableEdit ? traineeDetail?.first_name ?? '' : values.first_name,
              middle_name: disableEdit ? traineeDetail?.middle_name ?? '' : values.middle_name,
              last_name: disableEdit ? traineeDetail?.last_name ?? '' : values.last_name,
              mobile_number: disableEdit
                ? traineeDetail?.mobile_number ?? ''
                : values.mobile_number,
              experience: values.experience.map((e) => {
                let exp = e as typeof e & { id?: string };
                delete exp.id;
                return { ...exp };
              }),
              previous_training: values.previous_training.map((t) => {
                let training = t as typeof t & { id?: string };
                delete training.id;
                return { ...training };
              })
            }
          }).unwrap();
          toast.success('Profile updated successfully!');
          navigate('/');
        }
      } catch (error) {
        const err = error as { data: Record<string, string> };
        toast.error(err?.data ? JSON.stringify(err?.data) : 'Profile updated failed!');
      }
    } else {
      setActiveStep((step) => step + 1);
      setStepCompleted((oldState) => ({
        1: activeStep === 1 || oldState[1],
        2: activeStep === 2 || oldState[2]
      }));
    }
  };

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: getValidations(activeStep),
    initialValues,
    onSubmit: updateProfile
  });

  useEffect(() => {
    if (traineeDetail) {
      formik.resetForm({
        values: {
          ...initialValues,
          dob: traineeDetail.dob ?? '',
          first_name: traineeDetail.first_name ?? '',
          first_name_np: traineeDetail.first_name_np ?? '',
          middle_name: traineeDetail.middle_name ?? '',
          middle_name_np: traineeDetail.middle_name_np ?? '',
          last_name: traineeDetail.last_name ?? '',
          last_name_np: traineeDetail.last_name_np ?? '',
          mobile_number: traineeDetail.mobile_number ?? '',
          address: {
            province:
              provinceList
                .find((p) => p.name_en === traineeDetail.address.province?.name_en)
                ?.id?.toString() ?? '',
            district: traineeDetail.address.district?.toString(),
            municipality: traineeDetail.address.municipality,
            ward_no: traineeDetail.address.ward_no?.toString(),
            block_no: traineeDetail.address.block_no,
            house_num: traineeDetail.address.house_num,
            tole_street: traineeDetail.address.tole_street
          },
          experience:
            traineeDetail.experience.map((t) => ({
              ...t,
              is_first_service: t.is_first_service ? 'True' : 'False',
              is_current_service: t.is_current_service ? 'True' : 'False'
            })) ?? [],
          previous_training: traineeDetail.previous_training ?? []
          //   image: traineeDetail.image ?? null,
          //   signature: traineeDetail.signature ?? null
        }
      });

      setStepCompleted({
        1: true,
        2: true
      });
    }
  }, [traineeDetail]);

  return (
    <TraineeProfileWrapper>
      <div className="position-relative mx-n4 mt-n4">
        <div className="profile-wid-bg profile-setting-img">
          <img src="/assets/images/profile-bg.jpg" className="profile-wid-img" alt="" />
          <div className="overlay-content"></div>
        </div>
      </div>

      <div className="row">
        <div className="col-xxl-3">
          <div className="card mt-n5">
            <div className="card-body p-4">
              <div className="text-center">
                <div className="profile-user position-relative d-inline-block mx-auto  mb-4">
                  <img
                    src={
                      formik.values.image
                        ? getImage(formik.values.image)
                        : traineeDetail?.image ?? Avatar
                    }
                    className="rounded-circle avatar-xl img-thumbnail user-profile-image"
                    alt="user-profile-image"
                  />
                  <div className="avatar-xs p-0 rounded-circle profile-photo-edit">
                    <input
                      id="profile-img-file-input"
                      type="file"
                      className="profile-img-file-input"
                      name="image"
                      onChange={(e) => {
                        formik.handleChange({
                          target: { name: 'image', value: e.target.files?.[0] ?? null }
                        });
                      }}
                    />
                    <label
                      htmlFor="profile-img-file-input"
                      className="profile-photo-edit avatar-xs"
                    >
                      <span className="avatar-title rounded-circle bg-light text-body">
                        <i className="ri-camera-fill" />
                      </span>
                    </label>
                  </div>
                </div>

                <h5 className="fs-14 mb-1">Profile Image</h5>

                {formik.touched.image && formik.errors.image && (
                  <span className="text-danger">{formik.errors.image}</span>
                )}
              </div>
            </div>
          </div>

          <div className="card mt-5">
            <div className="card-body p-4">
              <div className="text-center">
                <div className="profile-user position-relative d-inline-block mx-auto  mb-4">
                  <img
                    src={
                      formik.values.signature
                        ? getImage(formik.values.signature)
                        : traineeDetail?.signature ?? Avatar
                    }
                    className="rounded-circle avatar-xl img-thumbnail user-profile-image"
                    alt="user-profile-image"
                  />
                  <div className="avatar-xs p-0 rounded-circle profile-photo-edit">
                    <input
                      id="profile-signature-file-input"
                      type="file"
                      className="profile-img-file-input"
                      name="signature"
                      onChange={(e) =>
                        formik.handleChange({
                          target: { name: 'signature', value: e.target.files?.[0] ?? null }
                        })
                      }
                    />
                    <label
                      htmlFor="profile-signature-file-input"
                      className="profile-photo-edit avatar-xs"
                    >
                      <span className="avatar-title rounded-circle bg-light text-body">
                        <i className="ri-camera-fill" />
                      </span>
                    </label>
                  </div>
                </div>

                <h5 className="fs-14 mb-1">Signature</h5>

                {formik.touched.signature && formik.errors.signature && (
                  <span className="text-danger">{formik.errors.signature}</span>
                )}
              </div>
            </div>
          </div>
        </div>

        <div className="col-xxl-9">
          <div className="card mt-xxl-n5">
            <div className="card-header">
              <ul
                className="nav nav-tabs-custom rounded card-header-tabs border-bottom-0"
                role="tablist"
              >
                <li className="nav-item">
                  <a
                    onClick={() => setActiveStep(1)}
                    className={`nav-link ${activeStep === 1 ? 'active' : ''}`}
                    data-bs-toggle="tab"
                    href="#personalDetail"
                    role="tab"
                  >
                    <i className="fas fa-home"></i> Personal Detail
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    onClick={() => stepCompleted[1] && setActiveStep(2)}
                    className={`nav-link ${
                      activeStep === 2 ? 'active' : stepCompleted[1] ? '' : 'disabled'
                    }`}
                    data-bs-toggle="tab"
                    href="#experience"
                    role="tab"
                  >
                    <i className="far fa-envelope"></i> Experience
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    onClick={() => stepCompleted[2] && setActiveStep(3)}
                    className={`nav-link ${
                      activeStep === 3 ? 'active' : stepCompleted[2] ? '' : 'disabled'
                    }`}
                    data-bs-toggle="tab"
                    href="#training"
                    role="tab"
                  >
                    <i className="far fa-user"></i> Training
                  </a>
                </li>
              </ul>
            </div>

            <div className="card-body p-4">
              <FormikProvider value={formik}>
                <form onSubmit={formik.handleSubmit}>
                  {activeStep === 1 && (
                    <div className="tab-content">
                      <div className="tab-pane active" id="personalDetail" role="tabpanel">
                        <PersonalInfo disableEdit={disableEdit} />
                      </div>
                    </div>
                  )}

                  {activeStep === 2 && (
                    <div className="tab-content">
                      <div className="tab-pane active" id="experience" role="tabpanel">
                        <Experience />
                      </div>
                    </div>
                  )}

                  {activeStep === 3 && (
                    <div className="tab-content">
                      <div className="tab-pane active" id="training" role="tabpanel">
                        <Training />
                      </div>
                    </div>
                  )}

                  <div className="row">
                    <div className="col mt-4">
                      <div className="d-flex justify-content-center">
                        {activeStep > 1 && (
                          <SecondaryButton
                            title="Previous Step"
                            onClick={() => setActiveStep((step) => step - 1)}
                          />
                        )}

                        <PrimaryButton
                          title={activeStep === 3 ? 'Submit Details' : 'Next Step'}
                          type="submit"
                          className="mx-4"
                          isLoading={isLoading}
                        />
                      </div>
                    </div>
                  </div>
                </form>
              </FormikProvider>
            </div>
          </div>
        </div>
      </div>
    </TraineeProfileWrapper>
  );
};

export default TraineeProfile;

const TraineeProfileWrapper: FC<{ children: ReactNode }> = ({ children }) => {
  return (
    <div className="main-content">
      <div className="page-content">
        <div className="container-fluid">{children}</div>
      </div>
    </div>
  );
};
