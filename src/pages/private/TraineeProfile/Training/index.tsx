import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import TextInput from 'components/ui/TextInput';
import { Formik, useFormikContext } from 'formik';
import React, { useState } from 'react';
import { Cell } from 'react-table';
import * as Yup from 'yup';

import { PlaceType, TraineeProfile } from '../';

const validationSchema = Yup.object().shape({
  from_date: Yup.string().required('From date is required'),
  to_date: Yup.string().required('To date is required'),
  name: Yup.string().required('Name is required'),
  place: Yup.string().required('Place is required')
});

function Training() {
  const defaultValue = {
    id: '',
    name: '',
    from_date: '',
    to_date: '',
    place_type: PlaceType.Nepal,
    place: ''
  };

  const formikWrapper = useFormikContext<TraineeProfile>();

  const [initialValues, setInitialValue] = useState(defaultValue);
  const [deleteTraining, setDeleteTraining] = useState<number | null>(null);

  const onDelete = (index: number) => {
    setDeleteTraining(index);
  };

  const cols = [
    {
      Header: 'Name',
      accessor: 'name'
    },
    { Header: 'Place', accessor: 'place' },
    {
      Header: 'Type',
      accessor: (row: TraineeProfile['previous_training'][0]) => PlaceType[row.place_type]
    },
    { Header: 'From Date', accessor: 'from_date' },
    { Header: 'To Date', accessor: 'to_date' },
    {
      Header: 'Actions',
      Cell: (cell: Cell<typeof defaultValue>) => {
        return (
          <>
            <PrimaryButton
              title="Edit"
              type="button"
              onClick={() =>
                setInitialValue({ ...cell.row.original, id: cell.row.index.toString() })
              }
            />
            <PrimaryButton
              title="Delete"
              type="button"
              isDanger
              className="m-2"
              onClick={() => onDelete(cell.row.index)}
            />
          </>
        );
      }
    }
  ];

  return (
    <div>
      <Formik
        enableReinitialize
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(training, { resetForm }) => {
          formikWrapper.setFieldValue(
            'previous_training',
            training.id
              ? formikWrapper.values.previous_training.map((t, index) =>
                  +training.id === index ? training : t
                )
              : [...formikWrapper.values.previous_training, training]
          );
          resetForm();
          setInitialValue(defaultValue);
        }}
      >
        {(formik) => (
          <div className="row">
            <TextInput
              label="Name"
              name="name"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.name}
              formik={formik}
              disableRequiredValidation
            />

            <TextInput
              label="Place"
              name="place"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.place}
              formik={formik}
              disableRequiredValidation
            />

            <div className="mt-3 col-md-4">
              <div className="mb-3">
                <label htmlFor="place_type" className="form-label">
                  Type
                </label>
                <select
                  id="place_type"
                  className="form-select col-md-12"
                  data-choices
                  data-choices-sorting="true"
                  value={formik.values.place_type}
                  onChange={formik.handleChange}
                >
                  <option selected>Choose...</option>
                  {[
                    { name: PlaceType[1], value: PlaceType.Nepal },
                    { name: PlaceType[2], value: PlaceType['Foreign Country'] }
                  ]?.map(({ name, value }) => {
                    return (
                      <option key={value} value={value}>
                        {name}
                      </option>
                    );
                  })}
                </select>
              </div>
            </div>

            <TextInput
              label="From Date"
              name="from_date"
              type="date"
              onChange={formik.handleChange}
              value={formik.values.from_date}
              formik={formik}
              disableRequiredValidation
            />

            <TextInput
              label="To Date"
              name="to_date"
              type="date"
              onChange={formik.handleChange}
              value={formik.values.to_date}
              formik={formik}
              disableRequiredValidation
            />

            <div className="col-12 mb-3 mt-4 d-flex justify-content-end">
              <PrimaryButton
                title={`${formik.values.id ? 'Update' : 'Add'} Training`}
                type="button"
                onClick={formik.handleSubmit}
              />
            </div>
          </div>
        )}
      </Formik>

      <ConfirmationModal
        isOpen={typeof deleteTraining === 'number'}
        onClose={() => setDeleteTraining(null)}
        onConfirm={() =>
          formikWrapper.values.experience.filter((e, index) => index !== deleteTraining)
        }
        title="Delete Confirmations"
      >
        <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
          <h4 className="text-muted">Are you sure you want to remove this experience?</h4>
        </div>
      </ConfirmationModal>

      <DataTable data={formikWrapper.values.previous_training} columns={cols} />
    </div>
  );
}

export default Training;
