import TextInput from 'components/ui/TextInput';
import { useFormikContext } from 'formik';
import React, { useEffect, useState } from 'react';
import {
  IProvince,
  useListDistrictByProvinceQuery,
  useListProvinceQuery
} from 'redux/reducers/geography';
import { convertToNepali } from 'utils/nepali-convert';

import { TraineeProfile } from '../';

function PersonalInfo({ disableEdit }: { disableEdit: boolean }) {
  const [page, setPage] = useState(1);
  const [province, setProvince] = useState('');
  const [districts, setDistricts] = useState<IProvince[]>([]);

  const formik = useFormikContext<TraineeProfile>();

  const { data: provinceList = [] } = useListProvinceQuery({});
  const { data: districtData } = useListDistrictByProvinceQuery(
    {
      provinceId: province,
      page
    },
    {
      skip: !province
    }
  );

  useEffect(() => {
    if (districtData) {
      setDistricts((oldData) => [...oldData, ...districtData.results]);
    }
  }, [districtData]);

  useEffect(() => {
    if (districtData?.next) {
      setPage((oldData) => oldData + 1);
    }
  }, [districtData]);

  useEffect(() => {
    setProvince(formik.values.address.province);
  }, [formik.values.address.province]);

  return (
    <div>
      <div className="row">
        <TextInput
          label="First Name"
          name="first_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.first_name}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          required
          disabled={disableEdit}
        />

        <TextInput
          label="Middle Name"
          name="middle_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.middle_name}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          disabled={disableEdit}
        />

        <TextInput
          label="Last Name"
          name="last_name"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.last_name}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          required
          disabled={disableEdit}
        />
      </div>

      <div className="row">
        <TextInput
          label="First Name (Nepali)"
          name="first_name_np"
          type="text"
          onChange={(e) => {
            e.target.value = convertToNepali(e.target.value);
            formik.handleChange(e);
          }}
          value={formik.values.first_name_np}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          required
          //   disabled={disableEdit}
        />

        <TextInput
          label="Middle Name (Nepali)"
          name="middle_name_np"
          type="text"
          onChange={(e) => {
            e.target.value = convertToNepali(e.target.value);
            formik.handleChange(e);
          }}
          value={formik.values.middle_name}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          //   disabled={disableEdit}
        />

        <TextInput
          label="Last Name (Nepali)"
          name="last_name_np"
          type="text"
          onChange={(e) => {
            e.target.value = convertToNepali(e.target.value);
            formik.handleChange(e);
          }}
          value={formik.values.last_name_np}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          required
          //   disabled={disableEdit}
        />
      </div>

      <div className="row">
        {/* <TextInput
          label="Email"
          name="email"
          type="text"
          onChange={formik.handleChange}
          value={''}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          required
          disableRequiredValidation
        /> */}

        <TextInput
          label="Mobile Number"
          name="mobile_number"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.mobile_number}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          required
          disabled={disableEdit}
        />

        <TextInput
          label="Date of Birth"
          name="dob"
          type="date"
          onChange={formik.handleChange}
          value={formik.values.dob}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          required
        />
      </div>

      <div className="row">
        <div className="mt-3 col-md-4">
          <div className="mb-3">
            <label htmlFor="province" className="form-label">
              Province
            </label>
            <select
              name="address.province"
              id="province"
              className="form-select col-md-12"
              data-choices
              data-choices-sorting="true"
              value={formik.values.address.province}
              onChange={(e) => {
                setPage(1);
                setDistricts([]);
                formik.handleChange(e);
              }}
            >
              <option selected>Choose...</option>
              {provinceList.map((province) => {
                return (
                  <option key={province.id} value={province.id}>
                    {province.name_en}
                  </option>
                );
              })}
            </select>

            {formik.touched.address?.province && formik.errors.address?.province && (
              <span className="text-danger">{formik.errors.address?.province}</span>
            )}
          </div>
        </div>

        <div className="mt-3 col-md-4">
          <div className="mb-3">
            <label htmlFor="district" className="form-label">
              District
            </label>
            <select
              name="address.district"
              id="district"
              className="form-select col-md-12"
              data-choices
              data-choices-sorting="true"
              value={formik.values.address.district}
              onChange={(e) => {
                formik.handleChange(e);
              }}
            >
              <option selected>Choose...</option>
              {districts.map((d) => {
                return (
                  <option key={d.id} value={d.id}>
                    {d.name_en}
                  </option>
                );
              })}
            </select>

            {formik.touched.address?.district && formik.errors.address?.district && (
              <span className="text-danger">{formik.errors.address?.district}</span>
            )}
          </div>
        </div>

        <TextInput
          label="Municipality"
          name="address.municipality"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.address.municipality}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          required
        />
      </div>

      <div className="row">
        <TextInput
          label="Ward"
          name="address.ward_no"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.address.ward_no}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          required
        />

        <TextInput
          label="Tole Name"
          name="address.block_no"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.address.block_no}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          // required
        />

        <TextInput
          label="Phone No"
          name="address.house_num"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.address.house_num}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          required
        />

        <TextInput
          label="Pan Number"
          name="address.tole_street"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.address.tole_street}
          onBlur={formik.handleBlur}
          formik={formik}
          disableRequiredValidation
          required
        />
      </div>
    </div>
  );
}

export default PersonalInfo;
