import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import TextInput from 'components/ui/TextInput';
import { Formik, useFormikContext } from 'formik';
import React, { useState } from 'react';
import { Cell } from 'react-table';
import * as Yup from 'yup';

import { TraineeProfile } from '../';

const defaultValue = {
  id: '',
  from_date: '',
  to_date: '',
  organization: '',
  department: '',
  level: '',
  is_first_service: 'False',
  is_current_service: 'False'
};

const validationSchema = Yup.object().shape({
  from_date: Yup.string().required('From date is required'),
  to_date: Yup.string().required('To date is required'),
  organization: Yup.string().required('Organization is required'),
  department: Yup.string().required('Department is required'),
  level: Yup.string().required('Level is required')
});

function Experience() {
  const formikWrapper = useFormikContext<TraineeProfile>();

  const [initialValue, setInitialValue] = useState(defaultValue);
  const [deleteExp, setDeleteExp] = useState<number | null>(null);

  const onDelete = (index: number) => {
    setDeleteExp(index);
  };

  const cols = [
    {
      Header: 'Organization',
      accessor: 'organization'
    },
    { Header: 'Department', accessor: 'department' },
    { Header: 'Level', accessor: 'level' },
    { Header: 'From Date', accessor: 'from_date' },
    { Header: 'To Date', accessor: 'to_date' },
    { Header: 'Is it your first job ?', accessor: 'is_first_service' },
    { Header: 'Currently working here?', accessor: 'is_current_service' },
    {
      Header: 'Actions',
      Cell: (cell: Cell<typeof defaultValue>) => {
        return (
          <>
            <PrimaryButton
              title="Edit"
              type="button"
              onClick={() =>
                setInitialValue({ ...cell.row.original, id: cell.row.index.toString() })
              }
            />
            <PrimaryButton
              title="Delete"
              type="button"
              isDanger
              className="m-2"
              onClick={() => onDelete(cell.row.index)}
            />
          </>
        );
      }
    }
  ];

  return (
    <div>
      <Formik
        enableReinitialize
        initialValues={initialValue}
        validationSchema={validationSchema}
        onSubmit={(experience, { resetForm }) => {
          formikWrapper.setFieldValue(
            'experience',
            experience.id
              ? formikWrapper.values.experience.map((exp, index) =>
                  +experience.id === index ? experience : exp
                )
              : [...formikWrapper.values.experience, experience]
          );
          resetForm();
          setInitialValue(defaultValue);
        }}
      >
        {(formik) => (
          <div className="row">
            <TextInput
              label="Organization"
              name="organization"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.organization}
              formik={formik}
              disableRequiredValidation
            />

            <TextInput
              label="Department"
              name="department"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.department}
              formik={formik}
              disableRequiredValidation
            />

            <TextInput
              label="Level"
              name="level"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.level}
              formik={formik}
              disableRequiredValidation
            />

            <TextInput
              label="From Date"
              name="from_date"
              type="date"
              onChange={formik.handleChange}
              value={formik.values.from_date}
              formik={formik}
              disableRequiredValidation
            />

            <TextInput
              label="To Date"
              name="to_date"
              type="date"
              onChange={formik.handleChange}
              value={formik.values.to_date}
              formik={formik}
              disableRequiredValidation
            />

            <div className="col-4 mb-3 mt-4 d-flex align-items-end">
              <div className="form-check form-switch form-switch-md mt-4">
                <label className="form-check-label" htmlFor="is_first_service">
                  Is it your first job ?
                </label>
                <input
                  type="checkbox"
                  className="form-check-input"
                  id="is_first_service"
                  checked={formik.values.is_first_service === 'True'}
                  onChange={() => {
                    formik.handleChange({
                      target: {
                        name: 'is_first_service',
                        value: formik.values.is_first_service === 'True' ? 'False' : 'True'
                      }
                    });
                  }}
                />
              </div>
            </div>

            <div className="col-4 mb-3 mt-4 d-flex align-items-end">
              <div className="form-check form-switch form-switch-md mt-4">
                <label className="form-check-label" htmlFor="is_current_service">
                  Currently working here?
                </label>
                <input
                  type="checkbox"
                  className="form-check-input"
                  id="is_current_service"
                  checked={formik.values.is_current_service === 'True'}
                  onChange={() => {
                    formik.handleChange({
                      target: {
                        name: 'is_current_service',
                        value: formik.values.is_current_service === 'True' ? 'False' : 'True'
                      }
                    });
                  }}
                />
              </div>
            </div>

            <div className="col-12 mb-3 mt-4 d-flex justify-content-end">
              <PrimaryButton
                title={`${formik.values.id ? 'Update' : 'Add'} Experience`}
                type="button"
                onClick={formik.handleSubmit}
              />
            </div>
          </div>
        )}
      </Formik>

      <ConfirmationModal
        isOpen={typeof deleteExp === 'number'}
        onClose={() => setDeleteExp(null)}
        onConfirm={() => formikWrapper.values.experience.filter((e, index) => index !== deleteExp)}
        title="Delete Confirmations"
      >
        <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
          <h4 className="text-muted">Are you sure you want to remove this experience?</h4>
        </div>
      </ConfirmationModal>

      <DataTable data={formikWrapper.values.experience} columns={cols} />
    </div>
  );
}

export default Experience;
