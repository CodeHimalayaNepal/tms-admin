import { USERTYPE } from 'common/enum';
import Container from 'components/ui/Container';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import {
  useCurrentUserDetailsQuery,
  useListAdministratorQuery
} from 'redux/reducers/administrator';
import { useDashboardDetailsQuery } from 'redux/reducers/dashboard';
import { UseListCoursesRoutineQuery } from 'redux/reducers/courses';
import DashboardImage from './tmsDashboard.png';
import traineecap from './icons/traineecap.svg';
import 'react-modern-calendar-datepicker/lib/DatePicker.css';
import chat from './icons/chat.png';
import chart from './icons/chart.png';
import feedbackIcon from './icons/fluent.png';
import configurationIcon from './icons/carbon_result.png';
import { useGetOngoingTrainingsQuery } from 'redux/reducers/dashboard';
import defaultTrainingImage from './images/img.png';

interface ICoordinator {
  coordinator_details: {
    coordinator_id: number;
    first_name: string;
    last_name: string;
    middle_name: string;
  };
  course_details: any;
  course_id: any;
  is_module_complete: boolean;
  is_module_coordinator: boolean;
  routine_coordinator_id: number;
}

const Dashboard = () => {
  const navigate = useNavigate();
  const { data: userDetails } = useCurrentUserDetailsQuery();
  const { data: adminData } = useListAdministratorQuery();
  const { data: onGoingTrainings, isLoading: onGoingTrainingsLoading } =
    useGetOngoingTrainingsQuery();
  const activeRoutines = onGoingTrainings?.data;

  const { data: responseData } = useDashboardDetailsQuery();
  const currentUserRole = adminData?.results?.find((item: any) => item.id === userDetails?.role);
  const isAdmin = 'Supervisor' === currentUserRole?.name;

  useEffect(() => {
    if (
      +userDetails?.data?.user_details?.user_type === USERTYPE.trainee &&
      !userDetails?.data?.image
    ) {
      navigate('/trainee/profile');
    }
  }, [userDetails]);

  return (
    <>
      <Container title="Dashboard" subTitle="Training Management System">
        {isAdmin ? (
          <div className="d-flex">
            <img
              src={DashboardImage}
              style={{
                margin: 'auto'
              }}
            />
            ;
          </div>
        ) : (
          <></>
        )}
        {/* <div className='row pt-4'>
          <div className='col-12 d-flex flex-column' style={{paddingLeft:'20px', color:'#5856d6'}}>
            <p style={{fontSize:'32px'}}>Training</p>
            <p style={{fontSize:'22px'}}>Management System-(TMS)</p>
          </div>
        </div> */}
        <div className="row">
          <div className="col-lg-9 p-4 ">
            <div className="row ">
              <div className="col-md-6 ">
                <div className="card p-3" style={{ height: '240px' }}>
                  <h4 className="card-title" style={{ fontSize: '26px' }}>
                    Users
                  </h4>
                  <div className="card-body">
                    <div className="row ">
                      <div className="col-lg-6 d-flex flex-direction-row align-items-center">
                        <img
                          src={traineecap}
                          alt="cap-icon"
                          style={{ height: '30px', width: '30px' }}
                        />
                        <div style={{ paddingLeft: '10px' }}>
                          <h6>
                            Trainee Count
                            <br />
                            <span style={{ fontWeight: 'bold', fontSize: '22px' }}>
                              {responseData?.trainee_count}
                            </span>
                          </h6>
                        </div>
                      </div>
                      <div className="col-lg-6 d-flex flex-direction-row align-items-center">
                        <img
                          src={traineecap}
                          alt="cap-icon"
                          style={{ height: '30px', width: '30px' }}
                        />
                        <div style={{ paddingLeft: '10px' }}>
                          <h6>
                            External Users
                            <br />
                            <span style={{ fontWeight: 'bold', fontSize: '22px' }}>
                              {responseData?.external_user}
                            </span>
                          </h6>
                        </div>
                      </div>
                      <div className="col-lg-6 mt-2 d-flex flex-direction-row align-items-center">
                        <img
                          src={traineecap}
                          alt="cap-icon"
                          style={{ height: '30px', width: '30px' }}
                        />
                        <div style={{ paddingLeft: '10px', paddingTop: '30px' }}>
                          <h6>
                            Internal Users
                            <br />
                            <span style={{ fontWeight: 'bold', fontSize: '22px' }}>
                              {responseData?.internal_user}
                            </span>
                          </h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="card p-3" style={{ minHeight: '240px' }}>
                  <h4 className="card-title" style={{ fontSize: '26px' }}>
                    Trainings
                  </h4>
                  <div className="card-body">
                    <div className="row">
                      <div className="col-lg-6 d-flex flex-direction-row align-items-center">
                        <img
                          src={traineecap}
                          alt="cap-icon"
                          style={{ height: '30px', width: '30px' }}
                        />
                        <div style={{ paddingLeft: '10px' }}>
                          <h6>
                            Upcoming Trainings
                            <br />
                            <span style={{ fontWeight: 'bold', fontSize: '22px' }}>
                              {responseData?.upcoming_training}
                            </span>
                          </h6>
                        </div>
                      </div>
                      <div className="col-lg-6 d-flex flex-direction-row align-items-center">
                        <img
                          src={traineecap}
                          alt="cap-icon"
                          style={{ height: '30px', width: '30px' }}
                        />
                        <div style={{ paddingLeft: '10px' }}>
                          <h6>
                            Enrolled Trainees in Routine
                            <br />
                            <span style={{ fontWeight: 'bold', fontSize: '22px' }}>
                              {responseData?.number_of_user_enrollment_in_routine}
                            </span>
                          </h6>
                        </div>
                      </div>
                      <div className="col-lg-6 mt-2 d-flex flex-direction-row align-items-center">
                        <img
                          src={traineecap}
                          alt="cap-icon"
                          style={{ height: '30px', width: '30px' }}
                        />
                        <div style={{ paddingLeft: '10px' }}>
                          <h6>
                            Completed Trainings
                            <br />
                            <span style={{ fontWeight: 'bold', fontSize: '22px' }}>
                              {responseData?.completed_training}
                            </span>
                          </h6>
                        </div>
                      </div>
                      <div className="col-6 mt-2 d-flex flex-direction-row align-items-center">
                        <img
                          src={traineecap}
                          alt="cap-icon"
                          style={{ height: '30px', width: '30px' }}
                        />
                        <div style={{ paddingLeft: '10px' }}>
                          <h6>
                            Running Trainings
                            <br />
                            <span style={{ fontWeight: 'bold', fontSize: '22px' }}>
                              {responseData?.running_training_count}
                            </span>
                          </h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-3 d-flex justify-content-center">
            {/* <Calender
            value={selectedDay}
            shouldHighlightWeekends
          
            colorPrimary='#0E548F'
          /> */}
          </div>
        </div>
        <div className="row p-3">
          <p style={{ fontSize: '18px', fontWeight: '500' }}>Ongoing Trainings</p>
        </div>

        <div className="row  ">
          <div className="col-lg-12">
            <div className="row d-flex justify-content-start">
              {activeRoutines?.map((routine: any, index: any) => {
                return (
                  <div
                    className="col-md-6 col-lg-6  d-flex justify-content-center align-items-stretch "
                    style={{ width: 'auto', padding: '0 10px' }}
                  >
                    <div className="card" style={{ width: '305px' }}>
                      <div
                        className="card-image"
                        style={{ height: '9rem', width: '110px', margin: 'auto' }}
                      >
                        <img
                          src={routine?.training_details?.image || defaultTrainingImage}
                          className="card-img-top"
                          alt="TrainingImg"
                          style={{ margin: 'auto', objectFit: 'contain', height: '100%' }}
                        />
                      </div>
                      <div className="card-body">
                        <div className="training_info ">
                          <h6 className="lead d-flex align-items-end">
                            {routine?.training_details?.training_name}
                          </h6>
                        </div>
                        <div className="d-flex flex-column">
                          <p>
                            <span>
                              <h6 style={{ fontWeight: '600' }}>Hall</h6>
                            </span>
                            {routine?.hall_details?.hall_name}
                          </p>
                          <p>
                            <span
                              style={{
                                display: 'flex',
                                flexDirection: 'row'
                              }}
                            >
                              <h6 style={{ fontWeight: '600' }}>Trainee Count:&nbsp;</h6>
                              <span>{routine?.trainee_count}</span>
                            </span>
                          </p>
                          <ul style={{ listStyle: 'none', margin: 0, padding: 0 }}>
                            <h6 style={{ fontWeight: '600' }}>Co-ordinators</h6>
                            {routine?.tr_routine_coordinator
                              ?.filter(
                                (coordinator: ICoordinator) =>
                                  coordinator.is_module_coordinator == false
                              )
                              .map((coordinator: ICoordinator) => (
                                <li>
                                  {coordinator?.coordinator_details?.first_name}{' '}
                                  {coordinator?.coordinator_details?.middle_name ?? ''}{' '}
                                  {coordinator?.coordinator_details?.last_name ?? ''}
                                </li>
                              ))}
                          </ul>
                          <div className="row">
                            <div className="col-6 d-flex flex-column">
                              <h6 style={{ fontWeight: '600' }}>Start date:</h6>
                              <p>{routine?.start_date}</p>
                            </div>
                            <div className="col-6 d-flex flex-column">
                              <h6 style={{ fontWeight: '600' }}>End date:</h6>
                              <p>{routine?.end_date}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row d-flex align-items-end">
                        <div
                          className="col-12 d-flex flex-direction-row justify-content-around"
                          style={{ paddingBottom: '15px' }}
                        >
                          <img
                            src={chat}
                            style={{ height: '20px', width: '20px', cursor: 'pointer' }}
                            onClick={() => {
                              navigate(`/trainings/${routine?.routine_master_id}`);
                            }}
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Show Discussions"
                          />
                          <img
                            src={chart}
                            style={{ height: '20px', width: '20px', cursor: 'pointer' }}
                            onClick={() => {
                              navigate(
                                `/routine-management/trainee-evaluation/${routine?.routine_master_id}`
                              );
                            }}
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Trainee Evaluation"
                          />
                          <img
                            src={feedbackIcon}
                            style={{ height: '20px', width: '20px', cursor: 'pointer' }}
                            onClick={() =>
                              navigate(`/routine-management/${routine?.routine_master_id}/feedback`)
                            }
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Feedbacks"
                          />
                          <img
                            src={configurationIcon}
                            style={{ height: '20px', width: '20px', cursor: 'pointer' }}
                            onClick={() =>
                              navigate(`/feedback/evaluation/${routine?.routine_master_id}`)
                            }
                            data-toggle="tooltip"
                            data-placement="bottom"
                            title="Feedback Criteria"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default Dashboard;
