import React, { useRef, useState } from 'react';
import { useFormik } from 'formik';
import PrimaryButton from 'components/ui/PrimaryButton';
import { useCreateCommentOnSessionMutation } from 'redux/reducers/dashboard';
import { toast } from 'react-toastify';
import { GrAttachment } from 'react-icons/gr';
import { FileIcon, defaultStyles } from 'react-file-icon';
import crossIcon from '../images/crossIcon.png';

const initialValues = {
  content: '',
  attachment: []
};
const AnnouncementForm = (props: any) => {
  const { loggedInUser, isActiveSessionId } = props;
  const [createComment, { isLoading: isCreating }] = useCreateCommentOnSessionMutation();
  const [fileName, setFileName] = useState('');
  const attachmentType = fileName.split('.').slice(-1).pop();
  const [fileSize, setFileSize] = useState<number | undefined>(0);

  const inputRef = useRef<HTMLInputElement>();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues,
    onSubmit: (values, actions) => {
      const sendValue = {
        ...values,
        user: +loggedInUser?.data?.id,
        id: +isActiveSessionId,
        routine: props.RoutineId
      };

      if (isActiveSessionId && (sendValue.content != '' || fileName)) {
        if (fileSize && fileSize > 31457280) {
          toast.error('File cannot be larger than 30 mb.');
        } else {
          createComment(sendValue)
            .unwrap()
            .then((data: any) => {
              setFileName('');
              setFileSize(0);
              formik.resetForm();
              toast.success('Successfully Posted Comment');
            });
        }
      } else {
        toast.error('Failed');
      }
    }
  });

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <div className="row mt-2">
          <div className="input-group mb-3">
            <input
              type="text"
              placeholder="Write comment here"
              className="form-control"
              style={{
                marginRight: '20px',
                boxShadow:
                  'rgb(204, 219, 232) 3px 3px 6px 0px inset, rgba(255, 255, 255, 0.5) -3px -3px 6px 1px inset'
              }}
              value={formik.values.content}
              onChange={(e) => formik.setFieldValue('content', e.target.value)}
            />
            <div className="input-group-append">
              <input
                type="file"
                style={{ display: 'none' }}
                ref={inputRef as any}
                onChange={(e) => {
                  setFileName((e.target.files?.[0] as { name: string }).name);
                  formik.setFieldValue('attachment', e.target.files?.[0]);
                  setFileSize(e.target.files?.[0].size);
                }}
              />
              <GrAttachment
                style={{ fontSize: '20px', cursor: 'pointer', marginRight: '20px' }}
                onClick={() => inputRef.current?.click()}
              />
              <PrimaryButton
                className="btn btn-outline-secondary"
                isLoading={isCreating}
                type="submit"
                title="Send"
              />
            </div>
          </div>
        </div>

        {fileName && (
          <div
            className="row pb-3 pt-3 pr-1"
            style={{ background: '#e1eaeb', borderRadius: '10px' }}
          >
            <div className="col-1 d-flex justify-content-center align-items-center">
              <div style={{ height: '100%', width: '60px' }}>
                <FileIcon extension={attachmentType} {...defaultStyles} />
              </div>
            </div>
            <div className="col-11 d-flex justify-content-between">
              {fileName}
              <img
                src={crossIcon}
                alt="cross_icon"
                style={{ height: '25px', width: '25px', cursor: 'pointer' }}
                onClick={() => {
                  setFileSize(0);
                  setFileName('');
                }}
              />
            </div>
          </div>
        )}
      </form>
    </>
  );
};

export default AnnouncementForm;
