import Container from 'components/ui/Container';
import PrimaryButton from 'components/ui/PrimaryButton';
import { useState, useRef, useEffect, SetStateAction } from 'react';
import { useParams } from 'react-router-dom';
import {
  useGetCourseTopicsByModuleIdQuery,
  useGetDataByRoutineMasterIdQuery,
  useGetActiveCourseTopicsByModuleIdQuery
} from 'redux/reducers/dashboard';
import AnnouncementForm from './AnnouncementForm';
import { useGetCommentsBySessionIdQuery } from 'redux/reducers/dashboard';
import img from '../images/img.png';
import { useFormik } from 'formik';
import { useCurrentUserDetailsQuery } from 'redux/reducers/administrator';
import { useCreateReplyOnCommentMutation } from 'redux/reducers/dashboard';
import replyImg from '../images/reply.png';
import { toast } from 'react-toastify';
import Avatar from '../images/avatar.png';
import useOutsideClick from 'hooks/useOutSideClick';
import { useGetAttachmentsByCourseQuery } from 'redux/reducers/dashboard';
import { GrAttachment } from 'react-icons/gr';
import { RiDownload2Line } from 'react-icons/ri';
import { FileIcon, defaultStyles } from 'react-file-icon';
import crossIcon from '../images/crossIcon.png';
import styles from './training.module.css';
import deleteIcon from '../../../../assets/images/icons/delete.png';
import ConfirmationModal from 'components/ui/ConfirmationModal/confirmation-modal';
import useToggle from 'hooks/useToggle';
import { useDeleteCommentMutation } from 'redux/reducers/dashboard';
import { downloadPdf } from 'utils/pdfDownload';
import axios from 'axios';
import { blob } from 'stream/consumers';

const initialValues = {
  content: '',
  attachment: []
};

const InputComponent = ({ comment, isActiveSessionId, setShowReply }: any) => {
  const [parentId, setParaentId] = useState();
  const [createReply, { isLoading: isCreating }] = useCreateReplyOnCommentMutation();
  const [fileName, setFileName] = useState('');
  const attachmentType = fileName.split('.').slice(-1).pop();
  const inputRef = useRef<HTMLInputElement>();
  const formik = useFormik({
    enableReinitialize: true,
    initialValues,
    onSubmit: (values, { resetForm }) => {
      const sendValue = {
        attachment: values.attachment,
        content: values?.content,
        id: +isActiveSessionId,
        parent_id: parentId
      };
      try {
        createReply(sendValue)
          .unwrap()
          .then((data: any) => {
            setFileName('');
            toast.success('Successfully Replied');
          });
      } catch (error) {
        toast.error('Something went Wrong');
      }
    }
  });

  return (
    <form
      onSubmit={formik.handleSubmit}
      style={{ width: '100%', display: 'flex', flexDirection: 'column' }}
    >
      <div className="row mt-2">
        <div className="col-12 col-lg-10 input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Write a reply"
            value={formik.values.content}
            onChange={(e) => {
              setParaentId(comment?.id);
              formik.setFieldValue('content', e.target.value);
            }}
            style={{
              marginRight: '20px',
              boxShadow:
                'rgb(204, 219, 232) 3px 3px 6px 0px inset, rgba(255, 255, 255, 0.5) -3px -3px 6px 1px inset'
            }}
          />
          <div className="input-group-append d-flex align-items-center">
            <input
              type="file"
              style={{ display: 'none' }}
              ref={inputRef as any}
              onChange={(e) => {
                setParaentId(comment?.id);
                setFileName((e.target.files?.[0] as { name: string }).name);
                formik.setFieldValue('attachment', e.target.files?.[0]);
              }}
            />
            <GrAttachment
              style={{ fontSize: '20px', cursor: 'pointer', marginRight: '20px' }}
              onClick={() => inputRef.current?.click()}
            />
          </div>
        </div>
        <div className="col-3 col-lg-1">
          <PrimaryButton
            className="btn btn-outline-secondary"
            type="submit"
            isLoading={isCreating}
            title="Send"
          />
        </div>
      </div>

      {fileName && (
        <div className="row pb-3 mt-1 pr-1" style={{ background: '#e1eaeb', borderRadius: '10px' }}>
          <div className="col-1 d-flex justify-content-center align-items-center">
            <div style={{ height: '100%', width: '60px' }}>
              <FileIcon extension={attachmentType} {...defaultStyles} />
            </div>
          </div>
          <div className="col-11 d-flex justify-content-between">
            {fileName}
            <img
              src={crossIcon}
              alt="cross_icon"
              style={{ height: '25px', width: '25px', cursor: 'pointer' }}
              onClick={() => setFileName('')}
            />
          </div>
        </div>
      )}
    </form>
  );
};

const TrainingNew = () => {
  const { id } = useParams<{ id: any }>();
  const valueRef = useRef();
  const courseTopicRef = useRef();
  const [isActiveSessionId, setIsActiveSessionId] = useState('');
  const { data, isLoading } = useGetDataByRoutineMasterIdQuery(id);
  const modules = data?.data?.training_details?.courses;
  const [isHover, setIsHover] = useState();
  const { data: activeCourseTopics, isLoading: ActiveCourseTopicsLoading } =
    useGetActiveCourseTopicsByModuleIdQuery({ id, isHover }, { skip: !id || !isHover });
  const { data: courseDetails, isLoading: CourseDetailsLoading } =
    useGetCourseTopicsByModuleIdQuery(isHover, { skip: !isHover });
  const [deleteComment, { isLoading: isCommentDeleting }] = useDeleteCommentMutation();

  const sessions = courseDetails?.data?.results;
  const [hoverStyle, setHoverStyle] = useState(false);
  const [routineSession, setRoutineSession] = useState([]);
  const [routineSessionId, setRoutineSessionId] = useState(null);
  const [show, setShow] = useState(false);
  const { targetRef, isOutsideClick } = useOutsideClick();
  const [showReply, setShowReply] = useState<{ [x: string]: boolean }>();
  const bottomRef = useRef<null | HTMLDivElement>(null);
  const [deleteModal, toggleDeleteModal] = useToggle(false);
  const [commentId, setCommentId] = useState('');

  const onDeleteConfirm = () => {
    try {
      deleteComment(commentId)
        .unwrap()
        .then(() => {
          toast.success('Comment deleted Successfully');
          toggleDeleteModal();
        });
      deleteComment(commentId)
        .unwrap()
        .then(() => {
          toast.success('Comment deleted Successfully');
          toggleDeleteModal();
        });
    } catch {
      (err: any) => {
        toast.error('Something went wrong! ');
      };
    }
  };

  const { data: attachments, isLoading: attachmentsLoading } = useGetAttachmentsByCourseQuery(
    { isActiveSessionId, id },
    { skip: !isActiveSessionId }
  );

  const { data: Comments, isLoading: isCommentsLoading } = useGetCommentsBySessionIdQuery(
    {
      type: 'coursetopic',
      id: parseInt(isActiveSessionId),
      routine: id
    },
    { skip: !isActiveSessionId }
  );

  useEffect(() => {
    if (Comments) {
      setShowReply(
        (Comments as [{ id: any }]).reduce((ac, c) => {
          return {
            ...ac,
            [c.id]: false
          };
        }, {})
      );
    }
  }, [Comments]);

  const [topic, setTopic] = useState('');

  const { data: loggedInUser, isLoading: userLoading } = useCurrentUserDetailsQuery();

  useEffect(() => {
    const id = (modules?.[0] as { id: any })?.id;
    setIsHover(id);
    valueRef.current = id;
  }, [modules]);

  useEffect(() => {
    if (activeCourseTopics?.data?.length > 0) {
      const couresId = activeCourseTopics?.data[0]?.course_topic_id;
      setRoutineSession(activeCourseTopics?.data[0]?.routine_session || '');
      setRoutineSessionId(activeCourseTopics?.data[0]?.routine_session?.[0]?.id);
      setIsActiveSessionId(couresId);
      setTopic(activeCourseTopics?.data[0]?.title);
      courseTopicRef.current = couresId;
    } else {
      setIsActiveSessionId('');
    }
  }, [activeCourseTopics?.data]);

  return (
    <>
      <Container title="Trainings">
        <div className="row">
          <div className={`${styles.menuParent} col-12 col-lg-2`} ref={targetRef as any}>
            {isLoading ? (
              <div className="d-flex align-items-center justify-content-center">
                <div className="spinner-border" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            ) : (
              <>
                <div className="text-center">
                  <h3>Modules</h3>
                  <hr></hr>
                </div>
                <div style={{ height: 'auto' }}>
                  {modules.map((item: any, index: number) => (
                    <div
                      key={item.id}
                      className={`${styles.parentItem} ${
                        item.id == isHover ? 'active' : 'inactive'
                      }`}
                      style={{
                        padding: '10px',
                        backgroundColor: item.id == isHover ? 'rgb(225 219 219)' : 'white'
                      }}
                      onClick={(e) => {
                        setIsHover(item?.id);
                        setShow(true);
                      }}
                    >
                      <h5 style={{ cursor: 'pointer', textAlign: 'center' }}>{item.title}</h5>
                    </div>
                  ))}
                </div>
              </>
            )}
            {isOutsideClick && (
              <div
                className={`${styles.menuChild} col-12 col-lg-2`}
                style={{
                  display: show ? 'block' : 'none'
                }}
              >
                <div className="row pt-3">
                  <hr className="d-lg-none"></hr>
                  <div className="col-12 d-flex justify-content-center align-items-center">
                    <h5 style={{ cursor: 'pointer' }}>Sessions</h5>
                  </div>
                  <hr></hr>
                  {activeCourseTopics?.data?.length > 0 ? (
                    activeCourseTopics?.data?.map((x: any) => (
                      <div
                        className={`${styles.childItem} col-12 d-flex justify-content-center justify-content-lg-start align-items-center`}
                        onClick={() => {
                          setTopic(x?.title);
                          setShow(false);
                          setIsActiveSessionId(x.course_topic_id);
                          setRoutineSession(x?.routine_session);
                          setRoutineSessionId(x.routine_session?.[0].id);
                        }}
                        onMouseEnter={() => {
                          setHoverStyle(true);
                        }}
                        onMouseLeave={() => {
                          setHoverStyle(false);
                        }}
                        style={{
                          background: x?.course_topic_id == isActiveSessionId ? '#ddd' : '',
                          cursor: hoverStyle ? 'pointer' : '',
                          padding: '10px',
                          width: '100%'
                        }}
                      >
                        <p>{x.title}</p>
                      </div>
                    ))
                  ) : (
                    <div className="row d-flex justify-content-center">
                      <p>No data</p>
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
          <div className="col-lg-10 col-md-12">
            <div className="row">
              <div className="card d-flex flex-direction-column p-1">
                <div className="d-flex flex-direction-row justify-content-between">
                  <h4>Training</h4>
                </div>
                <div className="row">
                  <div className="col-4 d-flex justify-content-start">
                    <p>Training name</p>
                  </div>
                  <div className="col-2 d-flex justify-content-start">
                    <p>Hall</p>
                  </div>
                  <div className="col-2 d-flex justify-content-start">
                    <p>batch 2022</p>
                  </div>
                  <div className="col-2 d-flex justify-content-start">
                    <p>Start Date</p>
                  </div>
                  <div className="col-2 d-flex justify-content-start">
                    <p>End Date</p>
                  </div>
                </div>
                <div className="row" style={{ fontWeight: '600' }}>
                  <div className="col-4 d-flex justify-content-start">
                    <p>{data?.data?.training_details?.training_name}</p>
                  </div>
                  <div className="col-2 d-flex justify-content-start">
                    <p>{data?.data?.hall_details?.hall_name}</p>
                  </div>
                  <div className="col-2 d-flex justify-content-start">
                    <p>{data?.data?.batch_name}</p>
                  </div>
                  <div className="col-2 d-flex justify-content-start">
                    <p>{data?.data?.start_date}</p>
                  </div>
                  <div className="col-2 d-flex justify-content-start">
                    <p>{data?.data?.end_date}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="card pl-4">
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                  <h4 style={{ color: '#0E548F' }}>Resources</h4>
                  <h5 style={{ color: '#0E548F' }}>
                    Session: <span style={{ color: '#000', fontSize: '16px' }}>{topic}</span>
                  </h5>
                </div>
                <div className="row">
                  <div
                    className="col-12 col-lg-9 card"
                    style={{
                      height:
                        Comments?.length == 1 ? '300px' : Comments?.length > 1 ? '520px' : '40px',
                      overflowX: 'hidden'
                    }}
                  >
                    <div
                      className="comments d-flex flex-column justify-content-start "
                      style={{ borderBottom: '1px solid #CCCCCC', zIndex: '0' }}
                    >
                      {isCommentsLoading ? (
                        <div className="d-flex align-items-center justify-content-center">
                          <div className="spinner-border" role="status">
                            <span className="sr-only">Loading...</span>
                          </div>
                        </div>
                      ) : (
                        Comments?.map((comment: any) => {
                          const key = comment.id;
                          return (
                            <>
                              <div
                                className="row comment d-flex flex-row "
                                key={comment.id}
                                ref={bottomRef}
                              >
                                <div className="col-2 col-lg-1">
                                  <div className="comment_image d-flex justify-content-end align-items-start">
                                    <img
                                      className="comment_image"
                                      src={img}
                                      style={{
                                        height: '45px',
                                        width: '45px',
                                        borderRadius: '52.2px'
                                      }}
                                    />
                                  </div>
                                </div>
                                <div
                                  className={`${styles.commentator_email} col-10 col-lg-11 comment_details d-flex flex-column`}
                                >
                                  <div className="user_details d-flex flex-row">
                                    <p
                                      className="commentator_email"
                                      style={{ maxWidth: '100%', fontWeight: 'bold' }}
                                    >
                                      {comment.user.email}
                                    </p>
                                    <small
                                      className="d-none d-lg-flex"
                                      style={{
                                        color: '#858585',
                                        fontSize: '10px',
                                        width: '220px',
                                        marginLeft: '20px'
                                      }}
                                    >
                                      {comment.posted}
                                    </small>
                                    {comment.user.id == loggedInUser.data.user_details.id ? (
                                      <div className="dropdown d-flex justify-content-end">
                                        <a
                                          href="#"
                                          role="button"
                                          id="dropdownMenuButton1"
                                          data-bs-toggle="dropdown"
                                          aria-expanded="false"
                                        >
                                          <i className="ri-more-2-fill"></i>
                                        </a>
                                        <ul
                                          className="dropdown-menu"
                                          aria-labelledby="dropdownMenuButton1"
                                        >
                                          <li>
                                            <a
                                              className="dropdown-item"
                                              onClick={() => {
                                                setCommentId(comment.id);
                                                toggleDeleteModal();
                                              }}
                                              style={{ cursor: 'pointer' }}
                                            >
                                              <img
                                                src={deleteIcon}
                                                style={{ padding: '4px', marginLeft: '3px' }}
                                              />{' '}
                                              Delete
                                            </a>
                                          </li>
                                        </ul>
                                      </div>
                                    ) : (
                                      ''
                                    )}
                                  </div>
                                  <p
                                    style={{ fontSize: '14px', maxWidth: '600px' }}
                                    dangerouslySetInnerHTML={{ __html: comment?.content }}
                                  ></p>
                                  {comment?.attachment?.url ? (
                                    <div className="mb-2" style={{ height: '50px', width: '50px' }}>
                                      <a
                                        href={comment?.attachment?.url}
                                        target="_blank"
                                        rel="noreferrer"
                                      >
                                        <FileIcon extension={comment?.attachment.type} />
                                      </a>
                                    </div>
                                  ) : (
                                    ''
                                  )}
                                </div>
                              </div>
                              <div className="row mt-2">
                                <div className="col-12 d-flex justify-content-start">
                                  <div
                                    className="reply-info"
                                    style={{
                                      width: '300px',
                                      paddingLeft: '10px',
                                      display: 'flex',
                                      alignItems: 'center',
                                      flexDirection: 'row',
                                      justifyContent: 'start'
                                    }}
                                  >
                                    <img src={replyImg} />
                                    <p
                                      style={{
                                        marginLeft: '20px',
                                        display: 'flex',
                                        alignItems: 'center',
                                        color: '#858585',
                                        fontSize: '10px',
                                        cursor: 'pointer'
                                      }}
                                      onClick={() =>
                                        setShowReply((prev) => ({
                                          ...prev,
                                          [comment.id]: !prev?.[comment.id]
                                        }))
                                      }
                                    >
                                      {comment?.replies?.length == 0
                                        ? comment?.replies?.length +
                                          ' replies.Click here to add reply.'
                                        : 'View ' + comment?.replies?.length + ' replies'}
                                    </p>
                                  </div>
                                </div>
                              </div>
                              {showReply && showReply[key] && (
                                <div>
                                  <div className="row">
                                    {comment?.replies?.map((reply: any) => (
                                      <div
                                        className="col-12 comment-replies d-flex align-items-start flex-row mt-3"
                                        style={{ paddingLeft: '18px' }}
                                        key={reply.id}
                                      >
                                        <img
                                          src={reply?.user?.profile || Avatar}
                                          style={{
                                            height: '30px',
                                            width: '30px',
                                            borderRadius: '52.2px'
                                          }}
                                        />
                                        <div
                                          className="comment_details d-flex flex-column"
                                          style={{ marginLeft: '20px' }}
                                        >
                                          <div className="user_details d-flex flex-row">
                                            <p style={{ width: '100%', fontWeight: 'bold' }}>
                                              {reply?.user?.email}
                                            </p>
                                            <small
                                              className="d-none d-lg-flex"
                                              style={{
                                                color: '#858585',
                                                fontSize: '10px',
                                                width: '150px'
                                              }}
                                            >
                                              {reply?.posted}
                                            </small>
                                          </div>
                                          <p className="reply_content">{reply?.content}</p>
                                          {reply?.attachment.url ? (
                                            <div style={{ height: '40px', width: '40px' }}>
                                              <a
                                                href={reply?.attachment.url}
                                                target="_blank"
                                                rel="noreferrer"
                                              >
                                                <FileIcon extension={reply?.attachment?.type} />
                                              </a>
                                            </div>
                                          ) : (
                                            ''
                                          )}
                                        </div>
                                      </div>
                                    ))}
                                  </div>

                                  <div className="row create-comment mt-2 pb-1 d-flex flex-row">
                                    <div className="col-2 col-lg-1 d-flex align-items-center justify-content-end">
                                      <img
                                        src={loggedInUser?.data?.image || Avatar}
                                        style={{
                                          height: '35px',
                                          width: '35px',
                                          borderRadius: '52.2px'
                                        }}
                                      />
                                    </div>
                                    <div className="col-10 col-lg-11">
                                      <InputComponent
                                        isActiveSessionId={isActiveSessionId}
                                        comment={comment}
                                        setShowReply={setShowReply}
                                      />
                                    </div>
                                  </div>
                                </div>
                              )}
                            </>
                          );
                        })
                      )}
                    </div>
                  </div>
                  <div
                    className="documents_section col-12 col-lg-3  d-flex flex-column justify-content-start align-items-start"
                    style={{ paddingLeft: '10px', overflowY: 'auto', height: '520px' }}
                  >
                    <div className="row">
                      <h5 style={{ paddingBottom: '5px' }}>Learning materials</h5>
                    </div>
                    <div className="row">
                      {attachmentsLoading && (
                        <div className="col-12 pt-4 d-flex justify-content-center">Loading....</div>
                      )}
                      {attachments?.length == 0 && (
                        <div className="col-12 pt-4 d-flex justify-content-center">No Data</div>
                      )}
                      {attachments?.length > 0 &&
                        attachments?.map((attachment: any) => (
                          <>
                            <div className="row d-flex flex-row" style={{ paddingTop: '20px' }}>
                              <div className="col-2">
                                <FileIcon extension={attachment.attachment.type} />
                              </div>
                              <div
                                className="col-8"
                                style={{ overflow: 'hidden', textOverflow: 'ellipsis' }}
                              >
                                <p style={{ fontSize: '12px', width: '250px', color: '#000' }}>
                                  {attachment.attachment.name}
                                </p>
                              </div>
                              <div
                                className="col-2"
                                style={{
                                  display: 'flex',
                                  justifyContent: 'start',
                                  alignItems: 'center',
                                  cursor: 'pointer'
                                }}
                              >
                                <RiDownload2Line
                                  size={22}
                                  onClick={(e) => {
                                    e.stopPropagation();
                                    axios.get(attachment?.attachment?.url).then((res) => {
                                      const blob = res.data;
                                      downloadPdf(blob, 'Resource.pdf', 'text/pdf');
                                    });
                                  }}
                                />
                              </div>
                            </div>
                          </>
                        ))}
                    </div>
                  </div>
                </div>
                <div className="row">
                  {activeCourseTopics?.data?.length > 0 ? (
                    <AnnouncementForm
                      routineSessionId={routineSessionId}
                      isHover={isHover}
                      loggedInUser={loggedInUser}
                      RoutineId={id}
                      isActiveSessionId={isActiveSessionId}
                    />
                  ) : (
                    <div className="row d-flex justify-content-center align-items-center p-4">
                      No data
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Container>
      <ConfirmationModal
        isOpen={deleteModal}
        onClose={toggleDeleteModal}
        onConfirm={onDeleteConfirm}
        title="Delete Confirmations"
        isLoading={isCommentDeleting}
      >
        <>
          <div className="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
            <h4>Are you Sure ?</h4>
            <p className="text-muted ">You want to Remove this Comment ?</p>
          </div>
        </>
      </ConfirmationModal>
    </>
  );
};
export default TrainingNew;
