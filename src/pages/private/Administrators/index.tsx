import Container from 'components/ui/Container';
import Modal from 'components/ui/Modal/modal';
import PrimaryButton from 'components/ui/PrimaryButton';
import SecondaryButton from 'components/ui/SecondaryButton';
import { useFormik } from 'formik';
import useToggle from 'hooks/useToggle';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { useInviteUserMutation } from 'redux/reducers/administrator';
import AdministratorInviteForm from './AdministratorInviteForm';
import AdminstratorsTable from './AdminstratorsTable';
import { inviteUserInitialValues, inviteUserSchemaValidations } from './schemas';

const Administrators = () => {
  const [inviteModal, setInviteModal] = useToggle(false);
  const [inviteUser] = useInviteUserMutation();
  const [inviteUserInitialFormState, setInviteUserForm] = useState(inviteUserInitialValues);

  const formik = useFormik({
    enableReinitialize: false,
    validationSchema: inviteUserSchemaValidations,
    initialValues: inviteUserInitialFormState,
    onSubmit: (values) => {
      let executeFunc = inviteUser;
      executeFunc(values)
        .unwrap()
        .then((data: any) => {
          toast.success('Invited Successfully');
          setInviteUserForm(inviteUserInitialFormState);
          setInviteModal();
        })
        .catch((err: any) => {
          toast.error('Failed to invite user');
        });
    }
  });
  return (
    <Container title="Administrators">
      <AdminstratorsTable
        tableHeaders={() => (
          <div className="hstack gap-2 justify-content-end">
            <PrimaryButton
              title="Invite Users"
              iconName="ri-add-line align-bottom me-1"
              type="button"
              onClick={setInviteModal}
            />
          </div>
        )}
      />

      <form onSubmit={formik.handleSubmit}>
        <Modal
          title="Invite Users"
          isModalOpen={inviteModal}
          closeModal={setInviteModal}
          modalSize="modal-md"
          renderFooter={() => (
            <div className="hstack gap-2 justify-content-end">
              <SecondaryButton onClick={setInviteModal} title="Cancel" />
              <PrimaryButton title="Invite User" type="submit" />
            </div>
          )}
        >
          <AdministratorInviteForm formik={formik} />
        </Modal>
      </form>
    </Container>
  );
};

export default Administrators;
