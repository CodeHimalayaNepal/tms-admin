import DataTable from 'components/ui/DataTable/data-table';
import PrimaryButton from 'components/ui/PrimaryButton';
import React, { FC } from 'react';
import { FaRegEdit } from 'react-icons/fa';
import { RiDeleteBin5Line } from 'react-icons/ri';
import { useListAdministratorQuery } from 'redux/reducers/administrator';

interface IAdministratorTable {
  tableHeaders?: () => React.ReactNode;
  onEditClicked?: (data: any) => void;
  onDeleteClicked?: (data: any) => void;
}

const AdminstratorsTable: FC<IAdministratorTable> = ({
  tableHeaders,
  onEditClicked,
  onDeleteClicked
}) => {
  const { data: adminData } = useListAdministratorQuery();
  const columns: any = [
    {
      Header: 'S.N',
      accessor: 'sn',
      Cell: (data: any) => {
        return <>{data.row.index + 1}</>;
      }
    },
    {
      Header: 'First Name',
      accessor: 'first_name'
    },
    {
      Header: 'Last Name',
      accessor: 'last_name'
    },
    {
      Header: 'Phone Number',
      accessor: 'mobile_number'
    },
    {
      Header: 'Email',
      Cell: (data: any) => {
        return <>{data.row.original.user?.email}</>;
      }
    },
    {
      Header: '  Action',
      Cell: (data: any) => {
        return (
          <>
            <FaRegEdit
              size={20}
              // onClick={() => onEditClicked(data.row.original)}
            />
            <RiDeleteBin5Line
              size={20}
              className="m-2"
              // onClick={() => onDeleteClicked(data.row.original)}
            />
          </>
        );
      }
    }
  ];
  return (
    <div>
      <DataTable
        columns={columns}
        data={adminData?.results[0]?.roleadmin || []}
        tableHeaders={tableHeaders}
      />
    </div>
  );
};

export default AdminstratorsTable;
