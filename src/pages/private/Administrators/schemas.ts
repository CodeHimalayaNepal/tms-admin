import * as Yup from 'yup';

export interface IInviteUserInput {
  email: string;
  role: number;
}

export const inviteUserInitialValues: IInviteUserInput = {
  email: '',
  role: 1
};

export const inviteUserSchemaValidations = Yup.object().shape({
  role: Yup.number().required('Role is required'),
  email: Yup.string().email('Invalid email').required('Email is required')
});
