import FormikValidationError from 'components/ui/FormikErrors';
import TextInput from 'components/ui/TextInput';
import { FormikProps } from 'formik';
import React, { FC, useState } from 'react';
import { useListRolesQuery } from 'redux/reducers/roles';
import { IInviteUserInput } from './schemas';

interface IInviteForm {
  formik: FormikProps<IInviteUserInput>;
}
const AdministratorInviteForm: FC<IInviteForm> = ({ formik }) => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data: rolesData } = useListRolesQuery({ page, pageSize });
  return (
    <div className="row">
      <TextInput
        label="Email"
        name="email"
        type="text"
        onChange={formik.handleChange}
        value={formik.values.email}
        onBlur={formik.handleBlur}
        formik={formik}
        containerClassName="col-md-12"
      />
      <div className="row mt-2">
        <div className="col-md-12">
          <div className="row">
            <div className="col-md-12">
              <div className="mb-3">
                <label htmlFor="trainingType-field" className="form-label">
                  Role
                </label>
                <select
                  id="trainingType-field"
                  className="form-select col-md-12"
                  data-choices
                  data-choices-sorting="true"
                  value={formik?.values?.role}
                  onChange={(e) => formik.setFieldValue('role', +e.target.value)}
                >
                  <option selected>Choose...</option>
                  {rolesData?.data?.map((item: any) => {
                    return (
                      <>
                        {' '}
                        <option value={+item.id}>{item.name}</option>
                      </>
                    );
                  })}
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdministratorInviteForm;
